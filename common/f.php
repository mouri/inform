<?php
/**
 * 表示フォーマット用の関数群を保有する特殊クラス 
 * 
 * @author kim
 */
class f {

	/**
	 * 文字列にhtmlspecialcharsをかけて出力する関数 
	 * 使用例：<?php f::p($aryData["name1"]); ?>
	 * 
	 * @param $strValue
	 */
	static function p($strValue){
		echo htmlspecialchars($strValue);
	}

	/**
	 * 文字列にhtmlspecialcharsをかけて出力する関数 
	 * 改行コードを<br>タグに変換する。 
	 * 
	 * @param $strValue
	 */
	static function br($strValue){
		echo nl2br(htmlspecialchars($strValue));
	}

	/**
	 * nl2br関数のWrapperメソッド
	 *
	 * @param string $strValue : 改行コードを変換したい文字列
	 *
	 * return : なし
	 **/
	static function nl($strValue){
		echo nl2br($strValue);
	}

	/**
	 * 整数文字にnumber_formatをかけて出力する関数 
	 * 念のため数値にキャストしてます。
	 * 
	 * @param $strValue
	 */
	static function n($strValue, $IntDecimals = 0){
		echo number_format((float)$strValue, $IntDecimals);
	}

	/**
	 * 年月日の出力関数 
	 * 
	 * @param $strValue
	 */
	static function d($strValue){
		echo date('Y/m/d', strtotime($strValue));
	}

	/**
	 * 年月日時分の出力関数 
	 * 
	 * @param $strValue
	 */
	static function t($strValue){
		echo date('Y/m/d H:i', strtotime($strValue));
	}

	/**
	 * データ確認用関数
	 * <pre>タグでくくってvar_dumpする。
	 *
	 * @param mixed $mixData : 内容を確認したい変数
	 **/
	static function vd($mixData){
		echo '<pre>';
		var_dump($mixData);
		echo '</pre>';
	}

	/**
	 * 長い文字列を省略表示する 
	 * 
	 * @param param
	 * @return return
	 */

	
	/**
	 * 省略文字列取得関数 
	 * 
	 * @param $strString 省略元の文字列
	 * @param $strLength 省略までの文字数　デフォルト：[30]
	 * @param $strAbbrCharcter 末尾に付加する省略文字　デフォルト：[...]
	 * @return string 省略語の文字列
	 */
	static function abbr($strString, $intLength = 30, $strAbbrCharcter = '…'){
		if( mb_strlen($strString) > $intLength){
			echo htmlspecialchars( mb_substr($strString, 0, $intLength-1) . $strAbbrCharcter );
		} else {
			echo htmlspecialchars( $strString );
		}
	}

}