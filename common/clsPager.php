<?php
/**
 * 共通ページャークラス
 *
 * 一覧系画面のページャ制御クラス
 *
 * @author 2012/02/08 Nambe
 *
 **/
class clsPager {
	private $intTotal = 0;				//合計データ数
	private $intPerPage = 10;			//1ページに表示するデータ数
	private $intLinkMax = 3;			//現在位置の左右に表示するリンク数
	private $intOmitLinkMax = 2;		//最前/最後から表示するリンク数
	private $strOmit = '...';			//省きに使用する文字
	private $strPrev = '&laquo;';		//一つ前のリンク文字
	private $strNext = '&raquo;';		//一つ後のリンク文字
	private $strUrl;					//ベースにするURL
	private $strQuery = 'pagerNumber';	//ページ番号に使用するパラメータ
	private $strUlClass = 'pager';		//リストタグのクラス名
	private $aryQuerys;
	private $intCurPage;
	private $strResultTitle = '検索結果';

	/**
	 * コンストラクタ
	 *
	**/
	function __construct() {
		$this->intCurPage = (isset( $_POST[$this->strQuery] ))? $_POST[$this->strQuery] : 1 ;
		$this->strUrl = $_SERVER['SCRIPT_NAME'];
	}

	/**
	 * ページャーの表題部分を設定する（デフォルト：検索結果）
	 *
	 * @param $strTitle
	 */
	function setTitle($strTitle){
		$this->strResultTitle = $strTitle;
	}
	
	/**
	 * 合計データ件数を設定します
	 *
	 *@param  $intTotal 合計データ件数
	 *@return なし
	 *
	 **/
	function setTotal($intTotal){
		$this->intTotal = $intTotal;
		
		//echo $this->intTotal."/".$this->intCurPage;
		$offset = ($this->intCurPage-1) * $this->intPerPage;
		
		//mb_send_mail("mouri@inform.co.jp","debug","total:".$this->intTotal."/offset:".$offset);
		
		//指定ページが合計データ件数を超えていた場合
		if($this->intTotal < ($offset + 1) ){
			$this->intCurPage = ceil( $this->intTotal / $this->intPerPage );
		}
	}
	
	/**
	 * 1ページに表示するデータ数(limit)を取得
	 *
	 *@return １ページに表示するデータ数
	 *
	 **/
	function getLimit(){
		return $this->intPerPage;
	}

	/**
	 * 1ページに表示するデータ数(limit)を設定
	 * 
	 * @param int $intPerPage
	 */
	function setLimit($intPerPage){
		$this->intPerPage = $intPerPage;
	}
	
	/**
	 * データ取得開始位置（offset）を取得
	 *
	 *@return １ページに表示するデータ数
	 *
	 **/
	function getOffset(){
		//return $this->intPerPage * (strlen($_POST["pagerNumber"]) > 0 ? $_POST["pagerNumber"]-1 : 0);
		
		// 2013-02-27 kanata POSTされたページ番号に0が入っていた場合
		if( 0 == $this->intCurPage ){
			$this->intCurPage = 1;
		}
		return ($this->intCurPage-1) * $this->intPerPage;
	}
	
	/**
	 * 設定された内容でページャのタグを取得します
	 *
	 *@return ページャタグ
	 *
	 **/
	function get() {
		$pageMax = ceil( $this->intTotal / $this->intPerPage );
		$intOmitLinkMax = $this->intOmitLinkMax;
		$intLinkMax = $this->intLinkMax;
		$intCurPage = $this->intCurPage;
		$prev = $intCurPage - 1;
		$next = $intCurPage + 1;
		$href =  $this->strUrl . '?' . $this->getQuery() . $this->strQuery . '=';
		$kensu = '';
		$list = '';

		//ページ数表示
		if($this->intTotal == 0){
			$kensu = $this->strResultTitle . "：0件";
		}else if($this->intTotal <= ($this->getOffset()+$this->getLimit()) ){
			$kensu = $this->strResultTitle . "（".($this->getOffset()+1)."～".($this->intTotal)."を表示／全".$this->intTotal."件）";
		}else{
			$kensu = $this->strResultTitle . "（".($this->getOffset()+1)."～".($this->getOffset()+$this->getLimit())."を表示／全".$this->intTotal."件）";
		}
		
		if (1 < $intCurPage) $list .= '<li class="prev"><a id="pagerNumber'.$prev.'" href="#">' . $this->strPrev . '</a></li>';

		$minLeft = $intCurPage - $intLinkMax;
		for ($i=0; $i < $intOmitLinkMax; $i++) {
			$curLink = $i + 1;
			if ($curLink >= $minLeft) break;
			$list .= '<li><a id="pagerNumber'.$curLink.'" href="#">' . $curLink . '</a></li>';
//			if ($curLink === $intOmitLinkMax ) $list .= '<li>' . $this->strOmit . '</li>';
			//2013/03/01 ishihara 現在ページが現在位置の左右に表示するリンク数+最前/最後から表示するリンク数+1の時は省略文字を表示しないように修正
			if ($curLink === $intOmitLinkMax && $intCurPage != ($intOmitLinkMax+$intLinkMax+1)) $list .= '<li>' . $this->strOmit . '</li>';
		}

		$curLink = $intCurPage - $intLinkMax;
		while ($curLink < $intCurPage) {
			if ($curLink > 0) {
				$list .= '<li><a id="pagerNumber'.$curLink.'" href="#">' . $curLink . '</a></li>';
			}
			$curLink++;
		}

		$list .= '<li><span>' . $intCurPage . '</span></li>';

		for ($i=0; $i < $intLinkMax; $i++) {
			$curLink = $intCurPage + ($i + 1);
			if ($curLink > $pageMax) break;
			$list .= '<li><a id="pagerNumber'.$curLink.'" href="#">' . $curLink . '</a></li>';
		}

		$maxRight = $intCurPage + $intLinkMax;
//		if (($pageMax - $maxRight) >= $intOmitLinkMax) {
		//2013/03/01 ishihara 現在ページが最大ページ数-現在位置の左右に表示するリンク数-最前/最後から表示するリンク数の時は省略文字を表示しないように修正
		if (($pageMax - $maxRight) >= $intOmitLinkMax && $intCurPage != ($pageMax-$intOmitLinkMax-$intLinkMax)) {
			$list .= '<li>' . $this->strOmit . '</li>';
		}
		for ($i=0; $i < $intOmitLinkMax; $i++) {
			$curLink = $pageMax - $intOmitLinkMax + $i + 1;
			if ($curLink <= $maxRight) continue;
			$list .= '<li><a id="pagerNumber'.$curLink.'" href="#">' . $curLink . '</a></li>';
		}

		if ($pageMax > $intCurPage) $list .= '<li class="next"><a id="pagerNumber'.$next.'" href="#">' . $this->strNext . '</a></li>';

		return $kensu .'<ul class="'.$this->strUlClass.'">' . $list . '</ul>';
	}

	/**
	 * URLクエリ文字列を設定します
	 *
	 *@param URLクエリ文字列配列
	 *
	 **/
	function setQuery($array) {
		$this->aryQuerys = $array;
		if (isset( $this->aryQuerys[$this->strQuery] )) {
			unset( $this->aryQuerys[$this->strQuery] );
		}
	}

	/**
	 * URLクエリ文字列を取得します
	 *
	 *@return URLクエリ文字列
	 *
	 **/
	function getQuery() {
		if (!isset( $this->aryQuerys )) return '';
		$query = '';
		foreach ($this->aryQuerys as $key => $value) {
			$query .= $key . '=' . $value . '&';
		}
		return $query;
	}
}
?>