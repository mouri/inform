<?php
// エラーハンドラ関数
function myErrorHandler ($errno, $errstr, $errfile, $errline) {
	
	if (!(error_reporting() & $errno)) {
		// error_reporting 設定に含まれていないエラーコードです
		return;
	}

	global $sql;		//SQL

	$err_msg = "";
	$user_agent  = getenv("HTTP_USER_AGENT");
	$remote_addr = getenv("REMOTE_ADDR");
	$remote_host = gethostbyaddr(getenv("REMOTE_ADDR"));

	switch ($errno) {
		case E_ERROR:
			$err_msg  = "ERROR: in line ".$errline." of file ".$errfile."\n";
			$err_msg .= "ERROR:[$errno] $errstr\n";
			$err_msg .= "エラー近くのSQL:".$sql."\n";
			$err_msg .= "user_agent:".$user_agent."\n";
			$err_msg .= "remote_addr:".$remote_addr."\n";
			$err_msg .= "remote_host:".$remote_host."\n";
		break;
		case E_WARNING:
			$err_flg = 1;

			$err_msg  = "WARNING: in line ".$errline." of file ".$errfile."\n";
			$err_msg .= "WARNING:[$errno] $errstr\n";
			$err_msg .= "エラー近くのSQL:".$sql."\n";
			$err_msg .= "user_agent:".$user_agent."\n";
			$err_msg .= "remote_addr:".$remote_addr."\n";
			$err_msg .= "remote_host:".$remote_host."\n";
		break;
		case E_USER_ERROR:
			$err_flg = 1;

			$err_msg  = "E_USER_ERROR: in line ".$errline." of file ".$errfile."\n";
			$err_msg .= "E_USER_ERROR:[$errno] $errstr\n";
			$err_msg .= "エラー近くのSQL:".$sql."\n";
			$err_msg .= "user_agent:".$user_agent."\n";
			$err_msg .= "remote_addr:".$remote_addr."\n";
			$err_msg .= "remote_host:".$remote_host."\n";
		break;
	}

	if ($err_msg != "") {
		$to      = "animation@doupa.net";
		$subject = "PG ERROR";
		$header  = "From: animation@doupa.net";
		mb_send_mail ($to,$subject,$err_msg,$header);
		$_SESSION["error_msg"] = $err_msg;
		$strUrl = clsDefinition::SYSTEM_DIR."/common/errLink.php";
		
		$_SESSION["error_msg"] = "システムエラーが発生しました。<br />お手数ですが、システム管理者までお問い合わせください。";
		require_once $_SERVER["DOCUMENT_ROOT"].$strUrl;
		
		exit();
	}
	
	 /* PHP の内部エラーハンドラを実行しません */
	return true;
}

$error_handler = set_error_handler("myErrorHandler");
?>
