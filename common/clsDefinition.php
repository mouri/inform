<?php
/**
 *	定義クラス
 *
 *	項目の定義をおこなうクラス
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */

class clsDefinition{

	//■システム名称
	const SYSTEM_NAME  = '【インフォーム管理】';
	const SYSTEM_DIR   = '/inform';

	//■データベース接続情報
	const DB_TYPE	   = 'mysql';						//接続するデータベースの種類
	const DB_HOST	   = 'test-rds01-aws.cuvcgorawlvc.ap-northeast-1.rds.amazonaws.com';				//接続先のホスト
	const DB_NAME	   = 'doupa_db';   	//接続するデータベース名称
	const DB_USER_NAME = 'doupa_user';						//データベースに接続するユーザーネーム
	const DB_PASSWORD  = '6JTH4vzDOW';						//データベースに接続するユーザーのパスワード

	//■ミルビィＡＰＩ関連
	const MILLVI_ID    = 'admin';
	const MILLVI_PASS  = '2358';
	const MILLVI_PCODE = 'millvi-demo-394';

	//■ＦＴＰ接続情報
	const FTP_SERVER_MILLVI = "125.103.252.62";
	const FTP_USER_MILLVI = "millvi-ftp-millvi-demo-394";
	const FTP_USER_PASS_MILLVI = "QZKODQdv";

	//■セッション名
	const SESSION_LOGIN                  = "login";
	const SESSION_CONTACT                = "contact";
	const SESSION_USER_MENU              = "user-menu";
	const SESSION_USER_ERROR             = "user-error";
	const SESSION_ADMIN_BASICINFO_LIST   = "admin-basicinfo-list";
	const SESSION_USER_HEADER            = "user-header"; // ヘッダーへのメッセージ出力
	const SESSION_REDIRECT               = "redirect";    // ログイン後の転送先URL
	const SESSION_RANDOM_SESSION         = "random_session";
	const SESSION_ADMIN_INFORMATION_LIST = "admin-information-informationlist";

	//GETパラメータ名
	const GET_CONTACT_ID       = "c";
	const GET_MENU_CATEGORY_CD = "m";
	const GET_CATEGORY_CD      = "ct";
	const GET_MOVIE_ID         = "v";
	const GET_ONETIME_PASS     = "key";
	const GET_SORT             = "s";

	//ソート順
	const SORT_NEW   = 1; //新着
	const SORT_COUNT = 2; //視聴回数

	//PC動画の変換完了コード（SQLのIN句に使用）
	const MOVIE_CHANGE_COMPLETE = '1,3,4,5';

	//ミルビィユーザ名
	const MOVIE_USER = 'millvi-demo-394';

	//ミルビィプレイヤー番号
	const MOVIE_PLAYER_NO = '6';

	//最大動画サイズ(バイト数) 500M
	const MOVIE_MAX_SIZE  = 524288000;

	//■動画フォルダ名
	const MOVIE_FOLDER = "/動画共有サイト会員クラウドサービス";
	
	// pcエンコード
	public static $PC_ENCODE = array(
									array("id" => 101, "name" => "H.264 500kbps"),
									array("id" => 102, "name" => "H.264 800kbps"),
									array("id" => 103, "name" => "H.264 1000kbps"),
									array("id" => 104, "name" => "H.264 1500kbps")
								);
	// スマフォエンコード
	public static $SMP_ENCODE = array(
									array("id" => 128,  "name" => "128kbps"),
									array("id" => 300,  "name" => "300kbps"),
									array("id" => 500,  "name" => "500kbps"),
									array("id" => 800,  "name" => "800kbps"),
									array("id" => 1000, "name" => "1000kbps")
								);

	// トランスコード Nishi add 20130724
	public static $TRANSCODE_FLG = array(
									array("flg" => 1,  "name" => "設定1"),
									array("flg" => 2,  "name" => "設定2"),
									array("flg" => 3,  "name" => "設定3")
								);

	// デザインテンプレート
	public static $DE_TEMPLETE = array(
									array("id" => "/default", "name" => "default")
								);
	
	//・ログインチェックを行わないプログラム一覧
	public static $IGNORE_LOGIN_CHECK_LIST = array(
		'/login/',
		'/user/account/accountentry/',
		'/user/account/repassword/',
	);

	//・ログインチェックを行わないプログラム一覧
	public static $REDIRECTABLE_LIST = array(
		'/user/menu/',
		'/user/movie/',
		'/user/movielist/'
	);

	/**
	 * インフォームFAQ情報 
	 */
	const FAQ_MAX_RANK 			= 2;						// 階層
	const FAQ_FORM_USE_FLG 		= 1;						// フォームフラグ
	const FAQ_SEND_MAIL_ADDRESS = "animation@doupa.net";	// mailaddress

	// お問い合わせ対応状況
	public static $FOLLOW_STATUS_LIST = array(
		0 => '未対応',
		1 => '対応中',
		2 => '対応済',
		3 => '保留'
	);

}
?>
