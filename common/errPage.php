<?php
require_once("clsDefinition.php");
require_once("session_mysql.php");
session_start();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title><?php echo clsDefinition::SYSTEM_NAME?> エラー</title>
<style type="text/css">
<!--
html{
   background: #ffffff;
}
body{
	padding:0;
	margin:0;
	font-family: "メイリオ", "Meiryo", "ヒラギノ角ゴ Pro W3", "Hiragino Kaku Gothic Pro", "ＭＳ Ｐゴシック", "MS P Gothic", "Osaka", Verdana, Arial, Helvetica, sans-serif;
	color:#474747;
	font-size:14px;
	 /* Old Browsers */
   background: #ffffff;

   /* IE9, Firefox 1.5-3.5, Safari 3.1-3.2, Opera 8.0-11.0, iOS Safari 3.2-4.0.3 */

   /* Safari 4.0-5.0, Chrome 3-9, iOS Safari 4.0.4-5.0, Android 2.1-3.0 */
   background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #EFEFEF), color-stop(100%, #ffffff));

   background: -webkit-linear-gradient(top, #EFEFEF, #ffffff);       /* Safari 5.1+, Chrome 10+, iOS Safari 5.1+, Android 4.0+ */
   background:    -moz-linear-gradient(top, #EFEFEF, #ffffff);       /* Firefox 3.6-15.0 */
   background:      -o-linear-gradient(top, #EFEFEF, #ffffff);       /* Opera 11.10-12.10 */
   background:         linear-gradient(to bottom, #EFEFEF, #ffffff); /* IE10, Firefox 16.0+, Opera 12.50+ */

   /* IE 8+ */
   /* "-ms-filter" should be included first before "filter" in order for the filter to work properly in Compatibility View */
   /* http://msdn.microsoft.com/en-us/library/ms530752%28v=vs.85%29.aspx */
   -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr='#EFEFEF', endColorstr='#ffffff', GradientType=0)";

   /* IE 5.5-7 */
   filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#EFEFEF', endColorstr='#ffffff', GradientType=0);
	 overflow:auto;
-->
}
</style>

</head>
<body>
<div style="margin:200px auto 200px auto;">
<p style="text-align:center"><?php echo $_SESSION["error_msg"] ?></p>
</div>
</body>
</html>