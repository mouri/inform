<?php
/**
 *	共通関数クラス
 *
 *	共通関数の定義をおこなうクラス
 *
 *	@author			Mouri 2012/01/21
 *	@version		1.0
 */
class clsCommonFunction{
	/**
	 * ヘッダ表示（管理者側）
	 *
	 */
	public static function dispHeaderManegement(){
		require_once $_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/templete/management/dspHeader.php";
	}
	
	/**
	 * フッタ表示（管理者側）
	 *
	 */
	public static function dispFooterManegement(){
		require_once $_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/templete/management/dspFooter.php";
	}
	
	/**
	 * ヘッダ表示（利用者側）
	 *
	 */
	public static function dispHeader(){
		require_once $_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/templete".$_SESSION[clsDefinition::SESSION_LOGIN]["templete"]."/dspHeader.php";
	}


	/**
	 * 利用者側のヘッダのメッセージ取り出し
	 *
	 */
	public static function pullHeaderMessage(){
		if($_SESSION[clsDefinition::SESSION_USER_HEADER]["message"] != ""){
			f::p($_SESSION[clsDefinition::SESSION_USER_HEADER]["message"] );
			$_SESSION[clsDefinition::SESSION_USER_HEADER]["message"] = "";
		}
	}

	/**
	 * 利用者側のエラーメッセージ取り出し
	 *
	 */
	public static function setUserErrorMessage( $strMessage ){
		$_SESSION[clsDefinition::SESSION_USER_ERROR]["message"] = $strMessage;
	}

	/**
	 * 利用者側のエラーメッセージ取り出し(出力後は)
	 *
	 */
	public static function pullUserErrorMessage(){
		if($_SESSION[clsDefinition::SESSION_USER_ERROR]["message"] != ""){
			f::p($_SESSION[clsDefinition::SESSION_USER_ERROR]["message"] );
			$_SESSION[clsDefinition::SESSION_USER_ERROR]["message"] = "";
		}
	}

	/**
	 * フッタ表示（利用者側）
	 *
	 */
	public static function dispFooter(){
		require_once $_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/templete".$_SESSION[clsDefinition::SESSION_LOGIN]["templete"]."/dspFooter.php";
	}

	/**
	 * アカウント編集リンク
	 * 
	 */
	public static function myAccount(){
		return clsDefinition::SYSTEM_DIR. "/user/account/accountedit/updateAccount.php?" . clsDefinition::GET_CONTACT_ID . "=" . base64_encode(clsContactSession::getContactId());
	}

	/**
	 * 登録動画一覧リンク
	 * 
	 */
	public static function myMovieList(){
		return clsDefinition::SYSTEM_DIR."/user/movieedit/list.php?" . clsDefinition::GET_CONTACT_ID . "=" . base64_encode(clsContactSession::getContactId());
	}

	/**
	 * 動画登録リンク
	 * 
	 */
	public static function myMovieUpload(){
		return clsDefinition::SYSTEM_DIR."/user/movieedit/upload.php?" . clsDefinition::GET_CONTACT_ID . "=" . base64_encode(clsContactSession::getContactId());
	}

	/**
	 * お気に入り（マイリスト）表示リンク
	 * 
	 */
	public static function myList(){
		return clsDefinition::SYSTEM_DIR."/user/movielist/mylist/myList.php". "?" . clsDefinition::GET_CONTACT_ID . "=" . base64_encode(clsContactSession::getContactId());
	}

	/**
	 * 再生履歴表示リンク
	 * 
	 */
	public static function myHistory(){
		return clsDefinition::SYSTEM_DIR."/user/movielist/mylist/myHistory.php". "?" . clsDefinition::GET_CONTACT_ID . "=" . base64_encode(clsContactSession::getContactId());
	}
	
	/**
	 * 実行ディレクトリ名を取得する
	 */
	public static function getScriptDirectory(){
		return str_replace("/", "-", dirname($_SERVER["SCRIPT_FILENAME"]));
	}

	/**
	 * 任意桁のパスワードを生成する
	 *
	 * @param $intCnt    パスワード桁数
	 * @return 指定された桁数のパスワード文字列
	 * 
	 **/
	public static function makePass($intCnt){
		//パスワード作成に使用する文字
		$seed_char = "abcdefghijklmnopqrstuvwxyz0123456789";

		$i_max = strlen($seed_char) - 1;	//文字列の最大値から1を引く（これをしないと７桁になる場合がある。）
		$pass = "";							//パスワードを格納する変数の初期化

		srand((double)microtime()*1000000);	//周期的に起こる同じ乱数の結果を起こりにくくする。

		for($i=1;$i<=$intCnt;$i++){
			$pass_tmp = substr($seed_char,rand(0, $i_max),1);	//パスワードの生成。
			$pass .= $pass_tmp;	//パスワードの生成。
		}

		return $pass;

	}

	/**
	 * ログアウト処理を行う
	 * セッションのログイン情報を消去する 
	 * 
	 * @return boolean
	 */
	static function logout(){
		$intContactId = clsContactSession::getContactId();
		$_SESSION = array();
		clsContactSession::set("contact_id", $intContactId);
	}

	/**
	 * データベースの汎用存在チェック関数 
	 * 
	 * @param string $strTableName  テーブル名
	 * @param string $strColumnName 列名
	 * @param mixd   $mixdValue     検索する値
	 * @return boolean true:存在する false:存在しない又はDBエラー
	 */
	public static function existsData($strTableName, $aryWhereParameters = array()){
		$objDatabase = clsMillviDatabase::getInstance();

		$strWhere = "";
		$aryParameters = array(
			":contact_id" => clsLoginSession::getContactId(),
		);

		foreach( $aryWhereParameters as $strWhereColumnName => $mixdValue ){
			$strWhere .= " AND {$strWhereColumnName} = :{$strWhereColumnName} ";
			$aryParameters[":{$strWhereColumnName}"] = $mixdValue;
		}

		$strSql =
<<< SQL
			SELECT COUNT(*) AS count 
			FROM $strTableName
			WHERE contact_id = :contact_id 
			$strWhere
SQL;

		$aryResult = $objDatabase->pullDbData($strSql, $aryParameters);
		
		if($objDatabase->isError($aryResult)){
			return false;
		}

		$intCount = $aryResult[0]["count"];
		if($intCount > 0){
			return true;
		} else {
			return false;
		}
	}


	/**
	 * contact_idの存在チェック関数 
	 * 
	 * @param int $intContactId
	 * @return boolean true:存在する false:存在しない又はDBエラー
	 */
	public static function existsContactId($intContactId){
		$objDatabase = clsMillviDatabase::getInstance();

		$strSql =<<<SQL

			SELECT COUNT(*) AS count 
			FROM contact_mst
			WHERE contact_id = :contact_id
			  AND delete_flg = 0
SQL;
		$aryResult = $objDatabase->pullDbData($strSql, array("contact_id" => $intContactId));
		
		if($objDatabase->isError($aryResult)){
			return false;
		}

		$intCount = $aryResult[0]["count"];
		if($intCount > 0){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * メニューカテゴリーフラグチェック関数
	 * 
	 * return : true 使用 , false 不使用
	 **/
	public static function chkCategoryFlg(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$strSql =
<<<SQL
	SELECT
		menu_category_flg
	FROM
		contact_mst
	WHERE
		contact_id = :contact_id AND
		delete_flg = :delete_flg
SQL;
	
		$aryParameter = array(
			':contact_id' => clsLoginSession::getContactId(),
			':delete_flg' => '0'
		);
		
		// SQLを実行し結果をaryDataへ格納する
		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		if( '0' == $aryResult[0]["menu_category_flg"] ){
			return false;
		} else {
			return true;
		}
		
	}

	/**
	 * メニュカテゴリー取得関数 
	 * 
	 * @return array
	 */
	public static function getMenuCategory(){
		$objDatabase = clsMillviDatabase::getInstance();
		$aryParameters = array(
			":contact_id" => clsContactSession::getContactId(),
		);

		$strSql =
<<< SQL
			SELECT
				menu_category_cd,
				menu_category_name
			FROM menu_category_mst
			WHERE contact_id = :contact_id
			  AND delete_flg = 0
			ORDER BY sort_no
SQL;
		$aryResult = $objDatabase->pullDbData($strSql, $aryParameters);

		if($objDatabase->isError($aryResult)){
			return array();
		}

		$aryReturn = array();

		foreach($aryResult as &$aryMenuCategory){
			$aryMenuCategory["link_url"] = clsDefinition::SYSTEM_DIR . "/user/menu/menu.php"
										 . "?" . clsDefinition::GET_CONTACT_ID       . "=" . base64_encode(clsContactSession::getContactId())
										 . "&" . clsDefinition::GET_MENU_CATEGORY_CD . "=" . $aryMenuCategory["menu_category_cd"];

			$aryMenuCategory["list_link_url"] = clsDefinition::SYSTEM_DIR . "/user/movielist/list.php"
											  . "?" . clsDefinition::GET_CONTACT_ID       . "=" . base64_encode(clsContactSession::getContactId())
											  . "&" . clsDefinition::GET_MENU_CATEGORY_CD . "=" . $aryMenuCategory["menu_category_cd"];
			$aryMenuCategory["first_category"] = self::getFirstCategories($aryMenuCategory["menu_category_cd"]);

			$aryReturn[$aryMenuCategory["menu_category_cd"]] = $aryMenuCategory;

		}

		return $aryReturn;
	}

	/**
	 * 第１カテゴリ一覧の取得
	 * 
	 * @return array
	 */
	static function getFirstCategories($intMenuCategoryCd = 1){

		$objDatabase = clsMillviDatabase::getInstance();

		$aryParameter = array(
			":contact_id"       => clsContactSession::getContactId(),
			":menu_category_cd" => $intMenuCategoryCd,
		);

		$strSql =
<<< SQL
			SELECT
				f_category_cd,
				f_category_name
			FROM first_category_mst
			WHERE contact_id       = :contact_id
			  AND menu_category_cd = :menu_category_cd
			  AND delete_flg = 0
			ORDER BY sort_no
SQL;
		$aryResult = $objDatabase->pullDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return array();
		}

		$aryReturn = array();

		foreach($aryResult as &$aryCategory){
			$aryCategory["link_url"] = clsDefinition::SYSTEM_DIR . "/user/movielist/list.php"
									 . "?" . clsDefinition::GET_CONTACT_ID  . "=" . base64_encode(clsContactSession::getContactId());
			if( clsContactSession::hasMenuCategory() ){
				$aryCategory["link_url"] .= "&" . clsDefinition::GET_MENU_CATEGORY_CD . "=" . $intMenuCategoryCd;
			}
			$aryCategory["link_url"] .= "&" . clsDefinition::GET_CATEGORY_CD . "=" . $aryCategory["f_category_cd"];
			$aryReturn[$aryCategory["f_category_cd"]] = $aryCategory;
		}

		return $aryReturn;
	}

	/**
	 * 連番の次の番号を取得する関数 
	 * 
	 * @param $strTableName  テーブル名
	 * @param $strColumnName 列名
	 * @return integer 次の連番 エラー時:false
	 */
	public static function getNextId($strTableName, $strColumnName, $aryWhereParameters = array()){
		$objDatabase = clsMillviDatabase::getInstance();


		$aryParameters = array(
			":contact_id" => clsContactSession::getContactId(),
		);

		$strWhere = "";

		foreach( $aryWhereParameters as $strWhereColumnName => $mixdValue ){
			$strWhere .= " AND {$strWhereColumnName} = :{$strWhereColumnName} ";
			$aryParameters[":{$strWhereColumnName}"] = $mixdValue;
		}


		$strGetNextSql =
<<< SQL
			SELECT COALESCE(MAX({$strColumnName}), 0) + 1 AS count 
			FROM {$strTableName}
			WHERE contact_id = :contact_id
			{$strWhere}
SQL;

		$aryResult = $objDatabase->pullDbData($strGetNextSql, $aryParameters);

		if($objDatabase->isError($aryResult)){
			return false;
		}

		return (int)$aryResult[0]["count"];
	}

	/**
	 * 現在時刻の文字列を取得 
	 * 
	 * @return string 'YYYY/MM/DD HH/MI/SS'
	 */
	public static function getCurrentDate(){
		return date('Y/m/d H:i:s');
	}
	
	/**
	 * 男女の名称を取得します
	 * 
	 * @param int $intSexCd
	 * @return string 男女名称
	 */
	public static function getSex($intSexCd){
		switch($intSexCd){
			case 1:
				return "男";
				break;
			case 2:
				return "女";
				break;
		}
	}
	
	/**
	 * グループの名称を取得します
	 * 
	 * @param int $intGroupCd グループコード
	 * @return string グループ名称
	 */
	public static function getGroupName( $intGroupCd ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$strGetGroupSql =
<<<SQL
	SELECT
		group_name
	FROM
		group_mst
	WHERE
		group_cd = :group_cd
SQL;
		
		$aryGetGroupParameter = array(
										':group_cd' => $intGroupCd
									);
		
		// SQLを実行し結果をaryDataへ格納する
		$aryResult = $objDatabase->pullDbData( $strGetGroupSql, $aryGetGroupParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult[0]["group_name"];
	}
	
	/**
	 * 権限取得
	 * 
	 * @param   選択されたユーザーID
	 * @return  取得したグループ権限
	 */
	public static function getAuth( $intUserId ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$strGroupAuthSql = 
<<<SQL
	SELECT
		gacm.auth_cd
	FROM
		login_mst AS lm 
			INNER JOIN group_auth_config_mst AS gacm
		 ON lm.contact_id = gacm.contact_id
		AND lm.group_cd   = gacm.group_cd
	WHERE
		lm.contact_id = :contact_id AND
		lm.user_id    = :user_id
SQL;
		
		$aryGroupAuthParameter = array(
			':contact_id'  => clsContactSession::getContactId(),
			':user_id'     => $intUserId
		);
		
		$aryGropuAuthResult = $objDatabase->pullDbData( $strGroupAuthSql, $aryGroupAuthParameter );
		if( $objDatabase->isError( $aryGropuAuthResult ) ){
			return false;
		}
		
		// グループ権限がある場合は結果を返して、処理終了
		if( '0' < COUNT( $aryGropuAuthResult ) ){
			return $aryGropuAuthResult;
		}
		
		// グループ権限がない場合はユーザ権限を返す
		$strUserAuthSql = 
<<<SQL
	SELECT
		auth_cd
	FROM
		user_auth_config_mst
	WHERE
		contact_id = :contact_id AND
		user_id    = :user_id
SQL;
		
		$aryUserAuthParameter = array(
			':contact_id'  => clsContactSession::getContactId(),
			':user_id'     => $intUserId
		);
		
		$aryUserAuthResult = $objDatabase->pullDbData( $strUserAuthSql, $aryUserAuthParameter );
		if( $objDatabase->isError( $aryUserAuthResult ) ){
			return false;
		}
		
		// グループ権限がある場合は結果を返して、処理終了
		if( '0' < COUNT( $aryUserAuthResult ) ){
			return $aryUserAuthResult;
		} else {
			return array();
		}
	}
	 
	/**
	 * 権限チェックボックス作成
	 *
	 * @param   $intUserkbn     選択されたユーザー区分
	 * @param   $aryAuthCd		選択されている権限コード
	 * @return  作成したチェックボックス
	 * @author  2013/03/06 kanata
	 */
	public static function initAuthCheckBox( $intUserkbn, $aryAuthCd ){
		if( false == is_array( $aryAuthCd ) ){
			$aryAuthCd = array();
		}
		
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		switch( $intUserkbn ){
			// 管理者
			case '1':
				$strWhereSql = "";
				break;
			// 利用者
			case '2':
				$strWhereSql = "AND user_kbn = 2";
				break;
			default:
				$strWhereSql = "";
				break;
		}
		
		$strGetAuthSql =
<<<SQL
	SELECT
		auth_cd,
		auth_name,
		user_kbn
	FROM
		authority_mst
	WHERE 
		contact_id = :contact_id 
		{$strWhereSql}
	ORDER BY
		sort_no ASC
SQL;
		
		$aryGetAuthParameter = array(
			':contact_id' => clsContactSession::getContactId()
		);
		
		// SQLを実行し結果をaryDataへ格納する
		$aryResult = $objDatabase->pullDbData( $strGetAuthSql, $aryGetAuthParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		// 初期化
		$strAuthCheckBox = "";
		foreach( $aryResult as $key => $val ){
			if( true == in_array( $val["auth_cd"], $aryAuthCd ) ){
				$strAuthCheckBox .= "<label class='inputCheckBox'><span class='checkButton'></span>" . $val["auth_name"] . "<input type='checkbox' name='auth_cd[]' id='auth_cd' value=" . $val["auth_cd"] . " checked='checked' ></label>";
			} else {
				$strAuthCheckBox .= "<label class='inputCheckBox'><span class='checkButton'></span>" . $val["auth_name"] . "<input type='checkbox' name='auth_cd[]' id='auth_cd' value=" . $val["auth_cd"] . "></label>";
			}
		}
		return $strAuthCheckBox;
	}
	
	/**
	 * グループ権限セレクトボックス作成
	 *
	 * @param   $intUserkbn		選択されたユーザー区分
	 * @param   $intGroupCd		選択されたグループコード
	 * @return  作成したセレクトボックス
	 */
	public static function initGroupSelectBox( $intUserkbn, $intGroupCd ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$aryGetGroupParameter = array(
										':contact_id' => clsContactSession::getContactId()
									);
									
		switch( $intUserkbn ){
			// 管理者
			case '1':
				$strWhereSql = "AND user_kbn = :user_kbn";
				$aryGetGroupParameter[":user_kbn"] = $intUserkbn;
				break;
			// 利用者
			case '2':
				$strWhereSql = "AND user_kbn = :user_kbn";
				$aryGetGroupParameter[":user_kbn"] = $intUserkbn;
				break;
			default:
				return "";
				break;
		}
		
		$strGetGroupSql =
<<<SQL
	SELECT
		group_cd,
		group_name
	FROM
		group_mst
	WHERE
		contact_id = :contact_id AND
		delete_flg = 0 
		{$strWhereSql}
SQL;
		
		// SQLを実行し結果をaryDataへ格納する
		$aryResult = $objDatabase->pullDbData( $strGetGroupSql, $aryGetGroupParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		// 初期化
		$strGroupSelectBox = "";
		foreach( $aryResult as $key => $val ){
			$strGroupSelectBox .= "<option value=" . $val["group_cd"] . " " . self::chkSelectedDate( $intGroupCd, $val["group_cd"] ) . " >" . $val["group_name"] . "</option>";
		}
		return $strGroupSelectBox;
	}
	
	/**
	 * グループ権限セレクトボックス作成（ユーザ区分指定なし）
	 *
	 * @update 2013/03/22 kanata 特権ユーザグループ非表示
	 *
	 * @param   $intGroupCd		選択されたグループコード
	 * @return  作成したセレクトボックス
	 */
	public static function initGroupSelectBoxAll( $intGroupCd ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$strGetGroupSql =
<<<SQL
	SELECT
		group_cd,
		group_name
	FROM
		group_mst
	WHERE
		contact_id = :contact_id AND
		delete_flg = 0 AND
		user_kbn  != 3
SQL;
		
		$aryGetGroupParameter = array(
										':contact_id' => clsContactSession::getContactId(),
									);
		
		// SQLを実行し結果をaryDataへ格納する
		$aryResult = $objDatabase->pullDbData( $strGetGroupSql, $aryGetGroupParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		// 初期化
		$strGroupSelectBox = "";
		foreach( $aryResult as $key => $val ){
			$strGroupSelectBox .= "<option value=" . $val["group_cd"] . " " . self::chkSelectedDate( $intGroupCd, $val["group_cd"] ) . " >" . $val["group_name"] . "</option>";
		}
		return $strGroupSelectBox;
	}
	
	/**
	 * 都道府県セレクトボックス作成
	 *
	 * @param   $intPrefCd		選択された都道府県コード
	 * @return  作成したセレクトボックス
	 */
	public static function initPrefSelectBox( $intPrefCd ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$strGetPrefSql =
<<<SQL
	SELECT 
		pref_cd,
		pref_name
	FROM
		pref_mst
	ORDER BY
		sort_no ASC
SQL;
		// 初期化
		$aryGetPrefParameter = array();
		// SQLを実行し結果をaryDataへ格納する
		$aryResult = $objDatabase->pullDbData( $strGetPrefSql, $aryGetPrefParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		// 初期化
		$strPrefSelectBox = "<option value='' " . self::chkSelectedDate( $intPrefCd, '' ) . " ></option>";
		foreach( $aryResult as $key => $val ){
			$strPrefSelectBox .= "<option value=" . $val["pref_cd"] . " " . self::chkSelectedDate( $intPrefCd, $val["pref_cd"] ) . " >" . $val["pref_name"] . "</option>";
		}
		return $strPrefSelectBox;
	}
	
	/**
	 * メニューカテゴリーセレクトボックス作成
	 *
	 * @return  作成したセレクトボックス
	 */
	public static function initMenuCategorySelectBox( $intMenuCategoryCd, $blnCheckUserEnterable = false ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		$strWhere = "";
		if( $blnCheckUserEnterable ){
			$strWhere = " AND user_enterable_flg = 1 ";
		}

		$strGetCategorySql =
<<<SQL
	SELECT
		menu_category_cd,
		menu_category_name
	FROM
		menu_category_mst
	WHERE
		contact_id = :contact_id
		$strWhere
	ORDER BY
		sort_no ASC
SQL;
		
		$aryGetCategoryParameter = array(
			':contact_id' => clsLoginSession::getContactId()
		);
		
		// SQLを実行し結果をaryDataへ格納する
		$aryResult = $objDatabase->pullDbData( $strGetCategorySql, $aryGetCategoryParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		// 初期化
		$strCategorySelectBox = "";
		foreach( $aryResult as $key => $val ){
			$strCategorySelectBox .= "<option value=" . $val["menu_category_cd"] . " " . self::chkSelectedDate( $intMenuCategoryCd, $val["menu_category_cd"] ) . ">" . $val["menu_category_name"] . "</option>";
		}
		return $strCategorySelectBox;
	}
	
	/**
	 * セレクトボックスを選択状態にする為の関数
	 *
	 * @param int $intPostDate ：送られてきたデータ
	 * @param int $intChkDate  ：選択状態にしたいデータ
	 * @return 一致した場合：選択状態にする
	**/
	public static function chkSelectedDate( $intPostDate, $intChkDate ){
		if( $intPostDate == $intChkDate ){
			return "selected=selected";
		}
	}
	
	/**
	 * チェックボックスを選択状態にする為の関数
	 *
	 * @param int $intPostDate ：送られてきたデータ
	 * @param int $intChkDate  ：選択状態にしたいデータ
	 * @return 一致した場合：選択状態にする
	**/
	public static function chkCheckedDate( $intPostDate, $intChkDate ){
		if( $intPostDate == $intChkDate ){
			return "checked=checked";
		}
	}

	/**
	 * 配列に一括してhtmlspecialcharsをかける
	 * 第二引数に除外するキー(htmlのname属性名)のリストを指定する。 
	 * 
	 * @param $aryHtmlData   htmlspecialchars対象の連想配列 $_POST等
	 * @param $aryIgnoreList 除外リスト:array("remarks", "comment")等
	 * @return array エスケープ処理された連想配列(構造は引数と同じ)
	 */
	static function escapeHtml($aryHtmlData, $aryIgnoreList = array()){
		$aryReturn = array();

		foreach( $aryHtmlData as $key => $value){
			if( in_array($key, $aryIgnoreList)){
				$aryReturn[$key] = $value;
			} else {
				$aryReturn[$key] = htmlspecialchars($value);
			}
		}

		return $aryReturn;
	}


	/**
	 * エンコード設定の選択ボックス作成
	 *
	 * @param   $intEncode		選択中のエンコード
	 * @return  作成したセレクトボックス
	 */
	public static function initEncodeSelectBox( $intEncode ){
		
		$aryEncode = clsDefinition::$PC_ENCODE;
		
		// 初期化
		$strEncodeSelectBox = "<option value='' " . self::chkSelectedDate( $intEncode, '' ) . " ></option>";
		foreach( $aryEncode as $key => $val ){
			$strEncodeSelectBox .= "<option value=" . $val["id"] . " " . self::chkSelectedDate( $intEncode, $val["id"] ) . " >" . $val["name"] . "</option>";
		}
		return $strEncodeSelectBox;
	}

	/**
	 * エンコード設定の選択ボックス作成
	 *
	 * @param   $intEncode		選択中のエンコード
	 * @return  作成したセレクトボックス
	 */
	public static function initEncodeSmpSelectBox( $intEncode ){
		
		$aryEncode = clsDefinition::$SMP_ENCODE;
		
		// 初期化
		$strEncodeSelectBox = "<option value='' " . self::chkSelectedDate( $intEncode, '' ) . " ></option>";
		foreach( $aryEncode as $key => $val ){
			$strEncodeSelectBox .= "<option value=" . $val["id"] . " " . self::chkSelectedDate( $intEncode, $val["id"] ) . " >" . $val["name"] . "</option>";
		}
		return $strEncodeSelectBox;
	}

	/**
	 * デザインテンプレート選択ボックス作成
	 *
	 * @param   $intEncode		選択中のテンプレート
	 * @return  作成したセレクトボックス
	 */
	public static function initTempleteSelectBox( $intTemplete ){
		
		$aryTemplete = clsDefinition::$DE_TEMPLETE;
		
		// 初期化
		$strTempleteSelectBox = "<option value='' " . self::chkSelectedDate( $intTemplete, '' ) . " ></option>";
		foreach( $aryTemplete as $key => $val ){
			$strTempleteSelectBox .= "<option value=" . $val["id"] . " " . self::chkSelectedDate( $intTemplete, $val["id"] ) . " >" . $val["name"] . "</option>";
		}
		return $strTempleteSelectBox;
	}

	/**
	 * サムネイルなし画像の差し込み 
	 * 
	 * @param $aryMovies 動画情報の配列
	 * @return return
	 */
	static function setMovieThumbnail(&$aryMovies){
		if(!is_array($aryMovies)){
			return;
		}
		foreach( $aryMovies as &$aryMovie){
			if( $aryMovie["delete_flg"] == 1 ){
				//削除
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR."/common/images/delete_movie.png";
			} else if( isset($aryMovie["not_open_flg"]) && 1 == $aryMovie["not_open_flg"] ){
				//非公開
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR."/common/images/not_open_movie.png";
			} else if( isset($aryMovie["end_flg"]) && 1 == $aryMovie["end_flg"] ){
				//配信終了
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR."/common/images/no_movie.png";
			} else if( empty($aryMovie["thumbnail_url"] )){
				//画像なし
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR."/common/images/no_image.png";
			} else {
				//画像取得httpsのため自サイト経由
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR."/common/thumbnail.php?u=" .$aryMovie["thumbnail_url"];
			}
		}
	}

	/**
	 * サムネイルなし画像の差し込み 
	 * 
	 * @param $aryMovies 動画情報の配列
	 * @return return
	 */
	static function setAdminMovieThumbnail(&$aryMovies, $blnMovieEdit = false){
		if(!is_array($aryMovies)){
			return;
		}
		foreach( $aryMovies as &$aryMovie){

			if( isset($aryMovie["change_complete_flg"]) && $aryMovie["change_complete_flg"] == 0 ) {
				//変換中
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR . '/common/images/convertProcessing.png';
			} else if( isset($aryMovie["change_complete_flg"]) && $aryMovie["change_complete_flg"] == 2 ) {
				//変換失敗
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR . '/common/images/convertFailure.png';
			} else if( !$blnMovieEdit && isset($aryMovie["change_complete_flg"]) && $aryMovie["change_complete_flg"] == 3 ) {
				//変換失敗
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR . '/common/images/convertProcessingSMP.png';
			} else if( !$blnMovieEdit && isset($aryMovie["change_complete_flg"]) && $aryMovie["change_complete_flg"] == 5 ) {
				//変換失敗
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR . '/common/images/convertFailureSMP.png';
			} else if( empty($aryMovie["thumbnail_url"] )){
				//画像なし
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR."/common/images/no_image.png";
			} else {
				//画像取得https
				$aryMovie["thumbnail_url"] = clsDefinition::SYSTEM_DIR."/common/thumbnail.php?u=" .$aryMovie["thumbnail_url"];
			}
		}
	}

	/**
	 * 再生ページヘのリンクURLの生成 
	 * 
	 * @param $aryMovies 動画情報の配列
	 * @return return
	 */
	static function setMovieLinkUrl(&$aryMovies){
		if(!is_array($aryMovies)){
			return;
		}
		foreach( $aryMovies as &$aryMovie){
			$aryMovie["link_url"] = clsDefinition::SYSTEM_DIR . "/user/movie/watch.php"
								  . "?" . clsDefinition::GET_CONTACT_ID . "=" . base64_encode(clsContactSession::getContactId())
								  . "&" . clsDefinition::GET_MOVIE_ID   . "=" . $aryMovie["millvi_nc_cd"];
		}
	}

	/**
	 * 
	 * 年選択オプションを作成する
	 * 
	 * $strValue		この選択コントローラーで使用されている値
	 * 
	 */
	public static function getYearOptionTags($strValue)
	{
		$ctl = "";
		$ar = array();
		$now_year = date("Y");

		// 現在年-10を取得する
		$end_year = $now_year-10;
		// 現在年-70を取得する
		$start_year = $now_year-70;

		//もし生年月日の年が未選択（初期表示）の場合現在日付-30を初期値とする
		//if( "" == $strValue )
		//{
		//	$strValue = $now_year-30;
		//}

		for($start_year;$start_year<= $end_year;$start_year++){
			// 配列に取得した内容を格納する
			array_push($ar, $start_year);
		}

		// 選択オプションを作成する
		$ctl .= "<option value='' >▼選択</option>";

		for ($i = 0; $i < count($ar); $i++)
		{
			if ($ar[$i] == $strValue)
			{
				$ctl .= "<option value='".$ar[$i]."' selected='selected' >".$ar[$i]."</option>";
			}
			else
			{
				$ctl .= "<option value='".$ar[$i]."' >".$ar[$i]."</option>";
			}
		}

		return $ctl;

	}


	/**
	 * 
	 * 月選択オプションを作成する
	 * 
	 * $strValue		この選択コントローラーで使用されている値
	 */
	public static function getMonthOptionTags($strValue)
	{
		$ctl = "";
		$ar = array();

		//1-12までの年の範囲を出力
		for($i=1;$i<13;$i++){

			// 配列に取得した内容を格納する
			array_push($ar, $i);

		}

		// 選択オプションを作成する
		$ctl .= "<option value='' >▼選択</option>";

		for ($i = 0; $i < count($ar); $i++)
		{
			if ($ar[$i] == $strValue)
			{
				$ctl .= "<option value='".$ar[$i]."' selected='selected' >".$ar[$i]."</option>";
			}
			else
			{
				$ctl .= "<option value='".$ar[$i]."' >".$ar[$i]."</option>";
			}
		}

		return $ctl;

	}

	/**
	 * 
	 * 日選択オプションを作成する
	 * 
	 * $strValue		この選択コントローラーで使用されている値
	 * $intYear			生年月日項目の年項目で使用されている値
	 * $intMonth		生年月日項目の月項目で使用されている値
	 */
	public static function getDayOptionTags($strValue, $intYear, $intMonth)
	{
		$ctl = "";
		$ar = array();

		//年か月が選択されていない場合は配列の中身なし
		if( "" == $intYear || "" == $intMonth)
		{
			$ar = array();
		}
		else
		{

			//年、月が選択されている場合その月の最終日を取得
			$day = date("t", mktime(0, 0, 0, $intMonth, 1, $intYear));

			for($i=1;$i<=$day;$i++){

				// 配列に取得した内容を格納する
				array_push($ar, $i);

			}

		}

		// 選択オプションを作成する
		$ctl .= "<option value='' >▼選択</option>";

		for ($i = 0; $i < count($ar); $i++)
		{
			if ($ar[$i] == $strValue)
			{
				$ctl .= "<option value='".$ar[$i]."' selected='selected' >".$ar[$i]."</option>";
			}
			else
			{
				$ctl .= "<option value='".$ar[$i]."' >".$ar[$i]."</option>";
			}
		}

		return $ctl;

	}
	
	/**
	 * 動画状態公開画像を生成
	 * 
	 * @return
	 */
	static function setMovieStatusImage( &$aryValues ){
		if( false == is_array( $aryValues ) ){
			return;
		}
		
		$strData = self::getCurrentDate();
		
		$strCompleteFlg = clsDefinition::MOVIE_CHANGE_COMPLETE;
		
		$objDatabase = clsMillviDatabase::getInstance();
		
		foreach( $aryValues as &$aryValue ){
			$strSql =
<<< SQL
	SELECT
		( CASE
			-- 公開前
			WHEN
				( start_day > DATE( :current_date )
				OR  ( start_day <= DATE( :current_date )
				AND ( end_day >= DATE( :current_date )
				OR  end_day IS NULL )
				AND change_complete_flg = 0 ) )
				AND not_open_flg = 0 THEN 'coming_soon'
			-- 公開中
			WHEN
				start_day <= DATE( :current_date )
				AND ( end_day >= DATE( :current_date )
				OR  end_day IS NULL )
				-- 1:PC変換完了 3:スマホ変換中 4:スマホ変換完了 5:スマホ変換失敗
				AND change_complete_flg IN ( {$strCompleteFlg} )
				AND not_open_flg = 0 THEN 'now_playing'
			-- 公開終了
			WHEN
				end_day < DATE( :current_date )
				AND not_open_flg = 0 THEN 'show_closed'
			-- 非公開
			WHEN
				-- 2:PC変換失敗
				change_complete_flg = 2
				OR not_open_flg = 1 THEN 'not_open'
		END ) AS movie_status
	FROM
		movie_basic_info_trn
	WHERE
		contact_id = :contact_id AND
		delete_flg = :delete_flg AND
		movie_id   = :movie_id
SQL;
			$aryParameters = array(
				":current_date" => $strData,
				":contact_id"   => clsLoginSession::getContactId(),
				":delete_flg"   => '0',
				":movie_id"     => $aryValue["movie_id"]
			);
			$aryResult = $objDatabase->pullDbData( $strSql, $aryParameters );
			if($objDatabase->isError($aryResult)){
				return false;
			}
			
			switch( $aryResult[0]["movie_status"] ){
				// 公開前
				case 'coming_soon':
					$aryValue["movie_status_image"] = '<span class="iconOpenFlg status1">公開前</span>';
					break;
				// 公開中
				case 'now_playing':
					$aryValue["movie_status_image"] = '<span class="iconOpenFlg status2">公開中</span>';
					break;
				// 公開終了
				case 'show_closed':
					$aryValue["movie_status_image"] = '<span class="iconOpenFlg status3">公開終了</span>';
					break;
				// 非公開
				case 'not_open':
					$aryValue["movie_status_image"] = '<span class="iconOpenFlg status4">非公開</span>';
				default:
					break;
			}
		}
	
	}
	
	/**
	 * 管理メニュー選択配列生成
	 * 
	 * @return
	 */
	static function getAdminMenu(){
		$aryAdminMenu =	array(
								1  => 'アカウント管理',
								2  => 'メニュー管理',
								3  => '動画管理',
								4  => '動画コメント',
								5  => '管理者報告',
								6  => 'ランキング',
								7  => 'お知らせ管理',
								8  => '配信メール管理',
								9  => '基本情報メンテナンス',
								10 => 'FAQメンテナンス',
								11 => 'FAQ',
								12 => 'お問い合わせ管理'
							);
		return $aryAdminMenu;
	}

	/**
	 * 管理メニュー authority_mst 登録用
	 * 
	 * @return
	 */
	static function setAdminMenu(){
		$aryAdminMenu =	array(
								1  => '6',
								2  => '7',
								3  => '8',
								4  => '9',
								5  => '10',
								6  => '11',
								7  => '12',
								8  => '13',
								9  => '14',
								10 => '15',
								11 => '16',
								12 => '17'
							);
		return $aryAdminMenu;
	}

	/**
	 * USER権限配列生成
	 * 
	 * @return
	 */
	static function getUserAuthList(){
		$aryUserAuthList =	array(
								1 => '視聴',
								2 => '投稿',
								3 => 'コメント入力',
								4 => '管理者報告',
								5 => '評価登録'
							);
		return $aryUserAuthList;
	}

		/**
	 * 違反リスト配列生成
	 * 
	 * @return
	 */
	static function getOutList(){
		$aryOutList =	array(
							1 => 'プライバシーの侵害',
							2 => '権利の侵害',
							3 => '誤解を招く表現',
							4 => '不快な表現',
							5 => 'テロップ間違い',
							6 => 'その他'
						);
		return $aryOutList;
	}

	 /**
	 * 対応状況セレクトボックス作成
	 *
	 *
	 * @param   $intCustomerId		選択された顧客コード
	 * @update  Nishi 2013/05/14
	 * @return  作成したセレクトボックス
	 */
	public static function initFollowStatusSelectBox( $intFollowStatus ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		// 初期化
		$strCustomerSelectBox = "";
		foreach( clsDefinition::$FOLLOW_STATUS_LIST as $key => $val ){
			$strCustomerSelectBox .= "<option value=" . $key . " " . self::chkSelectedDate( $intFollowStatus, $key ) . " >" . $val . "</option>";
		}
		return $strCustomerSelectBox;
	}

}
?>