<?php
/**
 * headインクルードファイル共通ファイル一括読み込み
 * 
 * @author mouri
 */
$aryPath = explode("/", $_SERVER["PHP_SELF"]);
require_once( $_SERVER["DOCUMENT_ROOT"]."/".$aryPath[1]."/common/clsDefinition.php" );

$systeDir = clsDefinition::SYSTEM_DIR;

$strEcho = 
<<<EOT
<link rel="stylesheet" href="$systeDir/common/css/import.css" type="text/css" media="all" />
<link rel="stylesheet" href="$systeDir/templete/default/css/import.css" type="text/css" media="all" />
<link rel="stylesheet" href="$systeDir/common/js/jquery-ui.css" type="text/css" media="all" />
<script type="text/javascript" src="$systeDir/common/js/jquery.js"></script>
<script type="text/javascript" src="$systeDir/common/js/jquery-ui.js"></script>
<script type="text/javascript" src="$systeDir/value_checker/value_checker.js"></script>
<script type="text/javascript" src="$systeDir/common/js/modalDialog.js"></script>
<script type="text/javascript" src="$systeDir/common/js/html5shiv.js"></script>
<script type="text/javascript" src="$systeDir/common/js/html5shiv-printshiv.js"></script>
<script type="text/javascript" src="$systeDir/common/js/pageTop.js"></script>
<script type="text/javascript" src="$systeDir/common/js/listCtrl.js"></script>
<script type="text/javascript" src="$systeDir/common/js/label_over.js"></script>
<script type="text/javascript" src="$systeDir/templete/default/js/pullDownMenu.js"></script>
<script type="text/javascript" src="/__utm.js"></script>
<script type="text/javascript">urchinTracker();</script>\r\n
EOT;

echo $strEcho;
