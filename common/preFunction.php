<?php
/**
 * 全画面共通前処理
 *
 * 全画面共通の前処理を記述
 *
 * @author Mouri 2012/01/16
 * @version		1.0
 *
 **/
//セッションの開始
require_once("session_mysql.php");
session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');

date_default_timezone_set('Asia/Tokyo');

$strDirName = dirname($_SERVER["SCRIPT_NAME"]) .'/';

$blnIgnoreLoinCheck = false;

foreach( clsDefinition::$IGNORE_LOGIN_CHECK_LIST as $strIgnoreDir ){
	if( strpos($strDirName, $strIgnoreDir) !== false){
		$blnIgnoreLoinCheck = true;
		break;
	}
}

$blnRedirectable = false;
foreach( clsDefinition::$REDIRECTABLE_LIST as $strRedirectableDir ){
	if( strpos($strDirName, $strRedirectableDir) !== false){
		$blnRedirectable = true;
		break;
	}
}

//ログインチェック除外リストのプログラム以外でセッション情報がない場合は不正アクセスエラー
if( $blnIgnoreLoinCheck ){
	//ログインしていてかつlogin画面にアクセスした場合以外はセッション情報を初期化する。
	if( !(strlen(clsLoginSession::getUserId()) > 0 && strpos($strDirName, "/login") !== false) ){
		$intContactId = base64_decode($_GET[clsDefinition::GET_CONTACT_ID]) ?: clsContactSession::getContactId();
		if( clsCommonFunction::existsContactId((int)$intContactId)){
			clsContactSession::init(array( "contact_id" => $intContactId ));
			clsLoginSession::init(array());
		} else {
			$_SESSION["error_msg"] = "ページ表示エラー。URLをご確認ください。";
			require_once $_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/errPage.php";
			exit();
		}
	}
} else {
	$objClsLogin = new clsLogin();
	$intContactId = base64_decode($_GET[clsDefinition::GET_CONTACT_ID]) ?: clsContactSession::getContactId();
	if( !$objClsLogin->validateSession()){

		clsContactSession::init(array( "contact_id" => $intContactId ));
		clsLoginSession::init(array());
	}

	if( !$objClsLogin->updateLoginInfo() ){
		$intContactId = base64_decode($_GET[clsDefinition::GET_CONTACT_ID]) ?: clsContactSession::getContactId();
		if($intContactId > 0){
			if($blnRedirectable){
				$_SESSION[clsDefinition::SESSION_REDIRECT] = $_SERVER["REQUEST_URI"];
			} else {
				$_SESSION[clsDefinition::SESSION_REDIRECT] = "";
			}
			header("Location:" . clsDefinition::SYSTEM_DIR .'/login/login.php?'
				               . clsDefinition::GET_CONTACT_ID . '=' . base64_encode($intContactId) );
		} else {
			$_SESSION["error_msg"] = "セッションエラー。再度ログインしてください。";
			require_once $_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/errPage.php";
		}
		exit();
	}
}

?>