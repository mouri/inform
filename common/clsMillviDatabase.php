<?php
require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR.'/common/clsDataBase.php');

/**
 * ミルビィ動画の拡張データベースクラス
 * 
 * @author kim 2013/1/31
 */
class clsMillviDatabase extends clsDataBase {

	private static $objInstance = null;

	/**
	 * シングルトンインスタンスの取得関数 
	 * 
	 * @return clsMillbiDatabaseオブジェクト
	 */
	static function getInstance(){
		if( !self::$objInstance ){
			self::$objInstance = new clsMillviDatabase(
				clsDefinition::DB_TYPE,
				clsDefinition::DB_HOST,
				clsDefinition::DB_NAME,
				clsDefinition::DB_USER_NAME,
				clsDefinition::DB_PASSWORD
			);
		}

		return self::$objInstance;
	}

	/**
	 * クエリの実行結果が成功か失敗かのみ判定する関数
	 * 
	 * @param $aryResult pullDbData等のクエリ実行関数の戻り値 
	 * @return boolean true:エラー false:成功
	 */
	public function isError($aryResult){

		if( $aryResult === true || $aryResult === 0 ){
			return false;
		} else if( false === is_array($aryResult) 
			|| false === $aryResult 
			|| self::DATABASE_ERROR === $aryResult 
			|| self::ARGUMENT_ERROR === $aryResult  
		){
			return true;
		} else {
			return false;
		}
	}

}