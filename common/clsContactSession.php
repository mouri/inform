<?php
/**
 * 契約者セッション管理クラス 
 * 
 * @author kim
 */
class clsContactSession{
	
	/**
	 * 契約者セッションを連想配列で一括登録 
	 * 
	 * @param array $aryContactData
	 */
	static function init($aryContactData){
		$_SESSION[clsDefinition::SESSION_CONTACT] = $aryContactData;
	}

	/**
	 * 契約者セッションの項目ごとのセッター 
	 * 
	 * @param string $strName  contact_mstの列名
	 * @param mixd   $mxdValue セットする値
	 */
	static function set($strName, $mxdValue){
		$_SESSION[clsDefinition::SESSION_CONTACT][$strName] = $mxdValue;
	}

	/**
	 * 契約者IDの取得関数 
	 * 
	 * @return int
	 */
	static function getContactId(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["contact_id"];
	}

	static function getContactName(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["contact_name"];
	}

	static function getEncodePc(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["encode_pc"];
	}

	static function getEncodeSmp(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["encode_smp"];
	}

	static function getTemplete(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["templete"];
	}
	
	static function getFromAddr(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["fromAddr"];
	}
	
	static function getFromAddrName(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["fromAddrName"];
	}
	
	static function getSignature(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["signature"];
	}

	static function hasMenuCategory(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["menu_category_flg"] == 1 ? true : false;
	}
	
	static function getMenuId(){
		return $_SESSION[clsDefinition::SESSION_CONTACT]["menu_id"];
	}
}