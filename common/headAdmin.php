<?php
/**
 * headインクルードファイル共通ファイル一括読み込み
 * 
 * @author mouri
 */
$aryPath = explode("/", $_SERVER["PHP_SELF"]);
require_once( $_SERVER["DOCUMENT_ROOT"]."/".$aryPath[1]."/common/clsDefinition.php" );

$systeDir = clsDefinition::SYSTEM_DIR;

$strEcoh = 
<<<EOT
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script type="text/javascript" src="$systeDir/common/js/jquery.js"></script>
<script type="text/javascript" src="$systeDir/common/js/jquery-ui.js"></script>
<script type="text/javascript" src="$systeDir/common/js/html5shiv.js"></script>
<script type="text/javascript" src="$systeDir/common/js/html5shiv-printshiv.js"></script>
<script type="text/javascript" src="$systeDir/common/js/pageTop.js"></script>
<script type="text/javascript" src="$systeDir/common/js/listCtrl.js"></script>
<script type="text/javascript" src="$systeDir/common/js/label_over.js"></script>
<link rel="stylesheet" href="$systeDir/common/js/jquery-ui.css" type="text/css" media="all" />
<script type="text/javascript" src="$systeDir/value_checker/value_checker.js"></script>
<script type="text/javascript" src="$systeDir/common/js/modalDialog.js"></script>
<script type="text/javascript" src="$systeDir/templete/management/js/accordionMenu.js"></script>
<!--[if IE 8 ]>
<link rel="stylesheet" href="$systeDir/common/css/ie8.css" type="text/css" />
<![endif]-->\r\n
<script type="text/javascript" src="$systeDir/common/js/jquery.cleditor.min.js"></script>
<link rel="stylesheet" href="$systeDir/common/js/jquery.cleditor.css" type="text/css" media="all" />
<script type="text/javascript" src="$systeDir/common/js/jquery.cleditor.js"></script>
<script type="text/javascript" src="/__utm.js"></script>
<script type="text/javascript">urchinTracker();</script>\r\n
<link rel="stylesheet" href="$systeDir/common/css/import.css" type="text/css" media="all" />
<link rel="stylesheet" href="$systeDir/templete/management/css/import.css" type="text/css" media="all" />
EOT;

echo $strEcoh;
?>