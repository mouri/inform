<?php 
/**
 * ログインセッションの管理クラス 
 * 
 * @author kim
 */
class clsLoginSession{
	/**
	 * ログインセッションを連想配列で一括登録 
	 * 
	 * @param array $aryContactData
	 */
	static function init($aryContactData){
		$_SESSION[clsDefinition::SESSION_LOGIN] = $aryContactData;
	}

	/**
	 * ログインセッションの項目ごとのセッター 
	 * 
	 * @param string $strName  contact_mstの列名
	 * @param mixd   $mxdValue セットする値
	 */
	static function set($strName, $mxdValue){
		$_SESSION[clsDefinition::SESSION_LOGIN][$strName] = $mxdValue;
	}
	
	static function getContactId(){
		return $_SESSION[clsDefinition::SESSION_LOGIN]["contact_id"];
	}
	
	static function getUserId(){
		return $_SESSION[clsDefinition::SESSION_LOGIN]["user_id"];
	}

	static function getFullName(){
		// 名前が登録されている場合、名前を返す
		if( "" != $_SESSION[clsDefinition::SESSION_LOGIN]["name1"].$_SESSION[clsDefinition::SESSION_LOGIN]["name2"] ){
			return $_SESSION[clsDefinition::SESSION_LOGIN]["name1"] . $_SESSION[clsDefinition::SESSION_LOGIN]["name2"];
		// 名前が未登録の場合、ローマ字を返す
		} else {
			return $_SESSION[clsDefinition::SESSION_LOGIN]["roma1"] . $_SESSION[clsDefinition::SESSION_LOGIN]["roma2"];
		}
	}

	static function getNickname(){
		return $_SESSION[clsDefinition::SESSION_LOGIN]["nickname"];
	}

	static function getMailAddress(){
		return $_SESSION[clsDefinition::SESSION_LOGIN]["mail_address"];
	}

	static function getPassword(){
		return $_SESSION[clsDefinition::SESSION_LOGIN]["password"];
	}

	static function getAuthCd(){
		return $_SESSION[clsDefinition::SESSION_LOGIN]["auth_cd"];
	}
	/**
	 * ログインユーザが管理者かチェック
	 * user_kbn=1 管理者
	 * user_kbn=2 利用者
	 *
	 * @update 2013/03/22 kanata
	 * user_kbn=3 特権ユーザ追加
	 *
	 * @return boolean
	 */
	static function isAdmin(){
		return $_SESSION[clsDefinition::SESSION_LOGIN]["user_kbn"] == 1 OR $_SESSION[clsDefinition::SESSION_LOGIN]["user_kbn"] == 3 ? true : false;
	}

	static function getGroupCd(){
		return $_SESSION[clsDefinition::SESSION_LOGIN]["group_cd"];
	}

	static function getRandomSession(){
		return $_SESSION[clsDefinition::SESSION_LOGIN]["random_session"];
	}

	static function isSmartphone() {
		$aryUserAgents = array(
			'iPhone',         // Apple iPhone
			'iPod',           // Apple iPod touch
			'Android',        // Android
			'IEMobile',       // Windows phone
			'dream',          // Pre 1.5 Android
			'CUPCAKE',        // 1.5+ Android
			'BlackBerry',     // BlackBerry
			'webOS',          // Palm Pre Experimental
			'incognito',      // Other iPhone browser
			'webmate',        // Other iPhone browser
		);

		$strPattern = '/'.implode('|', $aryUserAgents).'/i';
		return preg_match($strPattern, $_SERVER['HTTP_USER_AGENT']);
	}

	/**
	 * 動画の投稿・編集権限のチェック 
	 *
	 * @return boolean
	 */
	static function isMovieEnterable(){
		$aryAuthCd = $_SESSION[clsDefinition::SESSION_LOGIN]["auth_cd"];
		foreach($aryAuthCd as $auth){
			if($auth["auth_cd"] == "2"){
				return true;
			}
		}
		return false;
	}
}