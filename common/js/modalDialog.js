//■共通機能一覧
//　ダイアログ出力制御(ModalDialogオブジェクト)
//　ファンクションメニュー制御(FunctionMenuオブジェクト)
//■仕様ライブラリ
//　jQuery
//　Jquery UI Dialog(および、Dialog使用に必要なコンポーネント)

/**
 * ダイアログ出力制御オブジェクト
 * 
 * 確認や警告など、ユーザーにメッセージを表示するダイアログを
 * 生成・管理するオブジェクト
 * jQuery UI Dialogライブラリを使用
 * 
 * @author 2012/5/11 Kim
 **/
var ModalDialog = function(){
	//現在フォーカス中のIDを取得
	var objCurrentDialog = null;
};

//ダイアログの表示関数
ModalDialog.showDialog = function(category, strTitle, strMessage, callbackResponse){

	//タイトルを表示しないように変更(表示する場合は次行のコードを削除すること)
	strTitle = "";

	if($("#modalDialog").size() > 0 ){
		return;
	}

	//ダイアログ呼び出し前のフォーカスを取得
	var strFocusedAttr = "id";
	var strFocused = $(":focus").attr("id");
	if( typeof strFocused === "undefined" ){
		strFocusedAttr = "name";
		strFocused = $(":focus").attr("name");
		if( typeof strFocused === "undefined" ){
			strFocusedAttr = "none";
		}
	}

	var blnResult = false;
	var objButtons;
	var strDialogClass = category + "Dialog";

	//コールバック関数が未設定の場合は、falseを返す無名関数を設定
	if(typeof callbackResponse === 'undefined'){
		callbackResponse = function(blnResult){
			return false;
		};
	}


	//ダイアログの種類別にボタンを設定
	switch(category){
		//alertとconfirmはOK/閉じるボタンを使用
		case "alert":
		case "confirm":

			objButtons = {
				"ＯＫ" : function(){
					blnResult = true;
					$(this).dialog('close');
				},
				"キャンセル" : function(){
					$(this).dialog('close');
				}
			};
			break;

		//information とerrorは閉じるボタンのみ
		case "information":
		case "error":
			objButtons = {
				"閉じる" : function(){
					$(this).dialog('close');
				}
			};

			break;

		default:
			alert("ダイアログ種別ERROR");
			callbackResponse = function(blnResult){
			};
			return;
	}

	//ダイアログの入れ物の作成
	var objDialog = $('<div id="modalDialog"></div>');
	//BODY部のメッセージを生成

	var objMessageTable = $('<table><tr><td class="modalDialogIcon" style="height:50px;"></td><td rowspan="2"><div class="modalDialogMessage meiryo"></div></td></tr><tr><td></td></tr></table>');
	objMessageTable.css({
		"width" : "100%"
	});


	objMessageTable.find(".modalDialogIcon").css({
		"min-height": "50px",
		"min-width" : "30px",
		"background" : "url(/dentsu/common/js/images/" + category + ".png) 5px 5px no-repeat"
	});

	objMessageTable.find(".modalDialogMessage").css({
		"word-wrap"   : "break-word",
		"word-break"  : "break-all"
	});
	objMessageTable.find(".modalDialogMessage").html(strMessage);

	objDialog.append(objMessageTable);


	//jQuery UI のdialog関数を呼び出し、ダイアログオブジェクトを生成
	objDialog.dialog({
		autoOpen : false,
        open: function () {
            $(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar").addClass(strDialogClass);
             $(".ui-dialog-titlebar-close").hide();
             $(this).parents('.ui-dialog-buttonpane button:eq(0)').focus();
        },
        width    : "450px",
		title    : strTitle,
		modal    : true,
		zIndex   : 3999,
		closeOnEscape : true,
		resizable: false,
        overlay  : {
			backgroundColor: '#000',
			opacity        : 1
		},
		close : function(){
			if( strFocusedAttr == "id" ){
				$("#" + strFocused).focus();
			} else if( strFocusedAttr == "name" ){
				$('[name="' + strFocused + '"]').focus();
			}

			callbackResponse(blnResult);
			$(this).dialog("destroy").remove();
			this.objCurrentDialog = null;
		},
		buttons  : objButtons,
		dialogClass : strDialogClass
	});

	//ダイアログを画面に表示
	objDialog.dialog('open');

	//出力したダイアログをカレントダイアログ変数に格納
	this.objCurrentDialog = objDialog;
};

//現在表示しているダイアログオブジェクトのgetter
ModalDialog.getDialog = function(){
	return this.objCurrentDialog;
};

//確認ダイアログを呼び出す
ModalDialog.confirm = function(strTitle, strMessage, callbackResponse){
	ModalDialog.showDialog("confirm", strTitle, strMessage, callbackResponse);
};

//警告ダイアログを呼び出す
ModalDialog.alert = function(strTitle, strMessage, callbackResponse){
	ModalDialog.showDialog("alert", strTitle, strMessage, callbackResponse);
};

//情報ダイアログを呼び出す
ModalDialog.information = function(strTitle, strMessage, callbackResponse){
	ModalDialog.showDialog("information", strTitle, strMessage, callbackResponse);
};

//エラーダイアログを呼び出す。
ModalDialog.error = function(strTitle, strMessage, callbackResponse){
	ModalDialog.showDialog("error", strTitle, strMessage, callbackResponse);
};

