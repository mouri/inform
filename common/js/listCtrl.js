$(document).ready(function(){
	
	//<li>タグの最後にlastクラスを追加
	$("ul li:last-child").addClass("last");

	$("div.frameW316:nth-child(3n)").addClass("rightEnd");
	$("div.frameW316:nth-child(3n)").after("<br clear='all'>");

	$("table tr:last-child").addClass("last");

	$(".checkBoxDiv label.inputCheckBox:first-child").addClass("first");
	$(".checkBoxDiv label.inputCheckBox:last-child").addClass("last");

	$("select option:first-child").addClass("first");

	$("td.message > p:last-child").addClass("last");
	
	
	//div.box2の中にあるdivの高さ調整
	var id = $('div.box2').attr('id');
	var height = $('div#'+ id ).outerHeight();
	height = height -'30' + 'px';
	$('div#'+ id +'> div > div').css('height',height);


});

