//ページャのリンク制御
$(document).ready(function(){
	$("ul[class='pager'] a").click(function(event){
		var id = $(this).attr("id").replace("pagerNumber", "");
		
		$('<input />').attr('type', 'hidden')
						.attr('name', 'pagerNumber')
						.attr('value', id)
						.appendTo($(this).parents('form')[0]);
		$(this).parents('form')[0].submit();
	});
});