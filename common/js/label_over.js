jQuery.fn.labelOver = function(overClass) {
	return this.each(function(){
		var label = jQuery(this);
		var f = label.attr('for');
		if (f) {
			var input = jQuery('#' + f);
			
			this.hide = function() {
			  label.css({ textIndent: -10000 })
			}
			
			this.show = function() {
			  if (input.val() == '') label.css({ textIndent: 0 })
			}
			
			// handlers
			input.focus(this.hide);
			input.blur(this.show);
		  label.addClass(overClass).click(function(){ input.focus() });
			
			if (input.val() != '') this.hide(); 
		}
	})
}

$(document).ready(function(){
		$('input[type="text"] ~ label').labelOver('over');
		$('textarea ~ label').labelOver('over');
		$('textarea ~ label').labelOver('over');	

		//はじめからチェックがついていたらcheckedクラスをつける
		$('input[type="radio"]:checked').parent().parent().addClass('checked');
		$('input[type="checkbox"]:checked').parent().addClass('checked');
		
		//クリックしたラジオボタンにクラスを割り当てる
		var radio = $('div.radioDiv');
		$('label', radio).click(function() {
			$(this).parent().parent().each(function() {
				$('label',this).parent().removeClass('checked');	
			});
			$(this).parent().addClass('checked');
		});
		
		//クリックしたチェックボックスにクラスを割り当てる
		$('label.inputCheckBox').click(function() {
			if ( $('input',this).attr('checked') ) {
				$(this).addClass('checked');
			} else {
				$(this).removeClass('checked');
			}
		});

});
