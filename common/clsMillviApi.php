<?php
class clsMillviApi {
	// アカウントの制御
	private $_strId=clsDefinition::MILLVI_ID;		//ログインID
	private $_strPass=clsDefinition::MILLVI_PASS;	//パスワード
	private $_strPcode=clsDefinition::MILLVI_PCODE;	//X-PROJECTCODE(millvi管理ＵＲＬの名前を使用)

	const ORDER_NEW = "new";
	const ORDER_OLD = "old";
	
	const H264_500kbps = 101;
	const H264_800kbps = 102;
	const H264_1000kbps = 103;
	const H264_1500kbps = 104;
	
	/**
	 * 登録されている動画一覧を取得します。
	 *
	 * getVideoInfoのAPIを使用
	 *
	 * @param	$intIdPlayerdata	プレイヤーＩＤ【※必須】<br>
	 * 								貼り付けに使用するプレイヤーのプレイヤーID です。<br>
	 * 								※管理画面「プレイヤ作成/管理」の「プレイヤーの情報」画面内に「プレイヤーID」の記載があります。<br>
	 * <br>
	 * @param	$intIdVideodata		動画ＩＤ<br>
	 * 								情報を取得する動画の動画ID です。<br>
	 * 								指定しない場合、全動画の情報を取得します。<br>
	 * 								※管理画面「ファイル投稿/管理」の「動画の情報」画面内に「動画ID」の記載があります。<br>
	 * <br>
	 * @param	$intOffset			取得位置<br>
	 * 								全動画の情報を取得する場合、取得する位置を指定します。<br>
	 * 								例）全100 件中、20 件目から取得する場合は、20 を指定。<br>
	 * 								指定しない場合、1 件目から取得します。<br>
	 * <br>
	 * @param	$intLimit			取得個数<br>
	 * 								全動画の情報を取得する場合、取得する個数を指定します。<br>
	 * 								例）全100 件中、20 件目から10 件取得する場合は、offset に20 を、limitに10 を指定。<br>
	 * 								指定しない場合、全て取得します。<br>
	 * <br>
	 * @param	$strOrder			ソート方法<br>
	 * 								全動画の情報を取得する場合、ソート方法を指定します。<br>
	 * 								"new" → 最新順<br>
	 * 								"old" → 古い順<br>
	 * 								指定しない場合、最新順になります。<br>
	 * <br>
	 * @return	$aryXmlData			ＸＭＬデータ<br>
	 *								response XML ドキュメントルート<br>
	 *								│<br>
	 *								├─status API ステータスコード。true:成功、false:失敗。<br>
	 *								│<br>
	 *								├─query 入力クエリー情報<br>
	 *								│	├─id_videodata 入力クエリー:id_videodataの値<br>
	 *								│	├─id_playerdata 入力クエリー: id_playerdata の値<br>
	 *								│	├─offset 入力クエリー: offset の値<br>
	 *								│	├─limit 入力クエリー: limit の値<br>
	 *								│	└─order 入力クエリー: order の値<br>
	 *								│<br>
	 *								├─resultinfo 結果の付加情報<br>
	 *								│	└─count 総件数（入力クエリー:limit の値によらない、総件数）<br>
	 *								│<br>
	 *								├─videoinfo 動画リスト<br>
	 *								│	└─video 動画情報（動画が複数ある場合、video 要素は複数になる）<br>
	 *								│		├─id id_videodataと同等。<br>
	 *								│		├─id_videodata 動画のID（数値）<br>
	 *								│		├─videostatus 変換ステータス。done:変換成功で固定。<br>
	 *								│		├─duration 再生時間（単位:秒）<br>
	 *								│		├─thumbnail サムネイル画像のURI<br>
	 *								│		└─player 貼り付けプレイヤーの情報<br>
	 *								│			├─name プレイヤーの名前<br>
	 *								│			├─id_playerdata プレイヤーID<br>
	 *								│			└─canvas キャンバスの情報（キャンバスが複数ある場合、canvas 要素は複数になる）<br>
	 *								│				├─name キャンバスの名前<br>
	 *								│				└─embed キャンバスの貼り付けタグ<br>
	 * 
	 */
	function getVideoInfo($intIdPlayerdata, $intIdVideodata = null, $intOffset = null, $intLimit = null, $strOrder = null) {
		$aryOpts = array(
				'http'=>array(
						'method'=>'GET',
						'header'=>'X-AUTHCODE:' . sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE:'.$this->_strPcode
				)
		);

		$rscContext = stream_context_create($aryOpts);

		$strApiurl  = 'http://asp.millvi.jp/esapi/getVideoInfo?';
		$strApiurl .= 'id_playerdata='.$intIdPlayerdata;
		$strApiurl .= '&id_videodata='.$intIdVideodata;
		$strApiurl .= '&offset='.$intOffset;
		$strApiurl .= '&limit='.$intLimit;
		$strApiurl .= '&order='.$strOrder;

		$strData = file_get_contents ($strApiurl, false, $rscContext);
		$strXmldata = simplexml_load_string($strData);
		
		return $strXmldata;
	}

	/**
	 * 登録されている全動画の詳細な情報を取得します。
	 *
	 * getVideoStatusのAPIを使用
	 *
	 * @param	$intIdVideodata	動画ＩＤ<br>
	 * 							動画ID を指定すると、その動画のみの情報を返す。デフォルトは全ての動画の情報を返す。<br>
	 * 							カンマ区切りで複数の動画を指定することも可能。<br>
	 * <br>
	 * @return	aryXmlData		ＸＭＬデータ<br>
	 *							response XML ドキュメントルート<br>
	 *							│<br>
	 *							├─status API ステータスコード。true:成功、false:失敗。<br>
	 *							│<br>
	 *							├─video 動画情報<br>
	 *							│	├─@id_videodata 動画ID<br>
	 *							│	├─@id_videodata_nc 暗号化動画ID（貼り付けタグで動画ID の代わりに指定できます）<br>
	 *							│	├─@title 動画のタイトル<br>
	 *							│	├─@description 動画の説明文<br>
	 *							│	├─@tag 動画のタグ<br>
	 *							│	├─@duration 動画の再生時間<br>
	 *							│	├─@status 動画の変換ステータス（ 変換完了:done 変換失敗:failure 変換中:processing）<br>
	 *							│	├─@thumbnail サムネイルのURI<br>
	 *							│	├─@width 動画の横サイズ<br>
	 *							│	├─@height 動画の縦サイズ<br>
	 *							│	├─@id_encodesetting 動画の画質<br>
	 *							│	└─@filesize 動画のファイルサイズ<br>
	 *
	 */
	function getVideoStatus($intIdVideodata = null) {
		$aryOpts = array(
				'http'=>array(
						'method'=>'GET',
						'header'=>'X-AUTHCODE:' . sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE:'.$this->_strPcode
				)
		);

		$rscContext = stream_context_create($aryOpts);

		$strApiurl  = 'http://asp.millvi.jp/esapi/getVideoStatus?';
		$strApiurl .= '&id_videodata='.$intIdVideodata;
		
		$strData = file_get_contents ($strApiurl, false, $rscContext);
		$strXmldata = simplexml_load_string($strData);
		
		return $strXmldata;
	}

	/**
	 * 動画のサムネイル一覧を取得します。
	 *
	 * getThumbnailのAPIを使用
	 *
	 * @param	$intIdVideodata	動画ＩＤ【※必須】<br>
	 * <br>
	 * @return	aryXmlData		ＸＭＬデータ<br>
	 * 							response XML ドキュメントルート<br>
	 *							│<br>
	 *							├─status API ステータスコード。true:成功、false:失敗。<br>
	 *							│<br>
	 *							├─thumbnail サムネイル<br>
	 *							│	├─id_thumbnail サムネイルのID<br>
	 *							│	├─url サムネイルのURL<br>
	 *							│	└─selected 現在選択されているサムネイルなら"1"、それ以外は"0"。<br>
	 *
	 */
	function getThumbnail($intIdVideodata) {
		$aryOpts = array(
				'http'=>array(
						'method'=>'GET',
						'header'=>'X-AUTHCODE:' .sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE:'.$this->_strPcode
				)
		);
	
		$rscContext = stream_context_create($aryOpts);
	
		$strApiurl  = 'http://asp.millvi.jp/esapi/getThumbnail?';
		$strApiurl .= '&id_videodata='.$intIdVideodata;
	
		$strData = file_get_contents ($strApiurl, false, $rscContext);
		$strXmldata = simplexml_load_string($strData);
	
		return $strXmldata;
	}


	function setThumbnail($intIdVideodata, $intIdThumbnail) {
		$aryOpts = array(
				'http'=>array(
						'method'=>'GET',
						'header'=>'X-AUTHCODE:' .sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE:'.$this->_strPcode
				)
		);
	
		$rscContext = stream_context_create($aryOpts);
	
		$strApiurl  = 'http://asp.millvi.jp/esapi/setThumbnail?';
		$strApiurl .= '&id_videodata='.$intIdVideodata.'&id_thumbnail='.$intIdThumbnail;
	
		$strData = file_get_contents ($strApiurl, false, $rscContext);
		$strXmldata = simplexml_load_string($strData);
	
		return $strXmldata;
	}
	
	//========================================================================
	//		処理メッセージ表示
	//========================================================================
	// $msg : 出力メッセージ
	function outPutMessage($msg){
		echo $msg;
		ob_flush();
		flush();
	}

	/**
	 * 動画の登録をおこないます(HTTP)
	 *
	 * addVideoのAPIを使用
	 *
	 * @param	$strFilePath		動画ファイルパス【※必須】<br>
	 * 								登録する動画のファイルパス。
	 * <br>
	 * @param	$strTitle			動画のタイトル【※必須】<br>
	 * <br>
	 * @param	$strFolderPath		動画フォルダのパス<br>
	 * 								動画登録先フォルダのパス。"/"を使用して複数階層の指定も可能。パスが存在していなければ自動的に生成されます。<br>
	 * 								管理画面の「ファイル投稿/管理」で表示されるツリーのパスです。デフォルトはルート直下に置かれます。<br>
	 * <br>
	 * @param	$strTag				動画のタグ<br>
	 * 								半角スペース区切りで複数指定できます。
	 * <br>
	 * @param	$strDescription		動画の説明文<br>
	 * 								改行コードは"\ n"(0x0A)です<br>
	 * <br>
	 * @param	$intNoencode		動画ファイルの形式<br>
	 * 								登録する動画ファイルの形式が".flv , .mp4 , .m4v .f4v"の場合に、サーバーサイドで再度エンコードを行わせたくない場合は"1"を指定します。<br>
	 * 								上記ファイルフォーマット以外で"1"を指定した場合でもエンコードは行われます。<br>
	 * 								デフォルトではサーバーサイドでエンコードが行われます。下記のヘッダーと合わせて入力して下さい。<br>
	 * <br>
	 * @param	$intEncodesetting	動画の画質指定します。（noencode=0 のときのみ）<br>
	 * 								何も指定しなければ「H.264 800kbps」になります。<br>
	 * 								101: H.264 500kbps<br>
	 * 								102: H.264 800kbps （デフォルト）<br>
	 * 								103: H.264 1000kbps<br>
	 * 								104: H.264 1500kbps<br>
	 * <br>
	 * @return	$aryXmlData			ＸＭＬデータ<br>
	 * 								response XML ドキュメントルート<br>
	 *								│<br>
	 *								├─status API ステータスコード。true:成功、false:失敗。<br>
	 *								├─video 登録した動画の情報（複数）<br>
	 *								│	├─title 動画のタイトル（入力クエリーのtitle の内容）<br>
	 *								│	├─tag 動画のタグ（入力クエリーのtag の内容）<br>
	 *								│	├─description 動画の説明文（入力クエリーのdescription の内容）<br>
	 *								│	├─id_videodata 動画ID。プレイヤー貼り付けタグなどで、動画を指定するのに使用。<br>
	 *								│	└─path 登録先のパス。（入力クエリーのpath の内容）<br>
	 */
	function addVideo($strFilePath,$strTitle,$strFolderPath=null,$strTag=null,$strDescription=null,$intNoencode=null,$intEncodesetting=null){
		
		$strPost = "";
		$aryParam = array(
				'path' => $strFolderPath,
				'title' => $strTitle,
				'tag' => $strTag,
				'description' => $strDescription,
				'noencode' => $intNoencode,
				'id_encodesetting' => $intEncodesetting
		);
		
		$strBoundary = substr(md5(rand(0,32000)), 0, 10);
		
		foreach($aryParam as $key => $val) {
			$strPost .= "--".$strBoundary."\n";
			$strPost .= "Content-Disposition: form-data; name=\"".$key."\"\n\n".$val."\n";
		}
		
		$strPost .= "--".$strBoundary."\n";
		
		if (!empty($strFilePath)) {
			$strFileContents = file_get_contents($strFilePath);
			
			$strFilename = basename($strFilePath);
			$strPost .= "Content-Disposition: form-data; name=\"filedata\"; filename=\"{$strFilename}\"\n";
			$strPost .= "Content-Type: application/octet-stream\n";
			$strPost .= "Content-Transfer-Encoding: binary\n\n";
			$strPost .= $strFileContents."\n";
			$strPost .= "--$strBoundary\n";
		}
		
		$aryOpts = array('http' => array(
				'method' => 'POST',
				'header' => 'X-AUTHCODE: '.sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE: '.$this->_strPcode."\r\n".'Content-Type: multipart/form-data; boundary='.$strBoundary."\r\n",
				'content' => $strPost
		));
		
		$rscContext = stream_context_create($aryOpts);
		
		$strApiurl = "http://asp.millvi.jp/esapi/addVideo";
		
		if( ( $sResponce = @file_get_contents($strApiurl, false, $rscContext) ) == FALSE ){
			return false;
		}else{
			$strXmldata = simplexml_load_string($sResponce);
		}
		
		return $strXmldata;
	}
	
	/**
	 * 動画の登録をおこないます(FTP)
	 *
	 * addVideoのAPIを使用
	 *
	 * @param	$strFilePath		動画ファイルパス【※必須】<br>
	 * 								登録する動画のファイルパス。
	 * <br>
	 * @param	$strTitle			動画のタイトル【※必須】<br>
	 * <br>
	 * @param	$strFolderPath		動画フォルダのパス<br>
	 * 								動画登録先フォルダのパス。"/"を使用して複数階層の指定も可能。パスが存在していなければ自動的に生成されます。<br>
	 * 								管理画面の「ファイル投稿/管理」で表示されるツリーのパスです。デフォルトはルート直下に置かれます。<br>
	 * <br>
	 * @param	$strTag				動画のタグ<br>
	 * 								半角スペース区切りで複数指定できます。
	 * <br>
	 * @param	$strDescription		動画の説明文<br>
	 * 								改行コードは"\ n"(0x0A)です<br>
	 * <br>
	 * @param	$intNoencode		動画ファイルの形式<br>
	 * 								登録する動画ファイルの形式が".flv , .mp4 , .m4v .f4v"の場合に、サーバーサイドで再度エンコードを行わせたくない場合は"1"を指定します。<br>
	 * 								上記ファイルフォーマット以外で"1"を指定した場合でもエンコードは行われます。<br>
	 * 								デフォルトではサーバーサイドでエンコードが行われます。下記のヘッダーと合わせて入力して下さい。<br>
	 * <br>
	 * @param	$intEncodesetting	動画の画質指定します。（noencode=0 のときのみ）<br>
	 * 								何も指定しなければ「H.264 800kbps」になります。<br>
	 * 								101: H.264 500kbps<br>
	 * 								102: H.264 800kbps （デフォルト）<br>
	 * 								103: H.264 1000kbps<br>
	 * 								104: H.264 1500kbps<br>
	 * <br>
	 * @return	$aryXmlData			ＸＭＬデータ<br>
	 * 								response XML ドキュメントルート<br>
	 *								│<br>
	 *								├─status API ステータスコード。true:成功、false:失敗。<br>
	 *								├─video 登録した動画の情報（複数）<br>
	 *								│	├─title 動画のタイトル（入力クエリーのtitle の内容）<br>
	 *								│	├─tag 動画のタグ（入力クエリーのtag の内容）<br>
	 *								│	├─description 動画の説明文（入力クエリーのdescription の内容）<br>
	 *								│	├─id_videodata 動画ID。プレイヤー貼り付けタグなどで、動画を指定するのに使用。<br>
	 *								│	└─path 登録先のパス。（入力クエリーのpath の内容）<br>
	 */
	function addVideoFTP($strFilePath,$strTitle,$strFolderPath=null,$strTag=null,$strDescription=null,$intNoencode=null,$intEncodesetting=null){
		
		//ＦＴＰ接続してファイルをおく
		$aryFtp = array(
			'ftp_server' => clsDefinition::FTP_SERVER_MILLVI,
			'ftp_user_name' => clsDefinition::FTP_USER_MILLVI,
			'ftp_user_pass' => clsDefinition::FTP_USER_PASS_MILLVI
		);
		
		//アップロードファイルパス
		$strFile = $strFilePath;
		//ＦＴＰファイルパス
		$strFtpFile = basename($strFilePath);
		
		//ＦＴＰ接続
		$rscConnect = ftp_connect($aryFtp["ftp_server"],21,90) or die();
	
		//ＦＴＰログイン
		$login_result = ftp_login($rscConnect,$aryFtp['ftp_user_name'],$aryFtp['ftp_user_pass']) or die();
		//パッシブモードにする
		ftp_pasv($rscConnect, true);
		
		//ＦＴＰサーバにファイルアップ
		$test = ftp_put($rscConnect,$strFtpFile,$strFile,FTP_BINARY);
		

		$strPost = "";
		$aryParam = array(
				'path' => $strFolderPath,
				'title' => $strTitle,
				'tag' => $strTag,
				'description' => $strDescription,
				'noencode' => $intNoencode,
				'id_encodesetting' => $intEncodesetting,
				'ftp' => $strFtpFile
		);
		
		$strBoundary = substr(md5(rand(0,32000)), 0, 10);
		
		foreach($aryParam as $key => $val) {
			$strPost .= "--".$strBoundary."\n";
			$strPost .= "Content-Disposition: form-data; name=\"".$key."\"\n\n".$val."\n";
		}
		
		$strPost .= "--".$strBoundary."\n";
		
		$aryOpts = array('http' => array(
				'method' => 'POST',
				'header' => 'X-AUTHCODE: '.sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE: '.$this->_strPcode."\r\n".'Content-Type: multipart/form-data; boundary='.$strBoundary."\r\n",
				'content' => $strPost
		));
		
		$rscContext = stream_context_create($aryOpts);
		
		$strApiurl = "http://asp.millvi.jp/esapi/addVideo";
		
		//失敗時
		if( ( $sResponce = file_get_contents($strApiurl, false, $rscContext) ) == FALSE ){
			return false;
		//成功時
		}else{
			//結果をXMLで返却する
			$strXmldata = simplexml_load_string($sResponce);
			
			//ＦＴＰサーバからファイルを削除する
			ftp_delete($rscConnect,$strFtpFile);
		}
		
		//ＦＴＰ切断
		ftp_close($rscConnect);
		
		return $strXmldata;
	}
	
	/**
	 * 動画の登録をおこないます（ファイルリソース使用バージョン）
	 *
	 * addVideoのAPIを使用
	 *
	 * @param	$rscFile			動画ファイルのリソース<br>
	 * <br>
	 * @param	$strFileName		動画のファイル名<br>
	 * <br>
	 * @param	$strTitle			動画のタイトル【※必須】<br>
	 * <br>
	 * @param	$strFolderPath		動画フォルダのパス<br>
	 * 								動画登録先フォルダのパス。"/"を使用して複数階層の指定も可能。パスが存在していなければ自動的に生成されます。<br>
	 * 								管理画面の「ファイル投稿/管理」で表示されるツリーのパスです。デフォルトはルート直下に置かれます。<br>
	 * <br>
	 * @param	$strTag				動画のタグ<br>
	 * 								半角スペース区切りで複数指定できます。
	 * <br>
	 * @param	$strDescription		動画の説明文<br>
	 * 								改行コードは"\ n"(0x0A)です<br>
	 * <br>
	 * @param	$intNoencode		動画ファイルの形式<br>
	 * 								登録する動画ファイルの形式が".flv , .mp4 , .m4v .f4v"の場合に、サーバーサイドで再度エンコードを行わせたくない場合は"1"を指定します。<br>
	 * 								上記ファイルフォーマット以外で"1"を指定した場合でもエンコードは行われます。<br>
	 * 								デフォルトではサーバーサイドでエンコードが行われます。下記のヘッダーと合わせて入力して下さい。<br>
	 * <br>
	 * @param	$intEncodesetting	動画の画質指定します。（noencode=0 のときのみ）<br>
	 * 								何も指定しなければ「H.264 800kbps」になります。<br>
	 * 								101: H.264 500kbps<br>
	 * 								102: H.264 800kbps （デフォルト）<br>
	 * 								103: H.264 1000kbps<br>
	 * 								104: H.264 1500kbps<br>
	 * <br>
	 * @return	$aryXmlData			ＸＭＬデータ<br>
	 * 								response XML ドキュメントルート<br>
	 *								│<br>
	 *								├─status API ステータスコード。true:成功、false:失敗。<br>
	 *								├─video 登録した動画の情報（複数）<br>
	 *								│	├─title 動画のタイトル（入力クエリーのtitle の内容）<br>
	 *								│	├─tag 動画のタグ（入力クエリーのtag の内容）<br>
	 *								│	├─description 動画の説明文（入力クエリーのdescription の内容）<br>
	 *								│	├─id_videodata 動画ID。プレイヤー貼り付けタグなどで、動画を指定するのに使用。<br>
	 *								│	└─path 登録先のパス。（入力クエリーのpath の内容）<br>
	 */
	function addVideoResource($rscFile,$strFileName,$strTitle,$strFolderPath=null,$strTag=null,$strDescription=null,$intNoencode=null,$intEncodesetting=null){
	
		$strPost = "";
		$aryParam = array(
				'path' => $strFolderPath,
				'title' => $strTitle,
				'tag' => $strTag,
				'description' => $strDescription,
				'noencode' => $intNoencode,
				'id_encodesetting' => $intEncodesetting
		);
	
		$strBoundary = substr(md5(rand(0,32000)), 0, 10);
	
		foreach($aryParam as $key => $val) {
			$strPost .= "--".$strBoundary."\n";
			$strPost .= "Content-Disposition: form-data; name=\"".$key."\"\n\n".$val."\n";
		}
	
		$strPost .= "--".$strBoundary."\n";
	
		if (!empty($rscFile)) {
			$strFileContents = $rscFile;
	
			//$strFilename = basename($strFilePath);
			$strPost .= "Content-Disposition: form-data; name=\"filedata\"; filename=\"{$strFilename}\"\n";
			$strPost .= "Content-Type: application/octet-stream\n";
			$strPost .= "Content-Transfer-Encoding: binary\n\n";
			$strPost .= $strFileContents."\n";
			$strPost .= "--$strBoundary\n";
		}
	
	
		$aryOpts = array('http' => array(
				'method' => 'POST',
				'header' => 'X-AUTHCODE: '.sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE: '.$this->_strPcode."\r\n".'Content-Type: multipart/form-data; boundary='.$strBoundary."\r\n",
				'content' => $strPost
		));
	
		$rscContext = stream_context_create($aryOpts);
	
	
		$strApiurl = "http://asp.millvi.jp/esapi/addVideo";
	
	
		$strData = file_get_contents ($strApiurl, false, $rscContext);
	
		$strXmldata = simplexml_load_string($strData);
	
		return $strXmldata;
	
	
	}
	
	/**
	 * 動画のメタデータを編集します。
	 *
	 * updateMetadataのAPIを使用
	 *
	 * @param	$intIdVideodata	動画ＩＤ【※必須】<br>
	 * 							対象の動画ID です。<br>
	 * <br>
	 * @param	$strTitle		動画のタイトル<br>
	 * <br>
	 * @param	$strTag			動画のタグ<br>
	 * <br>
	 * @param	$strDescription	動画の説明文<br>
	 * 							改行コードは"\n"(0x0A)です。<br>
	 * <br>
	 * @return	aryXmlData		ＸＭＬデータ<br>
	 * 							response XML ドキュメントルート<br>
	 *							│<br>
	 *							├─status API ステータスコード。true:成功、false:失敗。<br>
	 *							├─query 入力クエリー情報<br>
	 *							│	├─id_videodata 入力クエリー"id_videodata"の値。<br>
	 *							│	├─title 入力クエリー"title"の値。<br>
	 *							│	├─tag 入力クエリー"tag"の値。<br>
	 *							│	└─description 入力クエリー"description"の値。<br>
	 *
	 */
	function updateMetadata($intIdVideodata,$strTitle=null,$strTag=null,$strDescription=null){
		$aryOpts = array(
				'http'=>array(
						'method'=>'GET',
						'header'=>'X-AUTHCODE:' .sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE:'.$this->_strPcode
				)
		);
		
		$rscContext = stream_context_create($aryOpts);
		
		$strApiurl  = 'http://asp.millvi.jp/esapi/updateMetadata?';
		$strApiurl .= '&id_videodata='.$intIdVideodata;
		$strApiurl .= '&title='.$strTitle;
		$strApiurl .= '&tag='.$strTag;
		$strApiurl .= '&description='.$strDescription;
		
		$strData = file_get_contents ($strApiurl, false, $rscContext);
		$strXmldata = simplexml_load_string($strData);
		
		return $strXmldata;
		
	}

	/**
	 * 動画を削除します。
	 *
	 * deleteVideoのAPIを使用
	 *
	 * @param	$intIdVideodata	動画ＩＤ【※必須】<br>
	 * <br>
	 * @return	aryXmlData		ＸＭＬデータ<br>
	 * 							response XML ドキュメントルート<br>
	 *							│<br>
	 *							├─status API ステータスコード。true:成功、false:失敗。<br>
	 *							├─query 入力クエリー情報<br>
	 *							│	└─id_videodata 入力クエリー"id_videodata"の値。<br>
	 */
	function deleteVideo($intIdVideodata){
		$aryOpts = array(
				'http'=>array(
						'method'=>'GET',
						'header'=>'X-AUTHCODE:' .sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE:'.$this->_strPcode
				)
		);
		
		$rscContext = stream_context_create($aryOpts);
		
		$strApiurl  = 'http://asp.millvi.jp/esapi/deleteVideo?';
		$strApiurl .= '&id_videodata='.$intIdVideodata;
		
		
		$strData = file_get_contents ($strApiurl, false, $rscContext);
		$strXmldata = simplexml_load_string($strData);
		
		return $strXmldata;
	}


	
	/**
	 * スマートフォン用動画への変換をリクエストする。
	 **/
	function requestEncodeSmartfon($intIdPlayerdata, $intEncodesetting) {
		$aryOpts = array(
				'http'=>array(
						'method'=>'GET',
						'header'=>'X-AUTHCODE:' . sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE:'.$this->_strPcode
				)
		);

		$rscContext = stream_context_create($aryOpts);

		$strApiurl  = 'http://asp.millvi.jp/esapi/encodeSmartfon?';
		$strApiurl .= 'id_videodata='.$intIdVideodata;
		$strApiurl .= '&type=download';
		$strApiurl .= '&bitrate='. $intEncodesetting;

		$strData = file_get_contents ($strApiurl, false, $rscContext);
		$strXmldata = simplexml_load_string($strData);
		
		return $strXmldata;
	}
	
	
	/**
	 * スマートフォン動画の変換状態を取得する。
	 **/
	function getSmartfonVudeoStatus($intIdPlayerdata) {
		$aryOpts = array(
				'http'=>array(
						'method'=>'GET',
						'header'=>'X-AUTHCODE:' . sha1($this->_strId.'/'.$this->_strPass)."\r\n".'X-PROJECTCODE:'.$this->_strPcode
				)
		);

		$rscContext = stream_context_create($aryOpts);

		$strApiurl  = 'http://asp.millvi.jp/esapi/listSmartfon?';
		$strApiurl .= 'id_videodata='.$intIdVideodata;

		$strData = file_get_contents ($strApiurl, false, $rscContext);
		$strXmldata = simplexml_load_string($strData);
		
		return $strXmldata;
	}
}

?>