<?php

//require_once("session_mysql.ini");

date_default_timezone_set('Asia/Tokyo');

// ユーザ定義のセッションハンドラを使用
ini_set("session.save_handler", "user") or die("ini_set(session.save_handler) failed.");
// セッションハンドラ関数を設定
session_set_save_handler('_open', '_close', '_read', '_write', '_destroy', '_clean');
// PHP5.0.5 以降は session_start() を呼ぶ前に必ずこれを実行
register_shutdown_function('session_write_close'); 
 
function _open()
{
    global $_sess_db;
 
    $db_host = clsDefinition::DB_HOST;
    $db_user = clsDefinition::DB_USER_NAME;
    $db_pass = clsDefinition::DB_PASSWORD;
    $db_db = clsDefinition::DB_NAME;
    
    if ($_sess_db = mysql_connect($db_host, $db_user, $db_pass))
    {
        return mysql_select_db($db_db, $_sess_db);
    }
    
    return FALSE;
}

function _close()
{
    global $_sess_db;
    
    return mysql_close($_sess_db);
}

function _read($id)
{
    global $_sess_db;

    $id = mysql_real_escape_string($id);

    $sql = "SELECT data FROM sessions WHERE id = '$id'";

    if ($result = mysql_query($sql, $_sess_db))
    {
        if (mysql_num_rows($result))
        {
            $record = mysql_fetch_assoc($result);

            return $record['data'];
        }
    }

    return '';
}

function _write($id, $data)
{   
    global $_sess_db;

    $access = time();

    $id = mysql_real_escape_string($id);
    $access = mysql_real_escape_string($access);
    $data = mysql_real_escape_string($data);

    $sql = "REPLACE INTO sessions VALUES ('$id', '$access', '$data')";

    return mysql_query($sql, $_sess_db);
}

function _destroy($id)
{
    global $_sess_db;
    
    $id = mysql_real_escape_string($id);

    $sql = "DELETE FROM sessions WHERE id = '$id'";

    return mysql_query($sql, $_sess_db);
}

function _clean($max)
{
    global $_sess_db;
    
    $old = time() - $max;
    $old = mysql_real_escape_string($old);

    $sql = "DELETE FROM sessions WHERE access < '$old'";

    return mysql_query($sql, $_sess_db);
}

?>