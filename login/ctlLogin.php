<?php
/**
 *	ログイン画面の制御クラス
 *
 *	ログイン画面の制御について記述
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */

class ctlLogin{
	
	/**
	 * コンストラクタ
	 *
	 **/
	function __construct() {
	}
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){
		
		$objClsLogin  = new clsLogin();              //ロジッククラス
		$objChecker   = new clsLoginChecker($_REQUEST); //エラーチェッククラス
		$strErrorJson = '';                          //エラー情報のJSON文字列

		//メニュー画面リダイレクト用URL
		$strMenuUrl = clsDefinition::SYSTEM_DIR . '/admin/menu/Menu.php';
		
		//ログイン画面リダイレクト用URL
		$strLoginUrl = clsDefinition::SYSTEM_DIR . '/login/login.php';

		$aryPostData = clsCommonFunction::escapeHtml($_POST);

		$objDatabase = clsMillviDatabase::getInstance();

		$strAction    = @$_POST["action"];

		switch($strAction){
			
			//ログイン処理
			case 'login':
				//POSTデータのエラーチェック
				$blnCheckResult = $objChecker->execute();

				if($blnCheckResult == false){
					break;
				}
				
				$intContactId   = 0;
				$strMailAddress = $_POST["mail_address"];
				$strPassword    = $_POST["password"];
				
				//ログイン処理　成功時はセッションにログイン情報が登録される
				$blnLoginResult = $objClsLogin->login($intContactId, $strMailAddress, $strPassword);
				
				if($blnLoginResult){
					$objClsLogin->registLoginStatus();

					if( $_SESSION[clsDefinition::SESSION_REDIRECT] != "" ){
						$strRdirect = $_SESSION[clsDefinition::SESSION_REDIRECT];
						$_SESSION[clsDefinition::SESSION_REDIRECT] = "";
						header('Location: ' . $strRdirect);
					} else {
						//ログインに成功した場合はメニュー画面にリダイレクト
						header('Location: ' . $strMenuUrl);
					}

					exit();
				} else {
					//失敗した場合はエラーメッセージを登録
					$objChecker->setError();
					$objChecker->setErrorMessage("mail_address", "");
					$objChecker->setErrorMessage("password", "Eメールアドレスまたはパスワードが<br>正しくありません。");
				}
				break;

			//ログアウト処理
			case 'logout':
				//セッション情報を破棄する
				clsCommonFunction::logout();
				//ログアウトに成功した場合は自身の画面にリダイレクト
				header('Location: ' . $strLoginUrl);
				exit();
				break;

			//初期処理
			default:
				if($objClsLogin->isLogin()){
					//ログインしている場合はメニュー画面にリダイレクト
					header('Location: ' . $strMenuUrl);
					exit();
				}

				break;
		}

		//エラーがあった場合はエラー情報のJSON文字列を取得する
		if( $objChecker->isError() ){
			$strErrorJson = $objChecker->getErrorJson();
		}

		require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . '/login/dspLogin.php');
	}
}

