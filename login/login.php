<?php
/**
 *	ログイン画面
 *
 *	ログイン画面の制御クラスを呼び出す
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */
require_once( $_SERVER["DOCUMENT_ROOT"] . "/include.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/login/ctlLogin.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/login/clsLogin.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/value_checker/execute_classes/clsLoginChecker.php" );

//ログイン制御クラス呼出し
$objCtlLogin = new ctlLogin();
$objCtlLogin->process();


