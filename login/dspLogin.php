<?php
/**
 *	ログイン画面の画面表示
 *
 *	ログイン画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title><?php echo clsDefinition::SYSTEM_NAME?>ログイン</title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headUser.php"); ?>
<script type="text/javascript">
	$(document).ready(function(){

		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		// ログイン画面専用にエラーメッセージ表示変更
		objAlertError.setAlertPattern(function(aryErrorInfo){
			jQuery.each( aryErrorInfo, function( index, value ){
				// エラーCSSを追加する
				$('#'+value.errId).addClass('errorLine');
				// エラーメッセージを追加する
				if(value.errMsg){
					$("#"+value.errId).after("<p class='errInfo'>"+value.errMsg+"</p>");
				}
			});
		});
		if(objAlertError.hasError()){
			objAlertError.show(2);
		}

	});

</script>
</head>
<body id="login">
<header>
  <h1 class="logo2"></h1>
</header>
   <div class="form" style="width:450px; padding:30px;">


	<form name="loginForm" id="loginForm" method="post" action="login.php">
		<input type="hidden" name="action" value="login" />
		<input type="hidden" name="contact_id" value="<?php echo  @$intContactId ?>" />


    <table width="" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
            <p class="komoku">Eメールアドレス</p>
              <input type="text" id="mail_address" name="mail_address" size="20" maxlength="100" value="<?php echo @$aryPostData['mail_address'] ?>" style="ime-mode:disabled;" class="hissu fullWidth" />
              <label for="mail_address" class="over" style="text-indent: 0px;width:290px;">Eメールアドレス</label>
            </td>
          </tr>
          <tr>
            <td>
            <p class="komoku">パスワード</p>
              <input type="password" id="password" name="password" size="20" maxlength="50"  value="<?php echo @$aryPostData['password'] ?>" style="ime-mode:disabled;" class="hissu fullWidth" />
              <label for="password" class="over" style="text-indent: 0px;">パスワード</label>
            </td>
          </tr>
          <tr>
            <td>
            	<input type="submit" id="btn_login" name="btn_login" value="ログイン" class="basicBtn marT30 marB15 centerBlock " />
            </td>
          </tr>
    </table>
	</form>

</div>
<footer>
</footer>
</body>
</html>