//コンストラクタ
var dspLogin = function() {};

//ログイン処理
dspLogin.prototype.Login = function(contactId,intId,strPassword){
	
	var result = $.ajax({
		type     : "post",
		url      : "./Login.php",
		data     : { "action" : "errCheck", 
				   "contact_id" : contactId,
				   "login_id" : intId,
				   "password" : strPassword
		},
		dataType : "json",
		success  :  function(httpObj, dataType){
			var data = new Object();
			data     = httpObj;
			
			//エラー初期化
			$("div.errInfo").remove();
			$("input.errLine").remove();
			
			// エラー判断
			if( data.errFlg === true ){
				ModalDialog.error("認証エラー","ログインＩＤ・パスワードが正しくありません。<br />");
				jQuery.each( data.errInfo, function( index, value ){
					// エラーCSSを追加する
					$('#'+value.errId).addClass('errorLine');
					// エラーメッセージを追加する
					$("#"+value.errId).after("<div class='errInfo'>"+value.errMsg+"</div>");
				});
				window.scroll(0,0);
				
			// メニュー画面に遷移
			}else{
				$("#loginForm").attr("action", "../user/menu/Menu.php");
				$("#loginForm").submit();
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown){
			ModalDialog.error("通信エラー","通信エラーが発生しました。<br />");
			return false;
		}
	});
	
}