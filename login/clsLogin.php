<?php
/**
 *	ログイン画面のモデルクラス
 *
 *	ログイン画面のビジネスロジックについて記述
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */
class clsLogin{

	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct(){
	}

	/**
	 * ログインしているか判定 
	 * 
	 * @return boolean
	 */
	public function isLogin(){
		if(    strlen(clsLoginSession::getContactId()) > 0
			&& strlen(clsLoginSession::getUserId())> 0
		){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ログイン処理を行う
	 * 成功した場合は、セッションを初期化し、セッションにログイン情報を登録する 
	 * 
	 * @param int    $intContactId
	 * @param string $strMailAddress
	 * @param string $strPassword
	 * @return boolean
	 */
	public function login($intContactId,$strMailAddress,$strPassword){
		//入力されたID、パスワードで認証を試みる
		$aryLoginData   = $this->getLoginInfo($intContactId, $strMailAddress, $strPassword);
		if( !$aryLoginData ){
			return false;
		}
		//ユーザ権限取得
		$aryLoginData["auth_cd"] = $this->getUserAuth($intContactId,$aryLoginData["user_id"]);
		
		//契約会社情報を取得
		$aryContactData = $this->getContactInfo($intContactId);
		if( !$aryContactData){
			return false;
		}

		//メニュー情報取得
		$aryContactData["menu_id"] = $this->getMenuInfo($intContactId);
		
		clsLoginSession::init($aryLoginData);
		clsContactSession::init($aryContactData);
		return true;
	}


	/**
	 * ログイン処理を行う
	 * 成功した場合は、セッションを初期化し、セッションにログイン情報を登録する 
	 * 
	 * @param int $intContactId
	 * @param int $intUserId
	 * @return boolean
	 */
	public function updateLoginInfo(){

		if( !$this->isLogin() ){
			return false;
		}

		$intContactId = clsContactSession::getContactId();
		$intUserId    = clsLoginSession::getUserId();

		//入力されたID、パスワードで認証を試みる
		$aryLoginData   = $this->getLoginInfoById($intContactId, $intUserId);
		if( !$aryLoginData ){
			return false;
		}

		//ユーザ権限取得
		$aryLoginData["auth_cd"] = $this->getUserAuth($intContactId,$aryLoginData["user_id"]);
		
		//契約会社情報を取得
		$aryContactData = $this->getContactInfo($intContactId);
		if( !$aryContactData){
			return false;
		}

		//メニュー情報取得
		$aryContactData["menu_id"] = $this->getMenuInfo($intContactId);
		
		clsLoginSession::init($aryLoginData);
		clsContactSession::init($aryContactData);
		return true;
	}


	/**
	 * ログイン情報を取得する
	 * 
	 * @param int    $intContactId 契約ＩＤ
	 * @param string $strMailAddress ログインＩＤ
	 * @param string $strPassword パスワード
	 * @return ログイン情報　取得できない場合はfalse
	 *
	 **/
	public function getLoginInfo($intContactId,$strMailAddress,$strPassword){
		if(strlen($intContactId) == 0 || strlen($strMailAddress) == 0 || strlen($strPassword) == 0){
			return false;
		}
		
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$strGetLoginInfoSql =
<<<SQL

		select
			lm.contact_id,
			lm.user_id,
			lm.password,
			lm.name1,
			lm.name2,
			lm.kana1,
			lm.kana2,
			lm.roma1,
			lm.roma2,
			lm.nickname,
			lm.birthday,
			lm.sex,
			lm.zipcd,
			lm.pref_cd,
			lm.address1,
			lm.address2,
			lm.address3,
			lm.tel,
			lm.fax,
			lm.mail_address,
			lm.onetime_password,
			lm.reissue_date,
			lm.belong1,
			lm.belong2,
			lm.remarks1,
			lm.remarks2,
			lm.user_kbn,
			lm.group_cd,
			cm.contact_name,
			cm.encode_pc,
			cm.encode_smp,
			cm.templete,
			cm.fromAddr,
			cm.fromAddrName,
			cm.signature
		from inform_login_mst lm
		inner join contact_mst cm
		   on lm.contact_id   = cm.contact_id
		where lm.contact_id   = :contact_id
		  and lm.mail_address = :mail_address
		  and lm.password     = :password
		  and lm.delete_flg   = 0
SQL;
		
		$aryGetLoginInfoParameters = array(
			":contact_id"   => $intContactId,
			":mail_address" => $strMailAddress,
			":password"     => $strPassword
		);

		$aryResult = $objDatabase->pullDbData( $strGetLoginInfoSql, $aryGetLoginInfoParameters );
		if( $objDatabase->isError($aryResult) ){
			return false;
		}
		
		return $aryResult[0];
	}

	/**
	 * ログイン情報を取得する
	 * 
	 * @param int $intContactId 契約ＩＤ
	 * @param int $intUserId    ユーザＩＤ
	 * @return ログイン情報　取得できない場合はfalse
	 *
	 **/
	public function getLoginInfoById($intContactId, $intUserId){
		
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$strGetLoginInfoSql =
<<<SQL

		select
			lm.contact_id,
			lm.user_id,
			lm.password,
			lm.name1,
			lm.name2,
			lm.kana1,
			lm.kana2,
			lm.roma1,
			lm.roma2,
			lm.nickname,
			lm.birthday,
			lm.sex,
			lm.zipcd,
			lm.pref_cd,
			lm.address1,
			lm.address2,
			lm.address3,
			lm.tel,
			lm.fax,
			lm.mail_address,
			lm.onetime_password,
			lm.reissue_date,
			lm.belong1,
			lm.belong2,
			lm.remarks1,
			lm.remarks2,
			lm.user_kbn,
			lm.group_cd,
			cm.contact_name,
			cm.encode_pc,
			cm.encode_smp,
			cm.templete,
			cm.fromAddr,
			cm.fromAddrName,
			cm.signature
		from inform_login_mst lm
		inner join contact_mst cm 
		   on lm.contact_id   = cm.contact_id
		where lm.contact_id   = :contact_id
		  and lm.user_id      = :user_id
		  and lm.delete_flg   = 0
SQL;
		
		$aryGetLoginInfoParameters = array(
			":contact_id" => $intContactId,
			":user_id"    => $intUserId
		);

		$aryResult = $objDatabase->pullDbData( $strGetLoginInfoSql, $aryGetLoginInfoParameters );
		if( $objDatabase->isError($aryResult) ){
			return false;
		}
		
		return $aryResult[0];
	}

	/**
	 * アカウント権限を取得する
	 * 
	 * @param int    $intContactId 契約ＩＤ
	 * @param int    $intUserId ユーザＩＤ
	 *
	 * @return 契約情報　取得できない場合はfalse
	 *
	 **/
	public function getUserAuth($intContactId,$intUserId){
		$objDatabase = clsMillviDatabase::getInstance();
		
		//グループ権限優先
		$strSql = 
<<< SQL
			select gacm.auth_cd
			from inform_login_mst lm 
			inner join group_auth_config_mst gacm on lm.contact_id = gacm.contact_id
			and lm.group_cd = gacm.group_cd
			where lm.contact_id = :contact_id
			and lm.user_id = :user_id
SQL;
		
		$aryParameter = array( ":contact_id" => $intContactId,
								":user_id" => $intUserId );
		
		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameter );
		if( $objDatabase->isError($aryResult) ){
			return false;
		}
		
		//グループ権限がある場合は結果を返して、処理終了
		if(count($aryResult) > 0){
			return $aryResult;
		}
		
		//グループ権限がない場合はユーザ権限を返す
		$strSql = 
<<< SQL
			select auth_cd
			from user_auth_config_mst
			where contact_id = :contact_id
			and user_id = :user_id
SQL;
		$aryParameter = array( ":contact_id" => $intContactId,
								":user_id" => $intUserId);

		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameter );
		if( $objDatabase->isError($aryResult) ){
			return false;
		}
		
		if(count($aryResult) > 0){
			return $aryResult;
		}else{
			return array();
		}
	}
	
	/**
	 * 契約会社情報を取得する
	 * 
	 * @param int    $intContactId 契約ＩＤ
	 *
	 * @return 契約情報　取得できない場合はfalse
	 *
	 **/
	public function getContactInfo($intContactId){
		$objDatabase = clsMillviDatabase::getInstance();
		$strSql = 
<<< SQL
			SELECT *
			FROM contact_mst
			WHERE contact_id = :contact_id
SQL;
		$aryParameter = array( ":contact_id" => $intContactId );

		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameter );
		if( $objDatabase->isError($aryResult) ){
			return false;
		}
		
		return $aryResult[0];
	}
	
	/**
	 * 契約会社別のメニュー情報取得
	 * 
	 * @param int    $intContactId 契約ＩＤ
	 *
	 * @return メニュー情報　取得できない場合はfalse
	 *
	 **/
	public function getMenuInfo($intContactId){
		$objDatabase = clsMillviDatabase::getInstance();
		$strSql =
<<< SQL
			select menu_id
			from admin_menu_mst
			where contact_id = :contact_id
SQL;
		$aryParameter = array( ":contact_id" => $intContactId );

		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameter );
		if( $objDatabase->isError($aryResult) ){
			return false;
		}
		return $aryResult;
	}

	/**
	 * セッションキーを登録する
	 *
	 * @return boolean
	 */
	public function registLoginStatus(){

		$aryParameter = array(
			"contact_id" => clsContactSession::getContactId(),
			"user_id"    => clsLoginSession::getUserId(),
		);

		$strRandomSession = clsCommonFunction::makePass(30);

		$blnResult = false;
		$objDatabase = clsMillviDatabase::getInstance();
		$objDatabase->execTransaction();
		if(!clsCommonFunction::existsData("login_status_trn", $aryParameter) ){

			$blnResult = $this->insertLoginStatus($strRandomSession);
			if(!$blnResult){
				$objDatabase->execRollback();
				return false;
			}
			$blnResult = $this->insertLoginStatusPast(1);
			if(!$blnResult){
				$objDatabase->execRollback();
				return false;
			}
		} else {
			$blnResult = $this->updateLoginStatus($strRandomSession);
			if(!$blnResult){
				$objDatabase->execRollback();
				return false;
			}
			$blnResult = $this->insertLoginStatusPast(2);
			if(!$blnResult){
				$objDatabase->execRollback();
				return false;
			}
		}

		$objDatabase->execCommit();
		$_SESSION[clsDefinition::SESSION_RANDOM_SESSION]  = $strRandomSession;

		return true;
	}

	public function insertLoginStatus($strRandomSession){

		$objDatabase = clsMillviDatabase::getInstance();


		$strCurrentDate = clsCommonFunction::getCurrentDate();

		$aryParameter = array(
			"contact_id"     => clsContactSession::getContactId(),
			"user_id"        => clsLoginSession::getUserId(),
			"session_random" => $strRandomSession,
			"reg_id"         => clsLoginSession::getUserId(),
			"reg_name"       => clsLoginSession::getFullName(),
			"reg_time"       => $strCurrentDate,
			"reg_ip"         => $_SERVER["HTTP_X_FORWARDED_FOR"],
			"reg_user_agent" => $_SERVER["HTTP_USER_AGENT"],
			"upd_id"         => clsLoginSession::getUserId(),
			"upd_name"       => clsLoginSession::getFullName(),
			"upd_time"       => $strCurrentDate,
			"upd_ip"         => $_SERVER["HTTP_X_FORWARDED_FOR"],
			"upd_user_agent" => $_SERVER["HTTP_USER_AGENT"],
		);

		$strSql =
<<< SQL
			INSERT INTO login_status_trn (
				contact_id,
				user_id,
				session_random,
				reg_id,
				reg_name,
				reg_time,
				reg_ip,
				reg_user_agent,
				upd_id,
				upd_name,
				upd_time,
				upd_ip,
				upd_user_agent
			) VALUES (
				:contact_id,
				:user_id,
				:session_random,
				:reg_id,
				:reg_name,
				:reg_time,
				:reg_ip,
				:reg_user_agent,
				:upd_id,
				:upd_name,
				:upd_time,
				:upd_ip,
				:upd_user_agent
			);
SQL;

		$aryResult = $objDatabase->addDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return false;
		}

		return true;
	}

	public function updateLoginStatus($strRandomSession){

		$objDatabase = clsMillviDatabase::getInstance();

		$strCurrentDate = clsCommonFunction::getCurrentDate();

		$aryParameter = array(
			"contact_id"     => clsContactSession::getContactId(),
			"user_id"        => clsLoginSession::getUserId(),
			"session_random" => $strRandomSession,
			"upd_id"         => clsLoginSession::getUserId(),
			"upd_name"       => clsLoginSession::getFullName(),
			"upd_time"       => $strCurrentDate,
			"upd_ip"         => $_SERVER["HTTP_X_FORWARDED_FOR"],
			"upd_user_agent" => $_SERVER["HTTP_USER_AGENT"],
		);

		$strSql =
<<< SQL
			UPDATE login_status_trn
			SET
				session_random = :session_random,
				upd_id         = :upd_id,
				upd_name       = :upd_name,
				upd_time       = :upd_time,
				upd_ip         = :upd_ip,
				upd_user_agent = :upd_user_agent
			WHERE contact_id = :contact_id
			  AND user_id    = :user_id
SQL;

		$aryResult = $objDatabase->addDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return false;
		}

		return true;
	}

	/**
	 * ログインステータスの履歴を登録 
	 * 
	 * @param param
	 * @return return
	 */
	 public function insertLoginStatusPast($intUpdateKubun ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$intMoviePastNumber = clsCommonFunction::getNextId( "login_status_trn_past", "past_number" );
		
		$strSql =
<<< SQL
	INSERT INTO login_status_trn_past (
		SELECT
			:past_number,
			:upd_kbn,
			:past_reg_id,
			login_status_trn.*
		FROM
			login_status_trn
		WHERE
			contact_id = :contact_id AND
			user_id    = :user_id
	)
SQL;

		$aryParameter = array(
			":past_number" => $intMoviePastNumber,
			":upd_kbn"     => $intUpdateKubun,
			":past_reg_id" => clsLoginSession::getUserId(),
			":contact_id"  => clsContactSession::getContactId(),
			":user_id"     => clsLoginSession::getUserId()
		);

		$blnResult = $objDatabase->addDbData( $strSql, $aryParameter );

		if( $objDatabase->isError( $blnResult ) ){
			return false;
		}

		return true;
	}

	/**
	 * セッションの有効性を検証する
	 *
	 * @return boolean
	 */
	static function validateSession(){

		//現在のキーとDBのキーが一致する場合はtrue
		$aryParameter = array(
			"contact_id"     => clsContactSession::getContactId(),
			"user_id"        => clsLoginSession::getUserid(),
			"session_random" => $_SESSION[clsDefinition::SESSION_RANDOM_SESSION],
		);

		if( clsCommonFunction::existsData("login_status_trn", $aryParameter) ){
			return true;
		} else {
			return false;
		}
	}
}

