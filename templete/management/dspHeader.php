<?php
/**
 *	ヘッダー部表示
 *
 *	各画面共通のヘッダー部を記述
 *
 *	@author			Mouri 2012/01/21
 *	@version		1.0
 *  @update			Nambe 2013/02/14
 *					FAQメンテのリンク追加
 */
//セッションからログイン情報取り出し
if(isset($aryLogin) == false){
	$aryLogin = $_SESSION[clsDefinition::SESSION_LOGIN];
}
$top = clsDefinition::SYSTEM_DIR."/user/menu/menu.php?c=".base64_encode(clsContactSession::getContactId())."&m=1";
$manual = "https://".$_SERVER["HTTP_HOST"].clsDefinition::SYSTEM_DIR."/manual/manual_administrator.pdf";
$systeDir = clsDefinition::SYSTEM_DIR;
$systeName = clsDefinition::SYSTEM_NAME;

$aryAuthlist = array();
$aryMenulist = array();
foreach (clsLoginSession::getAuthCd() as $key) {
	$aryAuthlist[] = $key['auth_cd'];
}
foreach (clsContactSession::getMenuId() as $key) {
	$aryMenulist[] = $key['menu_id'];
}
?>
<header class="clearFix">

	<div class="left">
    <h1><?php echo $systeName ?></h1>
    <h2>管理者メニュー</h2>
  </div>
  
  <div class="right">
    <ul class="linkList">
      <li><a href="#" onclick="$('#logout_form').submit();"><img src="/dentsu/templete/default/img/btnLogout.png" alt="ログアウト" width="90" height="20"></a></li>
    </ul>
    <p class="userInfo"><?php echo clsLoginSession::getFullName() ?><img src="<?php echo $systeDir ?>/templete/management/img/iconUser.jpg" alt=""></p>
  </div>
  
  <form id="logout_form" method="POST" action="<?php echo clsDefinition::SYSTEM_DIR ?>/login/login.php">
	<input type="hidden" name="action" value="logout" />
  </form>

</header>

<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td id="sidebar">

<form name="menuForm" id="menuForm" method="post">
<!--  <ul id="accordion" class="gNavi" >
    <li><span class="wrap link"><a id="menu_dashborad" href="#">ダッシュボード</a></span></li>
    <li><span class="wrap">基本情報メンテナンス<span class="arrow"></span></span>
		<ul>
	        <li><a id="menu_basicInfo_entry" href="#">基本情報登録</a></li>
			<li><a id="menu_basicInfo_list" href="#">基本情報一覧・編集</a></li>
	    </ul>
	</li>
  </ul>-->
  <ul id="accordion" class="gNavi" >
    <li><span class="wrap link"><a id="menu_dashborad" href="#">ダッシュボード</a></span></li>
    <li><span class="wrap">基本情報メンテナンス<span class="arrow"></span></span>
      <ul>
        <li><a id="menu_basicInfo_entry" href="#">基本情報登録</a></li>
        <li><a id="menu_basicInfo_list" href="#">基本情報一覧・編集</a></li>
        <li><a id="menu_basicInfo_csv" href="#">基本情報CSV出力</a></li>
      </ul>
    </li>
    <li><span class="wrap">お知らせ管理<span class="arrow"></span></span>
      <ul>
        <li><a id="menu_informationRegist" href="#">お知らせ登録</a></li>
        <li><a id="menu_informationList" href="#">お知らせ一覧・編集</a></li>
      </ul>
    </li>
    <li><span class="wrap link"><a id="menu_faqList" href="#">FAQメンテナンス</a></span></li>
	<li><span class="wrap link"><a id="menu_inquiryList" href="#">お問い合わせ管理</a></span></li>
	<li><span class="wrap link"><a id="menu_playInfoList" href="#">再生情報一覧</a></span></li>
  </ul>
  <input type="hidden" name="systemDir" id="systemDir" value='<?php echo clsDefinition::SYSTEM_DIR ?>' ?>
</form>
  
  
  <br />
  <br />
  <br />

	</td>
    <td id="contents">