$(function() {

	var id = $("body").attr("id");
	if(id == "dashborad" || id == "basicInfo" || id == "inquiryList" || id == "playInfoList"){
		$("a#menu_"+id).parent().addClass("active");
	}else{
		$("a#menu_"+id).parent().parent().show();
		$("a#menu_"+id).addClass("active");
	}



	$("#accordion > li > span").click(function(){
	 var click = $("+ul",this);
		if(click.css("display") == "none" ){
			 click.slideDown();
			 $(".arrow",this).addClass("rotate");
		}else{
			 click.slideUp();
			 $(".arrow",this).removeClass("rotate");
		}
		 return false;
	});

	//メニュー内のリンククリック時
	//動画登録
	$("#accordion a").click(function() {
		id = $(this).attr("id");
		url = "";
		
		//画面ごとに遷移先設定
		switch(id){
			// ダッシュボード
			case "menu_dashborad":
				url = $("#systemDir").val()+"/admin/menu/Menu.php";
				break;
			// 基本情報登録
			case "menu_basicInfo_entry":
				url = $("#systemDir").val()+"/admin/basicinfo/basicinfoentry/basicInfo.php";
				break;
			// 基本情報一覧・編集
			case "menu_basicInfo_list":
				url = $("#systemDir").val()+"/admin/basicinfo/basicinfolist/basicInfoList.php";
				break;
			// 基本情報CSV出力
			case "menu_basicInfo_csv":
				url = $("#systemDir").val()+"/admin/basicinfo/basicinfocsvoutput/basicInfoCsvOutput.php";
				break;
			// お知らせ登録
			case "menu_informationRegist":
				url = $("#systemDir").val()+"/admin/information/informationEntry/informationEntry.php";
				break;
			// お知らせ一覧・編集
			case "menu_informationList":
				url = $("#systemDir").val()+"/admin/information/informationList/informationList.php";
				break;
			// FAQメンテナンス
			case "menu_faqList":
				url = $("#systemDir").val()+"/admin/faq/faqMainte.php";
				break;
			// お問い合わせ管理
			case "menu_inquiryList":
				url = $("#systemDir").val()+"/admin/inquiry/inquiry.php";
				break;
			case "menu_playInfoList":
				url = $("#systemDir").val()+"/admin/playInfo/playInfoList.php";
				break;
		}
		
		$("#menuForm").attr('action', url);
		$("#menuForm").submit();
		
	});
});
