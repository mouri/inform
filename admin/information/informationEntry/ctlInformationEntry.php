<?php
/**
 *	お知らせ登録画面の制御
 *
 *	お知らせ登録画面の制御について記述
 *
 *	@author			Mouri 2012/02/18
 *
 *	@update			Nambe 2013/03/06
 *					お知らせカテゴリー、お知らせタイトル、お知らせURL、ファイルアップロード追加。
 *	@version		1.0
 */
class ctlInformationEntry{
	
	/**
	 * コンストラクタ
	 *
	**/
	function __construct() {
	}
	
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){
		
		$objChecker   = new clsAdminInformationEntryChecker($_REQUEST);	//エラーチェッククラス

		$strErrorJson = '';												//エラー情報のJSON文字列
		
		$strAction = $_POST["action"];
		
		//ＰＯＳＴの値で画面配列更新
		foreach ($_POST as $key => $val){
			$aryDisp[$key] = $_POST[$key];
		}

		$objInformationEntry = new clsInformationEntry($aryDisp);

		//会社名選択リスト
		$strContactSelectBox = $objInformationEntry->getContactList($aryDisp["contact_id"]);

		switch($strAction){
			//登録
			case "regist":

				//POSTデータのエラーチェック
				$blnCheckResult = $objChecker->execute();
				
				if($blnCheckResult == false){
					//エラーがあった場合はエラー情報のJSON文字列を取得する
					if( $objChecker->isError() ){
						$strErrorJson = $objChecker->getErrorJson();
					}

					if( 1 == $aryDisp["editMode"] ){
						require_once 'dspInformationUpdate.php';
					}else{
						require_once 'dspInformationEntry.php';
					}
					
					break;
				}else{
					if($aryDisp["editMode"] == 1){

						$blnResult = $objInformationEntry->updateInformation();
						// 検索条件をセッションに格納する
						$_SESSION[clsDefinition::SESSION_ADMIN_INFORMATION_LIST] = array(
							"pagerNumber" => $aryDisp["pagerNumber"]
						);
					}else{
						$blnResult = $objInformationEntry->registInformation();
					}

					header("Location: ".clsDefinition::SYSTEM_DIR."/admin/information/informationEntry/dspInformationComplete.php?editMode=".$aryDisp["editMode"]); 
				}
				
				break;
			//修正
			case "edit":
				$aryResultList = $objInformationEntry->getInformation();

				//データが存在していない場合
				if(count($aryResultList) == 0){
					require_once 'dspInformationNothing.php';
				}else{
					$aryDisp = $aryResultList;
					$aryDisp["select_information_id"] = $aryResultList["information_id"];
					$aryDisp["select_contact_id"] = $aryResultList["contact_id"];
					$aryDisp["editMode"]              = 1;

					require_once 'dspInformationUpdate.php';
				}
				break;

			//初期表示
			default:
				require_once 'dspInformationEntry.php';
				break;
		}
		
		
	}
}


?>