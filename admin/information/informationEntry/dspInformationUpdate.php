<?php
/**
 *	お知らせ登録画面の画面表示
 *
 *	お知らせ登録画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2012/02/18
 *					Nambe 2013/03/05 タイトル、URLリンク、ファイルアップロードの追加。
 *					Nishi 2013/03/12 デザイン修正
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　お知らせ一覧・編集</title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="./js/dspInformationEntry.js"></script>
<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
<script src="https://0-oo.googlecode.com/svn/gcalendar-holidays.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//画面ＪＳ
		var objDspInformationEntry = new dspInformationEntry();
		//エラー関連ＪＳ
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if(objAlertError.hasError()){
			objAlertError.show();
		}
		
		//登録ボタンクリック時
		$("#btn_regist").click(function(){
			//条件設定
			objDspInformationEntry.Regist();
		});
		
		//戻るボタンクリック時
		$("#btn_back").click(function(){
			//戻る設定
			objDspInformationEntry.Back();
		});

	});
</script>
</head>
<body id="informationList">
<?php echo clsCommonFunction::dispHeaderManegement(); ?>
<form name="informationEntryForm" id="informationEntryForm" enctype="multipart/form-data" method="post">
<div class="boxBeige marB20">
<h2>お知らせ編集</h2>
<div class="form">
	<table>
		<tr>
			<td colspan="2" class="c1 fullWidth">
				<p class="komoku">契約会社名</p>
				<?php echo $aryDisp["contact_name"] ?>
				<input type="hidden" id="contact_name" name="contact_name" value="<?php echo $aryDisp["contact_name"] ?>" />
				<input type="hidden" id="contact_id" name="contact_id" value="<?php echo $aryDisp["contact_id"] ?>" />
			</td>
			<td class="message"></td>
		</tr>
		<tr>
			<td colspan="2" class="c1 fullWidth">
				<p class="komoku">お知らせ本文</p>
				<textarea class="hissu" id="information_text" name="information_text" cols="40" rows="10"><?php echo $aryDisp["information_text"] ?></textarea>
			</td>
			<td class="message"></td>
		</tr>
		<tr>
			<td class="c1">
				<input type="button" name="btn_back" id="btn_back" value="一覧画面へ" class="basicBtn centerBlock gray" />
			</td>
			<td class="c2">
				<input type="button" name="btn_regist" id="btn_regist" value="編集内容を保存" class="basicBtn centerBlock" />
			</td>
			<td class="message"></td>
		</tr>
	</table>
	
	<input type="hidden" name="action" id="action" />
	<input type="hidden" name="editMode" id="editMode" value="<?php echo $aryDisp["editMode"] ?>" />
	<input type="hidden" name="select_contact_id" value="<?php echo $aryDisp["select_contact_id"] ?>" />
	<input type="hidden" name="select_information_id" value="<?php echo $aryDisp["select_information_id"] ?>" />
	<input type="hidden" name="pagerNumber" value="<?php echo $_POST["pagerNumber"] ?>" ?>
</div>
</div>
</form>
<!--<form action="dspFileDownLoad.php" method="POST" id="file_download_form">
	<input type="hidden" name="file_down_information_id" value="<?php echo $aryDisp["select_information_id"]; ?>" />
</form>-->
<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
