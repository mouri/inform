<?php
	/**
	 * お知らせでアップロードされたファイルを出力する
	 *
	 * @author 2013/03/06 Nambe
	 * 
	 **/
	
	require_once( $_SERVER["DOCUMENT_ROOT"] . "/include.php" );
	require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/information/informationEntry/clsInformationEntry.php" );

	$aryPostData["select_information_id"] = $_POST["file_down_information_id"];

	if( false == is_null($aryPostData["select_information_id"])){
		$objInformationEntry = new clsInformationEntry($aryPostData);
		$aryDispData = $objInformationEntry->getInformation();
	}

	// データが取得できれば、ファイル出力処理
	if( true == is_array($aryDispData) ){
		// DLするファイル名を設定
		$strExtension = pathinfo($aryDispData["file_path"],PATHINFO_EXTENSION);
		$strFileName  = pathinfo($aryDispData["file_path"],PATHINFO_FILENAME) . ".{$strExtension}";

header('Content-Description: File Transfer');
header("Content-Type: " . $aryDispData["file_type"]);
header('Content-Disposition: attachment; filename=' . $strFileName );
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($aryDispData["file_path"]));

readfile($aryDispData["file_path"]);
	}else{
		header("HTTP/1.1 404 Not Found");
	}


?>