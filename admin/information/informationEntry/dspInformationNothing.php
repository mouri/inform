<?php
/**
 *	お知らせ登録画面の画面表示
 *
 *	お知らせ登録画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　お知らせ登録</title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="./js/dspInformationEntry.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		//画面ＪＳ
		var objDspInformationEntry = new dspInformationEntry();
		//戻るボタンクリック時
		$("#btn_back").click(function(){
			//戻る設定
			objDspInformationEntry.Back();
		});
		
	});
</script>
</head>
<body id="informationRegist">
<?php echo clsCommonFunction::dispHeaderManegement(); ?>
<form name="informationEntryForm" id="informationEntryForm" method="post">
	<div>データが削除されています。</div>
	
	<input type="button" name="btn_back" id="btn_back" value="戻る" />
	<input type="hidden" name="action" id="action" />
	<input type="hidden" name="editMode" id="editMode" value="<?php echo $aryDisp["editMode"] ?>" />
	<input type="hidden" name="select_information_id" value="<?php echo $aryDisp["select_information_id"] ?>" />
</form>
<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
