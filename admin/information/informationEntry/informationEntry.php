<?php
/**
 *	お知らせ登録画面
 *
 *	お知らせ登録画面の制御クラスを呼び出す
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/include.php";
require_once "./ctlInformationEntry.php";
require_once "./clsInformationEntry.php";
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/value_checker/execute_classes/clsAdminInformationEntryChecker.php" );



//配信メール一覧制御クラス呼出し
$objCtlInformationEntry = new ctlInformationEntry();
$objCtlInformationEntry->process();
?>