<?php
/**
 *	お知らせ登録画面のモデルクラス
 *
 *	お知らせ登録画面のビジネスロジックについて記述
 *
 *	@author			Nishi 2013/04/04
 *	@version		1.0
 */
class clsInformationEntry{

	/**
	 * お知らせの記事区分を確定する定数（[information_category_mst].[management_kbn]）
	 * 1：システム（inform)からのお知らせ
	 * 2：管理者（DOUPAを提供している会社）からのお知らせ
	 * 3：ファイルアップロード
	 **/
	const MANAGEMENT_KBN = 1;

	/**
	 * アップロードされたファイル変数を格納する変数
	 **/
	private $_aryFile;

	/**
	 * ポストされた値を格納する変数
	 **/
	private $_aryDisp;
	
	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct($aryDisp){
		$aryFile = $_FILES;
		$this->_aryDisp = $aryDisp;
		$this->_aryFile = array_shift($aryFile);
	}
	
	/**
	 * お知らせ内容を取得します。
	 * 
	 * @return true:成功、false:失敗
	 */
	public function getInformation(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$contact_id = $this->_aryDisp["select_contact_id"];
		$delete_flg = 0;
		$information_id = $this->_aryDisp["select_information_id"];
		
		$strSelectSql =
<<<SQL
		select 
			trn.information_id,
			trn.management_kbn,
			trn.information_text,
			trn.reg_time,
			trn.reg_name,
			trn.upd_time,
			trn.upd_name,
			trn.contact_id,
			cm.contact_name
		from information_trn trn
		left join contact_mst cm
		on trn.contact_id = cm.contact_id
 		where trn.contact_id = :contact_id
		and trn.delete_flg = :delete_flg
		and trn.information_id = :information_id
		and trn.management_kbn = :management_kbn
SQL;
		$aryParameters = array(
			":contact_id"			=>	$contact_id,
			":delete_flg"			=>	$delete_flg,
			":information_id"		=>	$information_id,
			":management_kbn"		=>	self::MANAGEMENT_KBN
		);
		
		$aryResult = $objDatabase->pullDbData($strSelectSql , $aryParameters);
		
		if( $objDatabase->isError($aryResult) ){
			return false;
		}
		
		return $aryResult[0];
	}

	/**
	 * お知らせ内容を更新します。
	 * 
	 * @return true:成功、false:失敗
	 */
	public function updateInformation(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		// 更新処理をかける前のデータを取得
		$aryPreUpdData = $this->getInformation();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );


		//トランザクション開始
		$objDatabase->execTransaction();
		
		$contact_id          = $this->_aryDisp["select_contact_id"];
		$management_kbn      = self::MANAGEMENT_KBN;
		$information_id      = $this->_aryDisp["select_information_id"];
		$information_text    = $this->_aryDisp["information_text"];

		$upd_id = clsLoginSession::getUserId();
		$upd_name = clsLoginSession::getFullName();
		$upd_time = clsCommonFunction::getCurrentDate();
		
		$strUpdateSql =
<<<SQL
		UPDATE information_trn
		   SET information_text  = :information_text,
			   upd_id            = :upd_id,
			   upd_name          = :upd_name,
			   upd_time          = :upd_time
		 WHERE contact_id        = :contact_id
		   AND management_kbn    = :management_kbn
		   AND information_id    = :information_id
SQL;

		$aryParameters = array(
			":information_text"		=>	$information_text,
			":upd_id"				=>	$upd_id,
			":upd_name"				=>	$upd_name,
			":upd_time"				=>	$upd_time,
			":contact_id"			=>	$contact_id,
			":management_kbn"		=>	$management_kbn,
			":information_id"		=>	$information_id
		);
		
		$aryResult = $objDatabase->updDbData( $strUpdateSql, $aryParameters );
		if( $objDatabase->isError($aryResult) ){

			//ロールバック
			$objDatabase->execRollback();
			return false;
		}


		//$past_number = clsCommonFunction::getNextId("information_trn_past", "past_number");
		$upd_kbn = 2;

		$strInsertSql =
<<<SQL
		INSERT INTO information_trn_past
		     SELECT ( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM information_trn_past WHERE contact_id = :contact_id ) AS past_number,
					:upd_kbn,
					:past_reg_id,
					information_trn.*
			   FROM information_trn
			  WHERE contact_id = :contact_id
				AND management_kbn = :management_kbn
				AND information_id = :information_id
SQL;
		$aryParameters = array(
			//":past_number"			=>	$past_number,
			":upd_kbn"				=>	$upd_kbn,
			":past_reg_id"			=>	clsLoginSession::getUserId(),
			":contact_id"			=>	$contact_id,
			":management_kbn"		=>	$management_kbn,
			":information_id"		=>	$information_id
		);
		
		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameters);
		
		if($objDatabase->isError($aryResult)){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//コミット
		$objDatabase->execCommit();
		
	}

	/**
	 * お知らせ内容を登録します
	 *
	 * @update Nambe 2013/03/05
	 * 				 お知らせURL、ファイル情報等のカラム追加
	 * 
	 * @return true:成功、false:失敗
	 */
	public function registInformation(){
		// ファイルをアップロードした結果情報を変数に格納

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		//トランザクション開始
		$objDatabase->execTransaction();

		$contact_id     = $this->_aryDisp["contact_id"];
		$management_kbn = self::MANAGEMENT_KBN;


		$strSelectSql =
<<<SQL
	SELECT
		COALESCE( MAX( information_id ), 0 ) + 1 AS information_id
	FROM
		information_trn
	WHERE
		contact_id = :contact_id
SQL;
		$aryParameters = array(
			":contact_id"			=>	$contact_id
		);

		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		$information_id = $aryResult[0]["information_id"];

		//$information_id      = clsCommonFunction::getNextId("information_trn", "information_id");
		$strInformationTitle = 'システムからのお知らせ';
		$information_text    = $this->_aryDisp["information_text"];

		$delete_flg = 0;
		
		$reg_id = clsLoginSession::getUserId();
		$reg_name = clsLoginSession::getFullName();
		$reg_time = clsCommonFunction::getCurrentDate();
		
		$upd_id = clsLoginSession::getUserId();
		$upd_name = clsLoginSession::getFullName();
		$upd_time = clsCommonFunction::getCurrentDate();

		$strInsertSql =
<<<SQL
INSERT INTO information_trn(
	contact_id,
	management_kbn,
	information_id,
	information_title,
	information_text,
	delete_flg,
	reg_id,
	reg_name,
	reg_time,
	upd_id,
	upd_name,
	upd_time)
VALUES(
	:contact_id,
	:management_kbn,
	:information_id,
	:information_title,
	:information_text,
	:delete_flg,
	:reg_id,
	:reg_name,
	:reg_time,
	:upd_id,
	:upd_name,
	:upd_time
);
SQL;
		
		$aryParameters = array(
			":contact_id"			=>	$contact_id,
			":management_kbn"		=>	$management_kbn,
			":information_id"		=>	$information_id,
			":information_title"	=>	$strInformationTitle,
			":information_text"		=>	$information_text,
			":delete_flg"			=>	$delete_flg,
			":reg_id"				=>	$reg_id,
			":reg_name"				=>	$reg_name,
			":reg_time"				=>	$reg_time,
			":upd_id"				=>	$upd_id,
			":upd_name"				=>	$upd_name,
			":upd_time"				=>	$upd_time,
		);


		$aryResult = $objDatabase->addDbData( $strInsertSql, $aryParameters );
		if( $objDatabase->isError($aryResult) ){

			//ロールバック
			$objDatabase->execRollback();

			return false;
		}


		//$past_number = clsCommonFunction::getNextId("information_trn_past", "past_number");
		$past_reg_id = clsLoginSession::getUserId();
		$upd_kbn = 1;
		
		$strInsertSql =
<<<SQL
		insert into information_trn_past
		select
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM information_trn_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			:past_reg_id,
			information_trn.*
		from information_trn
		where contact_id = :contact_id
		and management_kbn = :management_kbn
		and information_id = :information_id
SQL;
		$aryParameters = array(
			//":past_number"			=>	$past_number,
			":past_reg_id"			=>	$past_reg_id,
			":upd_kbn"				=>	$upd_kbn,
			":contact_id"			=>	$contact_id,
			":management_kbn"		=>	$management_kbn,
			":information_id"		=>	$information_id
		);
		
		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameters);
		
		if($objDatabase->isError($aryResult)){
			//ロールバック
			$objDatabase->execRollback();
echo "パスト失敗";
exit();
			return false;
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;

	}


	/**
	 * 契約会社リスト生成 
	 * 
	 * @param param
	 * @return return
	 */
	public function getContactList($intContact){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSelectSql =
<<<SQL
		SELECT 
			contact_id,
			contact_name
		FROM
			contact_mst
		WHERE
			delete_flg = :delete_flg
SQL;
		$aryParameters = array(
			":delete_flg"	=>	0,
		);
		
		$aryResult = $objDatabase->pullDbData($strSelectSql , $aryParameters);
		

		if( $objDatabase->isError($aryResult) ){
			return false;
		}

		$strContactSelectBox = "<option value='' " . clsCommonFunction::chkSelectedDate( $intContact, '' ) . " ></option>";
		foreach( $aryResult as $key => $val ){
			$strContactSelectBox .= "<option value=" . $val["contact_id"] . " " . clsCommonFunction::chkSelectedDate( $intContact, $val["contact_id"] ) . " >" . $val["contact_name"] . "</option>";
		}

		return $strContactSelectBox;

	}


}
?>