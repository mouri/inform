<?php
/**
 *	お知らせ(修正）画面（完了）の画面表示
 *
 *	お知らせ(修正）画面（完了）のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2012/01/16
 *					Nishi 2013/03/12 デザイン修正
 *	@version		1.0
 */
 require_once $_SERVER["DOCUMENT_ROOT"]."/include.php";
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　お知らせ<?php echo $_GET["editMode"] == "1" ? "一覧・編集" : "登録" ?></title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="./js/dspMailEntry.js"></script>

</head>
<?php if($_GET["editMode"] == "1"){ ?>
<body id="informationList">
<?php }else{ ?>
<body id="informationRegist">
<?php } ?>

<?php echo clsCommonFunction::dispHeaderManegement(); ?>

<div align="center" style="margin-top:50px">
	<?php echo clsDefinition::SYSTEM_NAME ?>　お知らせ<?php echo $_GET["editMode"] == "1" ? "編集" : "登録" ?>完了
</div>

<div align="center">
	お知らせ<?php echo $_GET["editMode"] == "1" ? "編集" : "登録" ?>しました。
</div>

<?php if($_GET["editMode"] == "1"){ ?>
	<div align="center">
		<a href="../informationList/informationList.php?bk=1">一覧に戻る</a>
	</div>
<?php } ?>

<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
