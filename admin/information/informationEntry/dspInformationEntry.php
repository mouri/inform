<?php
/**
 *	お知らせ登録画面の画面表示
 *
 *	お知らせ登録画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Nishi 2013/04/04
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　お知らせ登録</title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="./js/dspInformationEntry.js"></script>
<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
<script src="https://0-oo.googlecode.com/svn/gcalendar-holidays.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//画面ＪＳ
		var objDspInformationEntry = new dspInformationEntry();
		//エラー関連ＪＳ
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if(objAlertError.hasError()){
			objAlertError.show();
		}

		//登録ボタンクリック時
		$("#btn_regist").click(function(){
			//条件設定
			objDspInformationEntry.Regist();
		});
		
		//戻るボタンクリック時
		$("#btn_back").click(function(){
			//戻る設定
			objDspInformationEntry.Back();
		});
		

	});
</script>
</head>
<body id="informationRegist">
<?php echo clsCommonFunction::dispHeaderManegement(); ?>
<form name="informationEntryForm" id="informationEntryForm" enctype="multipart/form-data" method="post">
<div class="boxBeige marB20">
<h2>お知らせ登録</h2>
<div class="form">
<table>
	<tr>
		<td class="c1 fullWidth">
			<p class="komoku">契約会社名</p>
			<select class="hissu" id="contact_id" name="contact_id">
			<?php echo $strContactSelectBox; ?>
			</select>
		</td>
		<td class="message"></td>
	</tr>
	<tr>
		<td class="c1 fullWidth">
			<p class="komoku">お知らせ本文</p>
			<textarea class="hissu" id="information_text" name="information_text" cols="40" rows="10"><?php echo $aryDisp["information_text"] ?></textarea>
		</td>
		<td class="message"></td>
	</tr>
	<tr>
		<td class="c1">
			<input type="button" name="btn_regist" id="btn_regist" value="登録" class="basicBtn centerBlock" />
		</td>
		<td class="message"></td>
	</tr>
</table>
	<input type="hidden" name="action" id="action" />
	<input type="hidden" name="editMode" id="editMode" value="<?php echo $aryDisp["editMode"] ?>" />
	<input type="hidden" name="select_information_id" value="<?php echo $aryDisp["select_information_id"] ?>" />
</div>
</div>
</form>
<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
