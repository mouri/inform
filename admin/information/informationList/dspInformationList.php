<?php
/**
 *	お知らせ一覧画面の画面表示
 *
 *	お知らせ一覧画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Nishi 2013/04/04
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　お知らせ一覧・編集</title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="./js/dspInformationList.js"></script>
<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//画面ＪＳ
		var objDspInformationList = new dspInformationList();
		//エラー関連ＪＳ
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if(objAlertError.hasError()){
			objAlertError.show();
		}
		
		//削除ボタンクリック時
		$("input[name='btn_del']").click(function(){
			//選択した行の行番号取得
			row = $(this).attr("id");
			row = row.replace("btn_del","");
			
			objDspInformationList.Delete(row);
		});
		
		//修正ボタンクリック時
		$("input[name='btn_edit']").click(function(){
			//選択した行の行番号取得
			row = $(this).attr("id");
			row = row.replace("btn_edit","");
			
			objDspInformationList.Edit(row);
			
		});
		
		// 検索ボタンクリック時
		$("#search_btn").click(function(){
			// 条件設定
			objDspInformationList.Search();
		});
		
	});
</script>
</head>
<body id="informationList">
<?php echo clsCommonFunction::dispHeaderManegement(); ?>
<form name="informationListForm" id="informationListForm" method="post">
	<div class="boxBeige">
		<h2>お知らせ一覧・編集</h2>
		<div class="form">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth">
						<p class="komoku">契約会社名</p>
						<select name="search_contact_id" id="search_contact_id">
							<?php echo $strContactSelectBox ?>
						</select>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td>
						<div class="basicBtn search">
							<input type="button" name="search_btn" id="search_btn" value="検索" />
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
		<tr>
			<th colspan="4" class="pager top"><?php echo $objPager->get(); ?></th>
		</tr>
		<tr>
			<th width="150px">契約ID</th>
			<th>契約会社名</th>
			<th width="180px">更新情報</th>
			<th width="100px">編集</th>
		</tr>
		<?php
			$iCnt = 1;
			foreach($aryResultList as $key => $val){ 
		?>
			<tr>
				<td align="center">
					<?php echo $val['contact_id'] . "：" . base64_encode( $val['contact_id'] )?>
				</td>
				<td>
					<?php echo f::br($val["contact_name"]) ?>
				</td>
				<td>
					登録者：<?php echo $val["reg_name"] ?><br/><?php echo $val["reg_time"] ?><!--<br/>
					更新者：<?php echo $val["upd_name"] ?><br/><?php echo $val["upd_time"] ?>-->
				</td>
				<td>
					<div class="basicBtn edit w40"><input type="button" name="btn_edit" id="btn_edit<?php echo $iCnt?>" value="編集"></div>
					<div class="basicBtn del w40"><input type="button" name="btn_del" id="btn_del<?php echo $iCnt?>" value="削除"></div>
				</td>
				<input type="hidden" id="information_id<?php echo $iCnt?>" name="information_id<?php echo $iCnt?>" value="<?php echo $val["information_id"] ?>" />
				<input type="hidden" id="contact_id<?php echo $iCnt?>" name="contact_id<?php echo $iCnt?>" value="<?php echo $val["contact_id"] ?>" />
				<input type="hidden" name="information_title<?php echo $iCnt?>" id="information_title<?php echo $iCnt?>" value="<?php echo f::br($val["information_title"]) ?>" />
			</tr>
		<?php
			$iCnt++;
			}
		?>
		<tr>
			<th colspan="4" class="pager top"><?php echo $objPager->get(); ?></th>
		</tr>
	</table>

	<input type="hidden" name="action" id="action" />
	<input type="hidden" name="select_information_id" id="select_information_id" />
	<input type="hidden" name="select_contact_id" id="select_contact_id" />
	<input type="hidden" name="editMode" id="editMode" />
	<input type="hidden" name="pagerNumber" value="<?php echo $_POST["pagerNumber"] ?>" ?>
</form>
<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
