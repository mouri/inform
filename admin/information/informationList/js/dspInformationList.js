//コンストラクタ
var dspInformationList = function() {};

//修正処理
dspInformationList.prototype.Edit = function(){
	$("#select_information_id").val($("#information_id"+row).val());
	$("#select_contact_id").val($("#contact_id"+row).val());
	$("#editMode").val("1");
	$("#action").val("edit");
	$("#informationListForm").attr("action", "../informationEntry/informationEntry.php");
	$("#informationListForm").submit();
}
//削除処理
dspInformationList.prototype.Delete = function(){
	$("#select_information_id").val($("#information_id"+row).val());
	$("#select_contact_id").val($("#contact_id"+row).val());
	var strMessage = $("#information_title"+row).val();
	//ModalDialog.confirm("", "以下のお知らせを削除してもよろしいですか？<br/>お知らせタイトル：" + strMessage, 
	ModalDialog.confirm("", "お知らせを削除してもよろしいですか？", 
		function(blnResult){
			if(blnResult){
				$("#action").val("delete");
				$("#informationListForm").attr("action", "./informationList.php");
				$("#informationListForm").submit();
			}
		}
	);
	
}

// 検索処理
dspInformationList.prototype.Search = function(){
	$("#action").val( "search" );
	$("#informationListForm").attr("action", "./informationList.php");
	$("#informationListForm").submit();
}


