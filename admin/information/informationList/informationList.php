<?php
/**
 *	お知らせ一覧画面
 *
 *	お知らせ一覧画面の制御クラスを呼び出す
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/include.php";
require_once "./ctlInformationList.php";
require_once "./clsInformationList.php";
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/value_checker/execute_classes/clsAdminInformationListChecker.php" );

//配信メール一覧制御クラス呼出し
$objCtlInformationList = new ctlInformationList();
$objCtlInformationList->process();
?>