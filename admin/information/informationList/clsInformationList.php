<?php
/**
 *	お知らせ一覧画面のモデルクラス
 *
 *	お知らせ一覧画面のビジネスロジックについて記述
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 */
class clsInformationList{
	/**
	 * お知らせの記事区分を確定する定数（[information_category_mst].[management_kbn]）
	 * 1：システム（inform)からのお知らせ
	 * 2：管理者（DOUPAを提供している会社）からのお知らせ
	 * 3：ファイルアップロード
	 **/
	const MANAGEMENT_KBN = 1;

	
	private $_aryDisp;
	
	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct($aryDisp){
		$this->_aryDisp = $aryDisp;
	}
	
	/**
	 * お知らせ情報の件数を取得します
	 * 
	 * @return 取得件数
	 */
	public function getInformationListCnt(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//$contact_id = clsContactSession::getContactId();
		//絞り込み条件のときcontact_idを設定する。
		
		$strWhereSet = '';
		
		if( '' != $this->_aryDisp["search_contact_id"]){
			$strWhereSet = "AND trn.contact_id = " . $this->_aryDisp["search_contact_id"];
		}
		
		$strFilterContactId = "";
		
		$strSelectSql =
<<<SQL
	select
		trn.contact_id,
		trn.information_id,
		trn.information_text,
		trn.reg_time,
		trn.reg_name,
		trn.upd_time,
		trn.upd_name
	from
		information_trn AS trn
			INNER JOIN (
				SELECT
					contact_id
				FROM
					contact_mst
				WHERE
					delete_flg = :delete_flg
			) AS cm
		ON trn.contact_id = cm.contact_id
	where
		management_kbn = :management_kbn and
		delete_flg     = :delete_flg
	order by
		reg_time desc
SQL;
		$aryParameters = array(
			":management_kbn" => self::MANAGEMENT_KBN,
			":delete_flg"     => 0
		);
		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError($aryResult) ){
			return false;
		}
		
		return count($aryResult);
	}
	
	/**
	 * お知らせ情報を取得します
	 * 
	 * @param $limit リミット(上限）
	 * @param $offset オフセット（開始位置）
	 * @return お知らせ一覧情報
	 */
	public function getInformationList($limit,$offset){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//$contact_id = clsContactSession::getContactId();
		//絞り込み条件のときcontact_idを設定する。
		$strWhereSet = '';
		
		if( '' != $this->_aryDisp["search_contact_id"]){
			$strWhereSet = "AND trn.contact_id = " . $this->_aryDisp["search_contact_id"];
		}
		
		$strlimitSql = "limit ".$limit." offset ".$offset;
		
		$strSelectSql =
<<<SQL
	select 
		cm.contact_id,
		cm.contact_name,
		trn.information_id,
		trn.management_kbn,
		trn.information_text,
		REPLACE(trn.reg_time,"-","/") as reg_time,
		trn.reg_name,
		REPLACE(trn.upd_time,"-","/") as upd_time,
		trn.upd_name,
		(case when max_dt.information_id is not null then 1 else 0 end) as flg
	from
		information_trn AS trn INNER JOIN (
			SELECT
				contact_id,
				contact_name
			FROM
				contact_mst
			WHERE
				delete_flg = :delete_flg
		) AS cm
		on trn.contact_id = cm.contact_id
	
		left outer join (
			select 
				trn.information_id 
			from
				information_trn trn
			where
				trn.delete_flg = :delete_flg
			order by
				upd_time desc
			limit 1
		) max_dt
		on trn.information_id = max_dt.information_id
	where
		trn.delete_flg     = :delete_flg AND
		trn.management_kbn = :management_kbn
		$strWhereSet 
	order by
		reg_time desc
	$strlimitSql
SQL;
		$aryParameters = array(
			":management_kbn"		=>	self::MANAGEMENT_KBN,
			":delete_flg"			=>	0
		);
		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError($aryResult) ){
			return false;
		}
		
		return $aryResult;
	}
	
	/**
	 * お知らせ情報を削除します
	 * 
	 * @return true:成功、false:失敗
	 */
	public function deleteInformation(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		//トランザクション開始
		$objDatabase->execTransaction();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$contact_id = $this->_aryDisp["select_contact_id"];
		$management_kbn = self::MANAGEMENT_KBN;
		$upd_id = clsLoginSession::getUserId();
		$upd_name = clsLoginSession::getFullName();
		$upd_time = clsCommonFunction::getCurrentDate();
		
		$strUpdateSql =
<<<SQL
		update information_trn
		set
			delete_flg = 1,
			upd_id = :upd_id,
			upd_name = :upd_name,
			upd_time = :upd_time
		where 
			contact_id = :contact_id
			and management_kbn = :management_kbn 
			and information_id = :information_id
SQL;
		
		$aryParameters = array(
			":upd_id"				=>	$upd_id,
			":upd_name"				=>	$upd_name,
			":upd_time"				=>	$upd_time,
			":contact_id"			=>	$contact_id,
			":management_kbn"		=>	$management_kbn,
			":information_id"		=>	$this->_aryDisp["select_information_id"],
		);
		
		$aryResult = $objDatabase->updDbData( $strUpdateSql, $aryParameters );
		if( $objDatabase->isError($aryResult) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		$past_number = clsCommonFunction::getNextId("information_trn_past", "past_number",array("management_kbn" => $management_kbn,"contact_id" => $contact_id));
		$upd_kbn = 2;
		
		$strInsertSql =
<<<SQL
		insert into information_trn_past
		select
			:past_number,
			:upd_kbn,
			:past_reg_id,
			information_trn.*
		from information_trn
		where contact_id = :contact_id
		and management_kbn = :management_kbn
		and information_id = :information_id
SQL;
		$aryParameters = array(
			":past_number"			=>	$past_number,
			":upd_kbn"				=>	$upd_kbn,
			":past_reg_id"			=>	clsLoginSession::getUserId(),
			":contact_id"			=>	$contact_id,
			":management_kbn"		=>	$management_kbn,
			":information_id"		=>	$this->_aryDisp["select_information_id"]
		);
		
		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameters);
		
		if($objDatabase->isError($aryResult)){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;

	}

	/**
	 * 契約会社リスト生成 
	 * 
	 * @param param
	 * @return return
	 */
	public function getContactList($intContact){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSelectSql =
<<<SQL
		SELECT 
			contact_id,
			contact_name
		FROM
			contact_mst
		WHERE
			delete_flg = :delete_flg
SQL;
		$aryParameters = array(
			":delete_flg"	=>	0,
		);
		
		$aryResult = $objDatabase->pullDbData($strSelectSql , $aryParameters);
		

		if( $objDatabase->isError($aryResult) ){
			return false;
		}

		//$strContactSelectBox = "<option value='' " . clsCommonFunction::chkSelectedDate( $intContact, '' ) . " ></option>";
		$strContactSelectBox = "<option value=''" . clsCommonFunction::chkSelectedDate( $intContact, '' ) . ">全て</option>";
		foreach( $aryResult as $key => $val ){
			$strContactSelectBox .= "<option value=" . $val["contact_id"] . " " . clsCommonFunction::chkSelectedDate( $intContact, $val["contact_id"] ) . " >" . $val["contact_name"] . "</option>";
		}

		return $strContactSelectBox;

	}

	public function getContactId($intContactId){
		$this->_aryDisp["search_contact_id"] = $intContactId;
	}
}
?>