<?php
/**
 *	お知らせ一覧画面の制御
 *
 *	お知らせ一覧画面の制御について記述
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 */
class ctlInformationList{
	
	/**
	 * コンストラクタ
	 *
	**/
	function __construct() {
	}
	
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){
		// 完了画面からの戻り値考慮
		if( "1" == $_GET["bk"] && true == isset( $_SESSION[clsDefinition::SESSION_ADMIN_INFORMATION_LIST] ) ){
			foreach( $_SESSION[clsDefinition::SESSION_ADMIN_INFORMATION_LIST] as $key => $val ){
				$_POST[$key] = $val;
			}
			unset( $_SESSION[clsDefinition::SESSION_ADMIN_INFORMATION_LIST] );
		}
		
		$objChecker   = new clsAdminInformationListChecker($_REQUEST);	//エラーチェッククラス
		$strErrorJson = '';												//エラー情報のJSON文字列
		
		// ページナンバー
		if( "" == $_POST["pagerNumber"] ){
			$_POST["pagerNumber"] = '1';
		}
	
		$strAction = $_POST["action"];
		
		//ＰＯＳＴの値で画面配列更新
		foreach ($_POST as $key => $val){
			$aryDisp[$key] = $_POST[$key];
		}
		$objInformationList = new clsInformationList($aryDisp);
		
		//ページャクラス作成
		$objPager = new clsPager();

		//会社名選択リスト
		$strContactSelectBox = $objInformationList->getContactList($aryDisp["search_contact_id"]);

		$objInformationList->getContactId($aryDisp["search_contact_id"]);

		switch($strAction){
			//検索
			case "search":
				// ページナンバー初期化
				$_POST["pagerNumber"] = '1';

				$objPager->setTotal($objInformationList->getInformationListCnt());
				$aryResultList = $objInformationList->getInformationList($objPager->getLimit(),$objPager->getOffset());

				require_once 'dspInformationList.php';
				break;
			//削除
			case "delete":
				$blnResult = $objInformationList->deleteInformation();
				
				$objPager->setTotal($objInformationList->getInformationListCnt());
				$aryResultList = $objInformationList->getInformationList($objPager->getLimit(),$objPager->getOffset());
				
				require_once 'dspInformationList.php';
				
				break;
			//初期表示
			default:
				$objPager->setTotal($objInformationList->getInformationListCnt());
				$aryResultList = $objInformationList->getInformationList($objPager->getLimit(),$objPager->getOffset());
				require_once 'dspInformationList.php';
				break;
		}
		
		
	}
}


?>