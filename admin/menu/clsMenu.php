<?php
/**
 *	管理者メニュー画面のモデルクラス
 *
 *	管理者メニュー画面のビジネスロジックについて記述
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */
class clsMenu{
	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct(){
	}
	
	/**
	 * ユーザー数取得
	 * @update 2013/03/22 kanata 特権ユーザ対象外
	 * @update 2013/04/18 Nishi 契約会社ごとで絞り込み
	 *
	 * @return array
	 */
	public function getUserCnt(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		// 2013/04/18 Nishi追加
		$strWhereSet = '';
		
		if( '' != $this->_aryDisp["search_contact_id"]){
			$strWhereSet = "AND lm.contact_id = " . $this->_aryDisp["search_contact_id"];
		}
		
		$strSql =
<<< SQL
	SELECT
		*
	FROM
		login_mst  AS lm
			INNER JOIN (
				SELECT
					contact_id
				FROM
					contact_mst
				WHERE
					delete_flg = :delete_flg
		) AS cm
	ON lm.contact_id = cm.contact_id
	WHERE
		delete_flg = :delete_flg AND
		user_kbn  != :user_kbn
		$strWhereSet
SQL;
		$aryParameter = array(
			":delete_flg" => '0',
			":user_kbn"   => '3'
		);
		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return COUNT( $aryResult );
	}
	
	/**
	 * 動画総数取得
	 * 
	 * @return array
	 */
	public function getMovieCnt(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strWhereSet = '';
		
		if( '' != $this->_aryDisp["search_contact_id"]){
			$strWhereSet = "AND mbit.contact_id = " . $this->_aryDisp["search_contact_id"];
		}
		
		$strSql =
<<< SQL
	SELECT
		COUNT( movie_id ) AS movie_cnt,
		( SUM( size ) / 1024 / 1024 ) AS movie_size_sum
	FROM
		movie_basic_info_trn  AS mbit
			INNER JOIN (
				SELECT
					contact_id
				FROM
					contact_mst
				WHERE
					delete_flg = :delete_flg
			) AS cm
		ON mbit.contact_id = cm.contact_id
	WHERE
		delete_flg = :delete_flg
		$strWhereSet
SQL;
		$aryParameter = array(
			":delete_flg"     => '0'
		);
		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult[0];
	}

	/**
	 * 再生回数(直近一か月)取得
	 * @update 2013/04/18 Nishi 契約会社ごとで絞り込み
	 * 
	 * @return array
	 */
	public function getMoviePlayCnt(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		// 2013/04/18　Nishi追加
		$strWhereSet = '';
		
		if( '' != $this->_aryDisp["search_contact_id"]){
			$strWhereSet = "AND mpitp.contact_id = " . $this->_aryDisp["search_contact_id"];
		}
		
		$strStartData = date("Y-m-d", strtotime("-1 month"));
		$strEndData   = date("Y-m-d", strtotime("-1 day"));
		
		$strSql = 
<<< SQL
	SELECT
		REPLACE( :start_data, '-', '/' ) AS start_data,
		REPLACE( :end_data, '-', '/' ) AS end_data,
		COUNT( * ) AS movie_play_cnt
	FROM
		movie_play_info_trn_past AS mpitp
			INNER JOIN (
				SELECT
					contact_id
				FROM
					contact_mst
				WHERE
					delete_flg = :delete_flg
			) AS cm
		ON mpitp.contact_id = cm.contact_id
	WHERE
		delete_flg        = :delete_flg AND
		play_time_in_flg  = :play_time_in_flg AND
		DATE( upd_time ) >= :start_data AND
		DATE( upd_time ) <= :end_data
		$strWhereSet
SQL;
		$aryParameter = array(
			":delete_flg"       => '0',
			":start_data"       => $strStartData,
			":end_data"         => $strEndData,
			":play_time_in_flg" => '1',
		);
		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult[0];
	}
	
	/**
	 * 棒グラフ用再生回数(直近二週間)取得
	 * @update 2013/04/18 Nishi 契約会社ごとで絞り込み
	 * 
	 * @return array
	 */
	public function getGraphMoviePlay(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		// 2013/04/18　Nishi追加
		$strWhereSet1 = '';
		$strWhereSet2 = '';
		if( '' != $this->_aryDisp["search_contact_id"]){
			$strWhereSet1 = "AND mpitp1.contact_id = " . $this->_aryDisp["search_contact_id"];
			$strWhereSet2 = "AND mpitp2.contact_id = " . $this->_aryDisp["search_contact_id"];
		}
		
		$strStartData = date("Y-m-d", strtotime("-14 day"));
		$strEndData   = date("Y-m-d", strtotime("-1 day"));
		
		$strDayBetweenSql = "";
		for($i=-14;$i<=-1;$i++){
			if($i == -14){
				$strDayBetweenSql .= "select '\"".date("m/d", strtotime($i." day"))."\"' as play_date ";
			}else{
				$strDayBetweenSql .= " union select '\"".date("m/d", strtotime($i." day"))."\"'";
			}
		}
		$strSql = 
<<< SQL
	SELECT
		-- 日付(形式："MM/DD","MM/DD"...)
		GROUP_CONCAT( day.play_date SEPARATOR ',' ) AS day,
		-- PC再生回数(形式：99,99...)
		GROUP_CONCAT( COALESCE(pc.play_sum,0) SEPARATOR ',' ) AS pc_sum,
		-- SMP再生回数(形式：99,99...)
		GROUP_CONCAT( COALESCE(smp.play_sum,0) SEPARATOR ',' ) AS smp_sum
	FROM
	
	-- 直近二週間(データがある日付のみ取得)
		(
			$strDayBetweenSql
		) AS day
	
	-- ＰＣ再生回数
		LEFT JOIN 
		(
			SELECT
				-- 更新日付をグラフ表示用に「YYYY-MM-DD」形式にし、ハイフンをスラッシュに置換
				CONCAT( '"', REPLACE( SUBSTR( upd_time, 6, 5 ), '-', '/' ), '"' ) AS play_date,
				SUM( play_time_in_flg ) AS play_sum
			FROM
				movie_play_info_trn_past AS mpitp1
					INNER JOIN (
						SELECT
							contact_id
						FROM
							contact_mst
						WHERE
							delete_flg = :delete_flg
					) AS cm1
				ON mpitp1.contact_id = cm1.contact_id
			WHERE
				delete_flg        = :delete_flg AND
				play_time_in_flg  = :play_time_in_flg AND
				DATE( upd_time ) >= :start_data AND
				DATE( upd_time ) <= :end_data AND
				smp_flg           = :pc_flg
				{$strWhereSet1}
			GROUP BY
				play_date
		) AS pc
		 ON day.play_date = pc.play_date
		
	-- スマートフォン再生回数
		LEFT JOIN 
		(
			SELECT
				-- 更新日付をグラフ表示用に「YYYY-MM-DD」形式にし、ハイフンをスラッシュに置換
				CONCAT( '"', REPLACE( SUBSTR( upd_time, 6, 5 ), '-', '/' ), '"' ) AS play_date,
				SUM( play_time_in_flg ) AS play_sum
			FROM
				movie_play_info_trn_past AS mpitp2
					INNER JOIN (
						SELECT
							contact_id
						FROM
							contact_mst
						WHERE
							delete_flg = :delete_flg
					) AS cm2
				ON mpitp2.contact_id = cm2.contact_id
			WHERE
				delete_flg        = :delete_flg AND
				play_time_in_flg  = :play_time_in_flg AND
				DATE( upd_time ) >= :start_data AND
				DATE( upd_time ) <= :end_data AND
				smp_flg           = :smp_flg
				{$strWhereSet2}
			GROUP BY
				play_date
		) AS smp
		 ON day.play_date = smp.play_date
SQL;
		$aryParameter = array(
			":delete_flg"       => '0',
			":start_data"       => $strStartData,
			":end_data"         => $strEndData,
			":play_time_in_flg" => '1',
			":pc_flg"           => '0',
			":smp_flg"          => '1'
		);
		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameter );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult[0];
	}

	/**
	 * 契約会社リスト生成 
	 * 
	 * @param param
	 * @return return
	 */
	public function getContactList($intContact){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSelectSql =
<<<SQL
		SELECT 
			contact_id,
			contact_name
		FROM
			contact_mst
		WHERE
			delete_flg = :delete_flg
SQL;
		$aryParameters = array(
			":delete_flg"	=>	0,
		);
		$aryResult = $objDatabase->pullDbData($strSelectSql , $aryParameters);
		if( $objDatabase->isError($aryResult) ){
			return false;
		}

		//$strContactSelectBox = "<option value='' " . clsCommonFunction::chkSelectedDate( $intContact, '' ) . " ></option>";
		$strContactSelectBox = "<option value=''" . clsCommonFunction::chkSelectedDate( $intContact, '' ) . ">全て</option>";
		foreach( $aryResult as $key => $val ){
			$strContactSelectBox .= "<option value=" . $val["contact_id"] . " " . clsCommonFunction::chkSelectedDate( $intContact, $val["contact_id"] ) . " >" . $val["contact_name"] . "</option>";
		}
		
		return $strContactSelectBox;
	}

	// 契約会社
	public function getContactId($intContactId){
		$this->_aryDisp["search_contact_id"] = $intContactId;
	}
	
}
?>