<?php
/**
 *	管理者メニュー画面
 *
 *	管理者メニュー画面の制御クラスを呼び出す
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/include.php";
require_once "./ctlMenu.php";
require_once "./clsMenu.php";

//ログイン制御クラス呼出し
$objCtlMenu = new ctlMenu();
$objCtlMenu->process();
?>