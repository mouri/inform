<?php
/**
 *	管理者メニュー画面の画面表示
 *
 *	管理者メニュー画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2012/01/16
 *　 @update 		Nishi 2013/04/18 契約会社ごとで絞り込み
 *	@version		1.0
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo clsDefinition::SYSTEM_NAME?>　ダッシュボード</title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="./js/dspMenu.js"></script>
<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/highcharts.js"></script>
<script type="text/javascript">
	var chart;
	$(document).ready(function () {

		//画面ＪＳ
		var objDspMenu = new dspMenu();

		// 検索ボタンクリック時
		$("#search_btn").click(function(){
			// 条件設定
			objDspMenu.Search();
		});

		// グラフのオプションを設定
		chart = new Highcharts.Chart({
			chart : {
				// グラフ表示させるdivをidで設定
				renderTo       : 'container',
				// グラフ右側のマージンを設定
				// marginRight : 140,
				// グラフ左側のマージンを設定
				marginBottom   : 50
			},
			
			// グラフのタイトルを設定
			title : {
				text :"直近２週間の再生回数"
			},
			
			// クレジットを設定
			credits : {
				text :  ''
			},
			
			// x軸の設定
			xAxis : {
				title : {
					text : '日付'
				},
				// x軸に表示するデータを設定
				categories    : [<?php echo $aryGraphMoviePlay["day"] ?>],
				// 小数点刻みにしない
				allowDecimals : false
			},
			
			// y軸の設定
			yAxis : [{
				title : {
					// タイトル名の設定
					text  : "再生回数",
					style : {
						// タイトルの色を設定
						color : '#4572A7',
					}
				},
				// y軸の表記設定
				labels : {
					formatter : function() {
						return this.value;
					},
					rotation: 90
				},
				// 小数点刻みにしない
				allowDecimals : false,
				// 最大値を設定
				// max : 6000,
				// 最小値を設定
				// min : 0
			}],
			
			// グラフにマウスオーバーすると出てくるポップアップの表示設定
			tooltip : {
				formatter : function () {
					return '<b>' + this.series.name + '</b><br/>' +
					this.x + ' : ' + this.y + '回';
				}
			},
			
			// 凡例の設定
			legend : {
				// 凡例が縦に並ぶ
				layout        : 'vertical',
				// 凡例の横位置
				align         : 'right',
				// 凡例の縦位置
				verticalAlign : 'top'
			},
			
			// グラフデータの設定
			series : [{
				// 名前を設定
				name  : "ＰＣ用",
				// 色の設定
				color : '#FF9999',
				// グラフタイプの設定(column:棒グラフ)
				type  : 'column',
				// x,y軸の設定
				data  : [<?php echo $aryGraphMoviePlay["pc_sum"] ?>]
			},
			{
				// 名前を設定
				name  : "スマホ用",
				// 色の設定
				color : '#99CC66',
				// グラフタイプの設定(column:棒グラフ)
				type  : 'column',
				// x,y軸の設定
				data  : [<?php echo $aryGraphMoviePlay["smp_sum"] ?>]
			}]
		});
	});
</script>
</head>
<body id="dashborad">
<?php echo clsCommonFunction::dispHeaderManegement(); ?>
<form name="MenuForm" id="MenuForm" method="post">
	<div class="boxBeige marB20">
		<h2>契約会社絞り込み</h2>
		<div class="form">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth">
						<p class="komoku">契約会社名</p>
						<select name="search_contact_id" id="search_contact_id">
							<?php echo $strContactSelectBox ?>
						</select>
					</td>
					<td>
						<p class="komoku"><br /></p>
						<div class="basicBtn search">
							<input type="button" name="search_btn" id="search_btn" value="検索" />
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</form>
  <!-- ▼統計 -->
  <div class="boxBeige marB20">
	<h2>総計</h2>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<th width="30%">ユーザ数</th>
		<th width="30%">動画数</th>
		<th width="30%">動画総サイズ数</th>
	  </tr>
	  <tr>
	  	<td class="f20 rightTxt"><strong><?php echo $intUserCnt ?><span class="f12 marL5">人</span></strong></td>
		<td class="f20 rightTxt"><strong><?php echo $aryMovieInfoCnt["movie_cnt"] ?><span class="f12 marL5">本</span></strong></td>
		<td class="f20 rightTxt"><strong><?php f::n( $aryMovieInfoCnt["movie_size_sum"] ) ?><span class="f12 marL5">ＭＢ</span></strong></td>
	  </tr>
	</table>
  </div>
  <!-- ▲統計 -->
  
  <!-- ▼閲覧状況 -->
  <div class="boxBeige marB20">
    <h2>閲覧状況<span class="f12">（ 過去1ヶ月分 : <?php echo $aryMoviePlayCnt["start_data"]; ?> ～ <?php echo $aryMoviePlayCnt["end_data"]; ?> ）</span></h2>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th width="50%">再生回数</th>
      </tr>
      <tr>
        <td class="f20 rightTxt"><strong><?php f::n( $aryMoviePlayCnt["movie_play_cnt"] ); ?><span class="f12 marL5">回</span></strong></td>
      </tr>
    </table>
  </div>
  <!-- ▲閲覧状況 -->
	
	<!-- ▼グラフ表示 -->
	<div id="container" style="width: 850px; height: 220px; margin: 0; z-index:1"></div>
	<!-- ▲グラフ表示 -->
	
<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
