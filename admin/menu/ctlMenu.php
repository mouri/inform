<?php
/**
 *	管理者メニュー画面の制御
 *
 *	管理者メニュー画面の制御について記述
 *
 *	@author			Mouri 2012/01/16
 * 　@update　		Nishi 2013/04/18 契約会社ごとで絞り込み
 *	@version		1.0
 */
class ctlMenu{
	
	/**
	 * コンストラクタ
	 *
	**/
	function __construct() {
	}
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){
		
		$objClsMenu = new clsMenu();
		$strAction = $this->_aryPost["action"];
		//セッションからログイン情報取り出し
		$aryLogin = $_SESSION[clsDefinition::SESSION_LOGIN];
		
		switch($strAction){
			//初期表示
			default:
				break;
		}

		// 契約会社名取得　2013/04/18　Nishi追加
		$strContactSelectBox = $objClsMenu->getContactList($_POST["search_contact_id"]);
							   $objClsMenu->getContactId($_POST["search_contact_id"]);
		
		// ユーザー数取得
		$intUserCnt        = $objClsMenu->getUserCnt();
		// 動画数、動画総サイズ数取得
		$aryMovieInfoCnt       = $objClsMenu->getMovieCnt();
		// 直近一か月の再生回数取得
		$aryMoviePlayCnt   = $objClsMenu->getMoviePlayCnt();
		// グラフ用動画再生回数取得
		$aryGraphMoviePlay = $objClsMenu->getGraphMoviePlay();
		
		require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . '/admin/menu/dspMenu.php' );
		
	}
}


?>