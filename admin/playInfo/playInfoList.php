<?php
/**
 *	グループ一覧(修正)画面
 *
 *	グループ一覧(修正)画面の制御クラスを呼び出す
 *
 *	@author			Kanata 2012/02/07
 */

require_once( $_SERVER["DOCUMENT_ROOT"] . "/include.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/playInfo/ctlPlayInfoList.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/playInfo/clsPlayInfoList.php" );
//require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/value_checker/execute_classes/clsAdminGroupListChecker.php" );

// 再生一覧クラス呼出し
$objCtlPlayInfoList = new ctlPlayInfoList();
$objCtlPlayInfoList->process();

?>