<?php
/**
 *	グループ一覧(修正)画面の画面表示
 *
 *	グループ一覧(修正)画面のＨＴＭＬ表示部分を記述
 *
 *	@author Kanata 2012/02/07
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　グループ一覧・編集</title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/playInfo/js/playInfoList.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
				
		var objPlayInfoList = new playInfoList();
		
		// 検索ボタンクリック時
		$("#search_btn").click(function(){
			
			if( '' == $("#search_contact_id option:selected").val() ){
				$(".message").html("<p class='errInfo'>・選択してください</p>");
				//alert('選択！');
			}else{
				$(".message").html("");
				// 条件設定
				objPlayInfoList.Search();
			}
			
		});
	});
</script>
</head>
<body id="playInfoList">
	<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	
	<div class="boxBeige">
		<h2>再生情報一覧</h2>
		<div class="form">
			<form name="searchForm" id="searchForm" method="post">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr id="hissu_contact_id">
					<td class="c1 fullWidth">
						<p class="komoku hissu">契約会社名</p>
						<select name="search_contact_id" id="search_contact_id">
							<?php echo $strContactSelectBox; ?>
						</select>
					</td>
					<td class="message">&nbsp;</td>
				</tr>
				<tr>
					<td class="c1 fullWidth">
						<p class="komoku">名前</p>
							<div class="lavel">
								<input type="text" name="search_name" id="search_name" value="<?php f::p( $aryPostDate["search_name"] ); ?>" />
								<label for="search_name1" class="over">名前</label>
							</div>
					</td>
					<td>&nbsp;</td>
		        </tr>
				<tr>
					<td class="c1 fullWidth">
						<p class="komoku">条件</p>
						<div class="radioDiv item3">
							<div class="left" style="width:95px;">
								<label for="search_disp0" class="inputRadio"><span class="radioButton"></span>全て<input type="radio" name="search_disp" id="search_disp0" value="0" <?php echo clsCommonFunction::chkCheckedDate( $aryPostDate['search_disp'], 0 ); ?> ></label>
							</div>
							<div class="center" style="width:120px;">
								<label for="search_disp1" class="inputRadio"><span class="radioButton"></span>再生0回<input type="radio" name="search_disp" id="search_disp1" value="1" <?php echo clsCommonFunction::chkCheckedDate( $aryPostDate['search_disp'], 1 ); ?> ></label>
							</div>
							<div class="right" style="width:235px;">
								<label for="search_disp2" class="inputRadio"><span class="radioButton"></span>再生0回&&表示0回以上<input type="radio" name="search_disp" id="search_disp2" value="2" <?php echo clsCommonFunction::chkCheckedDate( $aryPostDate['search_disp'], 2 ); ?> ></label>
							</div>
						</div>
					</td>
					<td>
						<p class="komoku">　</p>
						<div class="basicBtn search">
							<input type="button" name="search_btn" id="search_btn" value="検索" />
						</div>
					</td>
				</tr>
			</table>
			<input type='hidden' name='action' id='action' value='' />
			</form>
		</div>
	</div>
	<form name="ListForm" id="ListForm" method="post">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
			<tr>
				<th colspan="5" class="pager top"><?php echo $objPager->get(); ?></th>
			</tr>
			<tr>
				<th style="width:200px;">ユーザ名</th>
				<th>グループ名</th>
				<th>タイトル名</th>
				<th style="width:100px;">再生回数</th>
				<th style="width:100px;">表示回数</th>
			</tr>
			<?php if( 0 < count($aryResultList) ){ ?>
				<?php foreach( $aryResultList as $aryResultList ){ ?>
					<tr>
						<td><?php f::p($aryResultList['full_name']); ?></td>
						<td><?php f::p($aryResultList['group_name']); ?></td>
						<td><?php f::p($aryResultList['title']); ?></td>
						<td><?php f::p($aryResultList['play_count']); ?></td>
						<td><?php f::p($aryResultList['disp_count']); ?></td>
					</tr>
				<?php } ?>
			<?php } ?>
			<tr>
				<th colspan="5" class="pager"><?php echo $objPager->get(); ?></th>
			</tr>
		</table>
		<input type="hidden" name="search_contact_id" value="<?php echo $_POST['search_contact_id'] ?>">
		<input type="hidden" name="search_name" value="<?php echo $_POST['search_name'] ?>">
		<input type="hidden" name="search_disp" value="<?php echo $_POST['search_disp'] ?>">
		<input type="hidden" name="action" value="search">
	</form>
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>