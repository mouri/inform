<?php
/**
 *	グループ一覧(修正)画面のモデルクラス
 *
 *	グループ一覧(修正)画面のビジネスロジックについて記述
 *
 *	@author			Kanata 2012/02/07
 */
class clsPlayInfoList{

	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct( $aryPostDate ){
		$this->_aryPostDate = $aryPostDate;
	}
	
	/**
	 * POSTデータ整頓
	 *
	 **/
	public function pullConvertData( $aryPostDate ){
		// 検索配列
		$arySearchDate = array(
			'　'
		);
		// 置換配列
		$aryReplaceData = array(
			' '
		);
		// 前後の空白をなくす
		$aryPostDate['group_name'] = trim( str_replace( $arySearchDate, $aryReplaceData, $aryPostDate['group_name'] ) );
		
		return $aryPostDate;
	}
	
	/**
	 * 契約会社リスト生成 
	 * 
	 * @param param
	 * @return return
	 */
	public function getContactList($intContact){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSelectSql =
<<<SQL
		SELECT 
			contact_id,
			contact_name
		FROM
			contact_mst
		WHERE
			delete_flg = :delete_flg
SQL;
		$aryParameters = array(
			":delete_flg"	=>	0,
		);
		
		$aryResult = $objDatabase->pullDbData($strSelectSql , $aryParameters);
		

		if( $objDatabase->isError($aryResult) ){
			return false;
		}

		//$strContactSelectBox = "<option value='' " . clsCommonFunction::chkSelectedDate( $intContact, '' ) . " ></option>";
		$strContactSelectBox = "<option value=''" . clsCommonFunction::chkSelectedDate( $intContact, '' ) . ">選択してください</option>";
		foreach( $aryResult as $key => $val ){
			$strContactSelectBox .= "<option value=" . $val["contact_id"] . " " . clsCommonFunction::chkSelectedDate( $intContact, $val["contact_id"] ) . " >" . $val["contact_name"] . "</option>";
		}

		return $strContactSelectBox;

	}

	/**
	 * 再生情報一覧回数取得
	 *
	 *
	 *
	 */
	public function getPlayDispListCnt( $aryPostDate ){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strWhere = '';
		if($aryPostDate['search_name']){
			$strSearchName = '%' . $aryPostDate['search_name'] . '%';
			//$strWhere .= "AND ( name1 LIKE '" . $strSearchName . "' OR name2 LIKE '" . $strSearchName . "' OR roma1 LIKE '" . $strSearchName . "' OR roma2 LIKE '" . $strSearchName . "' ) ";
			$strWhere .= "AND ( lm.name1 LIKE '" . $strSearchName . "' OR lm.name2 LIKE '" . $strSearchName . "' OR lm.roma1 LIKE '" . $strSearchName . "' OR lm.roma2 LIKE '" . $strSearchName . "' OR lm.company_name LIKE '" . $strSearchName . "') ";
		}
		if($aryPostDate['search_disp']){

			$strWhere .= "AND play_count is null ";

			if( 2 == $aryPostDate['search_disp'] ){
				$strWhere .= "AND ( play_count is null AND disp_count > 0 ) ";
			}
		}

		
		$strSelectSql =
<<<SQL
select
		lm.contact_id,
		lm.user_id,
		CASE 
			WHEN company_name is not null THEN company_name
			WHEN ( lm.name1 is not null && lm.name2 is not null ) then concat( lm.name1,lm.name2 )
			ELSE concat( lm.roma1,lm.roma2 )
		END AS full_name,
		mbit.movie_id,
		mbit.title,
		mgm.movie_group_cd,
		mgm.group_name,
		CASE
			WHEN count_data.play_count is not null THEN count_data.play_count
			ELSE 0
		END play_count,
		CASE
			WHEN count_data.disp_count is not null THEN count_data.disp_count
			ELSE 0
		END disp_count
from
( 
(
select
 contact_id,
 user_id,
 null as company_name,
 name1,
 name2,
 roma1,
 roma2,
	delete_flg
 FROM login_mst
)UNION(
	select
	contact_id,
  company_id as user_id,
	company_name,
  null as name1,
  null as name2,
	null as roma1,
	null as roma2,
	delete_flg
	FROM agreement_login_mst
)
) AS lm
INNER JOIN
		movie_basic_info_trn mbit
	ON lm.contact_id = mbit.contact_id
	LEFT JOIN
	(SELECT
		m.contact_id,
		m.user_id,
		m.movie_id,
		m.movie_group_cd,
		m.disp_count,
		m.play_count,
		group_name,
		title
	FROM
	(
		(
			select
				A.contact_id,
				A.movie_id,
				A.user_id,
				A.movie_group_cd,
				disp_count,
				play_count
			from
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS disp_count
			FROM 
				movie_display_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS A
			left join
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS play_count
			FROM
				movie_play_info_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS B
			ON A.movie_id = B.movie_id
			AND A.contact_id = B.contact_id
			AND A.user_id = B.user_id

			)UNION(

			select
				D.contact_id,
				D.movie_id,
				D.user_id,
				D.movie_group_cd,
				disp_count,
				play_count
			from 
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS disp_count
			FROM 
				movie_display_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS C
			right join
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS play_count
			FROM
				movie_play_info_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS D
			ON C.movie_id = D.movie_id
			AND C.contact_id = D.contact_id
			AND C.user_id = D.user_id
		)
	) AS m

			LEFT JOIN movie_group_mst mgm
			ON m.contact_id = mgm.contact_id
			AND m.movie_group_cd = mgm.movie_group_cd

			LEFT JOIN movie_basic_info_trn mbit
			ON m.contact_id = mbit.contact_id
			AND m.movie_id = mbit.movie_id

			WHERE mbit.delete_flg = 0

			ORDER BY user_id,movie_group_cd,movie_id
	) AS count_data
	ON lm.contact_id = count_data.contact_id
	AND lm.user_id = count_data.user_id
	AND mbit.movie_id = count_data.movie_id
	LEFT JOIN
		(
		select
			contact_id,
			movie_id,
			movie_group_cd
		from
			movie_group_config_trn
		where
			delete_flg = 0
		) as mgct
	ON lm.contact_id = mgct.contact_id
	AND mbit.movie_id = mgct.movie_id
	LEFT JOIN
		movie_group_mst mgm
	ON lm.contact_id = mgm.contact_id
	AND mgct.movie_group_cd = mgm.movie_group_cd
WHERE
		lm.contact_id = :contact_id
	AND
		lm.delete_flg = 0
	AND
		mbit.delete_flg = 0

	{$strWhere}

group by user_id,movie_id
SQL;

		$aryParameters = array(
			":contact_id"	=>	$aryPostDate['search_contact_id'],
		);

		$aryResult = $objDatabase->pullDbData($strSelectSql , $aryParameters);

		if( $objDatabase->isError($aryResult) ){
			return false;
		}

		return count($aryResult);

	}

	/**
	 * 再生情報一覧取得
	 *
	 *
	 *
	 */
	public function getPlayDispList( $aryPostDate,$limit,$offset ){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strlimitSql = "limit ".$limit." offset ".$offset;

		$strWhere = '';
		/*if($aryPostDate['search_name']){
			$strSearchName = '%' . $aryPostDate['search_name'] . '%';
			$strWhere .= "AND ( name1 LIKE '" . $strSearchName . "' OR name2 LIKE '" . $strSearchName . "' OR roma1 LIKE '" . $strSearchName . "' OR roma2 LIKE '" . $strSearchName . "' ) ";
		}
		if($aryPostDate['search_disp']){

			$strWhere .= "AND play_count is null ";

			if( 2 == $aryPostDate['search_disp'] ){
				$strWhere .= "AND ( play_count is null AND disp_count > 0 ) ";
			}
		}*/

		if($aryPostDate['search_name']){
			$strSearchName = '%' . $aryPostDate['search_name'] . '%';
			$strWhere .= "AND ( lm.name1 LIKE '" . $strSearchName . "' OR lm.name2 LIKE '" . $strSearchName . "' OR lm.roma1 LIKE '" . $strSearchName . "' OR lm.roma2 LIKE '" . $strSearchName . "' OR lm.company_name LIKE '" . $strSearchName . "') ";
		}
		if($aryPostDate['search_disp']){

			$strWhere .= "AND play_count is null ";

			if( 2 == $aryPostDate['search_disp'] ){
				$strWhere .= "AND ( play_count is null AND disp_count > 0 ) ";
			}
		}

		
		$strSelectSql =
<<<SQL
select
		lm.contact_id,
		lm.user_id,
		CASE 
			WHEN company_name is not null THEN company_name
			WHEN ( lm.name1 is not null && lm.name2 is not null ) then concat( lm.name1,lm.name2 )
			ELSE concat( lm.roma1,lm.roma2 )
		END AS full_name,
		mbit.movie_id,
		mbit.title,
		mgm.movie_group_cd,
		mgm.group_name,
		CASE
			WHEN count_data.play_count is not null THEN count_data.play_count
			ELSE 0
		END play_count,
		CASE
			WHEN count_data.disp_count is not null THEN count_data.disp_count
			ELSE 0
		END disp_count
from
( 
(
select
 contact_id,
 user_id,
 null as company_name,
 name1,
 name2,
 roma1,
 roma2,
	delete_flg
 FROM login_mst
)UNION(
	select
	contact_id,
  company_id as user_id,
	company_name,
  null as name1,
  null as name2,
	null as roma1,
	null as roma2,
	delete_flg
	FROM agreement_login_mst
)
) AS lm
INNER JOIN
		movie_basic_info_trn mbit
	ON lm.contact_id = mbit.contact_id
	LEFT JOIN
	(SELECT
		m.contact_id,
		m.user_id,
		m.movie_id,
		m.movie_group_cd,
		m.disp_count,
		m.play_count,
		group_name,
		title
	FROM
	(
		(
			select
				A.contact_id,
				A.movie_id,
				A.user_id,
				A.movie_group_cd,
				disp_count,
				play_count
			from
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS disp_count
			FROM 
				movie_display_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS A
			left join
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS play_count
			FROM
				movie_play_info_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS B
			ON A.movie_id = B.movie_id
			AND A.contact_id = B.contact_id
			AND A.user_id = B.user_id

			)UNION(

			select
				D.contact_id,
				D.movie_id,
				D.user_id,
				D.movie_group_cd,
				disp_count,
				play_count
			from 
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS disp_count
			FROM 
				movie_display_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS C
			right join
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS play_count
			FROM
				movie_play_info_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS D
			ON C.movie_id = D.movie_id
			AND C.contact_id = D.contact_id
			AND C.user_id = D.user_id
		)
	) AS m

			LEFT JOIN movie_group_mst mgm
			ON m.contact_id = mgm.contact_id
			AND m.movie_group_cd = mgm.movie_group_cd

			LEFT JOIN movie_basic_info_trn mbit
			ON m.contact_id = mbit.contact_id
			AND m.movie_id = mbit.movie_id

			WHERE mbit.delete_flg = 0

			ORDER BY user_id,movie_group_cd,movie_id
	) AS count_data
	ON lm.contact_id = count_data.contact_id
	AND lm.user_id = count_data.user_id
	AND mbit.movie_id = count_data.movie_id
	LEFT JOIN
		(
		select
			contact_id,
			movie_id,
			movie_group_cd
		from
			movie_group_config_trn
		where
			delete_flg = 0
		) as mgct
	ON lm.contact_id = mgct.contact_id
	AND mbit.movie_id = mgct.movie_id
	LEFT JOIN
		movie_group_mst mgm
	ON lm.contact_id = mgm.contact_id
	AND mgct.movie_group_cd = mgm.movie_group_cd
WHERE
		lm.contact_id = :contact_id
	AND
		lm.delete_flg = 0
	AND
		mbit.delete_flg = 0

	{$strWhere}

group by user_id,movie_id
{$strlimitSql}
SQL;
/*<<<SQL
SELECT
		lm.contact_id,
		lm.user_id,
		CASE 
			WHEN ( lm.name1 is not null && lm.name2 is not null ) then concat( lm.name1,lm.name2 )
			ELSE concat( lm.roma1,lm.roma2 )
		END AS full_name,
		mbit.movie_id,
		mbit.title,
		mgm.movie_group_cd,
		mgm.group_name,
		CASE
			WHEN count_data.play_count is not null THEN count_data.play_count
			ELSE 0
		END play_count,
		CASE
			WHEN count_data.disp_count is not null THEN count_data.disp_count
			ELSE 0
		END disp_count
	FROM 
		login_mst lm
	INNER JOIN
		movie_basic_info_trn mbit
	ON lm.contact_id = mbit.contact_id
	LEFT JOIN
	(SELECT
		m.contact_id,
		m.user_id,
		m.movie_id,
		m.movie_group_cd,
		m.disp_count,
		m.play_count,
		CASE 
			WHEN ( l.name1 is not null && l.name2 is not null ) then concat( l.name1,l.name2 )
			ELSE concat( l.roma1,l.roma2 )
		END AS full_name,
		group_name,
		title
	FROM
	(
		(
			select
				A.contact_id,
				A.movie_id,
				A.user_id,
				A.movie_group_cd,
				disp_count,
				play_count
			from
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS disp_count
			FROM 
				movie_display_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS A
			left join
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS play_count
			FROM
				movie_play_info_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS B
			ON A.movie_id = B.movie_id
			AND A.contact_id = B.contact_id
			AND A.user_id = B.user_id

			)UNION(

			select
				D.contact_id,
				D.movie_id,
				D.user_id,
				D.movie_group_cd,
				disp_count,
				play_count
			from 
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS disp_count
			FROM 
				movie_display_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS C
			right join
			(SELECT
				contact_id,
				movie_id,
				user_id,
				movie_group_cd,
				count(movie_id) AS play_count
			FROM
				movie_play_info_trn_past
			WHERE
				contact_id = :contact_id
			and
				delete_flg = 0
			GROUP BY
				movie_id,
				user_id) AS D
			ON C.movie_id = D.movie_id
			AND C.contact_id = D.contact_id
			AND C.user_id = D.user_id
		)
	) AS m
			LEFT JOIN login_mst l
			ON m.contact_id = l.contact_id
			AND m.user_id = l.user_id
		
			LEFT JOIN movie_group_mst mgm
			ON m.contact_id = mgm.contact_id
			AND m.movie_group_cd = mgm.movie_group_cd

			LEFT JOIN movie_basic_info_trn mbit
			ON m.contact_id = mbit.contact_id
			AND m.movie_id = mbit.movie_id

			WHERE mbit.delete_flg = 0
			AND l.delete_flg = 0

			ORDER BY user_id,movie_group_cd,movie_id
	) AS count_data
	ON lm.contact_id = count_data.contact_id
	AND lm.user_id = count_data.user_id
	AND mbit.movie_id = count_data.movie_id
	LEFT JOIN
		(
		select
			contact_id,
			movie_id,
			movie_group_cd
		from
			movie_group_config_trn
		where
			delete_flg = 0
		) as mgct
	ON lm.contact_id = mgct.contact_id
	AND mbit.movie_id = mgct.movie_id
	LEFT JOIN
		movie_group_mst mgm
	ON lm.contact_id = mgm.contact_id
	AND mgct.movie_group_cd = mgm.movie_group_cd
	WHERE
		lm.contact_id = :contact_id
	AND
		lm.delete_flg = 0
	AND
		mbit.delete_flg = 0
	{$strWhere}

	group by user_id,movie_id
		{$strlimitSql}
SQL;
*/
		$aryParameters = array(
			":contact_id"	=>	$aryPostDate['search_contact_id'],
		);

		$aryResult = $objDatabase->pullDbData($strSelectSql , $aryParameters);

		if( $objDatabase->isError($aryResult) ){
			return false;
		}

		return $aryResult;

	}

}
