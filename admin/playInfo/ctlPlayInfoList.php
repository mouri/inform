<?php
/**
 *	再生情報一覧画面の制御クラス
 *
 *	再生情報一覧画面の制御について記述
 *
 *	@author			Nishi 2013/09/06
 */

class ctlPlayInfoList{
	
	/**
	 * コンストラクタ
	 *
	 **/
	function __construct() {
	}
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){
		
		$objClsPlayInfoList = new clsPlayInfoList( $_POST );			// ロジッククラス
		$strErrorJson    = '';									// エラー情報のJSON文字列
		$strAction       = @$_POST["action"];
		$aryResultList = array();


		// ページナンバー
		if( "" == $_POST["pagerNumber"] ){
			$_POST["pagerNumber"] = '1';
		}

		// 処理分岐
		switch( $strAction ){
			
			// 検索
			case 'search':

				$aryPostDate = $_POST;

				//ページャクラス作成
				$objPager = new clsPager();
				$objPager->setLimit(15);

				//一覧の総件数をセット
				$objPager->setTotal( $objClsPlayInfoList->getPlayDispListCnt($aryPostDate) );

				$aryResultList = $objClsPlayInfoList->getPlayDispList($aryPostDate,$objPager->getLimit(),$objPager->getOffset());

				$strContactSelectBox = $objClsPlayInfoList->getContactList($aryPostDate['search_contact_id']);
				break;
			
			// 初期処理
			default:
				//ページャクラス作成
				$objPager = new clsPager();
				$objPager->setLimit(15);

				$strContactSelectBox = $objClsPlayInfoList->getContactList('');
				break;
		}

		// グループリスト読み込み
		require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . '/admin/playInfo/dspPlayInfoList.php' );

	}
}

