<?php
/**
 *	基本情報メンテナンス画面の制御
 *
 *	基本情報メンテナンス画面の制御について記述
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 */
class ctlBasicInfo{
	
	/**
	 * コンストラクタ
	 *
	**/
	function __construct() {
	}
	
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){

		// ＰＯＳＴの値で画面配列更新
		foreach ($_POST as $key => $val){
			$aryDisp[$key] = $_POST[$key];
			$_SESSION['basic_info'][$key] = $aryDisp[$key];
		}

		$objBasicInfo = new clsBasicInfo( $aryDisp );
		$objChecker   = new clsAdminBasicInfoChecker( $_REQUEST );		//エラーチェッククラス
		$strErrorJson = '';												//エラー情報のJSON文字列
		$strAction    = $aryDisp["action"];
		$intChannelChk = 0;
		$dispBackFlg = $aryDisp["dispBackFlg"];
		$blnCheckResult = true;
		$blnAdminErrorFlg = true;
		$blnChannelErrorFlg = true;

		switch( $strAction ){
			// チャンネル登録画面
			case "channel":
					// POSTデータのエラーチェック
					if( 0 == $dispBackFlg ){
						$blnCheckResult = $objChecker->execute();
						if( false == $blnCheckResult ){
							// エラーがあった場合はエラー情報のJSON文字列を取得する
							if( $objChecker->isError() ){
								$strErrorJson = $objChecker->getErrorJson();
							}
							break;
						}
					}

					$aryDisp = $objBasicInfo->getBasicInfoData($aryDisp); // セッションデータ格納
					if($aryDisp["menu_category_name"]){
						$intChannelChk = 1;
					}

					require_once 'dspChannel.php';
					exit();
				break;

			// 管理メニュー選択画面
			case "admin_menu":

					if( 0 == $dispBackFlg){
						unset($_SESSION['basic_info']["menu_category_name"]);
						unset($_SESSION['basic_info']["user_enterable_flg"]);
						$_SESSION['basic_info']["menu_category_name"] = $aryDisp["menu_category_name"];
						$_SESSION['basic_info']["user_enterable_flg"] = $aryDisp["user_enterable_flg"];
					}

					$aryDisp = $objBasicInfo->getBasicInfoData($aryDisp); // セッションデータ格納

					// POSTデータのエラーチェック
					if( 0 == $dispBackFlg && 0 == intval($aryDisp["menu_category_flg"]) ){
						$blnCheckResult = $objChecker->execute();
						if( false == $blnCheckResult ){
							// エラーがあった場合はエラー情報のJSON文字列を取得する
							if( $objChecker->isError() ){
								$strErrorJson = $objChecker->getErrorJson();
							}
							break;
						}
					}else if( 0 == $dispBackFlg && 1 == intval($aryDisp["menu_category_flg"]) ){
						$blnCheckResult = $objChecker->channel();
						if( false == $blnCheckResult ){
							// エラーがあった場合はエラー情報のJSON文字列を取得する
							if( $objChecker->isError() ){
								$strErrorJson = $objChecker->getErrorJson();
							}
						$blnChannelErrorFlg = false;
						break;
						}
					}

					$strAdminMenuSelectBox = $objBasicInfo->getAdminMenuSelectBox($aryDisp["admin_menu_cd"]);

					require_once 'dspAdminMenuSelect.php';
					exit();
				break;

			// 確認画面
			case "confirm":

					// POSTデータのエラーチェック
					if( 0 == $aryDisp["dispBackFlg"] ){
						$blnCheckResult = $objChecker->adminMenu();
						if( false == $blnCheckResult ){
							// エラーがあった場合はエラー情報のJSON文字列を取得する
							if( $objChecker->isError() ){
								$strErrorJson = $objChecker->getErrorJson();
							}
							$blnAdminErrorFlg = false;
							break;
						}
					}
				
					if( 0 == $aryDisp["dispBackFlg"]){
						unset($_SESSION['basic_info']["admin_menu_cd"]);
						$_SESSION['basic_info']["admin_menu_cd"] = $aryDisp["admin_menu_cd"];
					}

					$aryDisp = $objBasicInfo->getBasicInfoData($aryDisp); // セッションデータ格納

					// pcエンコード
					/*foreach( clsDefinition::$PC_ENCODE AS $strPCEncode ){
						if( $aryDisp["encode_pc"] == $strPCEncode["id"] ){
							$aryDisp["encode_pc_name"] = $strPCEncode["name"];
						}
					}
					// スマフォエンコード
					foreach( clsDefinition::$SMP_ENCODE AS $strSMPEncode ){
						if( $aryDisp["encode_smp"] == $strSMPEncode["id"] ){
							$aryDisp["encode_smp_name"] = $strSMPEncode["name"];
						}
					}*/

					// トランスコード Nishi add 20130724
					foreach( clsDefinition::$TRANSCODE_FLG AS $strTranscode_flg ){
						if( $aryDisp["transcode_flg"] == $strTranscode_flg["flg"] ){
							$aryDisp["transcode_flg_name"] = $strTranscode_flg["name"];
						}
					}

					$strChannelList = $objBasicInfo->getChannelList($aryDisp);
					$strAdminMenuList = $objBasicInfo->getAdminMenuList($aryDisp);

					require_once 'dspBasicInfoConfirm.php';
					exit();

				break;

			// 登録処理
			case "regist":

				$aryDisp = $objBasicInfo->getBasicInfoData($aryDisp); // セッションデータ格納

				$aryDisp["contact_id"] = base64_encode($aryDisp["contact_id"]);

				$objBasicInfo->setAryData($aryDisp);

				// 修正処理
				if( 1 == $aryDisp["editMode"] ){
					$blnResult = $objBasicInfo->updateBasicInfo();
					// 検索条件をセッションに格納する
					$_SESSION[clsDefinition::SESSION_ADMIN_BASICINFO_LIST] = array(
						"pagerNumber" => $aryDisp["pagerNumber"]
					);
					
				// 登録処理
				} else {
					$blnResult = $objBasicInfo->addBasicInfo(); //基本情報登録
				}
				// 完了画面遷移
				if( false != $blnResult ){
					header("Location: ".clsDefinition::SYSTEM_DIR."/admin/basicinfo/basicinfoentry/dspBasicInfoComplete.php?editMode=".$aryDisp["editMode"]); 
				
				// 登録 or 修正画面遷移
				} else {
					unset( $_SESSION[clsDefinition::SESSION_ADMIN_BASICINFO_LIST] );
					$objChecker->setError();
					$objChecker->setErrorMessage( "auth", "ＤＢ登録エラーの為、登録できません。" );
					
				}
				
				break;
			
			// 修正
			case "edit":
				$aryResultList = $objBasicInfo->getBasicInfo();
				if( false == $aryResultList ){
					$objChecker->setError();
					$objChecker->setErrorMessage( "auth", "基本情報取得に失敗しました。" );
				}

				if(!$aryResultList["transcode_flg"]){
					$aryResultList["transcode_flg"] = 2; //設定2
				}

				$aryDisp["contact_id"]         = $aryResultList["contact_id"];
				$aryDisp["contact_name"]       = $aryResultList["contact_name"];
				$aryDisp["start_day"]          = $aryResultList["start_day"];
				$aryDisp["system_dir"]         = substr($aryResultList["system_dir"],1); //一文字目にはフォルダ登録用に「/」が入っているため省く
				$aryDisp["account_excel_name"] = $aryResultList["account_excel_name"];	// 2013/06/28 Nishi追加　ここから
				$aryDisp["login_tbl_name"]     = $aryResultList["login_tbl_name"];		//
				$aryDisp["mail_error_address"] = $aryResultList["mail_error_address"];	//
				$aryDisp["comment_flg"]        = $aryResultList["comment_flg"];			//
				$aryDisp["judge_flg"]          = $aryResultList["judge_flg"];			//
				$aryDisp["report_flg"]         = $aryResultList["report_flg"];			// 2013/06/28 Nishi追加　ここまで
				//$aryDisp["encode_pc"]          = $aryResultList["encode_pc"];
				//$aryDisp["encode_smp"]         = $aryResultList["encode_smp"];
				$aryDisp["transcode_flg"]      = $aryResultList["transcode_flg"];       // 20130724 Nishi add
				$aryDisp["templete"]           = $aryResultList["templete"];
				$aryDisp["menu_category_flg"]  = $aryResultList["menu_category_flg"];
				$aryDisp["sender_address"]     = $aryResultList["fromAddr"];
				$aryDisp["sender_name"]        = $aryResultList["fromAddrName"];
				$aryDisp["signature"]          = $aryResultList["signature"];
				$aryDisp["millvi_id"]          = $aryResultList["millvi_id"];			// 2013/10/08 Nishi追加　ここから
				$aryDisp["millvi_user_id"]     = $aryResultList["millvi_user_id"];		//
				$aryDisp["millvi_password"]    = $aryResultList["millvi_password"];		// 2013/10/08 Nishi追加　ここまで
				$aryDisp["movie_folder"]       = $aryResultList["movie_folder"]; //一文字目にはフォルダ登録用に「/」が入っているため省く
				$aryDisp["system_name"]        = $aryResultList["system_name"];			// 2913-10-18 kanata add

				$aryResultList = $objBasicInfo->getChannelData();
				if( false == $aryResultList ){
					$objChecker->setError();
					$objChecker->setErrorMessage( "auth", "チャンネル情報取得に失敗しました。" );
				}

				if( 1 == intval($aryDisp["menu_category_flg"]) ){
					for($i = 0; $i < count($aryResultList); $i++){
						$aryDisp["menu_category_name"][$i]	= $aryResultList[$i]["menu_category_name"];
						$aryDisp["user_enterable_flg"][$i]	= $aryResultList[$i]["user_enterable_flg"];
					}
				}else{
					$aryDisp["menu_category_name"] = array();
					$aryDisp["user_enterable_flg"] = array();
				}

				if($aryDisp["menu_category_name"]){
					$intChannelChk = 1;
				}

				$aryResultList = $objBasicInfo->getAdminMenuListData();
				if( false == $aryResultList ){
					$objChecker->setError();
					$objChecker->setErrorMessage( "auth", "管理者メニュー情報取得に失敗しました。" );
				}

				for($j = 0; $j < count($aryResultList); $j++){
					$aryDisp["admin_menu_cd"][$j] = $aryResultList[$j]["menu_id"];
				}

				// ＰＯＳＴの値で画面配列更新
				foreach ($aryDisp as $key => $val){
					$_SESSION['basic_info'][$key] = $aryDisp[$key];
				}

				$aryDisp = $objBasicInfo->getBasicInfoData($aryDisp); // セッションデータ格納

				break;

			case "first":
				$aryDisp = $objBasicInfo->getBasicInfoData($aryDisp); // セッションデータ格納
				break;

			//初期表示
			default:
					$aryDisp["transcode_flg"] = 2; //設定2
				break;
		}
		
		if(false == $blnAdminErrorFlg){
			$strAdminMenuSelectBox = $objBasicInfo->getAdminMenuSelectBox($aryDisp["admin_menu_cd"]);
			require_once 'dspAdminMenuSelect.php';
		}else if(false == $blnChannelErrorFlg){
			require_once 'dspChannel.php';
		}else{
			require_once 'dspBasicInfo.php';
		}
	}
}


?>