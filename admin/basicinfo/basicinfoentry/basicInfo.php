<?php
/**
 *	基本情報メンテナンス画面
 *
 *	基本情報メンテナンス画面の制御クラスを呼び出す
 *
 *	@author			Mouri 2012/02/19
 *	@version		1.0
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/include.php";
require_once "./ctlBasicInfo.php";
require_once "./clsBasicInfo.php";
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/value_checker/execute_classes/clsAdminBasicInfoChecker.php" );

//基本情報メンテナンス制御クラス呼出し
$objCtlBasicInfo = new ctlBasicInfo();
$objCtlBasicInfo->process();
?>