<?php
/**
 *	基本情報メンテナンス画面のモデルクラス
 *
 *	基本情報メンテナンス画面のビジネスロジックについて記述
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 */
class clsBasicInfo{
	
	private $_aryDisp;
	
	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct( $aryDisp ){
		$this->_aryDisp = $aryDisp;
	}

	/**
	 * 
	 *
	 **/
	public function setAryData( $aryDisp ){
		$this->_aryDisp = $aryDisp;
	}
	
	/**
	 * 基本情報を取得します。
	 * 
	 * @return 基本情報
	 */
	public function getBasicInfo(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSelectSql =
<<<SQL
	SELECT 
		contact_id,
		contact_name,
		(case when DATE_FORMAT(start_day,'%Y/%m/%d') = '0000/00/00' then null else DATE_FORMAT(start_day,'%Y/%m/%d') end) as start_day,
		system_dir,
		account_excel_name,
		login_tbl_name,
		mail_error_address,
		comment_flg,
		judge_flg,
		report_flg,
		encode_pc,
		encode_smp,
		transcode_flg,
		templete,
		menu_category_flg,
		fromAddr,
		fromAddrName,
		signature,
		millvi_id,
		millvi_user_id,
		millvi_password,
		movie_folder,
		system_name
	FROM
		contact_mst
	WHERE
		contact_id = :contact_id
SQL;
		$aryParameters = array(
			":contact_id" => base64_decode( $this->_aryDisp["contact_id"] )
		);
		
		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult[0];
	}
	/**
	 * チャンネル情報取得 
	 * 
	 * @param param
	 * @return return
	 */
	public function getChannelData(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSelectSql =
<<<SQL
	SELECT 
		contact_id,
		menu_category_cd,
		menu_category_name,
		user_enterable_flg,
		sort_no
	FROM
		menu_category_mst
	WHERE
		contact_id = :contact_id
	AND
		delete_flg = 0
SQL;
		$aryParameters = array(
			":contact_id" => base64_decode( $this->_aryDisp["contact_id"] )
		);
		
		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult;
	}

	/**
	 * 管理メニュー情報取得 
	 * 
	 * @param param
	 * @return return
	 */
	public function getAdminMenuListData(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSelectSql =
<<<SQL
	SELECT 
		contact_id,
		menu_id,
		menu_name
	FROM
		admin_menu_mst
	WHERE
		contact_id = :contact_id
	AND
		delete_flg = 0
SQL;
		$aryParameters = array(
			":contact_id" => base64_decode( $this->_aryDisp["contact_id"] )
		);
		
		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult;
	}

	/**
	 * 権限情報取得 
	 * 
	 * @param param
	 * @return return
	 */
	public function getAuthListData(){
	//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSelectSql =
<<<SQL
	SELECT 
		contact_id,
		user_kbn,
		auth_cd,
		auth_name,
		sort_no
	FROM
		authority_mst
	WHERE
		contact_id = :contact_id
	AND
		delete_flg = 0
SQL;
		$aryParameters = array(
			":contact_id" => base64_decode( $this->_aryDisp["contact_id"] )
		);
		
		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult;
	}



	/**
	 * 基本情報を登録します。
	 * 
	 * @return true:成功、false:失敗
	 */
	public function addBasicInfo(){
		
		// 新規登録フラグ 1:新規 2:更新
		$intNewDataFlg = 1;
		
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//トランザクション開始
		$objDatabase->execTransaction();
		
		$strSelectSql =
<<<SQL
	SELECT
		COALESCE( MAX( contact_id ), 0 ) + 1 AS contact_id
	FROM
		contact_mst
SQL;
		
		$aryResult = $objDatabase->pullDbData( $strSelectSql, array() );
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		$contact_id = $aryResult[0]["contact_id"];
		
		$strInsertSql =
<<<SQL
	INSERT INTO contact_mst (
		contact_id,
		contact_name,
		encode_pc,
		encode_smp,
		templete,
		menu_category_flg,
		fromAddr,
		fromAddrName,
		signature,
		config_flg,
		millvi_id,
		millvi_user_id,
		millvi_password,
		movie_folder,
		start_day,
		system_dir,
		account_excel_name,
		login_tbl_name,
		mail_error_address,
		comment_flg,
		judge_flg,
		report_flg,
		system_name,
		delete_flg,
		transcode_flg,
		reg_id,
		reg_name,
		reg_time,
		upd_id,
		upd_name,
		upd_time
	) VALUES (
		:contact_id,
		:contact_name,
		:encode_pc,
		:encode_smp,
		:templete,
		:menu_category_flg,
		:fromAddr,
		:fromAddrName,
		:signature,
		:config_flg,
		:millvi_id,
		:millvi_user_id,
		:millvi_password,
		:movie_folder,
		:start_day,
		:system_dir,
		:account_excel_name,
		:login_tbl_name,
		:mail_error_address,
		:comment_flg,
		:judge_flg,
		:report_flg,
		:system_name,
		:delete_flg,
		:transcode_flg,
		:reg_id,
		:reg_name,
		:reg_time,
		:upd_id,
		:upd_name,
		:upd_time
	)
SQL;

		$aryParameters = array(
			":contact_id"         => $contact_id,
			":contact_name"       => $this->_aryDisp["contact_name"],
			":encode_pc"          => $this->_aryDisp["encode_pc"],
			":encode_smp"         => $this->_aryDisp["encode_smp"],
			":templete"           => $this->_aryDisp["templete"],
			":menu_category_flg"  => $this->_aryDisp["menu_category_flg"],
			":fromAddr"           => $this->_aryDisp["sender_address"],
			":fromAddrName"       => $this->_aryDisp["sender_name"],
			":signature"          => $this->_aryDisp["signature"],
			":config_flg"         => 1,
			":millvi_id"          => $this->_aryDisp["millvi_id"],
			":millvi_user_id"     => $this->_aryDisp["millvi_user_id"],
			":millvi_password"    => $this->_aryDisp["millvi_password"],
			":movie_folder"       => $this->_aryDisp["movie_folder"],
			":start_day"          => $this->_aryDisp["start_day"],
			":system_dir"         => "/" . $this->_aryDisp["system_dir"],
			":account_excel_name" => $this->_aryDisp["account_excel_name"],	//2013/06/28ここから
			":login_tbl_name"     => $this->_aryDisp["login_tbl_name"],		//
			":mail_error_address" => $this->_aryDisp["mail_error_address"],	//
			":comment_flg"        => $this->_aryDisp["comment_flg"],		//
			":judge_flg"          => $this->_aryDisp["judge_flg"],			//
			":report_flg"         => $this->_aryDisp["report_flg"],			//2013/06/28ここまで追加
			":transcode_flg"      => $this->_aryDisp["transcode_flg"],		//20130724 Nishi add
			":system_name"        => $this->_aryDisp["system_name"],		// 2013-10-18 kanata add
			":delete_flg"         => 0,
			":reg_id"             => clsLoginSession::getUserId(),
			":reg_name"           => clsLoginSession::getFullName(),
			":reg_time"           => clsCommonFunction::getCurrentDate(),
			":upd_id"             => clsLoginSession::getUserId(),
			":upd_name"           => clsLoginSession::getFullName(),
			":upd_time"           => clsCommonFunction::getCurrentDate()
		);
		
		$aryResult = $objDatabase->addDbData( $strInsertSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		// 履歴登録
		$strInsertSql =
<<<SQL
	INSERT INTO contact_mst_past
		SELECT
			:past_number,
			:upd_kbn,
			:past_reg_id,
			contact_mst.*
		FROM
			contact_mst
		WHERE
			contact_id = :contact_id
SQL;
		$aryParameters = array(
			":past_number" => 1,
			":upd_kbn"     => 1,
			":past_reg_id" => clsLoginSession::getUserId(),
			":contact_id"  => $contact_id,
		);
		
		$aryResult = $objDatabase->addDbData( $strInsertSql , $aryParameters );
		$aryResult = $objDatabase->isError( $aryResult );

		if(false === $aryResult){	// 成功した場合、チャンネルを登録する。
			$aryResult = self::channel($intNewDataFlg,$contact_id);
			if(false === $aryResult){	// チャンネル登録に成功した場合、管理メニューを登録する。
				$aryResult = self::adminMenu($intNewDataFlg,$contact_id);
				if(false === $aryResult){	// 管理メニュー登録に成功した場合、権限を登録する。
					$aryResult = self::authority($intNewDataFlg,$contact_id);
					if(false === $aryResult){	// 権限登録に成功した場合、違反リストを登録する。
						$aryResult = self::outList($contact_id);
						if(false === $aryResult){	// 違反リスト登録に成功した場合、視聴者、管理者のデフォルト権限を設定する。
							$aryResult = self::defaultGroup($intNewDataFlg,$contact_id);
						}
					}
				}
			}
		}
		
		if( true === $aryResult ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}
	


	/**
	 * 基本情報を更新します。
	 * 
	 * @return true:成功、false:失敗
	 */
	public function updateBasicInfo(){

		// 新規登録フラグ 1:新規 2:更新
		$intNewDataFlg = 2;

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//トランザクション開始
		$objDatabase->execTransaction();
		
		$strUpdateSql =
<<<SQL
	UPDATE
		contact_mst
	SET
		contact_name       = :contact_name,
		encode_pc          = :encode_pc,
		encode_smp         = :encode_smp,
		templete           = :templete,
		menu_category_flg  = :menu_category_flg,
		fromAddr           = :fromAddr,
		fromAddrName       = :fromAddrName,
		signature          = :signature,
		millvi_id          = :millvi_id,
		millvi_user_id     = :millvi_user_id,
		millvi_password    = :millvi_password,
		movie_folder       = :movie_folder,
		start_day          = :start_day,
		system_dir         = :system_dir,
		account_excel_name = :account_excel_name,
		login_tbl_name     = :login_tbl_name,
		mail_error_address = :mail_error_address,
		comment_flg        = :comment_flg,
		judge_flg          = :judge_flg,
		report_flg         = :report_flg,
		transcode_flg      = :transcode_flg,
		system_name        = :system_name,
		upd_id             = :upd_id,
		upd_name           = :upd_name,
		upd_time           = :upd_time
	WHERE
		contact_id        = :contact_id
SQL;

		$aryParameters = array(
			":contact_name"       => $this->_aryDisp["contact_name"],
			":encode_pc"          => $this->_aryDisp["encode_pc"],
			":encode_smp"         => $this->_aryDisp["encode_smp"],
			":templete"           => $this->_aryDisp["templete"],
			":menu_category_flg"  => $this->_aryDisp["menu_category_flg"],
			":fromAddr"           => $this->_aryDisp["sender_address"],
			":fromAddrName"       => $this->_aryDisp["sender_name"],
			":signature"          => $this->_aryDisp["signature"],
			":millvi_id"          => $this->_aryDisp["millvi_id"],
			":millvi_user_id"     => $this->_aryDisp["millvi_user_id"],
			":millvi_password"    => $this->_aryDisp["millvi_password"],
			":movie_folder"       => $this->_aryDisp["movie_folder"],
			":start_day"          => $this->_aryDisp["start_day"],
			":system_dir"         => "/" . $this->_aryDisp["system_dir"],
			":account_excel_name" => $this->_aryDisp["account_excel_name"],	//2013/06/28ここから
			":login_tbl_name"     => $this->_aryDisp["login_tbl_name"],		//
			":mail_error_address" => $this->_aryDisp["mail_error_address"],	//
			":comment_flg"        => $this->_aryDisp["comment_flg"],		//
			":judge_flg"          => $this->_aryDisp["judge_flg"],			//
			":report_flg"         => $this->_aryDisp["report_flg"],			//2013/06/28ここまで追加
			":transcode_flg"      => $this->_aryDisp["transcode_flg"],		//20130724 Nishi add
			":system_name"        => $this->_aryDisp["system_name"],		// 2013-10-18 kanata add
			":upd_id"             => clsLoginSession::getUserId(),
			":upd_name"           => clsLoginSession::getFullName(),
			":upd_time"           => clsCommonFunction::getCurrentDate(),
			":contact_id"         => base64_decode( $this->_aryDisp["contact_id"] )
		);
		
		$aryResult = $objDatabase->updDbData( $strUpdateSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		// 履歴登録
		$strInsertSql =
<<<SQL
	INSERT INTO contact_mst_past
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM contact_mst_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			:past_reg_id,
			contact_mst.*
		FROM
			contact_mst
		WHERE
			contact_id = :contact_id
SQL;
		$aryParameters = array(
			":upd_kbn"     => 2,
			":past_reg_id" => clsLoginSession::getUserId(),
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);

		$contact_id = base64_decode( $this->_aryDisp["contact_id"] );
		
		$aryResult = $objDatabase->addDbData( $strInsertSql, $aryParameters );
		$aryResult = $objDatabase->isError( $aryResult );


		if(false == $aryResult){	// 成功した場合、チャンネルを登録する。
			$aryResult = self::channelUpdate($intNewDataFlg,$contact_id);
			if(false == $aryResult){	// チャンネル登録に成功した場合、管理メニューを登録する。
				$aryResult = self::adminMenuUpdate($intNewDataFlg,$contact_id);

				if(false == $aryResult){	// 管理メニュー登録に成功した場合、更新する前のグループ別履歴権限を登録する。
					$aryResult = self::groupAuthConfigUpdate($intNewDataFlg,$contact_id);
					if(false == $aryResult){	// 成功した場合、権限を登録する。
						$aryResult = self::authorityUpdate($intNewDataFlg,$contact_id);
						
						if(false == $aryResult){ // 権限の登録に成功した場合、更新後のグループ別権限を登録する。
							$aryResult = self::delGroupAuthConfigData($contact_id);
							if(false == $aryResult){

								for($i = 0; $i < 2; $i++){

									$intUserKbn = $i + 1; // ユーザ区分　1：管理者　2：視聴者

									if( 2 == $intUserKbn){ // 視聴者
										$intGroupCd = 1;
									}else if( 1 == $intUserKbn){ // 管理者
										$intGroupCd = 2;
									}

									$aryResult = self::groupAuthConfig($intNewDataFlg,$contact_id,$intUserKbn,$intGroupCd);
								}
							}
						}

					}
				}
			}
		}

		if( true === $aryResult ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}


	/**
	 * チャンネル登録 
	 * 
	 * @param param
	 * @return return
	 */
	public function channel($intNewDataFlg,$contact_id){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$intChannelCount = count($this->_aryDisp['menu_category_name']);

		// チャンネルを設定しなかった場合--------------------
		$strDefaultChannelName = "投稿";

		if(0 == $intChannelCount){
			$intChannelCount = 1;
			$this->_aryDisp['menu_category_name'][0] = $strDefaultChannelName;
			$this->_aryDisp['user_enterable_flg'][0] = 1;
		}
		//------------------------------------------

		// 登録されたチャンネル件数文ループ
		for($i = 0; $i < $intChannelCount; $i++){
		
			$strChannelSql =
<<<SQL
		INSERT INTO menu_category_mst (
			contact_id,
			menu_category_cd,
			menu_category_name,
			user_enterable_flg,
			sort_no,
			delete_flg,
			reg_id,
			reg_name,
			reg_time,
			upd_id,
			upd_name,
			upd_time
		) VALUES (
			:contact_id,
			:menu_category_cd,
			:menu_category_name,
			:user_enterable_flg,
			:sort_no,
			:delete_flg,
			:reg_id,
			:reg_name,
			:reg_time,
			:upd_id,
			:upd_name,
			:upd_time
		)
SQL;
			$aryParameters = array(
				":contact_id"         => $contact_id,
				":menu_category_cd"   => $i+1,
				":menu_category_name" => $this->_aryDisp['menu_category_name'][$i],
				":user_enterable_flg" => $this->_aryDisp['user_enterable_flg'][$i],
				":sort_no"            => $i+1,
				":delete_flg"         => 0,
				":reg_id"             => clsLoginSession::getUserId(),
				":reg_name"           => clsLoginSession::getFullName(),
				":reg_time"           => clsCommonFunction::getCurrentDate(),
				":upd_id"             => clsLoginSession::getUserId(),
				":upd_name"           => clsLoginSession::getFullName(),
				":upd_time"           => clsCommonFunction::getCurrentDate()
			);

			$aryResult = $objDatabase->addDbData($strChannelSql,$aryParameters);
			$aryResult = $objDatabase->isError( $aryResult );

		}

		if(false == $aryResult){
			$aryResult = self::channelPast($intNewDataFlg,$contact_id,$intChannelCount);
		}

		return $aryResult;
	}

	/**
	 * チャンネル更新処理 
	 * 
	 * @param param
	 * @return return
	 */
	public function channelUpdate($intNewDataFlg,$contact_id){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strChannelUpdateSql =
<<<SQL
		UPDATE
			menu_category_mst
		SET
			delete_flg = 1
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id" => $contact_id
		);

		$aryResult = $objDatabase->updDbData($strChannelUpdateSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		if(false == $aryResult){
			$aryResult = self::channelPast($intNewDataFlg,$contact_id,0);
			if(false == $aryResult){
				$aryResult = self::delChannelData($contact_id);
				if(false == $aryResult){
					$aryResult = self::channel($intNewDataFlg,$contact_id);
				}
			}
		}

		return $aryResult;
	}


	/**
	 * チャンネル履歴 
	 * 
	 * @param param
	 * @return return
	 */
	public function channelPast($intNewDataFlg,$contact_id,$intChannelCount){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		if( 2 == $intNewDataFlg ){
			$aryData = self::getChannelData();
			$intCount = count($aryData);
		}else{
			$intCount = $intChannelCount;
		}

		for($i = 0;$i < $intCount; $i++){

			if( 1 == $intNewDataFlg ){
				$aryData[$i]['menu_category_cd'] = $i+1;
			}

			$strPastSql =
<<<SQL
	INSERT INTO menu_category_mst_past
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM menu_category_mst_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			:past_reg_id,
			menu_category_mst.*
		FROM
			menu_category_mst
		WHERE
			contact_id = :contact_id
		AND
			menu_category_cd = :menu_category_cd
SQL;
			$aryParameters = array(
				":upd_kbn"     => $intNewDataFlg,
				":past_reg_id" => clsLoginSession::getUserId(),
				":contact_id"  => $contact_id,
				":menu_category_cd" => $aryData[$i]['menu_category_cd']
			);
		

		$aryResult = $objDatabase->addDbData($strPastSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		}
		
		return $aryResult;
	}

	/**
	 * 管理者メニュー登録 
	 * 
	 * @param param
	 * @return return
	 */
	public function adminMenu($intNewDataFlg,$contact_id){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$aryAdminMenu = clsCommonFunction::getAdminMenu();
		$aryMenuId = array();
		$aryMenuName = array();

		$intAdminMenuCount = count($this->_aryDisp['admin_menu_cd']);

		for($j = 0; $j < $intAdminMenuCount; $j++){
			foreach ($aryAdminMenu as $key => $value) {
				if($key == $this->_aryDisp['admin_menu_cd'][$j]){
					$aryMenuId[$j] = $key;
					$aryMenuName[$j] = $value;
				}
			}
		}

		// 選択したメニュー分ループ
		for($i = 0; $i < $intAdminMenuCount; $i++){

			$strAdminMenuSql =
<<<SQL
		INSERT INTO admin_menu_mst (
			contact_id,
			menu_id,
			menu_name,
			delete_flg,
			reg_id,
			reg_name,
			reg_time,
			upd_id,
			upd_name,
			upd_time
		) VALUES (
			:contact_id,
			:menu_id,
			:menu_name,
			:delete_flg,
			:reg_id,
			:reg_name,
			:reg_time,
			:upd_id,
			:upd_name,
			:upd_time
		)
SQL;
			$aryParameters = array(
				":contact_id"         => $contact_id,
				":menu_id"            => $aryMenuId[$i],
				":menu_name"          => $aryMenuName[$i],
				":delete_flg"         => 0,
				":reg_id"             => clsLoginSession::getUserId(),
				":reg_name"           => clsLoginSession::getFullName(),
				":reg_time"           => clsCommonFunction::getCurrentDate(),
				":upd_id"             => clsLoginSession::getUserId(),
				":upd_name"           => clsLoginSession::getFullName(),
				":upd_time"           => clsCommonFunction::getCurrentDate()
			);
		
			$aryResult = $objDatabase->addDbData($strAdminMenuSql,$aryParameters);
			$aryResult = $objDatabase->isError( $aryResult );
		}

		if(false == $aryResult){
			$aryResult = self::adminMenuPast($intNewDataFlg,$contact_id,$intAdminMenuCount,$aryMenuId);
		}

		return $aryResult;
	}

	/**
	 * 管理者メニュー更新処理 
	 * 
	 * @param param
	 * @return return
	 */
	public function adminMenuUpdate($intNewDataFlg,$contact_id){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strChannelUpdateSql =
<<<SQL
		UPDATE
			admin_menu_mst
		SET
			delete_flg = 1
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id" => $contact_id
		);

		$aryResult = $objDatabase->updDbData($strChannelUpdateSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		if(false == $aryResult){
			$aryResult = self::AdminMenuPast($intNewDataFlg,$contact_id,0,array());
			if(false == $aryResult){
				$aryResult = self::delAdminMenuData($contact_id);
				if(false == $aryResult){
					$aryResult = self::adminMenu($intNewDataFlg,$contact_id);
				}
			}
		}

		return $aryResult;
	}

	/**
	 * 管理メニュー履歴 
	 * 
	 * @param param
	 * @return return
	 */
	public function adminMenuPast($intNewDataFlg,$contact_id,$intAdminMenuCount,$aryMenuId){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		if( 2 == $intNewDataFlg ){
			$aryData = self::getAdminMenuListData();
			$intCount = count($aryData);
		}else{
			$intCount = $intAdminMenuCount;
		}

		for($i = 0;$i < $intCount; $i++){

			if( 1 == $intNewDataFlg ){
				$aryData[$i]["menu_id"] = $aryMenuId[$i];
			}

			$strPastSql =
<<<SQL
	INSERT INTO admin_menu_mst_past
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM admin_menu_mst_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			:past_reg_id,
			admin_menu_mst.*
		FROM
			admin_menu_mst
		WHERE
			contact_id = :contact_id
		AND
			menu_id = :menu_id
SQL;
			$aryParameters = array(
				":upd_kbn"     => $intNewDataFlg,
				":past_reg_id" => clsLoginSession::getUserId(),
				":contact_id"  => $contact_id,
				":menu_id"     => $aryData[$i]["menu_id"]
			);
		

		$aryResult = $objDatabase->addDbData($strPastSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		}

		return $aryResult;

	}



	/**
	 * 権限選択 
	 * 
	 * @param param
	 * @return return
	 */
	public function authority($intNewDataFlg,$contact_id){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		// 管理者権限
		$aryAdminMenu = clsCommonFunction::getAdminMenu();
		$aryAdminNum = clsCommonFunction::setAdminMenu();		// 2013/09/05 Nishi Add 
		$aryAuthData = array();
		$intDataCount = 0;

		// ユーザー権限
		$aryUserAuthList = clsCommonFunction::getUserAuthList();
		for($i = 0; $i < count($aryUserAuthList); $i++){
			foreach ($aryUserAuthList as $key => $value) {
				if(($i+1) == $key){
					$aryAuthData[$i]["user_kbn"] = 2;
					$aryAuthData[$i]["auth_name"] = $value;
					$aryAuthData[$i]["delete_flg"] = 0;
					$aryAuthData[$i]["auth_cd"] = $i+1;		// 2013/09/05 Nishi Add 
				}
			}
		}

		$intDataCount = count($aryAuthData);

		for($j = 0; $j < count($this->_aryDisp['admin_menu_cd']); $j++){
			foreach ($aryAdminMenu as $key => $value) {
				if($key == $this->_aryDisp['admin_menu_cd'][$j]){
					$aryAuthData[$intDataCount]["user_kbn"] = 1;
					$aryAuthData[$intDataCount]["auth_name"] = $value; //権限名設定
					$aryAuthData[$intDataCount]["delete_flg"] = 0;
					foreach ($aryAdminNum as $key => $value) {						// 2013/09/05 Nishi Add
						if($key == $this->_aryDisp['admin_menu_cd'][$j]){			//
							$aryAuthData[$intDataCount]["auth_cd"] = $value;		//
						}															//
					}																// 2013/09/05 Nishi Add
					$intDataCount++;
				}
			}
		}

		for($k = 0; $k < count($aryAuthData); $k++){

			//ユーザー権限登録
			$strAuthoritySql =
<<<SQL
			INSERT INTO authority_mst (
				contact_id,
				user_kbn,
				auth_cd,
				auth_name,
				sort_no,
				delete_flg,
				reg_id,
				reg_name,
				reg_time,
				upd_id,
				upd_name,
				upd_time
			) VALUES (
				:contact_id,
				:user_kbn,
				:auth_cd,
				:auth_name,
				:sort_no,
				:delete_flg,
				:reg_id,
				:reg_name,
				:reg_time,
				:upd_id,
				:upd_name,
				:upd_time
			)
SQL;
			$aryParameters = array(
				":contact_id"	=> $contact_id,
				":user_kbn"		=> $aryAuthData[$k]["user_kbn"],
				":auth_cd"		=> $aryAuthData[$k]["auth_cd"], //$k+1,
				":auth_name"	=> $aryAuthData[$k]["auth_name"],
				":sort_no"		=> $k+1,
				":delete_flg"	=> $aryAuthData[$k]["delete_flg"],
				":reg_id"		=> clsLoginSession::getUserId(),
				":reg_name"		=> clsLoginSession::getFullName(),
				":reg_time"		=> clsCommonFunction::getCurrentDate(),
				":upd_id"		=> clsLoginSession::getUserId(),
				":upd_name"		=> clsLoginSession::getFullName(),
				":upd_time"		=> clsCommonFunction::getCurrentDate()
				);

			$aryResult = $objDatabase->addDbData($strAuthoritySql,$aryParameters);
			$aryResult = $objDatabase->isError( $aryResult );
		}

		$intAuthDataCount = count($aryAuthData);

		if(false == $aryResult){
			$aryResult = self::authorityPast($intNewDataFlg,$contact_id,$intAuthDataCount);
		}

		return $aryResult;
	}

	/**
	 * 権限更新処理 
	 * 
	 * @param param
	 * @return return
	 */
	public function authorityUpdate($intNewDataFlg,$contact_id){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strChannelUpdateSql =
<<<SQL
		UPDATE
			authority_mst
		SET
			delete_flg = 1
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id" => $contact_id
		);

		$aryResult = $objDatabase->updDbData($strChannelUpdateSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		if(false == $aryResult){
			$aryResult = self::authorityPast($intNewDataFlg,$contact_id,0);
			if(false == $aryResult){
				$aryResult = self::delAuthorityData($contact_id);
				if(false == $aryResult){
					$aryResult = self::authority($intNewDataFlg,$contact_id);
				}
			}
		}

		return $aryResult;
	}

	/**
	 * 権限履歴 
	 * 
	 * @param param
	 * @return return
	 */
	public function authorityPast($intNewDataFlg,$contact_id,$intAuthDataCount){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		if( 2 == $intNewDataFlg){
			$aryData = self::getAuthListData();
			$intCount = count($aryData);
		}else{
			$intCount = $intAuthDataCount;
		}

		for($i = 0;$i < $intCount; $i++){

			if( 1 == $intNewDataFlg ){
				$aryData[$i]["auth_cd"] = $i+1;
			}

			$strPastSql =
<<<SQL
	INSERT INTO authority_mst_past
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM authority_mst_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			:past_reg_id,
			authority_mst.*
		FROM
			authority_mst
		WHERE
			contact_id = :contact_id
		AND
			auth_cd = :auth_cd
SQL;
			$aryParameters = array(
				":upd_kbn"     => $intNewDataFlg,
				":past_reg_id" => clsLoginSession::getUserId(),
				":contact_id"  => $contact_id,
				":auth_cd"     => $aryData[$i]["auth_cd"]
			);
		
		$aryResult = $objDatabase->addDbData($strPastSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );
		
		}

		return $aryResult;
	}





	/**
	 * 違反リスト登録 
	 * 
	 * @param param
	 * @return return
	 */
	public function outList($contact_id){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$aryOutList = clsCommonFunction::getOutList();
		$aryOutData = array();

		foreach ($aryOutList as $key => $value) {
			$aryOutData[$key]["out_cd"] = $key;
			$aryOutData[$key]["out_name"] = $value;
		}

		for($k = 0; $k < count($aryOutList); $k++){

			//ユーザー権限登録
			$strOutSql =
<<<SQL
			INSERT INTO out_mst (
				contact_id,
				out_cd,
				out_name,
				delete_flg,
				reg_id,
				reg_name,
				reg_time,
				upd_id,
				upd_name,
				upd_time
			) VALUES (
				:contact_id,
				:out_cd,
				:out_name,
				:delete_flg,
				:reg_id,
				:reg_name,
				:reg_time,
				:upd_id,
				:upd_name,
				:upd_time
			)
SQL;
			$aryParameters = array(
				":contact_id"	=> $contact_id,
				":out_cd"		=> $aryOutData[$k+1]["out_cd"],
				":out_name"		=> $aryOutData[$k+1]["out_name"],
				":delete_flg"	=> 0,
				":reg_id"		=> clsLoginSession::getUserId(),
				":reg_name"		=> clsLoginSession::getFullName(),
				":reg_time"		=> clsCommonFunction::getCurrentDate(),
				":upd_id"		=> clsLoginSession::getUserId(),
				":upd_name"		=> clsLoginSession::getFullName(),
				":upd_time"		=> clsCommonFunction::getCurrentDate()
				);

			$aryResult = $objDatabase->addDbData($strOutSql,$aryParameters);
			$aryResult = $objDatabase->isError( $aryResult );
		}

		return $aryResult;
	}




	/**
	 * 視聴者、管理者デフォルト権限設定
	 * 
	 * 2013/05/09　Nishi 追加
	 * @param param
	 * @return return
	 */
	public function defaultGroup($intNewDataFlg,$contact_id){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		// 視聴者デフォルト権限情報
		$aryGroupData[0] = array(
									"group_cd"			=> 1,
									"group_name"		=> "視聴者デフォルト権限",
									"user_kbn"			=> 2,
									"user_default_flg"	=> 1,
									"admin_default_flg"	=> 0
								);

		// 管理者全権限情報
		$aryGroupData[1] = array(
									"group_cd"			=> 2,
									"group_name"		=> "管理者全権限",
									"user_kbn"			=> 1,
									"user_default_flg"	=> 0,
									"admin_default_flg"	=> 1
								);

		$intDefaultNum = 2; // 初期設定する権限の数
		$aryGroupAuthConfigResult = false;

		for($i = 0; $i < $intDefaultNum; $i++){

			//$aryGroupAuthConfigResultがtrueならばクエリエラーなのでループを抜ける
			if($aryGroupAuthConfigResult){
				$aryResult = $aryGroupAuthConfigResult;
				break;
			}

			//ユーザー権限登録
			$strGroupSql =
<<<SQL
			INSERT INTO group_mst (
				contact_id,
				group_cd,
				group_name,
				user_kbn,
				user_default_flg,
				admin_default_flg,
				delete_flg,
				reg_id,
				reg_name,
				reg_time,
				upd_id,
				upd_name,
				upd_time
			) VALUES (
				:contact_id,
				:group_cd,
				:group_name,
				:user_kbn,
				:user_default_flg,
				:admin_default_flg,
				:delete_flg,
				:reg_id,
				:reg_name,
				:reg_time,
				:upd_id,
				:upd_name,
				:upd_time
			)
SQL;
			$aryParameters = array(
				":contact_id"			=> $contact_id,
				":group_cd"				=> $aryGroupData[$i]["group_cd"],
				":group_name"			=> $aryGroupData[$i]["group_name"],
				":user_kbn"				=> $aryGroupData[$i]["user_kbn"],
				":user_default_flg"		=> $aryGroupData[$i]["user_default_flg"],
				":admin_default_flg"	=> $aryGroupData[$i]["admin_default_flg"],
				":delete_flg"			=> 0 ,
				":reg_id"				=> clsLoginSession::getUserId(),
				":reg_name"				=> clsLoginSession::getFullName(),
				":reg_time"				=> clsCommonFunction::getCurrentDate(),
				":upd_id"				=> clsLoginSession::getUserId(),
				":upd_name"				=> clsLoginSession::getFullName(),
				":upd_time"				=> clsCommonFunction::getCurrentDate()
				);

			$aryResult = $objDatabase->addDbData($strGroupSql,$aryParameters);
			$aryResult = $objDatabase->isError( $aryResult );

			if(false == $aryResult){
				//履歴登録。
				$aryResult = self::groupMstPast($contact_id,$aryGroupData[$i]["group_cd"]);
			}

			// クエリエラーが発生していなければ、グループ別権限設定を行う。
			if(false == $aryResult){
				$aryGroupAuthConfigResult = self::groupAuthConfig($intNewDataFlg,$contact_id,$aryGroupData[$i]["user_kbn"],$aryGroupData[$i]["group_cd"]);
			}
		} //for文

		return $aryResult;
	}



	/**
	 * 視聴者、管理者グループ別権限設定更新
	 * 
	 * @param param
	 * @return return
	 */
	public function groupAuthConfigUpdate($intNewDataFlg,$contact_id){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strAuthConfigUpdateSql =
<<<SQL
		UPDATE
			group_auth_config_mst
		SET
			delete_flg = 1
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id" => $contact_id
		);

		$aryResult = $objDatabase->updDbData($strAuthConfigUpdateSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		if(false == $aryResult){
			for($i = 0; $i < 2; $i++){

				$intUserKbn = $i + 1; // ユーザ区分　1：管理者　2：視聴者

				$aryAuthCd = self::groupAuthCd($contact_id,$intUserKbn); // ユーザ区分別グループコード取得

				if( 2 == $intUserKbn){ // 視聴者
					$intGroupCd = 1;
				}else if( 1 == $intUserKbn){ // 管理者
					$intGroupCd = 2;
				}

				for($j = 0;$j < count($aryAuthCd); $j++){
					$aryResult = self::groupAuthConfigMstPast($intNewDataFlg,$contact_id,$intGroupCd,$aryAuthCd[$j]["auth_cd"]);
				}
			}
		}

		return $aryResult;
	}

	/**
	 * 視聴者、管理者グループ別権限コード取得
	 * 
	 * 2013/05/09　Nishi 追加
	 * @param param
	 * @return return
	 */
	public function groupAuthCd($contact_id,$intUserKbn){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strUserKbn = '';
		if( 2 == $intUserKbn ){
			$strUserKbn = 'AND user_kbn = ' . $intUserKbn;
		}

		$selectAuthCdSql =
<<<SQL
		SELECT
			auth_cd
		FROM
			authority_mst
		WHERE
			contact_id = :contact_id
		{$strUserKbn}
SQL;

		$aryAuthCdParameters = array(
					":contact_id"			=> $contact_id
				);

		$aryAuthCd = $objDatabase->pullDbData( $selectAuthCdSql, $aryAuthCdParameters );

		return $aryAuthCd;
	}


	/**
	 * 視聴者、管理者グループ別権限設定
	 * 
	 * 2013/05/09　Nishi 追加
	 * @param param
	 * @return return
	 */
	public function groupAuthConfig($intNewDataFlg,$contact_id,$intUserKbn,$intGroupCd){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$aryAuthCd = self::groupAuthCd($contact_id,$intUserKbn);

		for($j = 0; $j < count($aryAuthCd); $j++){
			//ユーザー権限登録
			$strGroupAuthConfigSql =
<<<SQL
			INSERT INTO group_auth_config_mst (
				contact_id,
				group_cd,
				auth_cd,
				delete_flg,
				reg_id,
				reg_name,
				reg_time,
				upd_id,
				upd_name,
				upd_time
			) VALUES (
				:contact_id,
				:group_cd,
				:auth_cd,
				:delete_flg,
				:reg_id,
				:reg_name,
				:reg_time,
				:upd_id,
				:upd_name,
				:upd_time
			)
SQL;
			$aryGroupAuthConfigParameters = array(
				":contact_id"			=> $contact_id,
				":group_cd"				=> $intGroupCd,
				":auth_cd"				=> $aryAuthCd[$j]["auth_cd"],
				":delete_flg"			=> 0,
				":reg_id"				=> clsLoginSession::getUserId(),
				":reg_name"				=> clsLoginSession::getFullName(),
				":reg_time"				=> clsCommonFunction::getCurrentDate(),
				":upd_id"				=> clsLoginSession::getUserId(),
				":upd_name"				=> clsLoginSession::getFullName(),
				":upd_time"				=> clsCommonFunction::getCurrentDate()
				);

			$aryGroupAuthConfigResult = $objDatabase->addDbData($strGroupAuthConfigSql,$aryGroupAuthConfigParameters);
			$aryGroupAuthConfigResult = $objDatabase->isError( $aryGroupAuthConfigResult );

			if(false == $aryGroupAuthConfigResult){
				//履歴登録。
				$aryGroupAuthConfigResult = self::groupAuthConfigMstPast($intNewDataFlg,$contact_id,$intGroupCd,$aryAuthCd[$j]["auth_cd"]);
			}

			if($aryGroupAuthConfigResult){ break; } // エラーが発生した場合ループを抜ける。

		} //for文（第二）

		return $aryGroupAuthConfigResult;
	}




	/**
	 * グループマスタ履歴
	 * 
	 * @param param
	 * @return return
	 */
	public function groupMstPast($contact_id,$intGroupCd){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strPastSql =
<<<SQL
	INSERT INTO group_mst_past
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM group_mst_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			:past_reg_id,
			group_mst.*
		FROM
			group_mst
		WHERE
			contact_id = :contact_id
		AND
			group_cd = :group_cd
SQL;
			$aryParameters = array(
				":upd_kbn"     => 1,
				":past_reg_id" => clsLoginSession::getUserId(),
				":contact_id"  => $contact_id,
				":group_cd"    => $intGroupCd
			);
		

		$aryResult = $objDatabase->addDbData($strPastSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		return $aryResult;

	}




	/**
	 * グループ別権限マスタ履歴
	 * 
	 * @param param
	 * @return return
	 */
	public function groupAuthConfigMstPast($intNewDataFlg,$contact_id,$intGroupCd,$intAuthCd){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strPastSql =
<<<SQL
	INSERT INTO group_auth_config_mst_past
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM group_auth_config_mst_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			:past_reg_id,
			group_auth_config_mst.*
		FROM
			group_auth_config_mst
		WHERE
			contact_id = :contact_id
		AND
			group_cd = :group_cd
		AND
			auth_cd = :auth_cd
SQL;
			$aryParameters = array(
				":upd_kbn"     => $intNewDataFlg,
				":past_reg_id" => clsLoginSession::getUserId(),
				":contact_id"  => $contact_id,
				":group_cd"    => $intGroupCd,
				":auth_cd"     => $intAuthCd,
			);
		

		$aryResult = $objDatabase->addDbData($strPastSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		return $aryResult;

	}





	/**
	 * チャンネルデータ削除（更新用 
	 * 
	 * @param param
	 * @return return
	 */
	public function delChannelData($contact_id){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strDelChannelSql =
<<<SQL
		DELETE
		FROM
			menu_category_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => $contact_id
		);

		$aryResult = $objDatabase->delDbData($strDelChannelSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		return $aryResult;

	}

	/**
	 * 管理メニュー削除（更新用 
	 * 
	 * @param param
	 * @return return
	 */
	public function delAdminMenuData($contact_id){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strDelAduminMenuSql =
<<<SQL
		DELETE
		FROM
			admin_menu_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => $contact_id
		);

		$aryResult = $objDatabase->delDbData($strDelAduminMenuSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		return $aryResult;

	}

	/**
	 * 権限データ削除（更新用 
	 * 
	 * @param param
	 * @return return
	 */
	public function delAuthorityData($contact_id){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strDelAuthoritySql =
<<<SQL
		DELETE
		FROM
			authority_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => $contact_id
		);

		$aryResult = $objDatabase->delDbData($strDelAuthoritySql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		return $aryResult;

	}

	/**
	 * グループ別権限データ削除（更新用 
	 * 
	 * @param param
	 * @return return
	 */
	public function delGroupAuthConfigData($contact_id){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strDelGroupAuthConfigSql =
<<<SQL
		DELETE
		FROM
			group_auth_config_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => $contact_id
		);

		$aryResult = $objDatabase->delDbData($strDelGroupAuthConfigSql,$aryParameters);
		$aryResult = $objDatabase->isError( $aryResult );

		return $aryResult;

	}





	/**
	 * 管理メニューセレクトボックス生成 
	 * 
	 * @return 
	 */
	public function getAdminMenuSelectBox( $aryAdminMenuCd ){
		if( false == is_array( $aryAdminMenuCd ) ){
			$aryAdminMenuCd = array();
		}

		$aryAdminMenu = clsCommonFunction::getAdminMenu();
		$strAdminMenuSelectBox = "";
		foreach( $aryAdminMenu as $key => $val ){
			if( true == in_array( $key, $aryAdminMenuCd ) ){
				$strAdminMenuSelectBox .= "<label class='inputCheckBox'><span class='checkButton'></span>" . $val . "<input type='checkbox' name='admin_menu_cd[]' id='admin_menu_cd' value=" . $key . " checked='checked' ></label>";
			} else {
				$strAdminMenuSelectBox .= "<label class='inputCheckBox'><span class='checkButton'></span>" . $val . "<input type='checkbox' name='admin_menu_cd[]' id='admin_menu_cd' value=" . $key . "></label>";
			}
		}
		return $strAdminMenuSelectBox;
	}

	/**
	 * チャンネル文字列生成。
	 * 
	 * @param //チャンネル名
	 * @param //ユーザー設定
	 * @return return
	 */
	public function getChannelList($aryDisp){
		$strChannelFlg_0_List = '';
		$strChannelFlg_1_List = '';
		$strChannelFlg_0      = '';
		$strChannelFlg_1      = '';
		$strChannelList       = '';
		// タグを入力していた場合
		if( true == is_array( $aryDisp["menu_category_name"] ) ){
			$intChannelCount = COUNT( $aryDisp["menu_category_name"] );
			for( $i=0; $i<$intChannelCount; $i++ ){
				//最後でない場合カンマをつける。
				if( 0 != $i ){
					if( 0 == $aryDisp["user_enterable_flg"][$i] ){
						if('' != $strChannelFlg_0){
							$strChannelFlg_0 .= ',';
						}
					}else{
						if('' != $strChannelFlg_1){
							$strChannelFlg_1 .= ',';
						}
					}
				}
				//仕分け
				if( 0 == $aryDisp["user_enterable_flg"][$i] ){
					$strChannelFlg_0 .= $aryDisp["menu_category_name"][$i];
				}else{
					$strChannelFlg_1 .= $aryDisp["menu_category_name"][$i];
				}
			}

			if( '' != $strChannelFlg_0 ){
				$strChannelFlg_0_List = '管理者チャンネル:' . $strChannelFlg_0;
			}
			if( '' != $strChannelFlg_1 ){
				$strChannelFlg_1_List = '視聴者チャンネル:' . $strChannelFlg_1;
			}

			//視聴者チャンネルが設定されている場合改行して表示
			if( '' != $strChannelFlg_0 && '' != $strChannelFlg_1 ){
				$strChannelFlg_0_List .= "<br />";
			}

			$strChannelList = $strChannelFlg_0_List . $strChannelFlg_1_List;
		}else{
			$strChannelList = "不使用";
		}

		return $strChannelList;
	}

	/**
	 * 管理メニュー文字列生成。
	 * 
	 * @param //チャンネル名
	 * @param //ユーザー設定
	 * @return return
	 */
	public function getAdminMenuList($aryDisp){
		$strAdminMenuList = "";
		$aryAdminMenu = clsCommonFunction::getAdminMenu();
		// 
		if( true == is_array( $aryDisp["admin_menu_cd"] ) ){
			$intAdminMenuCount = COUNT( $aryDisp["admin_menu_cd"] );
			for( $i=0; $i<$intAdminMenuCount; $i++ ){
				if( 0 != $i ){
					$strAdminMenuList .= ',';
				}
				foreach ($aryAdminMenu as $key => $value) {
					if($key == $aryDisp["admin_menu_cd"][$i]){
						$strAdminMenuList .= $value;
					}
				}
			}
		}
		return $strAdminMenuList;
	}

	/**
	 * $_SESSIONデータ格納 
	 * 
	 * @param param
	 * @return return
	 */
	public function getBasicInfoData($aryDisp){
		foreach ($_SESSION['basic_info'] as $key => $val){
			$aryDisp[$key] = $_SESSION['basic_info'][$key];
		}
		return $aryDisp;
	}
}
?>