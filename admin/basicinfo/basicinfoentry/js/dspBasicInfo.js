//コンストラクタ
var dspBasicInfo = function() {};

//チャンネル登録画面へ
dspBasicInfo.prototype.Channel = function(){
	$("#action").val("channel");
	$("#basicInfoForm").attr("action", "./basicInfo.php");
	$("#basicInfoForm").submit();
}

//管理メニュー選択画面へ
dspBasicInfo.prototype.AdminMenu = function(){
	$("#action").val("admin_menu");
	$("#basicInfoForm").attr("action", "./basicInfo.php");
	$("#basicInfoForm").submit();
}

//確認画面
dspBasicInfo.prototype.Confirm = function(){
	$("#action").val("confirm");
	$("#basicInfoForm").attr("action", "./basicInfo.php");
	$("#basicInfoForm").submit();
}

//登録処理
dspBasicInfo.prototype.Regist = function(){
	$("#action").val("regist");
	$("#basicInfoForm").attr("action", "./basicInfo.php");
	$("#basicInfoForm").submit();
}

//戻る処理
dspBasicInfo.prototype.Back = function(){
	var strDispMode = $("#dispMode").val();
	if("channel" == strDispMode){
		$("#action").val( "first" );
		$("#basicInfoForm").attr("action", "./basicInfo.php");
		$("#dispBackFlg").val("1");
	}else if("adminMenuSelect" == strDispMode){
		if( 1 == $('#menu_category_flg').val()){
			// チャンネルを使用している場合チャンネル画面へ
			$("#action").val( "channel" );
			$("#basicInfoForm").attr("action", "./basicInfo.php");
			$("#dispBackFlg").val("1");
		}else{
			// チャンネルを使用していない場合基本情報画面へ
			$("#action").val( "first" );
			$("#basicInfoForm").attr("action", "./basicInfo.php");
			$("#dispBackFlg").val("1");
		}
	}else if("confirm" == strDispMode){
		$("#action").val( "admin_menu" );
		$("#basicInfoForm").attr("action", "./basicInfo.php");
		$("#dispBackFlg").val("1");
	}else{
		$("#action").val( "back" );
		$("#basicInfoForm").attr("action", "../basicinfolist/basicInfoList.php");
	}
	$("#basicInfoForm").submit();
}

// タグ追加
dspBasicInfo.prototype.addChannel = function(){
	channel_text = $( '#channel_text' ).val();
	channel_flg = 0;
	channel_flg_text = "管理者チャンネル";
	if( 1 == $( '#user_enterable_flg:checked').val()){
		channel_flg = 1;
		channel_flg_text = "視聴者チャンネル";
	}

	if( !channel_text.match( /\S/g ) ){
		ModalDialog.error( "error",　"タグを入力してください。");
		return false;
	}
	channel_text = htmlspecialchars( channel_text );
	channel_text = channel_text.replace(/,/g, "、");
//	tag_text = tag_text.replace(/(\r\n|\r|\n)/g, '<br />');

	var length = $( '#channel :button').length;
	strHtml = "<div id='del_channel" + length + "' class='delChannel'><p>" + channel_text + "：" + channel_flg_text +
		"<input type='button' class='delBtn' name='del_channel" + length + "' onclick='delChannel( this.name )' value='削除' /></p>" +
		"<input type='hidden' name='menu_category_name[]' value='" + channel_text + "' />" +
		"<input type='hidden' name='user_enterable_flg[]' value='" + channel_flg + "' /></div>";

	$('#channel_text').attr( 'value', '' );
	$('#channel').append( strHtml );

};

// エラー時タグ作成
function makeChannel( aryChannelName, aryChannelFlg ){

	var strHtml = '';
	for( var i=0; i<aryChannelName.length; i++ ){
		channel_text = htmlspecialchars( aryChannelName[i] );
		channel_flg = htmlspecialchars( aryChannelFlg[i] );
		if( 1 == channel_flg ){
			channel_flg_text = "視聴者チャンネル";
		}else{
			channel_flg_text = "管理者チャンネル";
		}
		strHtml += "<div id='del_channel" + i + "' class='delChannel'><p>" + channel_text + "：" + channel_flg_text +
			"<input type='button' class='delBtn' name='del_channel" + i + "' onclick='delChannel( this.name )' value='削除' /></p>" +
			"<input type='hidden' name='menu_category_name[]' value='" + channel_text + "' />" +
			"<input type='hidden' name='user_enterable_flg[]' value='" + channel_flg + "' /></div>";
	}
	$('#channel').append( strHtml );
}

// タグ削除
function delChannel( buttonName ){
	$('#' + buttonName).remove();
}

function htmlspecialchars( ch ) {
    ch = ch.replace(/&/g,"&amp;") ;
    ch = ch.replace(/"/g,"&quot;") ;
    ch = ch.replace(/'/g,"&#039;") ;
    ch = ch.replace(/</g,"&lt;") ;
    ch = ch.replace(/>/g,"&gt;") ;
    return ch ;
}

