<?php
/**
 *	基本情報メンテナンス（完了）の画面表示
 *
 *	基本情報メンテナンス（完了）のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/include.php";
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　基本情報<?php echo $_GET["editMode"] == "1" ? "一覧・編集" : "登録" ?></title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
</head>
<body <?php if( '1' == $_GET["editMode"] ) echo 'id="basicInfo_list"'; else echo'id="basicInfo_entry"'; ?> >
<?php echo clsCommonFunction::dispHeaderManegement(); ?>

<div align="center" style="margin-top:50px">
	<?php echo clsDefinition::SYSTEM_NAME?>　基本情報<?php echo $_GET["editMode"] == "1" ? "編集" : "登録" ?>
</div>

<div align="center">
	基本情報を<?php echo $_GET["editMode"] == "1" ? "編集" : "登録" ?>しました。
</div>

<?php if( '1' == $_GET["editMode"] ) { ?>
<div align="center">
	<a href="../basicinfolist/basicInfoList.php?bk=1">一覧に戻る</a>
</div>
<?php } ?>

<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
