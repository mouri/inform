<?php
/**
 *	基本情報メンテナンス画面の画面表示
 *
 *	基本情報メンテナンス画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 *	@update 		Nishi 2013/03/28
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　基本情報<?php echo $aryDisp["editMode"] == "1" ? "編集・一覧" : "登録" ?></title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="./js/dspBasicInfo.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//画面ＪＳ
		var objDspBasicInfo = new dspBasicInfo();
		
		//エラー関連ＪＳ
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if(objAlertError.hasError()){
			objAlertError.show();
		}
		
		//次へボタンクリック時
		$("#btn_confirm").click(function(){
			//条件設定
			objDspBasicInfo.Confirm();
		});
		
		//戻るボタンクリック時
		$("#btn_back").click(function(){
			//戻る設定
			objDspBasicInfo.Back();
		});
		
	});
</script>
</head>
<body <?php echo $aryDisp["editMode"] == "1" ? "id='basicInfo_list'" : "id='basicInfo_entry'" ?> >
<?php echo clsCommonFunction::dispHeaderManegement(); ?>

<form name="basicInfoForm" id="basicInfoForm" method="post">
	<div class="boxBeige marB20">
		<h2>基本情報<?php echo $aryDisp["editMode"] == "1" ? "編集" : "登録" ?></h2>
		<div class="form">
			<div class="midashi  marT20">管理メニュー選択</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth" <?php if( 1 == $aryDisp["editMode"] ) echo "colspan='2'"; ?> >
						<p class="komoku">管理メニュー選択</p>
						<div id="authCheckBox" class="checkBoxDiv">
						<?php echo $strAdminMenuSelectBox; ?>
						</div>
					</td>
					<td class="message"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
<?php if( 1 == $aryDisp["editMode"] ){ ?>
<!-- 修正時 -->
					<td class="c1"><input type="button" name="btn_back" id="btn_back" value="戻る" class="basicBtn centerBlock gray marT20" /></td>
					<td class="c2"><input type="button" name="btn_confirm" id="btn_confirm" value="確認" class="basicBtn centerBlock marT20" /></td>
					<input type="hidden" name="pagerNumber" id="pagerNumber" value="<?php echo $aryDisp["pagerNumber"] ?>" />
<?php } else { ?>
<!-- 登録時 -->
					<td class="c1"><input type="button" name="btn_back" id="btn_back" value="戻る" class="basicBtn centerBlock gray marT20" /></td>
					<td class="c2"><input type="button" name="btn_confirm" id="btn_confirm" value="確認" class="basicBtn centerBlock marT20" /></td>
<?php } ?>
					<td class="message"></td>
				</tr>
			</table>
			<input type="hidden" name="action" id="action" />
			<input type="hidden" name="editMode" id="editMode" value="<?php echo $aryDisp["editMode"] ?>" />
			<input type="hidden" name="dispMode" id="dispMode" value="adminMenuSelect" />
			<input type="hidden" name="dispBackFlg" id="dispBackFlg" value="0" />
			<input type="hidden" name="menu_category_flg" id="menu_category_flg" value="<?php echo $aryDisp["menu_category_flg"]; ?>">
		</div>
	</div>
</form>

<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
