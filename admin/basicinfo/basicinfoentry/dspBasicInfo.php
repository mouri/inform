<?php
/**
 *	基本情報メンテナンス画面の画面表示
 *
 *	基本情報メンテナンス画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 *	@update 		Nishi 2013/03/11
 */

//編集画面からでない　かつ　戻るボタン　でない場合、基本情報に関するSESSIONを消す。
if( 1 != $aryDisp["editMode"] && 0 == $dispBackFlg && true == $blnCheckResult){
	unset($_SESSION['basic_info']);
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　基本情報<?php echo $aryDisp["editMode"] == "1" ? "編集・一覧" : "登録" ?></title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script type="text/javascript" src="./js/dspBasicInfo.js"></script>
<script src="https://0-oo.googlecode.com/svn/gcalendar-holidays.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//画面ＪＳ
		var objDspBasicInfo = new dspBasicInfo();
		
		//エラー関連ＪＳ
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if(objAlertError.hasError()){
			objAlertError.show();
		}
		
		//次へボタンクリック時
		$("#btn_next").click(function(){
			//条件設定
			dispFlg = $('input[name="menu_category_flg"]:checked').val();

			//チャンネルを使用しない場合管理メニュー選択画面へ
			if( 0 == dispFlg ){
				objDspBasicInfo.AdminMenu();
			//チャンネルを使用する場合チャンネル画面へ
			}else if( 1 == dispFlg ){
				objDspBasicInfo.Channel();
			}
		});
		
		//戻るボタンクリック時
		$("#btn_back").click(function(){
			//戻る設定
			objDspBasicInfo.Back();
		});

		//日付設定
		$("#start_day").datepicker();
		
	});
</script>
</head>
<body <?php echo $aryDisp["editMode"] == "1" ? "id='basicInfo_list'" : "id='basicInfo_entry'" ?> >
<?php echo clsCommonFunction::dispHeaderManegement(); ?>

<form name="basicInfoForm" id="basicInfoForm" method="post">
	<div class="boxBeige marB20">
		<h2>基本情報<?php echo $aryDisp["editMode"] == "1" ? "編集" : "登録" ?></h2>
		<div class="form">
			<div class="midashi">会社情報</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">会社名</p>
          				<div class="lavel">
							<input type="text" size="50" name="contact_name" id="contact_name" value="<?php echo $aryDisp["contact_name"] ?>" class="hissu" />
							<label for="contact_name">会社名</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">利用開始日</p>
          				<div class="lavel">
							<input type="text" name="start_day" id="start_day" value="<?php echo $aryDisp["start_day"] ?>" class="hissu" />
							<label for="start_day">利用開始日</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				
				<!-- 2013/10/18 kanata add -->
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">システム名称</p>
          				<div class="lavel">
							<input type="text" name="system_name" id="system_name" value="<?php echo $aryDisp["system_name"] ?>" class="hissu" />
						</div>
					</td>
					<td class="message"></td>
				</tr>
				
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">システムフォルダ</p>
          				<div class="lavel">
							<input type="text" name="system_dir" id="system_dir" value="<?php echo $aryDisp["system_dir"] ?>" class="hissu" />
							<label for="system_dir">システムフォルダ</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<!--2013/06/28 追加 Nishi-->
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">アカウントExcel雛形ファイル名</p>
          				<div class="lavel">
							<input type="text" name="account_excel_name" id="account_excel_name" value="<?php echo $aryDisp["account_excel_name"] ?>" class="hissu" />
							<label for="account_excel_name">アカウントExcel雛形ファイル名</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<!--2013/06/28 追加 Nishi-->
			</table>
			
			<div class="midashi  marT20">エンコード設定</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!--<tr>
					<td class="c1 fullWidth">
						<p class="komoku">PCエンコード</p>
            			<select class="hissu" id="encode_pc" name="encode_pc">
							<?php echo clsCommonFunction::initEncodeSelectBox($aryDisp["encode_pc"]); ?>
						</select>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td class="c1 fullWidth">
						<p class="komoku">スマホエンコード</p>
						<select class="hissu" id="encode_smp" name="encode_smp">
							<?php echo clsCommonFunction::initEncodeSmpSelectBox($aryDisp["encode_smp"]); ?>
						</select>
					</td>
					<td class="message"></td>
				</tr>-->
				<tr>
					<td class="c1 fullWidth">
						<p class="komoku">トランスコード</p>
						<div class="radioDiv item3 hissu">
							<div class="left">
								<label for="transcode_flg1" class="inputRadio"><span class="radioButton"></span>設定1<input type="radio" name="transcode_flg" id="transcode_flg1" value="1" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['transcode_flg'], 1 ); ?> ></label>
							</div>
							<div class="center">
								<label for="transcode_flg2" class="inputRadio"><span class="radioButton"></span>設定2<input type="radio" name="transcode_flg" id="transcode_flg2" value="2" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['transcode_flg'], 2 ); ?> ></label>
							</div>
							<div class="right">
								<label for="transcode_flg3" class="inputRadio"><span class="radioButton"></span>設定3<input type="radio" name="transcode_flg" id="transcode_flg3" value="3" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['transcode_flg'], 3 ); ?> ></label>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						設定1：500のビットレートが設定されます。<br />
						設定2：300、500、836の３種類のビットレートが設定されます。<br />
						設定3：300、500、836、1977の４種類のビットレートが設定されます。
					</td>
				</tr>
			</table>
			<!--20130724 Nishi add ここまで-->
			
			<div class="midashi  marT20">デザイン</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth">
			            <p class="komoku">デザインテンプレート</p>
						<select class="hissu" id="templete" name="templete">
							<?php echo clsCommonFunction::initTempleteSelectBox($aryDisp["templete"]); ?>
						</select>
					</td>
					<td class="message"></td>
				</tr>
			</table>
			
			<div class="midashi marT20">動画設定</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth" colspan="2">
						<p class="komoku">チャンネル</p>
						<div class="radioDiv">
							<div class="left">
								<label for="menu_category_flg1" class="inputRadio">
									<span class="radioButton"></span>
									<input type="radio" name="menu_category_flg" id="menu_category_flg1" value="0" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['menu_category_flg'], 0 ); ?> />使用しない
								</label>　
							</div>
							<div class="right">
								<label for="menu_category_flg2" class="inputRadio">
									<span class="radioButton"></span>
									<input type="radio" name="menu_category_flg" id="menu_category_flg2" value="1" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['menu_category_flg'], 1 ); ?> />使用する
								</label>
							</div>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td class="c1">
          				<p class="komoku">millviID</p>
          				<div class="lavel">
							<input type="text" maxlength="10" size="10" name="millvi_id" id="millvi_id" value="<?php echo $aryDisp["millvi_id"] ?>" class="hissu" />
							<label for="millvi_id">millviID</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">millviユーザID</p>
          				<div class="lavel">
							<input type="text" maxlength="50" size="50" name="millvi_user_id" id="millvi_user_id" value="<?php echo $aryDisp["millvi_user_id"] ?>" class="hissu" />
							<label for="millvi_user_id">millviユーザID</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">millviパスワード</p>
          				<div class="lavel">
							<input type="text" maxlength="50" size="50" name="millvi_password" id="millvi_password" value="<?php echo $aryDisp["millvi_password"] ?>" class="hissu" />
							<label for="millvi_password">millviパスワード</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">millvi動画フォルダ名</p>
          				<div class="lavel">
							<input type="text" maxlength="100" size="100" name="movie_folder" id="movie_folder" value="<?php echo $aryDisp["movie_folder"] ?>" class="hissu" />
							<label for="movie_folder">millvi動画フォルダ名</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>

				<!--2013/06/28 追加 Nishi-->
				<tr id="tr_info_comment">
					<td colspan="2" class="c1 fullWidth">
						<p class="komoku">コメント</p>
						<div class="radioDiv hissu">
							<div class="left">
								<label for="comment_flg1" class="inputRadio">
								<span class="radioButton"></span>あり<input type="radio" name="comment_flg" id="comment_flg1" value="1" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['comment_flg'], 1 ); ?> ></label>
							</div>
							<div class="right">
								<label for="comment_flg0" class="inputRadio">
								<span class="radioButton"></span>なし<input type="radio" name="comment_flg" id="comment_flg0" value="0" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['comment_flg'], 0 ); ?> ></label>
							</div>
						</div>
					</td>
					<td class="message"></td>
	          	</tr>
				<tr id="tr_info_judge">
					<td colspan="2" class="c1 fullWidth">
						<p class="komoku">動画評価</p>
						<div class="radioDiv hissu">
							<div class="left">
								<label for="judge_flg1" class="inputRadio">
								<span class="radioButton"></span>あり<input type="radio" name="judge_flg" id="judge_flg1" value="1" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['judge_flg'], 1 ); ?> ></label>
							</div>
							<div class="right">
								<label for="judge_flg0" class="inputRadio">
								<span class="radioButton"></span>なし<input type="radio" name="judge_flg" id="judge_flg0" value="0" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['judge_flg'], 0 ); ?> ></label>
							</div>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<tr id="tr_info_report">
					<td colspan="2" class="c1 fullWidth">
						<p class="komoku">管理者報告</p>
						<div class="radioDiv hissu">
							<div class="left">
								<label for="report_flg1" class="inputRadio">
								<span class="radioButton"></span>あり<input type="radio" name="report_flg" id="report_flg1" value="1" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['report_flg'], 1 ); ?> ></label>
							</div>
							<div class="right">
								<label for="report_flg0" class="inputRadio">
								<span class="radioButton"></span>なし<input type="radio" name="report_flg" id="report_flg0" value="0" <?php echo clsCommonFunction::chkCheckedDate( $aryDisp['report_flg'], 0 ); ?> ></label>
							</div>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<!--2013/06/28 追加 Nishi-->
			</table>

			<!--2013/06/28 追加 Nishi-->
			<div class="midashi marT20">一斉メール配信</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">使用するログインテーブル</p>
          				<div class="lavel">
							<input type="text" size="50" name="login_tbl_name" id="login_tbl_name" value="<?php echo $aryDisp["login_tbl_name"] ?>" class="hissu" style="ime-mode: disabled;" />
							<label for="login_tbl_name">使用するログインテーブル</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td class="c1 fullWidth">
          				<p class="komoku">不達となった際のエラー送信先アドレス</p>
          				<div class="lavel">
							<input type="text" size="50" name="mail_error_address" id="mail_error_address" value="<?php echo $aryDisp["mail_error_address"] ?>" class="hissu" style="ime-mode: disabled;" />
							<label for="mail_error_address">不達となった際のエラー送信先アドレス</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
			</table>
			
			<div class="midashi  marT20">メール差出人情報</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth" <?php if( 1 == $aryDisp["editMode"] ) echo "colspan='2'"; ?> >
						<p class="komoku">メール差出人（アドレス）</p>
						<div class="lavel">
							<input type="text" size="100" name="sender_address" id="sender_address" style="ime-mode: disabled;" value="<?php echo $aryDisp["sender_address"] ?>" class="hissu" />
							<label for="contact_name">メール差出人（アドレス）</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td class="c1 fullWidth" <?php if( 1 == $aryDisp["editMode"] ) echo "colspan='2'"; ?> >
						<p class="komoku">メール差出人（名称）</p>
						<div class="lavel"><input type="text" name="sender_name" id="sender_name" value="<?php echo $aryDisp["sender_name"] ?>" class="hissu" /><label for="fromAddrName">メール差出人（名称）</label></div>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td class="c1 fullWidth" <?php if( 1 == $aryDisp["editMode"] ) echo "colspan='2'"; ?> >
						<p class="komoku">メール差出人（署名）</p>
						<div class="lavel">
							<textarea id="signature" name="signature" cols="60" rows="20" class="hissu"><?php echo $aryDisp["signature"] ?></textarea>
							<label for="signature">署名</label>
						</div>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
<?php if( 1 == $aryDisp["editMode"] ){ ?>
<!-- 修正時 -->
					<td class="c1"><input type="button" name="btn_back" id="btn_back" value="一覧画面へ" class="basicBtn centerBlock gray marT20" /></td>
					<td class="c2"><input type="button" name="btn_next" id="btn_next" value="次へ" class="basicBtn centerBlock marT20" /></td>
					<input type="hidden" name="pagerNumber" id="pagerNumber" value="<?php echo $aryDisp["pagerNumber"] ?>" />
<?php } else { ?>
<!-- 登録時 -->
					<td class="c1"><input type="button" name="btn_next" id="btn_next" value="次へ" class="basicBtn centerBlock marT20" /></td>
<?php } ?>
					<td class="message"></td>
				</tr>
			</table>
			<input type="hidden" name="action" id="action" />
			<input type="hidden" name="editMode" id="editMode" value="<?php echo $aryDisp["editMode"] ?>" />
		</div>
	</div>
</form>
<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
