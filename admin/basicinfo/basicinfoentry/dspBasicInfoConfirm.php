<?php
/**
 *	基本情報メンテナンス画面の画面表示
 *
 *	基本情報メンテナンス画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2012/02/18
 *	@version		1.0
 *	@update 		Nishi 2013/03/28
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　基本情報<?php echo $aryDisp["editMode"] == "1" ? "編集・一覧" : "登録" ?></title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="./js/dspBasicInfo.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//画面ＪＳ
		var objDspBasicInfo = new dspBasicInfo();
		
		//エラー関連ＪＳ
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if(objAlertError.hasError()){
			objAlertError.show();
		}
		
		//登録ボタンクリック時
		$("#btn_regist").click(function(){
			//条件設定
			objDspBasicInfo.Regist();
		});
		
		//戻るボタンクリック時
		$("#btn_back").click(function(){
			//戻る設定
			objDspBasicInfo.Back();
		});
		
	});
</script>
</head>
<body <?php echo $aryDisp["editMode"] == "1" ? "id='basicInfo_list'" : "id='basicInfo_entry'" ?> >
<?php echo clsCommonFunction::dispHeaderManegement(); ?>

<form name="basicInfoForm" id="basicInfoForm" method="post">
	<div class="boxBeige marB20">
		<h2>基本情報メンテナンス確認</h2>
		<div class="form">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="confirm marB20">
			<tr>
				<th colspan="2" class="tit">基本情報内容</th>
			</tr>
			<tr>
				<th>会社名</th>
				<td><?php echo f::p($aryDisp["contact_name"]); ?></td>
			</tr>
			<tr>
				<th>利用開始日</th>
				<td><?php echo f::p($aryDisp["start_day"]); ?></td>
			</tr>
			
			<!-- 2013/10/18 kanata add -->
			<tr>
				<th>システム名称</th>
				<td><?php echo f::p($aryDisp["system_name"]); ?></td>
			</tr>
			
			<tr>
				<th>システムフォルダ</th>
				<td><?php echo f::p($aryDisp["system_dir"]); ?></td>
			</tr>
			<tr>
				<th>アカウントExcel雛形ファイル名</th>
				<td><?php echo f::p($aryDisp["account_excel_name"]); ?></td>
			</tr>
			<tr>
				<th>使用するログインテーブル</th>
				<td><?php echo f::p($aryDisp["login_tbl_name"]); ?></td>
			</tr>
			<tr>
				<th>エラー送信先アドレス</th>
				<td><?php echo f::p($aryDisp["mail_error_address"]); ?></td>
			</tr>
			<tr>
				<th>コメント</th>
				<td><?php echo 1 == $aryDisp["comment_flg"]?"あり":"なし"; ?></td>
			</tr>
			<tr>
				<th>評価</th>
				<td><?php echo 1 == $aryDisp["judge_flg"]?"あり":"なし"; ?></td>
			</tr>
			<tr>
				<th>管理者報告</th>
				<td><?php echo 1 == $aryDisp["report_flg"]?"あり":"なし"; ?></td>
			</tr>
			<!--<tr>
				<th>PCエンコード</th>
				<td><?php echo $aryDisp["encode_pc_name"]; ?></td>
			</tr>
			<tr>
				<th>スマホエンコード</th>
				<td><?php echo $aryDisp["encode_smp_name"]; ?></td>
			</tr>-->
			<tr>
				<th>トランスコード</th>
				<td><?php echo $aryDisp["transcode_flg_name"]; ?></td>
			</tr>
			<tr>
				<th>デザインテンプレート</th>
				<td><?php echo $aryDisp["templete"]; ?></td>
			</tr>
			<tr>
				<th>チャンネル</th>
				<td><?php echo $aryDisp["menu_category_flg"] == "1" ? "使用" : "不使用" ; ?></td>
			</tr>
			<tr>
				<th>millviID</th>
				<td><?php echo $aryDisp["millvi_id"]; ?></td>
			</tr>
			<tr>
				<th>millviユーザID</th>
				<td><?php echo $aryDisp["millvi_user_id"]; ?></td>
			</tr>
			<tr>
				<th>millviパスワード</th>
				<td><?php echo $aryDisp["millvi_password"]; ?></td>
			</tr>
			<tr>
				<th>millvi動画フォルダ名</th>
				<td><?php echo f::p($aryDisp["movie_folder"]); ?></td>
			</tr>
			<tr>
				<th>メール差出人（アドレス）</th>
				<td><?php echo f::p($aryDisp["sender_address"]); ?></td>
			</tr>
			<tr>
				<th>メール差出人（名称）</th>
				<td><?php echo f::p($aryDisp["sender_name"]); ?></td>
			</tr>
			<tr>
				<th>メール差出人（著名）</th>
				<td><?php echo f::br($aryDisp["signature"]); ?></td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="confirm marB20">
	      <tr>
	        <th colspan="2" class="tit">チャンネル</th>
	        </tr>
	      <tr>
	        <th>チャンネル</th>
	        <td><?php echo $strChannelList; ?></td>
	      </tr>

	    </table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="confirm marB20">
			<tr>
				<th colspan="2" class="tit">管理メニュー内容</th>
				</tr>
			<tr>
				<th>管理メニュー内容</th>
				<td><?php echo $strAdminMenuList; ?></td>
			</tr>
		</table>

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
	<?php if( 1 == $aryDisp["editMode"] ){ ?>
	<!-- 修正時 -->
				<td class="c1"><input type="button" name="btn_back" id="btn_back" value="戻る" class="basicBtn centerBlock gray marT20" /></td>
				<td class="c2"><input type="button" name="btn_regist" id="btn_regist" value="編集内容を保存" class="basicBtn centerBlock marT20" /></td>
				<input type="hidden" name="pagerNumber" id="pagerNumber" value="<?php echo $aryDisp["pagerNumber"] ?>" />
	<?php } else { ?>
	<!-- 登録時 -->
				<td class="c1"><input type="button" name="btn_back" id="btn_back" value="戻る" class="basicBtn centerBlock gray marT20" /></td>
				<td class="c2"><input type="button" name="btn_regist" id="btn_regist" value="登録" class="basicBtn centerBlock marT20" /></td>
	<?php } ?>
				<td class="message"></td>
			</tr>
		</table>
		
		<input type="hidden" name="action" id="action" />
		<input type="hidden" name="editMode" id="editMode" value="<?php echo $aryDisp["editMode"] ?>" />
		<input type="hidden" name="dispMode" id="dispMode" value="confirm" />
		<input type="hidden" name="dispBackFlg" id="dispBackFlg" value="0" />

	</div>
	</div>
</form>

<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
