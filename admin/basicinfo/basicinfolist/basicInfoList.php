<?php
/**
 *	基本情報一覧画面
 *
 *	基本情報一覧画面の制御クラスを呼び出す
 *
 *	@author kanata 2013/03/25
 */
require_once $_SERVER["DOCUMENT_ROOT"]."/include.php";
require_once "./ctlBasicInfoList.php";
require_once "./clsBasicInfoList.php";
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/value_checker/execute_classes/clsAdminBasicInfoChecker.php" );

//基本情報一覧制御クラス呼出し
$objBasicInfoList = new ctlBasicInfoList();
$objBasicInfoList->process();
?>