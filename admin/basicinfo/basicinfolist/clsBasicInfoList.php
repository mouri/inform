<?php
/**
 *	基本情報一覧画面のモデルクラス
 *
 *	基本情報一覧画面のビジネスロジックについて記述
 *
 *	@author kanata 2013/03/25
 */
class clsBasicInfoList{
	
	private $_aryDisp;
	
	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct( $aryDisp ){
		$this->_aryDisp = $aryDisp;
	}
	
	/**
	 * 基本情報一覧の件数を取得します。
	 * 
	 * @return 基本情報一覧件数
	 */
	public function getBasicInfoListCnt(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "kanata@inform-us.com" );
		
		$strSelectSql =
<<<SQL
	SELECT
		cm.contact_id,
		contact_name,
		menu_category_flg,
		millvi_user_id,
		millvi_password,
		system_dir,
		COALESCE( lm.cnt, 0) AS special_cnt,
		menu_category_name,
		menu_name
	FROM
		contact_mst AS cm
			LEFT JOIN (
				SELECT
					contact_id,
					COUNT( user_kbn ) AS cnt
				FROM
					login_mst
				WHERE
					user_kbn = :user_kbn
				GROUP BY
					contact_id
			) AS lm
		ON cm.contact_id = lm.contact_id
			
			LEFT JOIN (
				SELECT 
					contact_id,
					GROUP_CONCAT( menu_category_name ORDER BY sort_no ASC SEPARATOR '<br />' ) AS menu_category_name
				FROM
					menu_category_mst
				WHERE
					user_enterable_flg = 0 AND
					delete_flg         = :delete_flg
				GROUP BY
					contact_id
			) AS mcm
		ON cm.contact_id = mcm.contact_id
			
			LEFT JOIN (
				SELECT 
					contact_id,
					GROUP_CONCAT( menu_name ORDER BY menu_id ASC SEPARATOR '<br />' ) AS menu_name
				FROM
					admin_menu_mst
				WHERE
					delete_flg = :delete_flg
				GROUP BY
					contact_id
			) AS amm
		ON cm.contact_id = amm.contact_id
	WHERE
		delete_flg = :delete_flg
	ORDER BY
		cm.contact_id ASC
SQL;
		$aryParameters = array(
			":user_kbn"   => '3',
			":delete_flg" => '0'
		);
		
		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return COUNT( $aryResult );
	}
	
	/**
	 * 基本情報一覧を取得します。
	 * 
	 * @return 基本情報一覧
	 */
	public function getBasicInfoList( $intLimit, $intOffset ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "kanata@inform-us.com" );
		
		$strLimitSql = "LIMIT " . $intLimit . " OFFSET " . $intOffset;
		
		$strSelectSql =
<<<SQL
	SELECT
		cm.contact_id,
		contact_name,
		menu_category_flg,
		millvi_user_id,
		millvi_password,
		system_dir,
		COALESCE( lm.cnt, 0) AS special_cnt,
		menu_category_name,
		menu_name
	FROM
		contact_mst AS cm
			LEFT JOIN (
				SELECT
					contact_id,
					COUNT( user_kbn ) AS cnt
				FROM
					login_mst
				WHERE
					user_kbn = :user_kbn
				GROUP BY
					contact_id
			) AS lm
		ON cm.contact_id = lm.contact_id
			
			LEFT JOIN (
				SELECT 
					contact_id,
					GROUP_CONCAT( menu_category_name ORDER BY sort_no ASC SEPARATOR '<br />' ) AS menu_category_name
				FROM
					menu_category_mst
				WHERE
					delete_flg = :delete_flg
				GROUP BY
					contact_id
			) AS mcm
		ON cm.contact_id = mcm.contact_id
			
			LEFT JOIN (
				SELECT 
					contact_id,
					GROUP_CONCAT( menu_name ORDER BY menu_id ASC SEPARATOR '<br />' ) AS menu_name
				FROM
					admin_menu_mst
				WHERE
					delete_flg = :delete_flg
				GROUP BY
					contact_id
			) AS amm
		ON cm.contact_id = amm.contact_id
	WHERE
		delete_flg = :delete_flg
	ORDER BY
		cm.contact_id ASC
	{$strLimitSql}
SQL;
		$aryParameters = array(
			":user_kbn"   => '3',
			":delete_flg" => '0'
		);
		
		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult;
	}
	
	/**
	 * 特権ユーザ作成
	 * 
	 * @return true:成功 false:失敗
	 */
	public function addSuperUser(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "kanata@inform-us.com" );
		
		$arySuperUser = array(
			array(
				"user_id"      => "0",
				"name1"        => "インフォーム",
				"name2"        => "システム",
				"mail_address" => "inform@doupa.net",
				"user_kbn"     => "3"
			),
			array(
				"user_id"      => "-1",
				"name1"        => "エビリー",
				"name2"        => "",
				"mail_address" => "eviry@doupa.net",
				"user_kbn"     => "3"
			)
		);
		
		//トランザクション開始
		$objDatabase->execTransaction();
		
		// 特権ユーザ作成処理
		foreach( $arySuperUser AS $strSuperUser ){
			$strInsertSql =
<<<SQL
	INSERT INTO login_mst (
		contact_id,
		user_id,
		password,
		name1,
		name2,
		mail_address,
		user_kbn,
		group_cd,
		reg_id,
		reg_name,
		reg_time,
		upd_id,
		upd_name,
		upd_time
	) VALUES (
		:contact_id,
		:user_id,
		:password,
		:name1,
		:name2,
		:mail_address,
		:user_kbn,
		:group_cd,
		:reg_id,
		:reg_name,
		:reg_time,
		:upd_id,
		:upd_name,
		:upd_time
	)
SQL;
			$aryParameters = array(
				':contact_id'   => base64_decode( $this->_aryDisp["contact_id"] ),
				':user_id'      => $strSuperUser["user_id"],
				':password'     => clsCommonFunction::makePass(8),
				':name1'        => $strSuperUser["name1"],
				':name2'        => $strSuperUser["name2"],
				':mail_address' => $strSuperUser["mail_address"],
				':user_kbn'     => '3',	// 特権ユーザ
				':group_cd'     => '0',
				':reg_id'       => clsLoginSession::getUserId(),
				':reg_name'     => clsLoginSession::getFullName(),
				':reg_time'     => clsCommonFunction::getCurrentDate(),
				':upd_id'       => clsLoginSession::getUserId(),
				':upd_name'     => clsLoginSession::getFullName(),
				':upd_time'     => clsCommonFunction::getCurrentDate()
			);
			
			$aryResult = $objDatabase->addDbData( $strInsertSql, $aryParameters );
			if( $objDatabase->isError( $aryResult ) ){
				//ロールバック
				$objDatabase->execRollback();
				return false;
			}
			
			$strInsertSql =
<<<SQL
	INSERT INTO login_mst_past
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM login_mst_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			:past_reg_id,
			login_mst.*
		FROM
			login_mst
		WHERE
			contact_id = :contact_id AND
			user_id    = :user_id
SQL;
			$aryParameters = array(
				":upd_kbn"     => 1,
				":past_reg_id" => clsLoginSession::getUserId(),
				":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] ),
				':user_id'     => $strSuperUser["user_id"],
			);
			
			$aryResult = $objDatabase->addDbData( $strInsertSql, $aryParameters );
			if( $objDatabase->isError( $aryResult ) ){
				//ロールバック
				$objDatabase->execRollback();
				return false;
			}
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}
	
	/**
	 * 基本情報を削除します。
	 * 
	 * @return true:成功 false:失敗
	 */
	public function delBasicInfo(){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
		
		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//トランザクション開始
		$objDatabase->execTransaction();
		
		$strDeleteSql =
<<<SQL
	UPDATE
		contact_mst
	SET
		delete_flg = :delete_flg,
		upd_id     = :upd_id,
		upd_name   = :upd_name,
		upd_time   = :upd_time
	WHERE
		contact_id = :contact_id
SQL;
		$aryParameters = array(
			":delete_flg" => '1',
			":upd_id"     => clsLoginSession::getUserId(),
			":upd_name"   => clsLoginSession::getFullName(),
			":upd_time"   => clsCommonFunction::getCurrentDate(),
			":contact_id" => base64_decode( $this->_aryDisp["contact_id"] )
		);
		
		$aryResult = $objDatabase->updDbData( $strDeleteSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		//チャンネル削除
		$strDelChannelSql =
<<<SQL
		DELETE
		FROM
			menu_category_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);

		$aryResult = $objDatabase->delDbData($strDelChannelSql,$aryParameters);
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		//管理メニュー削除
		$strDelAduminMenuSql =
<<<SQL
		DELETE
		FROM
			admin_menu_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);

		$aryResult = $objDatabase->delDbData($strDelAduminMenuSql,$aryParameters);
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		//権限削除
		$strDelAuthoritySql =
<<<SQL
		DELETE
		FROM
			authority_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);

		$aryResult = $objDatabase->delDbData($strDelAuthoritySql,$aryParameters);
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		//違反削除
		$strDelOutSql =
<<<SQL
		DELETE
		FROM
			out_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);

		$aryResult = $objDatabase->delDbData($strDelOutSql,$aryParameters);
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		//グループ削除 2013/05/09 Nishi 追加
		$strDelGroupSql =
<<<SQL
		DELETE
		FROM
			group_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);

		$aryResult = $objDatabase->delDbData($strDelGroupSql,$aryParameters);
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		//グループ履歴削除 2013/05/09 Nishi 追加
		$strDelGroupPastSql =
<<<SQL
		DELETE
		FROM
			group_mst_past
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);

		$aryResult = $objDatabase->delDbData($strDelGroupPastSql,$aryParameters);
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		//グループ別権限削除 2013/05/09 Nishi 追加
		$strDelGroupAuthConfigSql =
<<<SQL
		DELETE
		FROM
			group_auth_config_mst
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);

		$aryResult = $objDatabase->delDbData($strDelGroupAuthConfigSql,$aryParameters);
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		//グループ別権限履歴削除 2013/05/09 Nishi 追加
		$strDelGroupAuthConfigPastSql =
<<<SQL
		DELETE
		FROM
			group_auth_config_mst_past
		WHERE
			contact_id = :contact_id
SQL;

		$aryParameters = array(
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);

		$aryResult = $objDatabase->delDbData($strDelGroupAuthConfigPastSql,$aryParameters);
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		
		$strInsertSql =
<<<SQL
	INSERT INTO contact_mst_past
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM contact_mst_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			:past_reg_id,
			contact_mst.*
		FROM
			contact_mst
		WHERE
			contact_id = :contact_id
SQL;
		$aryParameters = array(
			":upd_kbn"     => 2,
			":past_reg_id" => clsLoginSession::getUserId(),
			":contact_id"  => base64_decode( $this->_aryDisp["contact_id"] )
		);
		
		$aryResult = $objDatabase->addDbData( $strInsertSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}
	
}
?>