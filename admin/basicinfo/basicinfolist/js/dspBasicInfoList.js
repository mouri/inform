// コンストラクタ
var dspBasicInfoList = function() {};

// 編集処理
dspBasicInfoList.prototype.Edit = function( row ){
	$("#action").val( "edit" );
	$("#editMode").val( "1" );
	$("#contact_id").attr( "value", $("#contact_id" + row).val() );
	$("#basicInfoForm").attr( "action", "../basicinfoentry/basicInfo.php" );
	$("#basicInfoForm").submit();

}

// 削除処理
dspBasicInfoList.prototype.Delete = function( row ){
	contactName = $( '#contact_name' + row ).val();
	contactId   = $( '#contact_id' + row ).val();
	ModalDialog.confirm( "",　"以下の契約会社情報を削除してもよろしいですか？<br />契約会社名：" + contactName,
		function( blnResult ){
			if( true == blnResult ){
				$("#action").val( "delete" );
				$("#contact_id").attr( "value", contactId );
				$("#basicInfoForm").attr( "action", "./basicInfoList.php" );
				$("#basicInfoForm").submit();
			}
		}
	);
}

// 特権処理
dspBasicInfoList.prototype.Super = function( row ){
	contactName = $( '#contact_name' + row ).val();
	contactId   = $( '#contact_id' + row ).val();
	ModalDialog.confirm( "",　"以下の契約会社に特権ユーザを追加してもよろしいですか？<br />契約会社名：" + contactName,
		function( blnResult ){
			if( true == blnResult ){
				$("#action").val( "addSuper" );
				$("#contact_id").attr( "value", contactId );
				$("#basicInfoForm").attr( "action", "./basicInfoList.php" );
				$("#basicInfoForm").submit();
			}
		}
	);
}
