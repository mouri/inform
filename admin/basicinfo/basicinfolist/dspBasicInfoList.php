<?php
/**
 *	基本情報一覧画面の画面表示
 *
 *	基本情報一覧画面のＨＴＭＬ表示部分を記述
 *
 *	@author Kanata 2013/03/25
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
 <meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　基本情報一覧</title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/basicinfo/basicinfolist/js/dspBasicInfoList.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		// エラーJS
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if( objAlertError.hasError() ){
			objAlertError.show();
		}
		
		var objDspBasicInfoList = new dspBasicInfoList();
		
		// 検索ボタンクリック時
		$("#search_btn").click(function(){
			// 条件設定
			objDspBasicInfoList.Search();
		});
		
		// 編集ボタンクリック時
		$("input[name='edit_btn']").click(function(){
			// 選択した行の行番号取得
			row = $( this ).attr( "id" );
			row = row.replace( "edit_btn", "" );
			objDspBasicInfoList.Edit( row );
		});
		
		// 削除ボタンクリック時
		$("input[name='del_btn']").click(function(){
			// 選択した行の行番号取得
			row = $( this ).attr( "id" );
			row = row.replace( "del_btn", "" );
			objDspBasicInfoList.Delete( row );
		});
		
		// 特権ボタンクリック時
		$("input[name='super_btn']").click(function(){
			// 選択した行の行番号取得
			row = $( this ).attr( "id" );
			row = row.replace( "super_btn", "" );
			objDspBasicInfoList.Super( row );
		});
	});
</script>
</head>
<body id="basicInfo_list">
	<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	
	<form name="basicInfoForm" id="basicInfoForm" method="post">
	<div class="boxBeige">
		<h2>基本情報一覧・編集</h2>
<!--
		<div class="form">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth">
						<p class="komoku"></p>
					</td>
					<td class="message"></td>
				</tr>
				<tr>
					<td>
						<div class="basicBtn search">
							<input type="button" name="search_btn" id="search_btn" value="検索" />
						</div>
					</td>
				</tr>
			</table>
		</div>
-->
	</div>
	
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
			<tr>
				<th colspan="7" class="pager top"><?php echo $objPager->get(); ?></th>
			</tr>
			
			<tr>
				<th width="100px">契約ＩＤ</th>
				<th>契約会社名</th>
				<th>チャンネル</th>
				<th>管理者メニュー</th>
				<th width="100px">ミルビィユーザＩＤ</th>
				<th width="100px">ミルビィパスワード</th>
				<th width="150px">編集</th>
			</tr>
			
		<?php
			if( 0 != COUNT( $aryResultList ) ){
				$intCnt = 1;
				foreach( $aryResultList as $aryResultList ){
		?>
				<tr align="center">
					<td><?php echo $aryResultList['contact_id'] ?> : <?php echo base64_encode( $aryResultList['contact_id'] ) ?></td>
					<td><?php f::p( $aryResultList['contact_name'] ) ?></td>
					<td><?php echo $aryResultList["menu_category_flg"] == "1" ? $aryResultList["menu_category_name"] : "不使用" ?></td>
					<td><?php echo $aryResultList["menu_name"] ?></td>
					<td style="word-break:break-all"><?php f::p( $aryResultList["millvi_user_id"] ) ?></td>
					<td style="word-break:break-all"><?php f::p( $aryResultList["millvi_password"] ) ?></td>
					<td>
						<div class="basicBtn edit w40"><input type="button" name="edit_btn" id="edit_btn<?php echo $intCnt ?>" value="編集" /></div>
						<div class="basicBtn del w40"><input type="button" name="del_btn" id="del_btn<?php echo $intCnt ?>" value="削除" /></div>
					<?php if( 0 == $aryResultList["special_cnt"] ){ ?>
						<div class="basicBtn edit w40"><input type="button" name="super_btn" id="super_btn<?php echo $intCnt ?>" value="特権" /></div>
					<?php } ?>
					</td>
					<input type="hidden" id="contact_id<?php echo $intCnt ?>" value="<?php f::p( base64_encode( $aryResultList["contact_id"] ) ); ?>" />
					<input type="hidden" id="contact_name<?php echo $intCnt ?>" value="<?php f::p( $aryResultList["contact_name"] ); ?>" />
				</tr>
		<?php
					$intCnt++;
				}
			}
		?>
			<tr>
				<th colspan="7" class="pager top"><?php echo $objPager->get(); ?></th>
			</tr>
		</table>
		
		<input type="hidden" name="contact_id" id="contact_id" value="" />
		<input type="hidden" name="contact_name" id="contact_name" value="" />
		<input type="hidden" name="editMode" id="editMode" value="" />
		<input type="hidden" name="action" id="action" value="" />
		<input type="hidden" name="pagerNumber" value="<?php echo $_POST["pagerNumber"] ?>" />
	</form>
	
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>