<?php
/**
 *	基本情報一覧画面の制御
 *
 *	基本情報一覧画面の制御について記述
 *
 *	@author kanata 2013/03/25
 */
class ctlBasicInfoList{
	
	/**
	 * コンストラクタ
	 *
	**/
	function __construct() {
	}
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){
		
//		echo "<pre>";
//		var_dump($_SERVER);
//		echo "</pre>";
		
		// 完了画面からの戻り値考慮
		if( "1" == $_GET["bk"] && true == isset( $_SESSION[clsDefinition::SESSION_ADMIN_BASICINFO_LIST] ) ){
			foreach( $_SESSION[clsDefinition::SESSION_ADMIN_BASICINFO_LIST] as $key => $val ){
				$_POST[$key] = $val;
			}
			unset( $_SESSION[clsDefinition::SESSION_ADMIN_BASICINFO_LIST] );
		}
		
		// ＰＯＳＴの値で画面配列更新
		foreach ($_POST as $key => $val){
			$aryDisp[$key] = $_POST[$key];
		}
		
		$objBasicInfoList = new clsBasicInfoList( $aryDisp );
		$objChecker       = new clsAdminBasicInfoChecker( $_REQUEST );		//エラーチェッククラス
		$strErrorJson     = '';												//エラー情報のJSON文字列
		
		// ページナンバー
		if( "" == $aryDisp["pagerNumber"] || "search" == $aryDisp["action"] ){
			$_POST["pagerNumber"] = '1';
		}
		
		//ページャクラス作成
		$objPager = new clsPager();
		
		switch( $aryDisp["action"] ){
			// 特権ユーザ追加処理
			case 'addSuper':
				$blnResult = $objBasicInfoList->addSuperUser();
				if( false == $blnResult ){
					$objChecker->setError();
					$objChecker->setErrorMessage( "auth", "特権ユーザ作成に失敗しました。" );
				}
				// 検索条件をセッションに格納する
				$_SESSION[clsDefinition::SESSION_ADMIN_BASICINFO_LIST] = array(
					"pagerNumber" => $aryDisp["pagerNumber"]
				);
				header("Location: ".clsDefinition::SYSTEM_DIR."/admin/basicinfo/basicinfolist/basicInfoList.php?bk=1");
				break;
				
			// 削除処理
			case 'delete':
				$blnResult = $objBasicInfoList->delBasicInfo();
				if( false == $blnResult ){
					$objChecker->setError();
					$objChecker->setErrorMessage( "auth", "基本情報削除に失敗しました。" );
				}
				// 検索条件をセッションに格納する
				$_SESSION[clsDefinition::SESSION_ADMIN_BASICINFO_LIST] = array(
					"pagerNumber" => $aryDisp["pagerNumber"]
				);
				header("Location: ".clsDefinition::SYSTEM_DIR."/admin/basicinfo/basicinfolist/basicInfoList.php?bk=1");
				break;
				
			// 初期表示
			default:
				break;
		}
		
		// 一覧の総件数をセット
		$objPager->setTotal( $objBasicInfoList->getBasicInfoListCnt() );
		// 基本情報一覧取得
		$aryResultList = $objBasicInfoList->getBasicInfoList( $objPager->getLimit(), $objPager->getOffset() );
		if( false == $aryResultList ){
			$objChecker->setError();
			$objChecker->setErrorMessage( "auth", "基本情報取得に失敗しました。" );
		}
		
/* 2013-10-18 kanata 使用しない為コメントアウト
		// エンコード名取得
		$intCnt = 0;
		foreach( $aryResultList AS $strBasicInfo ){
			// pcエンコード
			foreach( clsDefinition::$PC_ENCODE AS $strPCEncode ){
				if( $strBasicInfo["encode_pc"] == $strPCEncode["id"] ){
					$aryResultList[$intCnt]["encode_pc_name"] = $strPCEncode["name"];
				}
			}
			// スマフォエンコード
			foreach( clsDefinition::$SMP_ENCODE AS $strSMPEncode ){
				if( $strBasicInfo["encode_smp"] == $strSMPEncode["id"] ){
					$aryResultList[$intCnt]["encode_smp_name"] = $strSMPEncode["name"];
				}
			}

			// トランスコード Nishi add 20130724
			foreach( clsDefinition::$TRANSCODE_FLG AS $strTranscode_flg ){
				if( $strBasicInfo["transcode_flg"] == $strTranscode_flg["flg"] ){
					$aryResultList[$intCnt]["transcode_flg_name"] = $strTranscode_flg["name"];
				}
			}
			// 契約ID
			$aryResultList[$intCnt]["contact_id"] = base64_encode( $strBasicInfo["contact_id"] );

			// ログインURL生成
//			$aryResultList[$intCnt]["login_url"] = $_SERVER["HTTP_X_FORWARDED_PROTO"] . "://" . $_SERVER["HTTP_HOST"] . $strBasicInfo["system_dir"] . "/login/login.php?c=" . base64_encode( $strBasicInfo["contact_id"] );
			$intCnt++;
		}
*/
		
		require_once 'dspBasicInfoList.php';
	}
}


?>