<?php
/**
 *	基本情報CSV出力画面のモデルクラス
 *
 *	基本情報CSV出力画面のビジネスロジックについて記述
 *
 *	@author			Nishi 2013/04/16
 */
class clsBasicInfoCsvOutput{
	
	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct( $aryPostDate ){
		$this->_aryPostDate = $aryPostDate;
	}
	
	/**
	 * POSTデータ整頓
	 *
	 **/
	public function pullConvertData( $aryPostDate ){
		return $aryPostDate;
	}
	
	/**
	 * 生成するCSVファイル名を取得する関数
	 * 
	 * @return string : bsdicInfo-ユーザID.csv
	 **/
	public function getFileName(){
		return 'basicInfo-' . clsLoginSession::getUserId() . '.csv';
	}
	
	/**
	 * CSV出力
	 *
	 **/
	public function csvOutput(){
		// ヘッダー
//		$strHeader = '"契約ＩＤ","契約会社名","エンコード設定（PC）","エンコード設定（スマホ）","デザインテンプレート","メニューカテゴリフラグ",'
//				   . '"メール差出人（アドレス）","メール差出人（名称）","メール著名",,"millviユーザID（数値）","millviユーザID（文字列）","millviパスワード","millvi動画フォルダ","利用開始日","システムフォルダ",'
//				   . '"アカウント雛形excelファイル名","ログインテーブル定義","メール配達不達アドレス","コメント有り無しフラグ","評価有り無しフラグ","管理者報告有り無しフラグ","トランスコード設定","削除フラグ"';
		$strHeader = '"契約ＩＤ","契約会社名","利用開始日","システム名称","システムフォルダ","アカウント雛形excelファイル名","ログインテーブル定義","メール配達不達アドレス","コメント有り無しフラグ",'
				   . '"評価有り無しフラグ","管理者報告有り無しフラグ","トランスコード設定","デザインテンプレート","メニューカテゴリフラグ","millviＩＤ","millviユーザＩＤ","millviパスワード",'
				   . '"millvi動画フォルダ","メール差出人（アドレス）","メール差出人（名称）","メール差出人（著名）","チャンネル","管理メニュー内容","初回設定フラグ"';
		
		// 日付
		$strTimeData = date( 'YmdHis' );
		// 変換パターン
		$aryPattern  = array( '/"/' );
		$aryReplace  = array( '/”/' );
		// ファイルポインタ
//		$strCsvFilePointer = $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . '/admin/account/accountcsvoutput/accountInfo-' . clsContactSession::getContactId() . '-' . clsLoginSession::getUserId() . '.csv';
		$strCsvFilePointer = $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . '/admin/basicinfo/basicinfocsvoutput/csv/' . self::getFileName();
		
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		// ログインマスタからCSV出力情報取得
		$strSql = 
<<<SQL
	SELECT
		cm.contact_id,
		contact_name,
		start_day,
		system_name,
		system_dir,
		account_excel_name,
		login_tbl_name,
		mail_error_address,
		comment_flg,
		judge_flg,
		report_flg,
		transcode_flg,
		templete,
		menu_category_flg,
		millvi_id,
		millvi_user_id,
		millvi_password,
		movie_folder,
		fromAddr,
		fromAddrName,
		signature,
		CASE
			WHEN mcm_admin.menu_category_name IS NOT NULL
			 AND mcm_user.menu_category_name IS NOT NULL
				THEN CONCAT( '管理者チャンネル:', mcm_admin.menu_category_name, '\n', '視聴者チャンネル:', mcm_user.menu_category_name )
			WHEN mcm_admin.menu_category_name IS NOT NULL
			 AND mcm_user.menu_category_name IS NULL
				THEN CONCAT( '管理者チャンネル:', mcm_admin.menu_category_name )
			WHEN mcm_user.menu_category_name IS NOT NULL
			 AND mcm_admin.menu_category_name IS NULL
				THEN CONCAT( '視聴者チャンネル:', mcm_user.menu_category_name )
		END AS menu_category_name,
		menu_name,
		config_flg
	FROM
		contact_mst AS cm
			LEFT JOIN (
				SELECT 
					contact_id,
					GROUP_CONCAT( menu_category_name ORDER BY sort_no ASC SEPARATOR ',' ) AS menu_category_name
				FROM
					menu_category_mst
				WHERE
					user_enterable_flg = 0 AND
					delete_flg         = :delete_flg
				GROUP BY
					contact_id
			) AS mcm_admin
			ON cm.contact_id = mcm_admin.contact_id
			
			LEFT JOIN (
				SELECT 
					contact_id,
					GROUP_CONCAT( menu_category_name ORDER BY sort_no ASC SEPARATOR ',' ) AS menu_category_name
				FROM
					menu_category_mst
				WHERE
					user_enterable_flg = 1 AND
					delete_flg         = :delete_flg
				GROUP BY
					contact_id
			) AS mcm_user
			ON cm.contact_id = mcm_user.contact_id
			
			LEFT JOIN (
				SELECT 
					contact_id,
					GROUP_CONCAT( menu_name ORDER BY menu_id ASC SEPARATOR ',' ) AS menu_name
				FROM
					admin_menu_mst
				WHERE
					delete_flg = :delete_flg
				GROUP BY
					contact_id
			) AS amm
			ON cm.contact_id = amm.contact_id
	WHERE
		delete_flg = :delete_flg
SQL;
	
		$aryParameters = array(
			":delete_flg" => 0
		);
		$aryResult = $objDatabase->pullDbData( $strSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		// ファイルポインタを書き込み(新規)で生成
		$rscFilePointer = fopen( $strCsvFilePointer, "w" );
		if( !$rscFilePointer ){
			return false;
		}
		
		// ヘッダー書き込み
		fwrite( $rscFilePointer, mb_convert_encoding( $strHeader, "sjis-win", "UTF-8" ) . "\r\n" );
		
		$aryCsvLine = array();
		$strCsvLine = "";
		foreach( $aryResult AS $aryLoginInfo ){
			// ダブルクォーテーションを全角化
			$aryCsvLine = preg_replace( $aryPattern, $aryReplace, $aryLoginInfo );
/*
			// pcエンコード
			foreach( clsDefinition::$PC_ENCODE AS $strPCEncode ){
				if( $aryCsvLine["encode_pc"] == $strPCEncode["id"] ){
					$aryCsvLine["encode_pc"] = $strPCEncode["name"];
				}
			}
			// スマフォエンコード
			foreach( clsDefinition::$SMP_ENCODE AS $strSMPEncode ){
				if( $aryCsvLine["encode_smp"] == $strSMPEncode["id"] ){
					$aryCsvLine["encode_smp"] = $strSMPEncode["name"];
				}
			}
			// エンコードPC
			switch( $aryCsvLine["encode_pc"] ){
				case '':
				case 'null':
					break;
				default:
					$aryCsvLine["encode_pc"] = $aryCsvLine["encode_pc"];
					break;
			}
			// エンコードスマホ
			switch( $aryCsvLine["encode_smp"] ){
				case '':
				case 'null':
					break;
				default:
					$aryCsvLine["encode_smp"] = $aryCsvLine["encode_smp"];
					break;
			}
*/
			// トランスコード Nishi add 20130724
			foreach( clsDefinition::$TRANSCODE_FLG AS $strTranscode_flg ){
				if( $aryCsvLine["transcode_flg"] == $strTranscode_flg["flg"] ){
					$aryCsvLine["transcode_flg"] = $strTranscode_flg["name"];
				}
			}
			// トランスコード
			switch( $aryCsvLine["transcode_flg"] ){
				case '':
				case 'null':
					break;
				default:
					$aryCsvLine["transcode_flg"] = $aryCsvLine["transcode_flg"];
					break;
			}

			// チャンネル使用フラグ
			switch( $aryCsvLine["menu_category_flg"] ){
				case '':
				case 'null':
				case '0':
				case 0:
					$aryCsvLine["menu_category_flg"] = "使用しない";
					break;
				case '1':
				case 1:
					$aryCsvLine["menu_category_flg"] = "使用する";
					break;
				default:
					break;
			}
			// 初回設定フラグ
			switch( $aryCsvLine["config_flg"] ){
				case 0:
					$aryCsvLine["config_flg"] = "未設定";
					break;
				case 1:
					$aryCsvLine["config_flg"] = "設定済";
					break;
				default:
					break;
			}
			
			// コメントフラグ 20130724 Nishi Add
			switch( $aryCsvLine["comment_flg"] ){
				case 0:
					$aryCsvLine["comment_flg"] = "なし";
					break;
				case 1:
					$aryCsvLine["comment_flg"] = "あり";
					break;
				default:
					break;
			}

			// 評価フラグ 20130724 Nishi Add
			switch( $aryCsvLine["judge_flg"] ){
				case 0:
					$aryCsvLine["judge_flg"] = "なし";
					break;
				case 1:
					$aryCsvLine["judge_flg"] = "あり";
					break;
				default:
					break;
			}

			// 管理者報告フラグ 20130724 Nishi Add
			switch( $aryCsvLine["report_flg"] ){
				case 0:
					$aryCsvLine["report_flg"] = "なし";
					break;
				case 1:
					$aryCsvLine["report_flg"] = "あり";
					break;
				default:
					break;
			}
/*
			// 削除フラグ
			switch( $aryCsvLine["delete_flg"] ){
				case 0:
					$aryCsvLine["delete_flg"] = "未削除";
					break;
				case 1:
					$aryCsvLine["delete_flg"] = "削除済";
					break;
				default:
					break;
			}
*/
			// メニューカテゴリー
			if( "" != $aryCsvLine["menu_category_name_admin"] ){
				$aryCsvLine["menu_category_name_admin"] = "管理者チャンネル:" . $aryCsvLine["menu_category_name_admin"];
			}
			if( "" != $aryCsvLine["menu_category_name_user"] ){
				$aryCsvLine["menu_category_name_user"] = "視聴者チャンネル:" . $aryCsvLine["menu_category_name_user"];
			}
			
			// 配列を区切り文字「","」で連結する。
			$strCsvLine = implode( '","', $aryCsvLine );
			// 先頭と末尾にダブルクォーテーションを追加してCSVフォーマットを完成させる。
			$strCsvLine = '"' . $strCsvLine . "\"\r\n";
			// ログイン情報書き込み
			fwrite( $rscFilePointer, mb_convert_encoding( $strCsvLine, "sjis-win", "UTF-8" ) );
			
		}
		fclose( $rscFilePointer );
		
		return true;
	}
	
}
