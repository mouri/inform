<?php
/**
 *	基本情報CSV出力画面の制御クラス
 *
 *	基本情報CSV出力画面の制御について記述
 *
 *	@author			Nishi 2013/04/16
 */

class ctlBasicInfoCsvOutput{
	
	/**
	 * コンストラクタ
	 *
	 **/
	function __construct() {
	}
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){
		$aryPostDate            = $_POST;
		$objClsBasicInfoCsvOutput = new clsBasicInfoCsvOutput( $aryPostDate );				// ロジッククラス
		$strAction              = @$aryPostDate["action"];
		
		// 処理分岐
		switch( $strAction ){
			// 出力
			case 'csvoutput':
				$blnResult = $objClsBasicInfoCsvOutput->csvOutput();
				if( false == $blnResult ){
					echo "error";
					break;
				}
				
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="basicInfo.csv"');
				readfile( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . '/admin/basicinfo/basicinfocsvoutput/csv/' . $objClsBasicInfoCsvOutput->getFileName() );
				return;
				break;
				
			// 初期処理
			default:
				break;
		}
		
		require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . '/admin/basicinfo/basicinfocsvoutput/dspBasicInfoCsvOutput.php' );
	}
}

