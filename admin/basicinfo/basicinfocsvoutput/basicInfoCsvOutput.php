<?php
/**
 *	基本情報CSV出力画面
 *
 *	基本情報CSV出力画面の制御クラスを呼び出す
 *
 *	@author		Nishi 2013/04/16
 */

require_once( $_SERVER["DOCUMENT_ROOT"] . "/include.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/basicinfo/basicinfocsvoutput/ctlBasicInfoCsvOutput.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/basicinfo/basicinfocsvoutput/clsBasicInfoCsvOutput.php" );

// アカウントCSV出力制御クラス呼出し
$objCtlBasicInfoCsvOutput = new ctlBasicInfoCsvOutput();
$objCtlBasicInfoCsvOutput->process();

?>