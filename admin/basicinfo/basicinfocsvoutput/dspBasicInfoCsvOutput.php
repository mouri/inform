<?php
/**
 *	基本情報CSV出力画面の画面表示
 *
 *	基本情報CSV出力画面のＨＴＭＬ表示部分を記述
 *
 *	@author nishi 2013/04/16
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
 <meta charset="UTF-8">
<title><?php echo clsDefinition::SYSTEM_NAME?>　基本情報CSV出力</title>
<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<script type="text/javascript">
	$(document).ready(function(){
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if( objAlertError.hasError() ){
			objAlertError.show();
		}
		// 出力ボタンクリック
		$("#output_btn").click( function(){
			$("#action").val( "csvoutput" );
			$("#basicInfoForm").attr( "action", "./basicInfoCsvOutput.php" );
			$("#basicInfoForm").submit();
		});
	});
</script>
</head>
<body id="basicInfo_csv">
	<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	
	<form name="basicInfoForm" id="basicInfoForm" method="post">
		<div class="boxBeige marB20">
			<h2>基本情報CSV出力</h2>
			<div class="form">
				<input type="button" name="output_btn" id="output_btn" class="basicBtn centerBlock" value="CSV出力" />
				<input type="hidden" name="action" id="action" />
			</div>
		</div>
	</form>
	
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>