<?php
	/**
	 * FAQメンテナンス画面
	 * 
	 * FAQメンテナンスで引き継がれた、$_POST["action"]の内容によって処理を分岐するクラス
	 *
	 * @author Nambe 2013/02/14
	 **/

class ctlFaqMainte{

	/**
	 * コンストラクタ
	 **/
	function __construct(){

	}

	/**
	 * 画面処理分岐メソッド
	 *
	 **/
	function process(){
		// インスタンス生成
		$objFaqMainte = new clsFaqMainte( $_POST["question_id"], $_POST["question_rank"], $_POST["search_contact_id"] ); 
		
		$strAction    = @$_POST["action"];
		// 処理分岐
		switch( $strAction ){

			// 質問の新規登録フォームを表示
			case 'dsp_insert_from':
				$objFaqMainte->dspInsertForm();
			break;

			// 質問の新規登録処理
			case 'add_question':
				// 質問を新規登録
				$objFaqMainte->addQuestion();
				// フォーム使用の有無で画面表示切替
				if( 1 == $objFaqMainte->getFormNeedFlg() ){
					// フォーム入力画面を表示
					$objFaqMainte->dspFormSetting();
				}else{
					// FAQのリストを表示
					$objFaqMainte->dspFaqList();
				}
			break;

			// 質問の更新画面を表示
			case 'dsp_update_form':
				$objFaqMainte->dspUpdateForm();
			break;

			// 質問の更新処理
			case 'upd_question':
				// 質問の更新処理
				$objFaqMainte->updQuestionData();
				// フォーム使用の有無で画面表示切替
				if( 1 == $objFaqMainte->getFormNeedFlg() ){
					$objFaqMainte->dspFormUpdate();
				}else{
					// FAQのリストを表示
					$objFaqMainte->dspFaqList();
				}
			break;

			// 質問の表示順番更新処理
			case 'upd_sort_no':
				// 質問の表示順番を更新
				$objFaqMainte->updSortNo($_POST["sort_flg"]);
				// FAQのリストを表示
				$objFaqMainte->dspFaqList();
			break;

			// 質問の削除処理
			case 'del_question':
				// 質問を削除
				$objFaqMainte->delQuestion();
				// FAQのリストを表示
				$objFaqMainte->dspFaqList();
			break;

			// 初期表示。トップ階層の質問一覧表示
			default:
				// FAQのリストを表示
				$objFaqMainte->dspFaqList();
			break;

		}

	}
}
?>
