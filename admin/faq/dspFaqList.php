<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8" />
	<title><?php echo clsDefinition::SYSTEM_NAME?>　FAQメンテナンス</title>
	<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/faq/js/faqMainte.js"></script>
	<script type="text/javascript">
	$(document).ready(function () {
		//画面ＪＳ
		var objDspFaq = new dspFaq();

		// 検索ボタンクリック時
		$("#search_btn").click(function(){
			// 条件設定
			objDspFaq.Search();
		});

	});
</script>
</head>
<body id="faqList">
	<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	<!-- フォーム開始 -->
	<form action="faqMainte.php" method="post" name="faq_mainte" id="faq_mainte">
		<input type="hidden" id="question_rank"      name="question_rank" value="<?php f::p($this->intQuestionRank); ?>" />
		<input type="hidden" id="question_id"        name="question_id"   value="<?php f::p($this->intQuestionId); ?>" />
		<input type="hidden" id="parent_question_id" name="parent_question_id" value="" />
		<input type="hidden" id="action"             name="action"   value="" />
		<input type="hidden" id="sort_flg"           name="sort_flg" value="" />

	<div class="boxBeige">
		<h2>FAQ一覧</h2>
		<div class="form">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="c1 fullWidth">
						<p class="komoku">契約会社名</p>
						<select name="search_contact_id" id="search_contact_id">
							<?php echo $strContactSelectBox; ?>
						</select>
					</td>
					<td>
						<p class="komoku"><br /></p>
						<div class="basicBtn search">
							<input type="button" name="search_btn" id="search_btn" value="検索" />
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>

<?php if( false == is_null($this->intQuestionId) ){ ?>

		<!-- 現在の階層の質問一覧を表示する -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
			<tr>
				<th colspan="5" class="top" style="text-align:left;">
				<!-- パンくずリスト表示 -->
					<ul>
					<?php for($i=0;$i<count($aryPankuzuData);$i++){ ?><!-- 配列に格納している分だけ、パンくずリストを生成 -->
						<li style="display:inline;"><a href="javascript:setValueSubmit('faq_mainte',{question_id:<?php f::p($aryPankuzuData[$i]["question_id"]); ?>,parent_question_id:<?php f::p($aryPankuzuData[$i]["question_id"]); ?>,action:'move_question_rank'})" ><?php f::p($aryPankuzuData[$i]["question_text"]); ?></a></li>
						 <?php if($i != count($aryPankuzuData)-1){ ?> &#62; <?php } ?>
					<?php } ?>
					</ul>
				</th>
			</tr>
			<tr>
				<th colspan="5" class="pager">
					<!-- ページャー表示 -->
					<?php echo $this->objPager->get(); ?>
				</th>
			<tr>
				<th >質問内容</th>
				<th >返答内容</th>
				<th >システムメッセージ</th>
				<th >順番入替</th>
				<th >一覧表示・編集・削除</th>
			</tr>
		<?php ###  TABLE DETAIL LOOP ####
		for($i=0;$i<count($aryFaqData);$i++){ 
			// 表示文言を変数に格納
			// 質問内容
			$strQuesionText = nl2br($aryFaqData[$i]["question_text"]);
			// 回答内容 親質問コードが0の場合、第一階層なのでメッセージを表示
			$strAnswerText  = ( 0 == $aryFaqData[$i]["parent_question_id"] ) ? '<span class="red">第一階層はカテゴリとなる為、返答内容は入力できません</span>' : $aryFaqData[$i]["answer_text"];
			// システムメッセージ代入 表示する質問の下位に紐付く質問がなく且つ回答の登録されていなければ、注意メッセージ表示
			$strSystemMessage = (0 == $aryFaqData[$i]["child_amount"] && "" == $aryFaqData[$i]["answer_text"]) ? '<span class="red">質問に対する答えがないか分岐となる質問がないため、サイトでは表記されない状態です</span>' : '' ;
		?>
			<tr>
				<td >
					 <?php echo $strQuesionText; ?>
				</td>
				<td ><?php echo $strAnswerText; ?></td>
				<td ><?php echo $strSystemMessage;?></td>
				<td >
					<?php if($aryFaqData[$i]["sort_no"] < $this->intQuestionAMount) { ?>
						<a href="javascript:setValueSubmit('faq_mainte',{question_id:<?php f::p($aryFaqData[$i]["question_id"]); ?>,action:'upd_sort_no',sort_flg:0})" >▼</a>
					<?php }else{ echo "　";} // 一番最後の質問は▼が表示されない為、位置を調整する為の全角スペースをechoする?>
					<?php if($aryFaqData[$i]["sort_no"] != 1){ ?>
						<a href="javascript:setValueSubmit('faq_mainte',{question_id:<?php f::p($aryFaqData[$i]["question_id"]); ?>,action:'upd_sort_no',sort_flg:1})" >▲</a>
					<?php } ?>
				</td>
				<td class="minW170">
					<?php // クラスに設定されている最大階層未満 且つ 回答が登録されていなければ この質問にぶら下がる質問が登録できるので階層移動ボタンを表示する
						if($aryFaqData[$i]["question_rank"] < $this->intMaxRank && $aryFaqData[$i]["answer_text"] == ""){ ?>
					<div class="basicBtn detail w40">
						<input type="button" onclick="setValueSubmit('faq_mainte',{question_id:<?php f::p($aryFaqData[$i]["question_id"]); ?>,question_rank:<?php f::p($aryFaqData[$i]["question_rank"]); ?>})" value="一覧" />
					</div>
					<?php } ?>
					<div class="basicBtn edit w40">
						<input type="button" onclick="setValueSubmit('faq_mainte',{question_id:<?php f::p($aryFaqData[$i]["question_id"]); ?>,action:'dsp_update_form',parent_question_id:<?php f::p($aryFaqData[$i]["parent_question_id"]); ?>})" value="編集" />
					</div>
					<div class="basicBtn del w40">
						<input type="button" value="削除" onclick="delConfirm('faq_mainte',<?php echo $aryFaqData[$i]['question_id'];?>)" />
					</div>
				</td>
			</tr>
		<?php } ?>
		</table>
		<div class="basicBtn edit w70 marT20">
			<input class="centerBlock" type="button" value="質問を追加" onclick="setValueSubmit('faq_mainte',{action:'dsp_insert_from',question_id:'<?php f::p($this->intQuestionId);?>'})"/>
		</div>
<?php } ?>
	</form>
	</div>
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
