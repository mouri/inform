//コンストラクタ
var dspFaq = function() {};

// 検索処理
dspFaq.prototype.Search = function(){
	//$("#action").val( "search" );
	$("#faq_mainte").attr("action", "./faqMainte.php");
	$("#faq_mainte").submit();
}

	/**
	 * フォームをサブミットする際に特定の要素の値を変更してサブミットする関数
	 *
	 * @author Nambe
	 *
	 * @param formId   : サブミットしたいフォームのIDを「'」で括って指定
	 * @param valueObj : 値をセットしたいname値：セットする値のハッシュ。
	 * 					 文字列は「'」で括ること
	 * 					 { dsiplay_flg:1,action:'delete'}
	 * 					 など
	 **/
	function setValueSubmit(formId,valueObj){
		jQuery.each(valueObj,function(key, val){
			$("input[name='" + key + "']").val(val);
		});
		$('#' + formId).submit();
	}

	/**
	 * 質問削除時にモーダルダイアログを表示して最終確認を行う関数
	 *
	 * @param integer questionId : 削除対象の質問ID
	 **/
	function delConfirm(formId,questionId){
		ModalDialog.confirm( "confirm",　"この質問に紐付く全ての質問も削除されますがよろしいですか？" ,
						function( blnResult ){
							if( true == blnResult ){
								setValueSubmit(formId,{question_id:questionId,action:'del_question'});
							}
						}
					);

	}

//==============================================================================
// 入力項目HTMLタグを作成する
//==============================================================================
function setFormTag(intRowNumber, strElem)
{
	if (strElem == undefined) strElem = '';

	var strTag              = '';						// 追加する入力項目HTMLタグ
	var strDisplayNameId    = 'display_name' + intRowNumber;		// 入力項目名称のID・name名
	var strInputTypeId      = 'input_type' + intRowNumber;			// 入力項目タイプのID・name名
	var strErrorCheckId     = 'err_check_type' + intRowNumber;		// エラーチェックタイプのID・name名
	var strNeedFlgId        = 'need_flg' + intRowNumber;			// 必須フラグのID・name名
	
	//-----------------------------
	// 入力項目のHTMLタグを作成する
	//-----------------------------
	strTag = '<tr>'
	 		+ '<td class="class_row_number" style="width:5px; vertical-align:middle;">'
			+ intRowNumber
			+ '</td>'
			+ '<td >'
			+ '項目名称名称 <input type="text" name="' + strDisplayNameId + '" id="' + strDisplayNameId + '" value="" size="11" maxlength="20" style="ime-mode:active;" /><br />'
			+ '入力項目タイプ '
			+ '<select name="' + strInputTypeId + '" id="' + strInputTypeId + '">'
			+ '	<option value="1">テキストボックス</option>'
			+ '	<option value="2">テキストエリア</option>'
			+ '</select>'
			+ '<br />'
			+ 'エラーチェック'
			+ '<select name="' + strErrorCheckId + '" id="' + strErrorCheckId + '">'
			+ '	<option value="9">エラーチェック無し</option>'
			+ '	<option value="1">半角英数字</option>'
			+ '	<option value="2">半角数字</option>'
			+ '	<option value="3">メールアドレス</option>'
			+ '	<option value="4">電話番号</option>'
			+ '</select>'
			+ '<br />'
			+ '任意/必須 '
			+ '<select name="' + strNeedFlgId + '" id="' + strNeedFlgId + '">'
			+ '	<option value="0">任意入力</option>'
			+ '	<option value="1">必須入力</option>'
			+ '</select>'
			+ '</td>'
			+ '<td style="vertical-align:middle;">'
			+ '<input value="削除" type="button" class="removeList" />'
			+ '　'
			+ '<input value="下に追加" type="button" class="addList" />'
			+ '</td>'
			+ '</tr>';

	// 入力項目のHTMLタグをtableタグの末尾に追加する
	if (strElem == '') {
		$("table[id='form_setting']").append(strTag);
	} else {
		strElem.parent().parent().after(strTag);
	}

	// IDなどの番号振りなおし
	setID();

}

//==============================================================================
// 各入力項目のID番号振りなおしを行う
//==============================================================================
function setID() {

	// tableのtr分だけループし、それぞれにIDを振り直す
	$('#form_setting tr').each(function(i) {

		// 各IDの再設定
		$(this).find("td[class='class_row_number']").html(i + 1);
		$(this).find("[id^='display_name']").attr('id', 'display_name' + (i + 1));
		$(this).find("[id^='display_name']").attr('name', 'display_name' + (i + 1));
		$(this).find("[id^='input_type']").attr('id', 'input_type' + (i + 1));
		$(this).find("[id^='input_type']").attr('name', 'input_type' + (i + 1));
		$(this).find("[id^='err_check_type']").attr('id', 'max_length' + (i + 1));
		$(this).find("[id^='err_check_type']").attr('name', 'max_length' + (i + 1));
		$(this).find("[id^='need_flg']").attr('id', 'min_image_width' + (i + 1));
		$(this).find("[id^='need_flg']").attr('name', 'min_image_width' + (i + 1));
			
	});
	
}
