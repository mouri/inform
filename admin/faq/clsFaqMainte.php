<?php
	/**
	 * FAQメンテナンス画面
	 * 
	 * FAQメンテナンスで引き継がれた、$_POST["action"]の内容によって処理を分岐するクラス
	 *
	 * @author Nambe 2013/02/14
	 **/

class clsFaqMainte{

	// FAQの最大階層を設定する変数
	private $intMaxRank;

	/**
	 * FAQでフォームを使用させるか否かの変数
	 * true  : 使用可
	 * false : 使用不可
	 **/
	private $blnFormUseFlg;

	// DB処理オブジェクトを格納する変数
	private $objDataBase;

	// ページャーオブジェクトを格納する変数
	private $objPager;

	// エラーチェッククラス
	private $objChecker;

	// 契約IDを格納する変数。コンストラクタで設定
	private $intContactId;

	// 表示する階層の質問数を保存する変数
	private $intQuestionAMount;

	// 処理を行う質問IDをセットする変数
	private $intQuestionId;

	// 処理を行う質問階層をセットする変数
	private $intQuestionRank;

	// 処理している質問にフォームを使用するか否かのフラグ
	private $intFormNeedFlg;

	// メッセージ格納変数
	private $strErrorJson;

	/**
	 * コンストラクタ
	 **/
	function __construct( $intQuestionId, $intQuestionRank, $strSearchContactId ){
		//データベース接続のインスタンス生成
		$this->objDatabase = clsMillviDatabase::getInstance();
		// エラー発生時のメール送信先設定
		$this->objDatabase->setAdminAddress( "nishi@inform.co.jp" );
		// ページャクラスのインスタンス生成
		$this->objPager = new clsPager();
		// エラーチェッククラスのインスタンス生成
		$this->objChecker = new clsFaqChecker( $_POST );
		// 契約ID、質問ID、質問階層をメンバー変数に設定
		$this->intContactId = $strSearchContactId;//clsContactSession::getContactId();
		// 契約ID別のFAQの設定を取得
		/*$ayFaqConfig = $this->getFaqConfig();
		// データが取得できればFAQが使用可能としてメンバー変数に設定情報を格納
		/*if( 1 == count($ayFaqConfig)){
			$this->intMaxRank    = $ayFaqConfig[0]["max_rank"];
			$this->blnFormUseFlg = ( 1 == $ayFaqConfig[0]["form_use_flg"]) ? true : false;
			$this->setQuestionId($intQuestionId);
			$this->setQuestionRank($intQuestionRank);
		}*/

		// 契約会社が選択されている場合
		if( '' != $this->intContactId ){
			$this->intMaxRank    = clsDefinition::FAQ_MAX_RANK;
			$this->blnFormUseFlg = ( 1 == clsDefinition::FAQ_FORM_USE_FLG ) ? true : false;
			$this->setQuestionId($intQuestionId);
			$this->setQuestionRank($intQuestionRank);
		}
	}

	/**
	 *
	 **/
	function getFormNeedFlg(){
		return $this->intFormNeedFlg;
	}


	/**
	 * 質問を削除する関数
	 * 送信された質問IDを削除し、削除された質問を根とするツリー構造の質問全てを削除。
	 * 削除された「質問と同階層」且つ「表示順番が大きい」且つ「表示状態」の質問全ての表示順番をデクリメントする
	 *
	 * @author Nambe
	 * @date   2013/02/20
	 **/
	function delQuestion(){
		
		// 更新者情報設定
		$intDeleteId   = clsLoginSession::getUserId();
		$strDeleteName = clsLoginSession::getFullName();
		$strDeleteTime = clsCommonFunction::getCurrentDate();
		
		// 削除ボタンが押下された質問を削除
		// 削除データを設定
		$aryDeleteData = array("delete_flg" => 1,
							   "upd_id"     => $intDeleteId,
							   "upd_name"   => $strDeleteName,
							   "upd_time"   => $strDeleteTime
							  );
		// 削除条件を設定
		$aryQueryParam = array("contact_id"  => $this->intContactId,
							   "question_id" => $_POST["question_id"]
							  );
		// データを削除（削除フラグ更新）
		$this->updQuestion($aryDeleteData,$aryQueryParam);
		// 削除ボタンが押下された質問をルートとする質問一覧を配列で取得
		$aryDelQuestion = $this->getDellList($_POST["question_id"]);
		// ループ処理で対象となる質問を全て削除（削除フラグ更新）
		while($intDelQuestionId = array_shift($aryDelQuestion)){
			$aryQueryParam = array("contact_id"  => $this->intContactId,
								   "question_id" => $intDelQuestionId
								  );
			$this->updQuestion($aryDeleteData,$aryQueryParam);
		}
		
		// ここから表示順番更新処理
		// 削除ボタンが押下された質問データを取得
		$aryDeleteData     = $this->getQuestion(array("question_id" => $_POST["question_id"]));
		$aryDeleteQuestion = array_shift($aryDeleteData);
		
		// 表示順番更新対象の質問を取得
		$sqlGetQuestions =<<<SQL
SELECT question_id,
       sort_no
  FROM inform_faq_trn
 WHERE parent_question_id = :parent_question_id 
   AND sort_no > :sort_no
   AND delete_flg = 0
   AND contact_id = :contact_id
;
SQL;
		// プリペアードパラメーターを設定
		$aryPrePare = array(":parent_question_id" => $aryDeleteQuestion["parent_question_id"],
							":sort_no"            => $aryDeleteQuestion["sort_no"],
							":contact_id"         => $this->intContactId
						   );
		
		$aryUpdateQuestions = $this->objDatabase->pullDbData($sqlGetQuestions, $aryPrePare);
		
		// ループ処理でそれぞれの表示順番を更新
		while($arySortUpd = array_shift($aryUpdateQuestions)){
			$aryUpdData = array("sort_no"    => $arySortUpd["sort_no"] -1,
							    "upd_id"     => $intDeleteId,
							    "upd_name"   => $strDeleteName,
							    "upd_time"   => $strDeleteTime
							   );
			$aryUpdParam = array("contact_id"  => $this->intContactId,
								 "question_id" => $arySortUpd["question_id"]
								);
			
			$this->updQuestion($aryUpdData,$aryUpdParam);
		}
		
		$this->dspMainteComplete(3,$this->intContactId);
	}

	/**
	 * 削除ボタンが押下された質問から再帰的に一番深い階層まで削除対象として、質問IDを取得する関数。
	 * 削除ボタンが押下された質問IDは含まれない
	 *
	 * @author Nambe
	 * @date   2013/02/20
	 *
	 * @param integer $intQuestionCd : 削除ボタンが押下された質問の質問ID
	 *
	 * return array : 削除対象の質問IDを値に持つ数値添え字配列
	 **/
	function getDellList($intQuestionCd){
		$aryParentQuestion  = array();
		$aryTempDellList    = array();
		$aryDellList        = array();

		// 子を取得する質問IDを変数に格納
		$aryParentQuestion[] = $intQuestionCd;
		// 子を取得したい質問件数分ループ
		while( count($aryParentQuestion) > 0){
			// 削除対象の質問件数分、子となる質問を取得して削除対象にする
			while($intTargetQuestionId = array_shift($aryParentQuestion)){
				// 削除対象となっている質問の子供を取得
				$aryDellQuestion = $this->getQuestion(array("parent_question_id" => $intTargetQuestionId));
				// 同じ親を持つ質問一覧を削除対象として変数に格納
				if( 0 < count($aryDellQuestion) ){
					while($aryTmpData = array_shift($aryDellQuestion)){
						$aryTempDellList[] = $aryTmpData["question_id"];
						$aryDellList[]     = $aryTmpData["question_id"];
					}
				}
			}
			// 削除対象の質問を親に持つ質問を削除対象としてコピー。
			// 対象データが存在しなかった場合、空配列を格納
			$aryParentQuestion = $aryTempDellList;
			// 削除対象の質問コードを親に持つ質問ID一覧を格納する変数初期化
			$aryTempDellList = array();
			
		}

		return $aryDellList;
	}

	/**
	 * 質問入力更新フォームを表示
	 **/
	function dspUpdateForm(){

		// 画面遷移時に修正したい質問の質問IDがPOSTされているので、該当データを取得
		$mixResult = $this->getQuestion(array("question_id" => $_POST["question_id"]));

		// データ取得エラー
		if( $this->objDatabase->isError( $mixResult ) ){
			$this->objChecker->setError();
			$this->objChecker->setErrorMessage( "error", "質問データを取得できませんでした。" );
			$this->strErrorJson = $this->objChecker->getErrorJson();
		}// 該当質問無し
		elseif( 0 == count($mixResult)){
			$this->objChecker->setError();
			$this->objChecker->setErrorMessage( "error", "質問データは存在しません。" );
			$this->strErrorJson = $this->objChecker->getErrorJson();
		}// 削除済み
		elseif( 1 == $mixResult[0]["delete_flg"]){
			$this->objChecker->setError();
			$this->objChecker->setErrorMessage( "error", "選択された質問は削除済みです。" );
			$this->strErrorJson = $this->objChecker->getErrorJson();
		}// データ取得成功
		else{
			$aryQuestionData = array_shift($mixResult);
			// 編集使用としている質問を親とする数を取得
			$aryQuestionData["child_amount"] = $this->getFaqCntByParent($aryQuestionData["question_id"]);
			// 質問階層が1階層目、又は編集しようとしている質問を親に持つ質問があれば回答が入力できないので
			// エラーメッセージを設定しておく
			if( 1 == $aryQuestionData["question_rank"] ){
				$this->objChecker->setError();
				$this->objChecker->setErrorMessage( "error", "一階層目の質問には回答は入力できません。" );
				$this->strErrorJson = $this->objChecker->getErrorJson();
			}elseif( 0 != $aryQuestionData["child_amount"]){
				$this->objChecker->setError();
				$this->objChecker->setErrorMessage( "error", "この質問に紐付く質問がある為、回答は入力できません。" );
				$this->strErrorJson = $this->objChecker->getErrorJson();
			}
		}

		// パンくずリストを表示する為に、選択された質問の親質問情報を取得しメンバー変数の質問ID・ランクにセット
		$aryParentData = $this->getQuestion(array("question_id" => $aryQuestionData["parent_question_id"]));
		$this->setQuestionId($aryParentData[0]["question_id"]);
		$this->setQuestionRank($aryParentData[0]["question_rank"]);

		$aryPankuzuData = $this->getPankuzuData();


		// 更新フォーム表示
		require_once('dspUpdateForm.php');
	}

	/**
	 * 質問入力更新時にエラーが発生した際にフォームを再表示
	 **/
	function dspUpdateErrorForm(){

		$aryQuestionData = $_POST;
		// 編集使用としている質問を親とする数を取得
		$aryQuestionData["child_amount"] = $this->getFaqCntByParent($aryQuestionData["question_id"]);
		// 質問階層が1階層目、又は編集しようとしている質問を親に持つ質問があれば回答が入力できないので
		// エラーメッセージを設定しておく
		if( 1 == $aryQuestionData["question_rank"] ){
			$this->objChecker->setError();
			$this->objChecker->setErrorMessage( "error", "一階層目の質問には回答は入力できません。" );
			$this->strErrorJson = $this->objChecker->getErrorJson();
		}elseif( 0 != $aryQuestionData["child_amount"]){
			$this->objChecker->setError();
			$this->objChecker->setErrorMessage( "error", "この質問に紐付く質問がある為、回答は入力できません。" );
			$this->strErrorJson = $this->objChecker->getErrorJson();
		}

		// パンくずリストを表示する為に、選択された質問の親質問情報を取得しメンバー変数の質問ID・ランクにセット
		$aryParentData = $this->getQuestion(array("question_id" => $aryQuestionData["parent_question_id"]));
		$this->setQuestionId($aryParentData[0]["question_id"]);
		$this->setQuestionRank($aryParentData[0]["question_rank"]);

		$aryPankuzuData = $this->getPankuzuData();


		// 更新フォーム表示
		require_once('dspUpdateForm.php');
	}

	/**
	 * 質問入力フォームを表示
	 **/
	function dspInsertForm(){
		// POSTされたデータを表示用変数に格納
		$aryPostData = $_POST;

		// パンくずデータを取得
		$aryPankuzuData = $this->getPankuzuData();

		require_once('dspInsertForm.php');
		exit();
	}

	/**
	 * 入力された質問をデータベースに登録
	 * 制御ファイルで読み込みをされていたエラーチェッククラスを使用して、入力された値を確認
	 * エラーが発生していなければ、データ登録処理を行う
	 * 
	 *
	 **/
	function addQuestion(){
		// エラーチェック
		$this->objChecker->chkFaqMasterInput();

		// エラーが発生してれば、入力画面を再度表示して終了
		if( true == $this->objChecker->isError() ){
			$this->strErrorJson = $this->objChecker->getErrorJson();
			$this->dspInsertForm();
		}

		// question_idの最大値を取得
		// $intQuestionId = clsCommonFunction::getNextId('inform_faq_trn', 'question_id');

		// Nishi追加　各契約IDごとのquestion_idの最大値を取得　2013/04/23　ここから
		$strSelectSql =<<<SQL
	 SELECT COALESCE( MAX( question_id ), 0) + 1 AS question_id
     FROM inform_faq_trn
     WHERE contact_id = :contact_id
SQL;

		$arySelectData = array(
								":contact_id" => $this->intContactId
								);

		$intQuestionId = $this->objDatabase->pullDbData($strSelectSql, $arySelectData);
		$intQuestionId = $intQuestionId[0]["question_id"];
		//　ここまで

		// 質問ランクを設定。
		// 親質問コードが0であれば、第一階層。それ以外であれば、親質問コードの質問階層+1を設定する。
		switch($this->intQuestionRank){
			case 0:
				$intQuestionRank = 1;
			break;

			default:
				// 親質問の情報を取得
				$aryParentQuestion = $this->getQuestion(array("question_id" => $this->intQuestionId));
				$intQuestionRank = $aryParentQuestion[0]["question_rank"] + 1;

			break;
		}
		// 親質問IDを設定
		$intParentQuestionId = $this->intQuestionId;
		
		// 送信されたquestion_idを親に持つテーブルの表示順番の最大値を取得
		/*$aryGetSortParam = array("parent_question_id" => $this->intQuestionId,
								 "delete_flg" => 0
								);
		$intSortNumber = clsCommonFunction::getNextId('inform_faq_trn', 'sort_no',$aryGetSortParam);*/

		// Nishi追加　各契約IDのsortを取得　2013/04/23　ここから
		$strSelectSql =<<<SQL
	 SELECT COALESCE( MAX( sort_no ), 0) + 1 AS sort_no
     FROM inform_faq_trn
     WHERE contact_id = :contact_id
     AND parent_question_id = :parent_question_id
     AND delete_flg = :delete_flg
SQL;

		$arySelectData = array(
								":contact_id" => $this->intContactId,
								":parent_question_id" => $this->intQuestionId,
								":delete_flg" => 0
								);

		$intSortNumber = $this->objDatabase->pullDbData($strSelectSql, $arySelectData);
		$intSortNumber = $intSortNumber[0]["sort_no"];
		// ここまで

		$strQuestionText = trim($_POST["question_text"]);
		$strAnswerText   = trim($_POST["answer_text"]);

		$this->intFormNeedFlg  = ( 1 == $_POST["form_need_flg"] ) ? $_POST["form_need_flg"] : 0;
		$intDeleteFlg    = 0;
		// 登録者・更新者情報設定
		$intInsertId   = clsLoginSession::getUserId();
		$strInsertName = clsLoginSession::getFullName();
		$strInsertTime = clsCommonFunction::getCurrentDate();

		// 登録するデータを配列に格納
		$aryInsertData = array("contact_id"         => $this->intContactId,
							   "question_id"        => $intQuestionId,
							   "question_rank"      => $intQuestionRank,
							   "parent_question_id" => $intParentQuestionId,
							   "sort_no"            => $intSortNumber,
							   "question_text"      => $strQuestionText,
							   "answer_text"        => $strAnswerText,
							   "form_need_flg"      => $this->intFormNeedFlg,
							   "delete_flg"         => $intDeleteFlg,
							   "reg_id"             => $intInsertId ,
							   "reg_name"           => $strInsertName,
							   "reg_time"           => $strInsertTime,
							   "upd_id"             => $intInsertId ,
							   "upd_name"           => $strInsertName,
							   "upd_time"           => $strInsertTime
							  );

		// ループ処理でSQLの文字列とプリペアードパラメーターを設定
		foreach($aryInsertData as $strColumn => $mixValue){
			$aryColumn[]   = $strColumn;
			$aryPrepare[]  = ":{$strColumn}";
			$aryQueryParam[":{$strColumn}"] = $mixValue;
		}

		// 配列に格納したSQL文字列を連結
		$strColumn  = implode(", ",$aryColumn);
		$strPrepare = implode(", ",$aryPrepare);

		// Insert SQL 生成
		$sqlInsert =<<<SQL
INSERT INTO inform_faq_trn ({$strColumn}) 
     VALUES ($strPrepare)
SQL;

		$this->objDatabase->execTransaction();

		$mixResult = $this->objDatabase->addDbData($sqlInsert, $aryQueryParam);
		
		if( $this->objDatabase->isError( $mixResult ) ){
			// ロールバック
			$this->objDatabase->execRollback();
			$this->objChecker->setError();
			$this->objChecker->setErrorMessage( "error", "質問の登録に失敗しました。" );
			$this->strErrorJson = $this->objChecker->getErrorJson();
			
			return false;
		}
		
		// 契約者の履歴番号の最大値を取得
		//$intPastNumber = clsCommonFunction::getNextId("inform_faq_trn_past", "past_number");

		// Nishi追加　各契約IDごとの履歴番号を取得　2013/04/23　ここから
		$strSelectSql =<<<SQL
	 SELECT COALESCE( MAX( past_number ), 0) + 1 AS past_number
     FROM inform_faq_trn_past
     WHERE contact_id = :contact_id
SQL;

		$arySelectData = array(
								":contact_id" => $this->intContactId
								);

		$intPastNumber = $this->objDatabase->pullDbData($strSelectSql, $arySelectData);
		$intPastNumber = $intPastNumber[0]["past_number"];
		//　ここまで

		// 登録した質問を履歴に保存
		$aryPastData = array("past_number" => $intPastNumber,
							 "upd_kbn"     => 1
							);

		$aryPastParam = array("contact_id"  => $this->intContactId,
							  "question_id" => $intQuestionId
							 );

		$mixResult = $this->addQuestionPast($aryPastData, $aryPastParam);

		if( $this->objDatabase->isError( $mixResult ) ){
			$this->objDatabase->execRollback();
			$this->objChecker->setError();
			$this->objChecker->setErrorMessage( "error", "履歴の登録に失敗しました。" );
			$this->strErrorJson = $this->objChecker->getErrorJson();
			return false;
		}

		$this->objDatabase->execCommit();

		$this->dspMainteComplete(1,$this->intContactId);

	}


	/**
	 * FAQトラン履歴にデータを登録する関数
	 * 履歴テーブルにのみ保存するデータ以外は、FAQトランからSELECT INSERTで保存する
	 * 
	 * @author Nambe
	 * @date   2013/02/19
	 *
	 * @param array $aryUpdateData : 履歴テーブルにのみ登録するカラムをキー、値をvalueに持つ連想配列
	 * @param array $aryWhereParam : FAQトランのデータを指定する為のWhere句に使用するカラムをキーと値をvalueに持つ連想配列
	 * 
	 * return boolean : 履歴に登録成功した場合true。失敗した場合false
	 **/
	function addQuestionPast($aryInsertData, $aryWhereParam){
		// インサートデータ数分クエリ文字列、プリペアードパラメーターを生成
		foreach($aryInsertData as $strInsertColumn => $strInsertValue){
			$aryDataArray[] = ":add_{$strInsertColumn}";
			$aryQueryParam[":add_{$strInsertColumn}"] = $strInsertValue;
		}
		
		// Where句用の文字列、パラメーターを設定
		foreach($aryWhereParam as $strWhereColumn => $strWhereValue){
			$aryWhereArray[] = "{$strWhereColumn} = :where_{$strWhereColumn}";
			$aryQueryParam[":where_{$strWhereColumn}"] = $strWhereValue;
		}
		
		// 履歴テーブルのみに登録する値をクエリ文字列に変換
		$strInsertQuery = implode(', ', $aryDataArray);
		
		// FAQトランを特定するWHERE句をクエリ文字列に変換
		if( false == empty($aryWhereArray)){
			$strWhereData  = implode(' AND ',$aryWhereArray);
			$strWhereQuery = " WHERE " . $strWhereData;
		}

		// 履歴にInsertするクエリを生成
		$sqlInsertPast = <<<SQL
INSERT INto inform_faq_trn_past(
	SELECT {$strInsertQuery} ,
           inform_faq_trn.*
      FROM inform_faq_trn
     {$strWhereQuery}
	)
SQL;

		return $this->objDatabase->addDbData($sqlInsertPast, $aryQueryParam);

	}

	/**
	 * 質問編集画面から送信された質問内容、回答内容を更新する
	 *
	 * @author Nambe
	 * @date   2013/02/20
	 **/
	function updQuestionData(){
		// エラーチェック
		$this->objChecker->chkFaqMasterInput();

		// エラーが発生してれば、入力画面を再度表示して終了
		if( true == $this->objChecker->isError() ){
			$this->strErrorJson = $this->objChecker->getErrorJson();
			$this->dspUpdateErrorForm();
			exit();
		}
		
		$this->intFormNeedFlg = ( 1 == $_POST["form_need_flg"] ) ? $_POST["form_need_flg"] : 0;
		// 更新データ配列を作成
		$aryUpdateData = array("question_text" => trim($_POST["question_text"]),
							   "answer_text"   => trim($_POST["answer_text"]),
							   "form_need_flg" => $this->intFormNeedFlg,
							   "upd_id"        => clsLoginSession::getUserId(),
							   "upd_name"      => clsLoginSession::getFullName(),
							   "upd_time"      => clsCommonFunction::getCurrentDate()
							  );

		// データ更新条件配列を作成
		$aryWhereQuery = array("contact_id"  => $this->intContactId,
							   "question_id" => $_POST["question_id"]
							  );

		// 質問を更新
		$mixResult = $this->updQuestion($aryUpdateData, $aryWhereQuery);

		if( $this->objDatabase->isError( $mixResult ) ){
			$this->objChecker->setError();
			$this->objChecker->setErrorMessage( "error", "質問の更新に失敗しました。" );
			$this->strErrorJson = $this->objChecker->getErrorJson();
		}else{
			$this->dspMainteComplete(2,$this->intContactId);
		}

	}

	/**
	 * 質問を更新する関数
	 *
	 * @author Nambe
	 * @date   2013/02/18
	 *
	 * @param array $aryUpdateData : 更新するカラムをキー、値をvalueに持つ連想配列
	 * @param array $aryWhereParam : 更新するデータのWhere句に使用するカラムをキーと値をvalueに持つ連想配列
	 **/
	function updQuestion($aryUpdateData, $aryWhereParam){
		// アップデートデータ件数分クエリ文字列、プリペアードパラメータを生成
		foreach($aryUpdateData as $strUpdateColumn => $strUpdateValue){
			$aryDataArray[] = "{$strUpdateColumn} = :upd_{$strUpdateColumn}";
			$aryQueryParam[":upd_{$strUpdateColumn}"] = $strUpdateValue;
		}
		
		// Where句用の文字列、パラメーターを設定
		foreach($aryWhereParam as $strWhereColumn => $strWhereValue){
			$aryWhereArray[] = "{$strWhereColumn} = :where_{$strWhereColumn}";
			$aryQueryParam[":where_{$strWhereColumn}"] = $strWhereValue;
		}
		
		// アップデートする値をクエリ文字列に変換
		$strUpdateData = implode(', '   ,$aryDataArray);
		
		// データ更新条件があれば、WHERE句を作成
		if( false == empty($aryWhereArray)){
			$strWhereData  = implode(' AND ',$aryWhereArray);
			$strWhereData = " WHERE " . $strWhereData;
		}
		
		// アップデートクエリ作成
		$sqlUpdateQuery = "UPDATE inform_faq_trn SET {$strUpdateData} {$strWhereData}";
		
		$this->objDatabase->execTransaction();
		$mixResult = $this->objDatabase->updDbData($sqlUpdateQuery, $aryQueryParam);
		
		if( false == $mixResult){
			$this->objDatabase->execRollback();
			return false;
		}
		
		// 契約者の履歴番号の最大値を取得
		//$intPastNumber = clsCommonFunction::getNextId("inform_faq_trn_past", "past_number");

		// Nishi追加　各契約IDごとの履歴番号を取得　2013/04/23　ここから
		$strSelectSql =<<<SQL
	 SELECT COALESCE( MAX( past_number ), 0) + 1 AS past_number
     FROM inform_faq_trn_past
     WHERE contact_id = :contact_id
SQL;

		$arySelectData = array(
								":contact_id" => $this->intContactId
								);

		$intPastNumber = $this->objDatabase->pullDbData($strSelectSql, $arySelectData);
		$intPastNumber = $intPastNumber[0]["past_number"];
		//　ここまで
		
		// inform_faq_trn_pastにのみ保存する値を設定
		$aryInsertData = array("past_number" => $intPastNumber,
							   "upd_kbn"     => 2
							  );
		
		
		$mixResult = $this->addQuestionPast($aryInsertData, $aryWhereParam);

		if( false == $mixResult){
			$this->objDatabase->execRollback();
			return false;
		}


		$this->objDatabase->execCommit();

		return true;
	}


	/**
	 * 質問一覧の表示順番を更新する関数
	 *
	 * @author Nambe
	 * @date   2013/02/18
	 * 
	 * @param integer $intSortFlag : 表示順番を処理するフラグ。
	 * 								 1:上げる
	 * 								 0:下げる
	 * 
	 **/
	function updSortNo($intSortFlag){
		// 処理ボタンが押下された質問情報を取得
		$aryTempData         = $this->getQuestion( array("question_id" => $this->intQuestionId) );
		$aryMainQuestionData = array_shift($aryTempData);
		// 処理ボタンが押下された質問と同じ階層の質問件数を取得
		$intQuestionAMount = $this->getFaqCntByParent($aryMainQuestionData["parent_question_id"]);

		// 「表示順番が１番でなく、表示順番を上げる」 又は 「表示順番が最下位でなく順番を下げる」　場合に順番変更処理を行う
		if( (1 != $aryMainQuestionData["sort_no"] && 1 == $intSortFlag )
			|| ( $intQuestionAMount != $aryMainQuestionData["sort_no"] && 0 == $intSortFlag )
		){
			
			switch($intSortFlag){
				//順位を下げる
				case 0:
				// 変更する表示順番
				$intChangeSortNo = $aryMainQuestionData["sort_no"]+1;
				break;
				
				case 1:	//順位を上げる
				// 変更する表示順番
				$intChangeSortNo = $aryMainQuestionData["sort_no"]-1;
				break;
			}
			
			// ボタンを押下された質問と表示順番を入れ替える質問の情報を取得
			$aryTempData = $this->getQuestion( array("parent_question_id" => $aryMainQuestionData["parent_question_id"],
													 "sort_no"            => $intChangeSortNo,
													 "delete_flg"         => 0
													 )
											 );
			$aryTargetQuestionData = array_shift( $aryTempData );
			// データ更新者ID、データ更新者名、データ更新日時を取得
			$intUpdId   = clsLoginSession::getUserId();						//ログインしているユーザID取得
			$strUpdName = clsLoginSession::getFullName();
			$strUpdTime = clsCommonFunction::getCurrentDate();
			
			// ボタンを押下された質問の更新データ、更新条件を作成
			$aryMainUpdData = array("sort_no"  => $intChangeSortNo,
									"upd_id"   => $intUpdId,
									"upd_name" => $strUpdName,
									"upd_time" => $strUpdTime
								   );
			$aryMainWhereParam = array("contact_id"  => $this->intContactId,
									   "question_id" => $aryMainQuestionData["question_id"]
									  );
			
			$this->updQuestion($aryMainUpdData, $aryMainWhereParam);
			
			// ボタンを押下された質問と位置が入れ替わる質問を更新
			$aryTargetUpdData = array("sort_no"  => $aryMainQuestionData["sort_no"],
									  "upd_id"   => $intUpdId,
									  "upd_name" => $strUpdName,
									  "upd_time" => $strUpdTime
									 );
			$aryTargetWhereParam = array("contact_id"  => $this->intContactId,
										 "question_id" => $aryTargetQuestionData["question_id"]
										);
			$this->updQuestion($aryTargetUpdData, $aryTargetWhereParam);

		} // 順番入れ替え処理ここまで

		// 質問一覧画面を表示する為、ボタンを押下された質問の親質問データを取得
		$aryParentQuestionData = $this->getQuestion( array("question_id" => $aryMainQuestionData["parent_question_id"]) );

		// 質問ID、質問階層をメンバー変数に設定
		$this->setQuestionId($aryParentQuestionData[0]["question_id"]);
		$this->setQuestionRank($aryParentQuestionData[0]["question_rank"]);

	}

	/**
	 * 現在表示されている質問の階層構造を配列で取得する関数。
	 * ループ処理を行い、パンくずリストで使用
	 *
	 * @author Nambe
	 * @date   2013/02/18
	 **/
	function getPankuzuData( ){
		// 再帰的にデータを取得する際に質問IDを格納する変数を初期化
		$intQuestionId = $this->intQuestionId;
		// 質問の情報を取得
		$aryQuestionData = $this->getQuestion(array("question_id"=>$intQuestionId));

		// 表示するパンくずデータを格納する配列の初期化
		$aryPankuzuData = array();
		// トップ階層の質問ID・文言を設定
		$aryPankuzuData[0]["question_id"]        = 0;
		$aryPankuzuData[0]["parent_question_id"] = 0;
		$aryPankuzuData[0]["question_text"]      = "トップ";


		if( 0 != $this->intQuestionId){
			for($i = $aryQuestionData[0]["question_rank"];$i>0;$i--){

			$aryTempData = $this->getQuestion( array("question_id" => $intQuestionId));

			// パンくずで押下された質問情報を取得
			$aryTemporaryData = array_shift( $aryTempData );
			// パンくず配列に取得したデータを格納
			$aryPankuzuData[$i]["question_id"]        = $aryTemporaryData["question_id"];
			$aryPankuzuData[$i]["parent_question_id"] = $aryTemporaryData["parent_question_id"];
			$aryPankuzuData[$i]["question_text"]      = $aryTemporaryData["question_text"];

			$intQuestionId = $aryTemporaryData["parent_question_id"];
			}
		}
		return $aryPankuzuData;
	}


	/**
	 * FAQ一覧画面を表示する関数
	 *
	 * @author Nambe 2013/02/14
	 * 
	 * @param integer $intQuestionRank : 表示したい質問一覧の質問階層
	 *
	 * 
	 **/
	function dspFaqList( ){

		//契約会社リスト取得
		$strContactSelectBox = $this->getContactList($this->intContactId);

		// 質問IDが設定されているか確認
		
		if( true == is_null($this->intQuestionId)){
			$this->objChecker->setError();
			$this->objChecker->setErrorMessage( "error", "FAQ機能の使用が許可されていません。" );
			$this->strErrorJson = $this->objChecker->getErrorJson();
		}else{
			// パンくずデータを取得
			$aryPankuzuData = $this->getPankuzuData();

			// 選択された質問を親として持つFAQ件数をセットする変数
			$this->intQuestionAMount = $this->getFaqCntByParent( $this->intQuestionId );

			if ( 0 == $this->intQuestionAMount){
				$this->objChecker->setError();
				$this->objChecker->setErrorMessage( "error", "現在登録されているFAQはありません。" );
				$this->strErrorJson = $this->objChecker->getErrorJson();
			}else{
				// FAQの総件数をセット
				$this->objPager->setTotal( $this->intQuestionAMount );

				// FAQのデータを取得
				$aryFaqData = $this->getFaqListByParentId( $this->intQuestionId, $this->objPager->getOffset(), $this->objPager->getLimit() );

				// データ取得処理結果の判断
				if( true == $this->objDatabase->isError( $aryFaqData )){
					$this->objChecker->setError();
					$this->objChecker->setErrorMessage( "error", "FAQデータを取得できませんでした。" );
					$this->strErrorJson = $this->objChecker->getErrorJson();
				}
			}

		}
		require_once('dspFaqList.php');

		exit();
	}

	/**
	 * FQAの登録・更新・削除画面を表示する関数
	 *
	 * @author Nambe
	 * @date   2013/02/26
	 *
	 * @param integer $intMainteMode : メッセージの切替に使用する
	 * 								   1 : 新規登録
	 * 								   2 : 質問更新
	 * 								   3 : 質問削除
	 **/
	function dspMainteComplete($intMainteMode,$intContactId){
		header("Location:dspMainteComplete.php?dm={$intMainteMode}&sc={$intContactId}");
	}

	/**
	 * 質問内容を取得する関数。
	 * 取得したい条件をパラメーターに設定して取得。
	 * ※「=」 で紐付くデータしか取得できないので注意
	 *
	 * @author Nambe
	 * @date 2013/02/18
	 *
	 * @param array $aryWhereParam : 取得したい質問を指定する条件。カラムをキー、valueを値に持つ連想配列。
	 *
	 * return array : inform_faq_trnの全てのカラムのデータを保持した連想配列
	 **/
	function getQuestion( $aryWhereParam ){
		// 契約IDを設定
		$intContactId = $this->intContactId;

		// Where句を生成
		foreach( $aryWhereParam as $strWhereColumnName => $mixdValue ){
			// WHERE句の条件数が不明な為、一旦配列にWHERE句を格納
			$aryWhere[] = "{$strWhereColumnName} = :{$strWhereColumnName} ";
			$aryQueryParam[":{$strWhereColumnName}"] = $mixdValue;
		}
		// 配列に格納されたWHERE句を" AND "で連結し、クエリに変換
		$strWhereQuery = implode(" AND ",$aryWhere);

		$sqlGetQuestion =<<<SQL
SELECT * 
  FROM inform_faq_trn
 WHERE contact_id = {$intContactId} AND 
       {$strWhereQuery}
;
SQL;


		return $this->objDatabase->pullDbData( $sqlGetQuestion, $aryQueryParam );
	}

	/**
	 * 契約ID毎のFAQの設定情報を取得する関数
	 * コンストラクタで取得し、メンバー変数に設定する
	 *
	 * @authro Nambe
	 * @date   2013/02/26
	 **/
	/*function getFaqConfig(){
		// SQLを作成
		$sqlGetQuestion = <<<SQL
SELECT * FROM faq_contact_mst
 WHERE contact_id = :contact_id
SQL;

		return $this->objDatabase->pullDbData($sqlGetQuestion,array("contact_id" => $this->intContactId));

	}*/




	/**
	 * 指定された質問階層のデータを取得する関数
	 *
	 * @author Nambe
	 * @date   2013/02/14
	 *
	 * @param integer $intParentQuestionId : 表示したい質問一覧の親質問ID
	 * @param integer $intOffset           : 表示したい質問一覧のデータ取得開始位置
	 * @param integer $IntLimit            : 表示したい質問一覧の取得データ上限数
	 *
	 * return array : 数値添え字配列で各データにはinform_faq_trnのカラム名をキーに持つ連想配列。
	 **/
	function getFaqListByParentId( $intParentQuestionId, $intOffset, $intLimit ){
		// 契約ID情報を設定
		$intContactId = $this->intContactId;


		// 質問一覧を取得するクエリを設定
		$sqlGetFaq = <<<SQL
SELECT (SELECT count(ft_tmp.question_id) 
          FROM inform_faq_trn ft_tmp 
         WHERE ft_tmp.parent_question_id = ft.question_id 
           AND ft_tmp.delete_flg = 0
           AND ft_tmp.contact_id = {$intContactId}
       ) as child_amount,
       ft.* 
  FROM inform_faq_trn ft
 WHERE ft.delete_flg = 0 
   AND ft.parent_question_id = :parent_question_id
   AND ft.contact_id = {$intContactId} 
 ORDER BY ft.sort_no ASC ,ft.question_id ASC
 LIMIT {$intLimit}
 OFFSET {$intOffset}
SQL;

		// プリペアードステートメントのパラメーター設定
		$aryQueryParam = array( ":parent_question_id" => $intParentQuestionId );

		return $this->objDatabase->pullDbData( $sqlGetFaq, $aryQueryParam );
	}

	/**
	 * 特定の質問に紐付くFAQの登録件数取得
	 *
	 * @author Nambe 2013/02/14
	 *
	 * @param $intParentQuestionId : 質問一覧の親となる質問ID
	 *
	 * @return integer : FAQの件数件数
	 */
	public function getFaqCntByParent( $intParentQuestionId ){
		// 契約ID情報を設定
		$intContactId = $this->intContactId;

		// 削除されていないFAQのデータ件数を取得するクエリ
		$sqlGetQuestionAmount = 
<<<SQL
	SELECT *
	  FROM inform_faq_trn
	 WHERE delete_flg = 0
       AND contact_id = {$intContactId}
       AND parent_question_id = :parent_question_id
SQL;

		// クエリパラメータ
		$aryQueryParameter = array(	':parent_question_id' => $intParentQuestionId );

		// SQLを実行し結果を$aryResultへ格納する
		$aryQueryResult = $this->objDatabase->pullDbData( $sqlGetQuestionAmount, $aryQueryParameter );

		if( $this->objDatabase->isError( $aryQueryResult ) ){
			return false;
		}


		return count($aryQueryResult);
	}


	/**
	 * 質問IDのセッター
	 * 引数がからの場合、初期値として0を格納
	 * 
	 * @author Nambe
	 * @date   2013/02/18
	 **/
	function setQuestionId( $intQuestionId = null ){

		if( true == is_int(intval($intQuestionId)) && false == is_null($intQuestionId)){
			$this->intQuestionId = $intQuestionId;
		}else{
			$this->intQuestionId = 0;
		}
	}
	/**
	 * 質問ランクのセッター
	 * 引数がからの場合、初期値として1を格納
	 * 
	 * @author Nambe
	 * @date   2013/02/18
	 **/
	function setQuestionRank( $intQuestionrank = null ){
		if( true == is_int(intval($intQuestionrank)) && false == is_null($intQuestionrank)){
			$this->intQuestionRank = $intQuestionrank;
		}else{
			$this->intQuestionRank = 1;
		}
	}

	/**
	 * 契約会社リスト生成 
	 * 
	 * @param param
	 * @return return
	 */
	public function getContactList($intContact){
				
		$strSelectSql =
<<<SQL
		SELECT 
			contact_id,
			contact_name
		FROM
			contact_mst
		WHERE
			delete_flg = :delete_flg
SQL;
		$aryParameters = array(
			":delete_flg"	=>	0
		);
		
		$aryResult = $this->objDatabase->pullDbData($strSelectSql , $aryParameters);

		if( $this->objDatabase->isError($aryResult) ){
			return false;
		}

		$strContactSelectBox = "<option value='' " . clsCommonFunction::chkSelectedDate( $intContact, '' ) . " ></option>";
		foreach( $aryResult as $key => $val ){
			$strContactSelectBox .= "<option value=" . $val["contact_id"] . " " . clsCommonFunction::chkSelectedDate( $intContact, $val["contact_id"] ) . " >" . $val["contact_name"] . "</option>";
		}
		return $strContactSelectBox;

	}
}
?>
