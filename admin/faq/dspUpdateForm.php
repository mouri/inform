<?php
//	f::p($aryQuestionData["answer_text"]);
?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8" />
	<title><?php echo clsDefinition::SYSTEM_NAME?>　FAQメンテナンス</title>
	<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/faq/js/faqMainte.js"></script>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			var objAlertError = new AlertError('<?php echo @$this->strErrorJson ?>');
			if( objAlertError.hasError() ){
				objAlertError.show();
			}
	
			CKEDITOR.config.toolbar = [
				 [ 'NewPage', '-', 'Bold', 'Strike', '-', 'RemoveFormat' ]
			];
			// テキストエリアの幅
			CKEDITOR.config.width = '450px';
			// テキストエリアの高さ
			CKEDITOR.config.height = '250px';
			// テキストエリアのリサイズ不許可
			CKEDITOR.config.resize_enabled = false;
			
		});
	</script>
</head>
<body id="faqList">
	<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	<form action="faqMainte.php" method="post" name="faq_mainte" id="faq_mainte">
		<input type="hidden" id="question_id"        name="question_id"   value="<?php echo $aryQuestionData["question_id"]; ?>" />
		<input type="hidden" id="question_rank"      name="question_rank" value="<?php echo $aryQuestionData["question_rank"]; ?>" />
		<input type="hidden" id="parent_question_id" name="parent_question_id" value="<?php echo $aryQuestionData["parent_question_id"]; ?>" />
		<input type="hidden" id="search_contact_id"  name="search_contact_id"  value="<?php f::p($this->intContactId); ?>" />
		<input type="hidden" id="action"             name="action"   value="" />

	<div class="boxBeige marB20">
		<h2>FAQ編集</h2>
		<div class="form">
			<!-- パンクズリスト -->
			<div >
				<ul>
				<?php for($i=0;$i<count($aryPankuzuData);$i++){ ?><!-- 配列に格納している分だけ、パンくずリストを生成 -->
					<li style="display:inline"><a href="javascript:setValueSubmit('faq_mainte',{question_id:<?php f::p($aryPankuzuData[$i]["question_id"]); ?>,parent_question_id:<?php f::p($aryPankuzuData[$i]["question_id"]); ?>,action:'move_question_rank'})" ><?php f::p($aryPankuzuData[$i]["question_text"]); ?></a></li>
					<?php if($i != count($aryPankuzuData)-1){ ?> &#62; <?php } ?>
				<?php } ?>
				</ul>
				</div>
		<!-- 質問項目登録ここから -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="c1 fullWidth">
					<p class="komoku">質問内容</p>
					<input type="text" name="question_text" id="question_text" maxlength="256" value="<?php f::p($aryQuestionData["question_text"]);?>" class="hissu"/>
				</td>
				<td class="message"></td>
			</tr>
			<!-- 修正対象のデータの親コードが0でない（1階層目でない） 且つ この質問を親に持つ質問がなければ、回答が入力可能 -->
		<?php if( 1 != $aryQuestionData["question_rank"] && 0 == $aryQuestionData["child_amount"] ){ ?>
			<tr>
				<td class="c1">
					<p class="komoku">質問に対する返答</p>
					<textarea name="answer_text" id="answer_text" class="ckeditor" cols="40" rows="5"><?php echo $aryQuestionData["answer_text"] ?></textarea>
				</td>
				<td class="message"></td>
			</tr>
		<?php if ( true == $this->blnFormUseFlg ){ ?>
			<tr>
				<td class="c1">
					<p class="komoku">この質問でフォームを使用する？</p>
					<div id="authCheckBox" class="checkBoxDiv">
						<label class="inputCheckBox">
							<span class="checkButton"></span>フォームを使用する
							<input type="checkbox" id="form_need_flg" name="form_need_flg" value="1" <?php echo ( 1 == $aryQuestionData["form_need_flg"] ) ? 'checked="checked"' : '' ?>>
						</label>
					</div>
				</td>
				<td class="message"></td>
			</tr>
			<?php } ?>
		<?php } ?>
			<tr>
				<td class="c1">
					<input class="basicBtn centerBlock" type="button" value="編集内容を保存" onclick="setValueSubmit('faq_mainte',{action:'upd_question',question_id:<?php f::p($aryQuestionData["question_id"]);?>})" />
				</td>
				<td class="message"></td>
			</tr>
		</table>
		</div>
	</div>
</form>
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
