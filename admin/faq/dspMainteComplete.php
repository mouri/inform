<?php
	/**
	 * データ更新完了画面
	 * DB処理完了後にheader()で画面遷移する為、このファイルのみfaqMainte.phpをコントローラーとするMVCモデルからは外れる
	 * @author Nambe
	 * @date   2013/02/26
	 **/
require_once( $_SERVER["DOCUMENT_ROOT"] . "/include.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/value_checker/execute_classes/clsFaqChecker.php" );
$objChecker = new clsFaqChecker( $_POST );

$objChecker->setError();

	switch($_GET["dm"]){
		// 新規登録
		case 1:
		$strMessage = "登録";
		$objChecker->setErrorMessage( "error", "質問を登録しました。" );
		break;

		// 更新処理
		case 2:
		$strMessage = "編集";
		$objChecker->setErrorMessage( "error", "質問を編集しました。" );
		break;

		// 削除処理
		case 3:
		$strMessage = "削除";
		$objChecker->setErrorMessage( "error", "質問を削除しました。" );
		break;
	}

	$intSelectContactId = $_GET["sc"];
$strErrorJson = $objChecker->getErrorJson();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8" />
	<title><?php echo clsDefinition::SYSTEM_NAME?>　FAQメンテナンス</title>
	<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/faq/js/faqMainte.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if( objAlertError.hasError() ){
			objAlertError.show();
		}
		
	});

	function faqList(){
		$("#faqForm").submit();
	}
	</script>
</head>
<body id="faqList">
	<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	<div align="center" style="margin-top:50px;">
		質問<?php echo $strMessage ?>完了
	</div>
	<div align="center">
		質問を<?php echo $strMessage ?>しました。
	</div>

	<div>
		<div id="error"></div>
	</div>
	<!-- メイン部分 -->
	<div align="center">
		<!--<a href="faqMainte.php">質問一覧に戻る</a>-->
		<form id="faqForm" name="faqForm" action="faqMainte.php" method="post" >
		<input type="hidden" id="search_contact_id" name="search_contact_id" value="<?php echo $intSelectContactId ?>" >
		</form>
		<a href="#" onclick="faqList(); return false;">質問一覧に戻る</a>
	</div>
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
