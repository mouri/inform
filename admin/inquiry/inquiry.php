<?php
/**
 *	お問い合わせ管理画面
 *
 *	お問い合わせ管理画面の制御クラスを呼び出す。
 *
 *	@author			Nishi 2013/05/13
 *	@version		1.0
 */

require_once( $_SERVER["DOCUMENT_ROOT"] . "/include.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/inquiry/ctlInquiry.php");
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/inquiry/clsInquiry.php");

//ログイン制御クラス呼出し
$objCtlInquiry = new ctlInquiry();
$objCtlInquiry->process();
?>
