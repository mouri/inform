<?php
/**
 *	お問い合わせ管理画面
 *
 *	お問い合わせ管理画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Nishi 2013/05/13
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8" />
	<title><?php echo clsDefinition::SYSTEM_NAME?>　お問い合わせ管理</title>
	<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
	<link rel="stylesheet" href="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/jquery.ajaxSuggest.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/jquery.ajaxSuggest.js"></script>
<!--<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/faq/js/faqMainte.js"></script>-->
<script type="text/javascript">
	$(document).ready(function(){

		//Ａタグリンク
		$(".follow_data_btn").click(function(){
			id = $(this).attr("name");
			elem = $(this).parent().parent();
			contact_id = $("#contact_id_"+id).val();
			inquiry_id =  $("#inquiry_id_"+id).val();

			if($(".inc_"+id).size() == 0){
				$.ajax({
					url: "inquiry.php",
					type: "POST",
					data: {
						action: "getFollowInfo",
						rows:id,
						contact_id: contact_id,
						inquiry_id: inquiry_id
					},
					dataType: "json"
				})
				.done(function( data ) {
					if(data != ""){
						elem.after(data);
					}
					//$("#rows_"+id).text("閉じる");
					$("#rows_"+id).val("閉じる");

				})
				.fail(function( data ) {
				})
				.always(function( data ) {
				});
			}else{
				$(".inc_"+id).remove();
				//$("#rows_"+id).text("もっと見る");
				$("#rows_"+id).val("もっと見る");

			}
			

		});
		//新規書き込みボタン
		$("#btn_new").click(function(){
			$("#topForm").attr("action", "./entry/entry.php");
			$("#topForm").submit();
		});

		// 検索ボタンクリック時
		$("#search_btn").click(function(){
			// 条件設定
			$("#action").val( "search" );
			$("#topForm").attr("action", "./inquiry.php");
			$("#topForm").submit();
		});

		$('#search_tag').ajaxSuggest('./ajaxSuggest.php',{'database':0, "contact_id":<?php echo clsContactSession::getContactId(); ?>});
	});


	//編集ボタンクリック（お問い合わせ）
	function edit(contact_id,inquiry_id){
		$("#select_contact_id").val(contact_id);
		$("#select_inquiry_id").val(inquiry_id);
		$("#topForm").attr("action", "./entry/entry.php");
		$("#action").val("edit");
		$("#topForm").submit();
	}

	//削除ボタンクリック（お問い合わせ）
	function del(contact_id,inquiry_id){
		ModalDialog.confirm("", "お問い合わせを削除してもよろしいですか？", 
			function(blnResult){
				if(blnResult){
					$("#select_contact_id").val(contact_id);
					$("#select_inquiry_id").val(inquiry_id);
					$("#topForm").attr("action", "./entry/entry.php");
					$("#action").val("delete");
					$("#topForm").submit();
				}
			}
		);
	}

	//追加ボタンクリック
	function follow(contact_id,inquiry_id){
		$("#select_contact_id").val(contact_id);
		$("#select_inquiry_id").val(inquiry_id);
		$("#topForm").attr("action", "./followentry/followEntry.php");
		$("#topForm").submit();
	}
	
	//編集ボタンクリック
	function follow_edit(contact_id,inquiry_id,history_id){
		$("#select_contact_id").val(contact_id);
		$("#select_inquiry_id").val(inquiry_id);
		$("#select_history_id").val(history_id);
		$("#topForm").attr("action", "./followentry/followEntry.php");
		$("#action").val("editInit");
		$("#topForm").submit();
	}
	//フォロー削除ボタンクリック
	function follow_del(contact_id,inquiry_id,history_id){
		ModalDialog.confirm("", "対応内容を削除してもよろしいですか？", 
			function(blnResult){
				if(blnResult){
					$("#select_contact_id").val(contact_id);
					$("#select_inquiry_id").val(inquiry_id);
					$("#select_history_id").val(history_id);
					$("#topForm").attr("action", "./followentry/followEntry.php");
					$("#action").val("followDel");
					$("#topForm").submit();
				}
			}
		);
	}


</script>
</head>
<body id="inquiryList">
<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	<form method="post" name="topForm" id="topForm">


	<div class="form">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="c1 fullWidth">
					<p class="komoku">契約会社名</p>
					<select name="search_contact_id" id="search_contact_id">
						<?php echo $strContactSelectBox ?>
					</select>
				</td>
				<td class="message"></td>
			</tr>
			<tr>
				<td class="c1 fullWidth">
					<p class="komoku">タグ</p>
					<div class="lavel">
						<input type="text" name="search_tag" id="search_tag" size="100" maxlength="100" value="<?php f::p( $_POST["search_tag"] ) ?>" />
					</div>
				</td>
				<td class="message"></td>
			</tr>
			<tr>
				<td class="c1 fullWidth">
					<p class="komoku">対応状況</p>
					<div class="radioDiv item5">
						<div class="left">
							<label for="inquiry_status_null" class="inputRadio">
								<span class="radioButton"></span>全て<input type="radio" name="search_inquiry_status" id="inquiry_status_null" <?php echo clsCommonFunction::chkCheckedDate( "", $_POST["search_inquiry_status"] ) ?> value="">
							</label>
						</div>
						<div class="center">
							<label for="inquiry_status0" class="inputRadio">
								<span class="radioButton"></span>未対応<input type="radio" name="search_inquiry_status" id="inquiry_status0" <?php echo clsCommonFunction::chkCheckedDate( "0", $_POST["search_inquiry_status"] ) ?> value="0">
							</label>
						</div>
						<div class="center">
							<label for="inquiry_status1" class="inputRadio">
								<span class="radioButton"></span>対応中<input type="radio" name="search_inquiry_status" id="inquiry_status1" <?php echo clsCommonFunction::chkCheckedDate( "1", $_POST["search_inquiry_status"] ) ?> value="1">
							</label>
						</div>
						<div class="center">
							<label for="inquiry_status2" class="inputRadio">
								<span class="radioButton"></span>対応済<input type="radio" name="search_inquiry_status" id="inquiry_status2" <?php echo clsCommonFunction::chkCheckedDate( "2", $_POST["search_inquiry_status"] ) ?> value="2">
							</label>
						</div>
						<div class="right">
							<label for="inquiry_status3" class="inputRadio">
								<span class="radioButton"></span>保留<input type="radio" name="search_inquiry_status" id="inquiry_status3" <?php echo clsCommonFunction::chkCheckedDate( "3", $_POST["search_inquiry_status"] ) ?> value="3">
							</label>
						</div>
					</div>
				</td>
				<td class="message"></td>
			</tr>
			<tr>
				<td>
					<div class="basicBtn search">
						<input type="button" name="search_btn" id="search_btn" value="検索" />
					</div>
				</td>
			</tr>
		</table>
	</div>

	<input type="button" name="btn_new" id="btn_new" value="新規書き込み" />
	<input type="hidden" name="select_contact_id" id="select_contact_id" />
	<input type="hidden" name="select_inquiry_id" id="select_inquiry_id" />
	<input type="hidden" name="select_history_id" id="select_history_id" />

	<?php if(!$aryInquiryList){ echo "お問い合わせは0件です"; } ?>

	<div class="pager top"><?php echo $objPager->get(); ?></div>
	<?php
		$iCnt = 1;
		foreach( $aryInquiryList AS $params ){ 
	?>
	<table border="1">
		<tr>
			<td><?php echo clsDefinition::$FOLLOW_STATUS_LIST[$params["inquiry_status"]] ?></td>
			<td width="200">お客様名：<?php echo $params["contact_name"] ?></td>
			<td>更新者名：<?php echo $params["upd_name"] ?></td>
			<td>初回登録日：<?php echo $params["reg_time"] ?></td>
			<td rowspan="2">
				<input type="button" name="btn_add" id="btn_add" value="追加" onClick="follow(<?php echo $params["contact_id"]?>,<?php echo $params["inquiry_id"] ?>)" />
				<input type="button" name="btn_edit" id="btn_edit" value="編集" onClick="edit(<?php echo $params["contact_id"]?>,<?php echo $params["inquiry_id"] ?>)" />
				<input type="button" name="btn_del" id="btn_del" value="削除" onClick="del(<?php echo $params["contact_id"]?>,<?php echo $params["inquiry_id"] ?>)" />
			</td>
		</tr>
		<tr>
			<td><?php echo 'Re:'.$params["cnt"].'件' ?></td>
			<td><?php echo $params["inquiry_title"] ?></td>
			<td>タグ：<?php echo $params["tag_name"] ?></td>
			<td>更新日：<?php echo $params["upd_time"] ?></td>
		</tr>
		<tr>
		</tr>
			<td colspan="5"><?php echo f::br($params["inquiry_memo"]); ?></td>
		<tr>
			<?php if(strlen($params["follow_memo"]) > 0){ ?>
			<td colspan="4">更新日：<?php echo $params["fol_upd_time"] ?><br/><?php echo f::br($params["follow_memo"]); ?></td>
			<td><input type="button" name="btn_edit" id="btn_edit<?php echo $params["history_id"] ?>" value="編集" onclick="follow_edit(<?php echo $params["fol_contact_id"] ?>,<?php echo $params["fol_inquiry_id"] ?>,<?php echo $params["history_id"] ?>)" />
			<input type="button" name="btn_follow_del" id="btn_follow_del<?php echo $params["history_id"] ?>" value="削除" onclick="follow_del(<?php echo $params["fol_contact_id"] ?>,<?php echo $params["fol_inquiry_id"] ?>,<?php echo $params["history_id"] ?>)" /></td>
			<?php } ?>
		</tr>
		<?php if($params["cnt"] > 1){ ?>
		<tr>
			<td colspan="5"><!--<a id="rows_<?php echo $iCnt ?>" value="<?php echo $iCnt ?>" href="#">もっと見る</a>-->
			<input type="button" class="follow_data_btn" id="rows_<?php echo $iCnt ?>" name="<?php echo $iCnt ?>" value="もっと見る">
			<input type="hidden" id="inquiry_id_<?php echo $iCnt ?>" value="<?php echo $params["inquiry_id"] ?>" />
			<input type="hidden" id="contact_id_<?php echo $iCnt ?>" value="<?php echo $params["contact_id"] ?>" />
		</tr>
		<?php } ?>
	</table><br/>
	<?php 
		$iCnt++;
		} 
	?>
	<div class="pager top"><?php echo $objPager->get(); ?></div>
	
	<input type="hidden" name="action" id="action" />
	<input type="hidden" name="pagerNumber" value="<?php echo $_POST["pagerNumber"] ?>" ?>
	</form>
	
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
