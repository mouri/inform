<?php
/**
 *	対応履歴登録画面の制御
 *
 *	対応履歴登録画面の制御について記述
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */
class ctlFollowEntry{
	
	/**
	 * コンストラクタ
	 *
	**/
	function __construct() {
	}
	
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){
		$objChecker   = new clsFollowEntryChecker($_REQUEST);	//エラーチェッククラス
		$strErrorJson = '';								//エラー情報のJSON文字列
		
		//DB接続
		$objDatabase = clsMillviDatabase::getInstance();
		//データクラス作成
		$objFollowEntry = new clsFollowEntry();

		$strAction	 = @$_POST["action"];
		
		switch( $strAction ){
			//修正初期
			case 'editInit':
				//問い合わせ取得
				$aryInquiryList = $objFollowEntry->getInquiryInfoEdit($_POST["select_contact_id"],$_POST["select_inquiry_id"],$_POST["select_history_id"]);
				require_once 'dspFollowEdit.php';
				exit();
				break;
			//修正
			case 'edit':
				//POSTデータのエラーチェック
				$blnCheckResult = $objChecker->execute();
				if($blnCheckResult == false){
					//問い合わせ取得
					$aryInquiryList = $objFollowEntry->getInquiryInfoEdit($_POST["select_contact_id"],$_POST["select_inquiry_id"],$_POST["select_history_id"]);
					$aryInquiryList[0]["follow_memo"] = $_POST["follow_memo"];
					//エラーがあった場合はエラー情報のJSON文字列を取得する
					if( $objChecker->isError() ){
						$strErrorJson = $objChecker->getErrorJson();
					}
					require_once 'dspFollowEdit.php';
					break;
				}

				//対応履歴の更新
				$blnResult = $objFollowEntry->Update($_POST);
				$blnModeFlg = 1;
				require_once 'dspFollowComplete.php';
				
				break;
			// 対応削除
			case 'followDel':

				//対応履歴の削除
				$aryInquiryList = $objFollowEntry->followDelete($_POST);
				$blnModeFlg = 2;
				require_once 'dspFollowComplete.php';
				break;
			//登録
			case 'regist':
				//POSTデータのエラーチェック
				$blnCheckResult = $objChecker->execute();
				
				if($blnCheckResult == false){
					//問い合わせ取得
					$aryInquiryList = $objFollowEntry->getInquiryInfo($_POST["select_contact_id"],$_POST["select_inquiry_id"]);
					//エラーがあった場合はエラー情報のJSON文字列を取得する
					if( $objChecker->isError() ){
						$strErrorJson = $objChecker->getErrorJson();
					}
					
					require_once 'dspFollowEntry.php';
					break;
				}
				
				//対応履歴の登録
				$blnResult = $objFollowEntry->Insert($_POST);
				//echo json_encode($blnResult);
				$blnModeFlg = 0;
				require_once 'dspFollowComplete.php';

				break;


			//初期表示
			default:
				//問い合わせ取得
				$aryInquiryList = $objFollowEntry->getInquiryInfo($_POST["select_contact_id"],$_POST["select_inquiry_id"]);
				require_once 'dspFollowEntry.php';
				break;
		}
		
		
	}
}


?>