<?php
/**
 *	対応履歴登録画面表示
 *
 *	対応履歴登録画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2013/04/16
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title><?php echo clsDefinition::SYSTEM_NAME?>対応<?php if( 1 == $blnModeFlg ){ echo "編集"; }else if( 2 == $blnModeFlg ){ echo "削除"; }else{ echo "登録"; } ?>完了</title>
	<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<!--<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/faq/js/faqMainte.js"></script>-->
<script type="text/javascript">
	$(document).ready(function(){
		//前に戻る
		$("#btn_top").click(function(){
			$("#action").val("");
			$("#compForm").attr("action", "../inquiry.php");
			$("#compForm").submit();
		});
	});
	
</script>
</head>
<body id="inquiryList">
<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	<form method="post" name="compForm" id="compForm">
	
	<div>対応<?php if( 1 == $blnModeFlg ){ echo "編集"; }else if( 2 == $blnModeFlg ){ echo "削除"; }else{ echo "登録"; } ?>完了しました。</div>
	
	<input type="button" name="btn_top" id="btn_top" value="お問い合わせリストへ" />
	<input type="hidden" id="pagerNumber" name="pagerNumber" value="<?php echo $_POST["pagerNumber"] ?>" ?>

	<!--検索条件-->
	<input type="hidden" id="search_contact_id"		name="search_contact_id"		value="<?php f::p( $_POST["search_contact_id"] ); ?>" />
	<input type="hidden" id="search_tag"			name="search_tag"				value="<?php f::p( $_POST["search_tag"] ); ?>" />
	<input type="hidden" id="search_inquiry_status"	name="search_inquiry_status"	value="<?php f::p( $_POST["search_inquiry_status"] ); ?>" />
	
	</form>
	
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
