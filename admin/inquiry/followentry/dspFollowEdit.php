<?php
/**
 *	対応履歴登録画面表示
 *
 *	対応履歴登録画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2013/04/16
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title><?php echo clsDefinition::SYSTEM_NAME?>対応編集</title>
	<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<!--<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/faq/js/faqMainte.js"></script>-->
<script type="text/javascript">
	$(document).ready(function(){
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		// ログイン画面専用にエラーメッセージ表示変更
		objAlertError.setAlertPattern(function(aryErrorInfo){
			jQuery.each( aryErrorInfo, function( index, value ){
				// エラーCSSを追加する
				$('#'+value.errId).addClass('errorLine');
				// エラーメッセージを追加する
				if(value.errMsg){
					$("#"+value.errId).after("<p class='errInfo'>"+value.errMsg+"</p>");
				}
			});
		});
		if(objAlertError.hasError()){
			objAlertError.show(2);
		}
		//編集ボタン
		$("#btn_edit").click(function(){
			$("#action").val("edit");
			$("#topForm").attr("action", "followEntry.php");
			$("#topForm").submit();
		});
		//前に戻る
		$("#btn_back").click(function(){
			$("#action").val("");
			$("#topForm").attr("action", "../inquiry.php");
			$("#topForm").submit();
		});
	});
	
</script>
</head>
<body id="inquiryList">
<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	<form method="post" name="topForm" id="topForm">
	<table border="1">
	<?php
		$params = $aryInquiryList[0];
	?>
		<tr>
			<td><?php echo clsDefinition::$FOLLOW_STATUS_LIST[$params["inquiry_status"]] ?></td>
			<td>お客様名：<?php echo $params["contact_name"] ?></td>
			<td>更新者名：<?php echo $params["upd_name"] ?></td>
			<td colspan="2">初回登録日：<?php echo $params["reg_time"] ?></td>
		</tr>
		<tr>
			<td><?php echo 'Re:'.$params["cnt"].'件' ?></td>
			<td><?php echo $params["inquiry_title"] ?></td>
			<td></td>
			<td colspan="2">更新日：<?php echo $params["upd_time"] ?></td>
		</tr>
		<tr>
		</tr>
			<td colspan="5"><?php echo f::br($params["inquiry_memo"]); ?></td>
		<tr>
			<td colspan="5">更新日：<?php echo $params["fol_upd_time"] ?></td>
		</tr>
		<tr>
			<td colspan="5"><textarea id="follow_memo" name="follow_memo" style="width:400px;height:150px;"><?php echo $params["follow_memo"] ?></textarea></td>
		</tr>
		<tr>
			<td colspan="3"><select id="inquiry_status" name="inquiry_status"><?php echo clsCommonFunction::initFollowStatusSelectBox($params["inquiry_status"]) ?></select></td>
			<td><input type="button" name="btn_edit" id="btn_edit" value="編集" /></td>
			<td><input type="button" name="btn_back" id="btn_back" value="前に戻る" /></td>
		</tr>
	</table>
	<input type="hidden" name="action" id="action" />
	<input type="hidden" name="select_inquiry_id" id="select_inquiry_id" value="<?php echo $_POST["select_inquiry_id"] ?>" />
	<input type="hidden" name="select_contact_id" id="select_contact_id" value="<?php echo $_POST["select_contact_id"] ?>" />
	<input type="hidden" name="select_history_id" id="select_history_id" value="<?php echo $_POST["select_history_id"] ?>" />
	<input type="hidden" id="pagerNumber" name="pagerNumber" value="<?php echo $_POST["pagerNumber"] ?>" ?>

	<!--検索条件-->
	<input type="hidden" id="search_contact_id"		name="search_contact_id"		value="<?php f::p( $_POST["search_contact_id"] ); ?>" />
	<input type="hidden" id="search_tag"			name="search_tag"				value="<?php f::p( $_POST["search_tag"] ); ?>" />
	<input type="hidden" id="search_inquiry_status"	name="search_inquiry_status"	value="<?php f::p( $_POST["search_inquiry_status"] ); ?>" />

	</form>
	
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
