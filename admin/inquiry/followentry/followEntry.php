<?php
/**
 *	対応履歴登録画面
 *
 *	対応履歴登録画面の制御クラスを呼び出す
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */

require_once( $_SERVER["DOCUMENT_ROOT"] . "/include.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/inquiry/followentry/ctlFollowEntry.php");
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/inquiry/followentry/clsFollowEntry.php");
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/value_checker/execute_classes/clsFollowEntryChecker.php");

//ログイン制御クラス呼出し
$objCtlFollowEntry = new ctlFollowEntry();
$objCtlFollowEntry->process();
?>
