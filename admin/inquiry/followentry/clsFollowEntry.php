<?php
/**
 *	対応履歴登録画面のモデルクラス
 *
 *	対応履歴登録画面のビジネスロジックについて記述
 *
 *	@author			Mouri 2013/04/18
 *	@version		1.0
 */
class clsFollowEntry{
	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct(){
	
	}

	/**
	 * 対応履歴の登録
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function Insert($aryParam){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//トランザクション開始
		$objDatabase->execTransaction();
		
		//対応履歴登録
		$blnResult = $this->InsertFollowHistory($objDatabase,$aryParam);
		if($blnResult == false){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//問い合わせ状態更新
		$blnResult = $this->UpdateInquiryStatus($objDatabase,$aryParam);
		if($blnResult == false){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}
	
	
	/**
	 * 対応履歴の更新
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function Update($aryParam){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//トランザクション開始
		$objDatabase->execTransaction();
		
		//対応履歴更新
		$blnResult = $this->UpdateFollowHistory($objDatabase,$aryParam,0);
		if($blnResult == false){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//問い合わせ状態更新
		$blnResult = $this->UpdateInquiryStatus($objDatabase,$aryParam);
		if($blnResult == false){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}

	/**
	 * 対応履歴の削除
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function followDelete($aryParam){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//トランザクション開始
		$objDatabase->execTransaction();
		
		//対応履歴更新
		$blnResult = $this->UpdateFollowHistory($objDatabase,$aryParam,1);
		if($blnResult == false){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		$aryParameters = array(
							":contact_id" => $aryParam["select_contact_id"],
							":inquiry_id" => $aryParam["select_inquiry_id"]
						);

		$strSelectSql =
<<<SQL
		SELECT
	case 
		WHEN itp.upd_time IS NULL THEN 0
		ELSE itp.inquiry_status
	END AS inquiry_status
	FROM
		follow_history_trn fht
	LEFT JOIN
		inquiry_trn_past itp
	ON fht.contact_id = itp.contact_id
	AND fht.inquiry_id = itp.inquiry_id
	AND fht.reg_time = itp.upd_time 
	WHERE
		fht.contact_id = :contact_id
	AND
		fht.inquiry_id = :inquiry_id
	AND
		fht.delete_flg = 0
	ORDER BY itp.upd_time DESC
	LIMIT 1
SQL;

		$aryInquiryData = $objDatabase->pullDbData($strSelectSql , $aryParameters);
		$aryParam["inquiry_status"] = $aryInquiryData[0]["inquiry_status"];

		//問い合わせ状態更新
		$blnResult = $this->UpdateInquiryStatus($objDatabase,$aryParam);
		if($blnResult == false){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}
	 
	/**
	 * 対応履歴の登録
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function InsertFollowHistory($objDatabase,$aryParam){

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//$history_id = clsCommonFunction::getNextId("follow_history_trn", "history_id",array("inquiry_id" => $aryParam["select_inquiry_id"],"contact_id" => $aryParam["select_contact_id"]));

		$arySelectParameter = array(
								":inquiry_id" => $aryParam["select_inquiry_id"],
								":contact_id" => $aryParam["select_contact_id"]
								);

		$arySelectSql =
<<<SQL
		select
			COALESCE( MAX( history_id ), 0) + 1 AS history_id
		from
			follow_history_trn
		where
			inquiry_id = :inquiry_id
		and
			contact_id = :contact_id
SQL;

		$history_id = $objDatabase->pullDbData($arySelectSql , $arySelectParameter);
		$history_id = $history_id[0]["history_id"];

		$reg_id = clsLoginSession::getUserId();
		$reg_name = clsLoginSession::getFullName();
		$reg_time = clsCommonFunction::getCurrentDate();
		$upd_id = clsLoginSession::getUserId();
		$upd_name = clsLoginSession::getFullName();
		$upd_time = clsCommonFunction::getCurrentDate();
		
		$aryParameter = array(
			":inquiry_id" => $aryParam["select_inquiry_id"],
			":contact_id" => $aryParam["select_contact_id"],
			":history_id" => $history_id,
			":follow_memo" => $aryParam["follow_memo"],
			":delete_flg" => 0,
			":reg_id" => $reg_id,
			":reg_name" => $reg_name,
			":reg_time" => $reg_time,
			":upd_id" => $upd_id,
			":upd_name" => $upd_name,
			":upd_time" => $upd_time
		);
		
		$strInsertSql =
<<< SQL
			insert into follow_history_trn(
				inquiry_id,
				contact_id,
				history_id,
				follow_memo,
				delete_flg,
				reg_id,
				reg_name,
				reg_time,
				upd_id,
				upd_name,
				upd_time)
			values(
				:inquiry_id,
				:contact_id,
				:history_id,
				:follow_memo,
				:delete_flg,
				:reg_id,
				:reg_name,
				:reg_time,
				:upd_id,
				:upd_name,
				:upd_time
			);
SQL;

		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameter);
		
		if($objDatabase->isError($aryResult)){
			return false;
		}
		
		//$past_number = clsCommonFunction::getNextId("follow_history_trn_past", "past_number",array("inquiry_id" => $aryParam["select_inquiry_id"],"contact_id" => $aryParam["select_contact_id"],"history_id" => $history_id));
		$upd_kbn = 1;
		
		$aryParameter = array(
			":upd_kbn" => $upd_kbn,
			":inquiry_id" => $aryParam["select_inquiry_id"],
			":contact_id" => $aryParam["select_contact_id"],
			":history_id" => $history_id
		);
		
		$strInsertSql =
<<<SQL
		insert into follow_history_trn_past
		select
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM follow_history_trn_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			fht.*
		from follow_history_trn fht
		where inquiry_id = :inquiry_id
		and contact_id = :contact_id
		and history_id = :history_id
SQL;

		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameter);
		
		if($objDatabase->isError($aryResult)){
			return false;
		}
		
		
		return true;
		
	}
	 
	/**
	 * 対応履歴の更新
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function updateFollowHistory($objDatabase,$aryParam,$intDelteFlg){

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$upd_id = clsLoginSession::getUserId();
		$upd_name = clsLoginSession::getFullName();
		$upd_time = clsCommonFunction::getCurrentDate();
		
		$aryParameter = array(
			":follow_memo" => $aryParam["follow_memo"],
			":upd_id" => $upd_id,
			":upd_name" => $upd_name,
			":upd_time" => $upd_time,
			":inquiry_id" => $aryParam["select_inquiry_id"],
			":contact_id" => $aryParam["select_contact_id"],
			":history_id" => $aryParam["select_history_id"],
			":delete_flg" => $intDelteFlg
		);
		
		$strSql =
<<<SQL
			update follow_history_trn
			set follow_memo = :follow_memo,
			upd_id = :upd_id,
			upd_name = :upd_name,
			upd_time = :upd_time,
			delete_flg = :delete_flg
			where inquiry_id = :inquiry_id
			and contact_id = :contact_id
			and history_id = :history_id
SQL;
		
		$aryResult = $objDatabase->updDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return false;
		}
		
		//$past_number = clsCommonFunction::getNextId("follow_history_trn_past", "past_number",array("inquiry_id" => $aryParam["select_inquiry_id"],"contact_id" => $aryParam["select_contact_id"],"history_id" => $aryParam["select_history_id"]));
		$upd_kbn = 2;
		
		$aryParameter = array(
			":upd_kbn" => $upd_kbn,
			":inquiry_id" => $aryParam["select_inquiry_id"],
			":contact_id" => $aryParam["select_contact_id"],
			":history_id" => $aryParam["select_history_id"]
		);
		
		$strInsertSql =
<<<SQL
		insert into follow_history_trn_past
		select
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM follow_history_trn_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			fht.*
		from follow_history_trn fht
		where inquiry_id = :inquiry_id
		and contact_id = :contact_id
		and history_id = :history_id
SQL;

		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameter);
		
		if($objDatabase->isError($aryResult)){
			return false;
		}
		
		return $aryResult;
		
	}
	
	
	/**
	 * 問い合わせ情報の更新
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function updateInquiryStatus($objDatabase,$aryParam){

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$upd_id = clsLoginSession::getUserId();
		$upd_name = clsLoginSession::getFullName();
		$upd_time = clsCommonFunction::getCurrentDate();
		
		$aryParameter = array(
			":inquiry_id" => $aryParam["select_inquiry_id"],
			":contact_id" => $aryParam["select_contact_id"]
		);
		
		//問い合わせ状態変更チェック
		$strSql =
<<<SQL
			select inquiry_status 
			from inquiry_trn
			where inquiry_id = :inquiry_id
			and contact_id = :contact_id
SQL;
		
		$aryResult = $objDatabase->pullDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return array();
		}
		
		//変更がない場合は更新せずに終了
		if($aryResult[0]["inquiry_status"] == $aryParam["inquiry_status"]){
			//var_dump($aryResult[0]["inquiry_status"]."/".$aryParam["inquiry_status"]);
			return true;
		}

		//対応が0件になった場合は未対応にする。
		if( '' == $aryParam["inquiry_status"]){
			$aryParam["inquiry_status"] = 0;
		}
		
		
		$aryParameter = array(
			":inquiry_status" => $aryParam["inquiry_status"],
			":upd_id" => $upd_id,
			":upd_name" => $upd_name,
			":upd_time" => $upd_time,
			":inquiry_id" => $aryParam["select_inquiry_id"],
			":contact_id" => $aryParam["select_contact_id"]
		);
		
		$strSql =
<<<SQL
			update inquiry_trn
			set inquiry_status = :inquiry_status,
			upd_id = :upd_id,
			upd_name = :upd_name,
			upd_time = :upd_time
			where inquiry_id = :inquiry_id
			and contact_id = :contact_id
SQL;
		
		$aryResult = $objDatabase->updDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return false;
		}
		
		
		//$past_number = clsCommonFunction::getNextId("inquiry_trn_past", "past_number",array("inquiry_id" => $aryParam["select_inquiry_id"],"customer_id" => $aryParam["select_customer_id"]));
		$upd_kbn = 2;
		
		$aryParameter = array(
			":upd_kbn" => $upd_kbn,
			":inquiry_id" => $aryParam["select_inquiry_id"],
			":contact_id" => $aryParam["select_contact_id"]
		);
		
		$strInsertSql =
<<<SQL
		insert into inquiry_trn_past
		select
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM inquiry_trn_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			it.*
		from inquiry_trn it
		where inquiry_id = :inquiry_id
		and contact_id = :contact_id
SQL;

		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameter);
		
		if($objDatabase->isError($aryResult)){
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 問合せ情報の取得（登録用）
	 *
	 * @param int $contact_id 顧客ＩＤ
	 * @param int $inquiry_id 問い合わせＩＤ
	 * @return array 問い合わせ情報
	 */
	public function getInquiryInfo($contact_id,$inquiry_id){

		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		//現在の時間
		$strCurrentDate = clsCommonFunction::getCurrentDate();

		$aryParameter = array(
			":delete_flg"       => 0,
			":contact_id"      => $contact_id,
			":inquiry_id"       => $inquiry_id
		);


		$strCompleteFlg = clsDefinition::MOVIE_CHANGE_COMPLETE;
		$strSql =
<<< SQL
			select it.inquiry_id,
				it.contact_id,
				cm.contact_name,
				it.inquiry_status,
				it.inquiry_title,
				it.inquiry_memo,
				it.reg_name,
				it.reg_time,
				it.upd_name,
				it.upd_time,
				ifnull(fht.cnt, 0) as cnt,
				fol.follow_memo,
				fol.upd_time as fol_upd_time
			from inquiry_trn it 
			left outer join contact_mst cm 
			on it.contact_id = cm.contact_id
			left outer join 
				(select inquiry_id,
					contact_id,
					count(*) as cnt
				from follow_history_trn
				group by inquiry_id, contact_id) fht 
			on it.inquiry_id = fht.inquiry_id
			and it.contact_id = fht.contact_id 
			left outer join 
				(select fht.inquiry_id,
					fht.contact_id,
					fht.history_id,
					fht.follow_memo,
					fht.upd_time
				from follow_history_trn fht 
				inner join 
					(select inquiry_id,
						contact_id,
						max(history_id) as history_id
					from follow_history_trn
					group by inquiry_id, contact_id) max_dt 
				on fht.inquiry_id = max_dt.inquiry_id
				and fht.contact_id = max_dt.contact_id
				and fht.history_id = max_dt.history_id) fol 
			on it.inquiry_id = fol.inquiry_id
			and it.contact_id = fol.contact_id
			where it.delete_flg = :delete_flg
			and it.contact_id = :contact_id
			and it.inquiry_id = :inquiry_id
SQL;

		$aryResult = $objDatabase->pullDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return array();
		}
		return $aryResult;
	}
	
	/**
	 * 問合せ情報の取得（修正用）
	 *
	 * @param int $contact_id 顧客ＩＤ
	 * @param int $inquiry_id 問い合わせＩＤ
	 * @param int $history_id 履歴ＩＤ
	 * @return array 問い合わせ情報
	 */
	public function getInquiryInfoEdit($contact_id,$inquiry_id,$history_id){
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		//現在の時間
		$strCurrentDate = clsCommonFunction::getCurrentDate();

		$aryParameter = array(
			":history_id"       => $history_id,
			":delete_flg"       => 0,
			":contact_id"      => $contact_id,
			":inquiry_id"       => $inquiry_id
		);


		$strCompleteFlg = clsDefinition::MOVIE_CHANGE_COMPLETE;
		$strSql =
<<< SQL
			select it.inquiry_id,
				it.contact_id,
				cm.contact_name,
				it.inquiry_status,
				it.inquiry_title,
				it.inquiry_memo,
				it.reg_name,
				it.reg_time,
				it.upd_name,
				it.upd_time,
				ifnull(fcnt.cnt, 0) as cnt,
				fht.follow_memo,
				fht.upd_time as fol_upd_time
			from inquiry_trn it 
			left outer join contact_mst cm 
			on it.contact_id = cm.contact_id
			left outer join 
				(select inquiry_id,
					contact_id,
					count(*) as cnt
				from follow_history_trn
				group by inquiry_id, contact_id) fcnt 
			on it.inquiry_id = fcnt.inquiry_id
			and it.contact_id = fcnt.contact_id 
			left outer join follow_history_trn fht
			on it.inquiry_id = fht.inquiry_id
			and it.contact_id = fht.contact_id
			and fht.history_id = :history_id
			where it.delete_flg = :delete_flg
			and it.contact_id = :contact_id
			and it.inquiry_id = :inquiry_id
SQL;

		$aryResult = $objDatabase->pullDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return array();
		}
		return $aryResult;
		
	}
}
?>