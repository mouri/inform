<?php
/**
 *	お問い合わせ管理画面
 *
 *	お問い合わせ管理画面の制御について
 *
 *	@author			Mouri 2013/05/13
 *	@version		1.0
 */
class ctlInquiry{
	
	/**
	 * コンストラクタ
	 *
	**/
	function __construct() {
	}
	
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){

		//データクラス作成
		$objClsInquiry = new clsInquiry();

		$strAction	 = @$_POST["action"];

		$strContactSelectBox = $objClsInquiry->getContactList($_POST["search_contact_id"]);

		// ページナンバー
		if( "" == $_POST["pagerNumber"] ){
			$_POST["pagerNumber"] = '1';
		}

		//ページャクラス作成
		$objPager = new clsPager();
		
		switch( $strAction ){
			case 'getFollowInfo':
				//対応履歴の取得
				$blnResult = $objClsInquiry->getFollowHistory($_POST["rows"],$_POST["contact_id"],$_POST["inquiry_id"]);
				echo json_encode($blnResult);
				exit();

				break;

			case 'search':
				//検索結果一覧取得
				// ページナンバー初期化
				$_POST["pagerNumber"] = '1';
				$objPager->setTotal($objClsInquiry->getInquiryListCnt($_POST));
				$aryInquiryList = $objClsInquiry->getInquiryList($objPager->getLimit(),$objPager->getOffset(),$_POST);
				require_once 'dspInquiry.php';
				break;

			default:

				//問い合わせ一覧取得
				$objPager->setTotal($objClsInquiry->getInquiryListCnt($_POST));
				$aryInquiryList = $objClsInquiry->getInquiryList($objPager->getLimit(),$objPager->getOffset(),$_POST);
				require_once 'dspInquiry.php';
				break;
		}
		
		
	}
}


?>