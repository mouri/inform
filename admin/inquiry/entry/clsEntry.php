<?php
/**
 *	問い合わせ新規書き込み画面のモデルクラス
 *
 *	問い合わせ新規書き込み画面のビジネスロジックについて記述
 *
 *	@author			Mouri 2013/04/18
 *	@version		1.0
 */
class clsEntry{
	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct(){
	
	}

	/**
	 * 問い合わせ内容の登録
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function regist($aryParam){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		//トランザクション開始
		$objDatabase->execTransaction();
		
		//$inquiry_id = clsCommonFunction::getNextId("inquiry_trn", "inquiry_id",array("contact_id" => $aryParam["contact_id"]));

		$arySelectParameter = array(
								":contact_id" => $aryParam["contact_id"]
								);

		$arySelectSql =
<<<SQL
		select
			COALESCE( MAX( inquiry_id ), 0) + 1 AS inquiry_id
		from
			inquiry_trn
		where
			contact_id = :contact_id
SQL;

		$inquiry_id = $objDatabase->pullDbData($arySelectSql , $arySelectParameter);
		$inquiry_id = $inquiry_id[0]["inquiry_id"];

		$reg_id = clsLoginSession::getUserId();
		$reg_name = clsLoginSession::getFullName();
		$reg_time = clsCommonFunction::getCurrentDate();
		$upd_id = clsLoginSession::getUserId();
		$upd_name = clsLoginSession::getFullName();
		$upd_time = clsCommonFunction::getCurrentDate();
		
		$aryParameter = array(
			":inquiry_id" => $inquiry_id,
			":contact_id" =>$aryParam["contact_id"],
			":inquiry_status" => 0,
			":inquiry_title" => $aryParam["inquiry_title"],
			":inquiry_memo" => $aryParam["inquiry_memo"],
			":delete_flg" => 0,
			":reg_id" => $reg_id,
			":reg_name" => $reg_name,
			":reg_time" => $reg_time,
			":upd_id" => $upd_id,
			":upd_name" => $upd_name,
			":upd_time" => $upd_time
		);
		
		$strInsertSql =
<<< SQL
			insert into inquiry_trn
				(inquiry_id,
				contact_id,
				inquiry_status,
				inquiry_title,
				inquiry_memo,
				delete_flg,
				reg_id,
				reg_name,
				reg_time,
				upd_id,
				upd_name,
				upd_time)
			values(
				:inquiry_id,
				:contact_id,
				:inquiry_status,
				:inquiry_title,
				:inquiry_memo,
				:delete_flg,
				:reg_id,
				:reg_name,
				:reg_time,
				:upd_id,
				:upd_name,
				:upd_time
			);
SQL;

		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameter);
		
		if($objDatabase->isError($aryResult)){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
	//	$past_number = clsCommonFunction::getNextId("inquiry_trn_past", "past_number",array("inquiry_id" => $inquiry_id,"contact_id" => $aryParam["contact_id"]));
		$upd_kbn = 1;
		
		$aryParameter = array(
			":upd_kbn" => 1,
			":inquiry_id" => $inquiry_id,
			":contact_id" => $aryParam["contact_id"]
		);
		
		$strInsertSql =
<<<SQL
		insert into inquiry_trn_past
		select
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM inquiry_trn_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			it.*
		from inquiry_trn it
		where inquiry_id = :inquiry_id
		and contact_id = :contact_id
SQL;

		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameter);

		if($objDatabase->isError($aryResult)){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}


		// タグを入力していた場合
		if( true == is_array( $aryParam["tag_name"] ) ){
			$aryTagInfo = array(
						"contact_id"	=> $aryParam["contact_id"],
						"inquiry_id"	=> $inquiry_id,
						"tag_id"		=> 0,
						"tag_name"		=> '',
						"delete_flg"	=> 0,
						"reg_id"		=> $reg_id,
						"reg_name"		=> $reg_name,
						"reg_time"		=> $reg_time,
						"upd_id"		=> $upd_id,
						"upd_name"		=> $upd_name,
						"upd_time"		=> $upd_time
						);

			// タグの数だけループ処理
			for( $i=0; $i<COUNT( $aryParam["tag_name"] ); $i++ ){

				// タグIDの取得
				$aryTagInfo["tag_id"] = self::getTagId($aryTagInfo);

				$aryTagInfo["tag_name"] = $aryParam["tag_name"][$i];

				// タグトラン 書き込み
				$blnInsertResult = self::insertTag($aryTagInfo);

				if( false == $blnInsertResult ){
					// ロールバック
					$objDatabase->execRollback();
					return false;
				}
				
				// 履歴登録
				$blnInsertPastResult = self::insertTagPast($aryTagInfo,1);

				if( false == $blnInsertPastResult ){
					// ロールバック
					$objDatabase->execRollback();
					return false;
				}
			}
		}
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}

	/**
	 * タグID取得
	 *
	 *
	 */
	public function getTagId($aryTagInfo){

		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$arySelectParameter = array(
								":contact_id" => $aryTagInfo["contact_id"],
								":inquiry_id" => $aryTagInfo["inquiry_id"]
								);

		$arySelectSql =
<<<SQL
				select
					COALESCE( MAX( tag_id ), 0) + 1 AS tag_id
				from
					inquiry_tag_trn
				where
					contact_id = :contact_id
				and
					inquiry_id = :inquiry_id
SQL;

		$intTagId = $objDatabase->pullDbData($arySelectSql , $arySelectParameter);

		return $intTagId[0]["tag_id"];

	}

	/**
	 * タグ登録 
	 * 
	 * @return boolean
	 */
	public function insertTag($aryTagInfo){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		// タグIDの連番取得
		//$this->tag_id = clsCommonFunction::getNextId( "movie_tag_trn", "tag_id", array( "movie_id" => $this->movie_id ) );

		$strSql =
<<< SQL
	INSERT INTO inquiry_tag_trn (
		contact_id,
		inquiry_id,
		tag_id,
		tag_name,
		delete_flg,
		reg_id,
		reg_name,
		reg_time,
		upd_id,
		upd_name,
		upd_time
	) VALUES (
		:contact_id,
		:inquiry_id,
		:tag_id,
		:tag_name,
		:delete_flg,
		:reg_id,
		:reg_name,
		:reg_time,
		:upd_id,
		:upd_name,
		:upd_time
	)
SQL;

		$aryParameter = array(
			':contact_id' => $aryTagInfo["contact_id"],
			':inquiry_id' => $aryTagInfo["inquiry_id"],
			':tag_id'     => $aryTagInfo["tag_id"],
			':tag_name'   => $aryTagInfo["tag_name"],
			':delete_flg' => $aryTagInfo["delete_flg"],
			":reg_id"     => $aryTagInfo["reg_id"],
			":reg_name"   => $aryTagInfo["reg_name"],
			":reg_time"   => $aryTagInfo["reg_time"],
			":upd_id"     => $aryTagInfo["upd_id"],
			":upd_name"   => $aryTagInfo["upd_name"],
			":upd_time"   => $aryTagInfo["upd_time"]
		);

		// 取得したタグIDを配列へ格納
		//$aryTagInfo["tag_id"] = $tag_id;
		
		$blnResult = $objDatabase->addDbData( $strSql, $aryParameter );
		if( $objDatabase->isError( $blnResult ) ){
			return false;
		}
		
		return true;
	}

	/**
	 * タグ履歴を登録 
	 * 
	 * @param int $intUpdateKubun 区分
	 * @return boolean
	 */
	public function insertTagPast( $aryTagInfo,$intUpdateKubun ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//$intMoviePastNumber = clsCommonFunction::getNextId( "movie_tag_trn_past", "past_number" );

		$strSql =
<<< SQL
	INSERT INTO inquiry_tag_trn_past (
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM inquiry_tag_trn_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			inquiry_tag_trn.*
		FROM
			inquiry_tag_trn
		WHERE
			contact_id = :contact_id AND
			inquiry_id = :inquiry_id AND
			tag_id     = :tag_id
	)
SQL;
		
		$aryParameter = array(
			":upd_kbn"          => $intUpdateKubun,
			":contact_id"       => $aryTagInfo["contact_id"],
			":inquiry_id"       => $aryTagInfo["inquiry_id"],
			":tag_id"           => $aryTagInfo["tag_id"]
		);
		
		$blnResult = $objDatabase->addDbData( $strSql, $aryParameter );

		if( $objDatabase->isError( $blnResult ) ){
			return false;
		}
		
		return true;
	}

	/**
	 * 問い合わせ内容の編集
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function edit($aryParam){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		//トランザクション開始
		$objDatabase->execTransaction();
		
		$upd_id = clsLoginSession::getUserId();
		$upd_name = clsLoginSession::getFullName();
		$upd_time = clsCommonFunction::getCurrentDate();
		
		$aryParameter = array(
			":inquiry_id" =>$aryParam["select_inquiry_id"],
			":contact_id" =>$aryParam["select_contact_id"],
			":inquiry_title" => $aryParam["inquiry_title"],
			":inquiry_memo" => $aryParam["inquiry_memo"],
			":upd_id" => $upd_id,
			":upd_name" => $upd_name,
			":upd_time" => $upd_time
		);
		
		$strUpdateSql =
<<< SQL
			Update inquiry_trn set
				inquiry_title = :inquiry_title,
				inquiry_memo = :inquiry_memo,
				upd_id = :upd_id,
				upd_name = :upd_name,
				upd_time = :upd_time
			where
				inquiry_id = :inquiry_id
			and
				contact_id = :contact_id
SQL;

		$aryResult = $objDatabase->addDbData($strUpdateSql , $aryParameter);
		
		if($objDatabase->isError($aryResult)){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		$upd_kbn = 2;
		
		$aryParameter = array(
			":upd_kbn" => 1,
			":inquiry_id" => $aryParam["select_inquiry_id"],
			":contact_id" => $aryParam["select_contact_id"]
		);
		
		$strInsertSql =
<<<SQL
		insert into inquiry_trn_past
		select
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM inquiry_trn_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			it.*
		from inquiry_trn it
		where inquiry_id = :inquiry_id
		and contact_id = :contact_id
SQL;

		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameter);

		if($objDatabase->isError($aryResult)){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}

		// 動画タグトラン タグ情報取得
		$blnSelectResult = self::getInquiryTagInfo($aryParam["select_contact_id"],$aryParam["select_inquiry_id"]);
		if( false == $blnSelectResult ){
			// ロールバック
			$objDatabase->execRollback();
			return false;
		}

		if( '0' != $blnSelectResult[0]["tag_cnt"] ){
			// DELETE INSERTの為、登録情報を格納
			$reg_id   = $blnSelectResult[0]["reg_id"];
			$reg_name = $blnSelectResult[0]["reg_name"];
			$reg_time = $blnSelectResult[0]["reg_time"];

		} else {
			$reg_id = clsLoginSession::getUserId();
			$reg_name = clsLoginSession::getFullName();
			$reg_time = clsCommonFunction::getCurrentDate();
		}

		// タグを入力していた場合
		if( true == is_array( $aryParam["tag_name"] ) ){
			$aryTagInfo = array(
						"contact_id"	=> $aryParam["select_contact_id"],
						"inquiry_id"	=> $aryParam["select_inquiry_id"],
						"tag_id"		=> 0,
						"tag_name"		=> '',
						"delete_flg"	=> 0,
						"reg_id"		=> $reg_id,
						"reg_name"		=> $reg_name,
						"reg_time"		=> $reg_time,
						"upd_id"		=> $upd_id,
						"upd_name"		=> $upd_name,
						"upd_time"		=> $upd_time
						);


			if( '0' != $blnSelectResult[0]["tag_cnt"] ){
				// 動画タグトラン 物理削除
				for($i = 0; $i < $blnSelectResult[0]["tag_cnt"]; $i++){

					// タグIDの取得
					$aryTagInfo["tag_id"] =  $i + 1;

					$blnDeleteResult = self::deleteTagPast($aryTagInfo);

					if( false == $blnDeleteResult ){
						// ロールバック
						$objDatabase->execRollback();
						return false;
					}
				}
				
				// 動画タグトラン 物理削除
				$blnDeleteResult = self::deleteTag($aryParam["select_contact_id"],$aryParam["select_inquiry_id"]);
				
				if( false == $blnDeleteResult ){
					// ロールバック
					$objDatabase->execRollback();
					return false;
				}
			}

			// タグの数だけループ処理
			for( $i=0; $i<COUNT( $aryParam["tag_name"] ); $i++ ){

				// タグIDの取得
				$aryTagInfo["tag_id"] = self::getTagId($aryTagInfo);

				$aryTagInfo["tag_name"] = $aryParam["tag_name"][$i];

				// タグトラン 書き込み
				$blnInsertResult = self::insertTag($aryTagInfo);

				if( false == $blnInsertResult ){
					// ロールバック
					$objDatabase->execRollback();
					return false;
				}
				
				// 履歴登録
				$blnInsertPastResult = self::insertTagPast($aryTagInfo,2);

				if( false == $blnInsertPastResult ){
					// ロールバック
					$objDatabase->execRollback();
					return false;
				}
			}
		}
		
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}

	/**
	 * 問い合わせ内容の削除
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function delete($aryParam){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		//トランザクション開始
		$objDatabase->execTransaction();
		
		$upd_id = clsLoginSession::getUserId();
		$upd_name = clsLoginSession::getFullName();
		$upd_time = clsCommonFunction::getCurrentDate();
		
		$aryParameter = array(
			":inquiry_id" =>$aryParam["select_inquiry_id"],
			":contact_id" =>$aryParam["select_contact_id"],
			":delete_flg" => 1,
			":upd_id" => $upd_id,
			":upd_name" => $upd_name,
			":upd_time" => $upd_time
		);
		
		$strUpdateSql =
<<< SQL
			Update inquiry_trn set
				delete_flg = :delete_flg,
				upd_id = :upd_id,
				upd_name = :upd_name,
				upd_time = :upd_time
			where
				inquiry_id = :inquiry_id
			and
				contact_id = :contact_id
SQL;

		$aryResult = $objDatabase->addDbData($strUpdateSql , $aryParameter);
		
		if($objDatabase->isError($aryResult)){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		$upd_kbn = 2;
		
		$aryParameter = array(
			":upd_kbn" => 1,
			":inquiry_id" => $aryParam["select_inquiry_id"],
			":contact_id" => $aryParam["select_contact_id"]
		);
		
		$strInsertSql =
<<<SQL
		insert into inquiry_trn_past
		select
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM inquiry_trn_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			it.*
		from inquiry_trn it
		where inquiry_id = :inquiry_id
		and contact_id = :contact_id
SQL;

		$aryResult = $objDatabase->addDbData($strInsertSql , $aryParameter);

		if($objDatabase->isError($aryResult)){
			//ロールバック
			$objDatabase->execRollback();
			return false;
		}
		
		
		//コミット
		$objDatabase->execCommit();
		
		return true;
	}
	 
	 
	/**
	 * 問合せ一覧の取得
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
/*	public function getInquiryList(){

		$objDatabase = clsMillviDatabase::getInstance();

		//現在の時間
		$strCurrentDate = clsCommonFunction::getCurrentDate();

		$aryParameter = array(
			":delete_flg"       => 0
		);


		$strCompleteFlg = clsDefinition::MOVIE_CHANGE_COMPLETE;
		$strSql =
<<< SQL
			select it.inquiry_id,
				it.contact_id,
				cm.contact_name,
				it.inquiry_status,
				it.inquiry_title,
				it.inquiry_memo,
				it.reg_name,
				it.reg_time,
				it.upd_name,
				it.upd_time,
				ifnull(fht.cnt, 0) as cnt,
				fol.follow_memo,
				fol.upd_time as fol_upd_time
			from inquiry_trn it 
			left outer join contact_mst cm 
			on it.contact_id = cm.contact_id
			left outer join 
				(select inquiry_id,
					contact_id,
					count(*) as cnt
				from follow_history_trn
				group by inquiry_id, contact_id) fht 
			on it.inquiry_id = fht.inquiry_id
			and it.contact_id = fht.contact_id 
			left outer join 
				(select fht.inquiry_id,
					fht.contact_id,
					fht.history_id,
					fht.follow_memo,
					fht.upd_time
				from follow_history_trn fht 
				inner join 
					(select inquiry_id,
						contact_id,
						max(history_id) as history_id
					from follow_history_trn
					group by inquiry_id, contact_id) max_dt 
				on fht.inquiry_id = max_dt.inquiry_id
				and fht.contact_id = max_dt.contact_id
				and fht.history_id = max_dt.history_id) fol 
			on it.inquiry_id = fol.inquiry_id
			and it.contact_id = fol.contact_id
			where it.delete_flg = :delete_flg
SQL;

		$aryResult = $objDatabase->pullDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return array();
		}
		return $aryResult;
	}*/

	/**
	 * inquiry_tag_trnデータを取得します
	 * 
	 * @return タグ情報
	 */
	public function getInquiryTagInfo($contact_id,$inquiry_id){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSelectSql =
<<<SQL
	SELECT
		COALESCE( COUNT( tag_id ), 0 ) AS tag_cnt,
		reg_id,
		reg_name,
		reg_time
	FROM
		inquiry_tag_trn
	WHERE
		contact_id = :contact_id AND
		inquiry_id = :inquiry_id
SQL;
		
		$aryParameters = array(
			':contact_id' => $contact_id,
			':inquiry_id' => $inquiry_id
		);
		
		$aryResult = $objDatabase->pullDbData( $strSelectSql, $aryParameters );
		if( $objDatabase->isError( $aryResult ) ){
			return false;
		}
		
		return $aryResult;
	}



	/**
	 * 動画タグトラン 物理削除
	 * 
	 * @return boolean
	 */
	public function deleteTag($contact_id,$inquiry_id){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSql =
<<<SQL
	DELETE
		FROM
			inquiry_tag_trn
		WHERE
			contact_id = :contact_id AND
			inquiry_id = :inquiry_id
SQL;
		
		$aryParameter = array(
			":contact_id" => $contact_id,
			":inquiry_id" => $inquiry_id
		);
		
		$blnResult = $objDatabase->delDbData( $strSql, $aryParameter );
		if( $objDatabase->isError( $blnResult ) ){
			return false;
		}
		
		return true;
	}
	
	/**
	 * 動画タグトラン履歴 論理削除
	 * 
	 * @return boolean
	 */
	public function deleteTagPast( $aryTagInfo ){
		//データベース接続
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$strSql =
<<< SQL
	INSERT INTO inquiry_tag_trn_past (
		SELECT
			( SELECT COALESCE( MAX( past_number ), 0) + 1 FROM inquiry_tag_trn_past WHERE contact_id = :contact_id ) AS past_number,
			:upd_kbn,
			contact_id,
			inquiry_id,
			tag_id,
			tag_name,
			:delete_flg,
			reg_id,
			reg_name,
			reg_time,
			:upd_id,
			:upd_name,
			:upd_time
		FROM
			inquiry_tag_trn
		WHERE
			contact_id = :contact_id AND
			inquiry_id = :inquiry_id AND
			tag_id     = :tag_id
	)
SQL;
		
		$aryParameter = array(
			":upd_kbn"     => 2,
			":delete_flg"  => 1,
			":upd_id"      => $aryTagInfo["upd_id"],
			":upd_name"    => $aryTagInfo["upd_name"],
			":upd_time"    => $aryTagInfo["upd_time"],
			":contact_id"  => $aryTagInfo["contact_id"],
			":inquiry_id"  => $aryTagInfo["inquiry_id"],
			":tag_id"      => $aryTagInfo["tag_id"]
		);
		
		$blnResult = $objDatabase->addDbData( $strSql, $aryParameter );

		if( $objDatabase->isError( $blnResult ) ){
			return false;
		}
		
		return true;
	}




/**
	 * 問合せ情報の取得
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function getInquiryData($aryParam){

		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		//現在の時間
		$strCurrentDate = clsCommonFunction::getCurrentDate();

		$aryParameter = array(
			":delete_flg"	=> 0,
			":contact_id"	=> $aryParam["select_contact_id"],
			":inquiry_id"	=> $aryParam["select_inquiry_id"]
		);

		$strSql =
<<< SQL
			select it.inquiry_id,
				it.contact_id,
				cm.contact_name,
				it.inquiry_status,
				it.inquiry_title,
				it.inquiry_memo,
				it.reg_name,
				it.reg_time,
				it.upd_name,
				it.upd_time,
				tag_name
			from inquiry_trn it 
			left outer join contact_mst cm 
			on it.contact_id = cm.contact_id
			LEFT JOIN 
							(
								SELECT
									contact_id,
									inquiry_id,
									GROUP_CONCAT( tag_name SEPARATOR ',' ) AS tag_name
								FROM
									inquiry_tag_trn
								WHERE
									delete_flg = 0
								GROUP BY
									contact_id,
									inquiry_id
							) AS itt
			 ON it.contact_id = itt.contact_id
			AND it.inquiry_id   = itt.inquiry_id
			where
				it.contact_id = :contact_id
			and
				it.inquiry_id = :inquiry_id
			and
				it.delete_flg = :delete_flg
SQL;

		$aryResult = $objDatabase->pullDbData($strSql, $aryParameter);

		if($objDatabase->isError($aryResult)){
			return array();//見直し
		}

		if( "" != $aryResult[0]["tag_name"] ){
			// 文字列を配列に分割
			$aryResult[0]["tag_name"] = explode( ",", $aryResult[0]["tag_name"] );
		}

		return $aryResult[0];
	}

	/**
	 * 契約会社リスト生成 
	 * 
	 * @param param
	 * @return return
	 */
	public function getContactList($intContact){

		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strSelectSql =
<<<SQL
		SELECT 
			contact_id,
			contact_name
		FROM
			contact_mst
		WHERE
			delete_flg = :delete_flg
SQL;
		$aryParameters = array(
			":delete_flg"	=>	0
		);
		
		$aryResult = $objDatabase->pullDbData($strSelectSql , $aryParameters);

		if( $objDatabase->isError($aryResult) ){
			return false;
		}

		$strContactSelectBox = "<option value='' " . clsCommonFunction::chkSelectedDate( $intContact, "" ) . " >選択してください</option>";
		foreach( $aryResult as $key => $val ){
			$strContactSelectBox .= "<option value=" . $val["contact_id"] . " " . clsCommonFunction::chkSelectedDate( $intContact, $val["contact_id"] ) . " >" . $val["contact_name"] . "</option>";
		}
		return $strContactSelectBox;

	}
	
}
?>