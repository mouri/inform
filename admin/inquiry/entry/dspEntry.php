<?php
/**
 *	問い合わせ新規書き込み画面の画面表示
 *
 *	問い合わせ新規書き込み画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Mouri 2013/04/16
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
	<title><?php echo clsDefinition::SYSTEM_NAME?>　お問い合わせ<?php if( 1 == $blnModeFlg ){ echo "編集"; }else{ echo "登録"; } ?></title>
	<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
	<link rel="stylesheet" href="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/jquery.ajaxSuggest.css" type="text/css" media="all" />
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/jquery.ajaxSuggest.js"></script>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/inquiry/entry/js/entry.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//画面ＪＳ
		var objDspEntry = new dspEntry();

		//エラー関連ＪＳ
		var objAlertError = new AlertError('<?php echo @$strErrorJson ?>');
		if(objAlertError.hasError()){
			objAlertError.show();
		}

		// タグ追加ボタンクリック時
		$("#btn_tag").click(function(){
			objDspEntry.addTag();
		});

		// タグを入力されていた場合
		if( 1 == <?php echo $intTagChk ?> ){
			makeTag( <?php echo json_encode( $aryData["tag_name"] ) ?> );
		}

		
		//登録ボタンクリック
		$("#btn_regist").click(function(){
			$("#action").val("regist");
			$("#entryForm").attr("action", "entry.php");
			$("#entryForm").submit();
		});

		//登録ボタンクリック
		$("#btn_edit").click(function(){
			$("#action").val("edit_info");
			$("#entryForm").attr("action", "entry.php");
			$("#entryForm").submit();
		});
		
		//戻るボタンクリック
		$("#btn_back").click(function(){
			$("#action").val("");
			$("#entryForm").attr("action", "../inquiry.php");
			$("#entryForm").submit();
		});
		
		//サジェスト機能
		$('#tag_text').ajaxSuggest('./ajaxSuggest.php',{'database':0, "contact_id":<?php echo clsContactSession::getContactId(); ?>});
	});
	
</script>
</head>
<body id="inquiryList">
<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	<form name="entryForm" id="entryForm" method="post">
		<table  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2">お客様：<?php if( 0 == $blnModeFlg ){ ?> <select id="contact_id" name="contact_id"><?php echo $strContactSelectBox; ?></select>
							<?php }else if( 1 == $blnModeFlg ){ echo $aryData["contact_name"]; } ?>
				</td>
				<td class="message"></td>
			</tr>
			<tr>
				<td colspan="2">問い合わせタイトル：<input type="text" id="inquiry_title" name="inquiry_title" value="<?php echo $aryData["inquiry_title"]; ?>" /></td>
				<td class="message"></td>
			</tr>
			<tr>
				<td colspan="2">問い合わせ内容：<textarea style="width:250px;height:200px;" id="inquiry_memo" name="inquiry_memo"><?php echo $aryData["inquiry_memo"]; ?></textarea></td>
				<td class="message"></td>
			</tr>
			<tr>
				<td class="lavel">タグ:
					<input type="text" name="tag_text" id="tag_text" size="100" maxlength="100" />
				</td>
				<td class="c2">
					<input type="button" name="btn_tag" id="btn_tag" class="basicBtn gray w70 marT20" value="追加" />
				</td>
				<td class="message"></td>
			</tr>
			<tr>
				<td colspan="3">
					<div id="tag">
						<?php// echo $strTagName ?>
					</div>
				</td>
			</tr>
		</table>
		<?php if( 0 == $blnModeFlg ){ ?>
			<input type="button" name="btn_regist" id="btn_regist" value="登録する" />
		<?php }else if( 1 == $blnModeFlg ){ ?>
			<input type="button" name="btn_regist" id="btn_edit" value="編集する" />
		<?php } ?>
		<input type="button" name="btn_back" id="btn_back" value="戻る" />
		<input type="hidden" name="action" id="action" />

		<input type="hidden" name="select_contact_id" id="select_contact_id" value="<?php if($aryData["contact_id"]){ echo $aryData["contact_id"]; }else{ echo $aryData["select_contact_id"]; } ?>" >
		<input type="hidden" name="select_inquiry_id" id="select_inquiry_id" value="<?php if($aryData["inquiry_id"]){ echo $aryData["inquiry_id"]; }else{ echo $aryData["select_inquiry_id"]; } ?>" >
		<input type="hidden" id="contact_name" name="contact_name" value="<?php echo $aryData["contact_name"]; ?>" >
		<input type="hidden" id="pagerNumber" name="pagerNumber" value="<?php echo $aryData["pagerNumber"] ?>" ?>

		<!--検索条件-->
		<input type="hidden" id="search_contact_id"		name="search_contact_id"		value="<?php f::p( $_POST["search_contact_id"] ); ?>" />
		<input type="hidden" id="search_tag"			name="search_tag"				value="<?php f::p( $_POST["search_tag"] ); ?>" />
		<input type="hidden" id="search_inquiry_status"	name="search_inquiry_status"	value="<?php f::p( $_POST["search_inquiry_status"] ); ?>" />

	</form>

	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>

