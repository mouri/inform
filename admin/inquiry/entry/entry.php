<?php
/**
 *	問い合わせ新規書き込み画面
 *
 *	問い合わせ新規書き込み画面の制御クラスを呼び出す
 *
 *	@author			Mouri 2012/01/16
 *	@version		1.0
 */
require_once( $_SERVER["DOCUMENT_ROOT"] . "/include.php" );
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/inquiry/entry/ctlEntry.php");
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/admin/inquiry/entry/clsEntry.php");
require_once( $_SERVER["DOCUMENT_ROOT"] . clsDefinition::SYSTEM_DIR . "/value_checker/execute_classes/clsEntryChecker.php" );

$objCtlEntry = new ctlEntry();
$objCtlEntry->process();
?>
