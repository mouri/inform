<?php
/**
 *	お問い合わせ登録画面表示
 *
 *	お問い合わせ登録画面のＨＴＭＬ表示部分を記述
 *
 *	@author			Nishi 2013/05/14
 *	@version		1.0
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title><?php echo clsDefinition::SYSTEM_NAME?>お問い合わせ<?php if( 1 == $blnModeFlg ){ echo "編集"; }else if( 2 == $blnModeFlg ){ echo "削除"; }else{ echo "登録"; } ?>完了</title>
	<?php require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR."/common/headAdmin.php"); ?>
<!--<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/common/js/Pager.js"></script>
	<script type="text/javascript" src="<?php echo clsDefinition::SYSTEM_DIR?>/admin/faq/js/faqMainte.js"></script>-->
<script type="text/javascript">
	$(document).ready(function(){
		//前に戻る
		$("#btn_top").click(function(){
			$("#action").val("");
			$("#compForm").attr("action", "../inquiry.php");
			$("#compForm").submit();
		});
	});
	
</script>
</head>
<body id="top">
<!-- ヘッダー呼出 -->
	<?php echo clsCommonFunction::dispHeaderManegement(); ?>
	<form method="post" name="compForm" id="compForm">
	
	<div>お問い合わせの<?php if( 1 == $blnModeFlg ){ echo "編集"; }else if( 2 == $blnModeFlg ){ echo "削除"; }else{ echo "登録"; } ?>が完了しました。</div>
	
	<input type="button" name="btn_top" id="btn_top" value="お問い合わせリストへ" />
	
	</form>
	
	<!-- フッター呼出 -->
	<?php echo clsCommonFunction::dispFooterManegement(); ?>
</body>
</html>
