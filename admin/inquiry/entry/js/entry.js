//コンストラクタ
var dspEntry = function() {};

// タグ追加
dspEntry.prototype.addTag = function(){
	tag_text = $( '#tag_text' ).val();
	if( !tag_text.match( /\S/g ) ){
		ModalDialog.error( "error",　"タグを入力してください。");
		return false;
	}
	tag_text = htmlspecialchars( tag_text );
	tag_text = tag_text.replace(/,/g, "、");
//	tag_text = tag_text.replace(/(\r\n|\r|\n)/g, '<br />');

	var length = $( '#tag :button').length;
	strHtml = "<div id='del_tag" + length + "' class='delTag'><p style='word-wrap:break-word;'>" + tag_text +
		"</p><input type='button' class='delBtn' name='del_tag" + length + "' onclick='delTag( this.name )' value='削除' />" +
		"<input type='hidden' name='tag_name[]' value='" + tag_text + "' />";

	$('#tag_text').attr( 'value', '' );
	$('#tag').append( strHtml );

};

// エラー時タグ作成
function makeTag( aryTag ){
	var strHtml = '';
	for( var i=0; i<aryTag.length; i++ ){
		tag_text = htmlspecialchars( aryTag[i] );
		strHtml += "<div id='del_tag" + i + "' class='delTag'><p>" + tag_text +
			"</p><input type='button' class='delBtn' name='del_tag" + i + "' onclick='delTag( this.name )' value='削除' />" +
			"<input type='hidden' name='tag_name[]' value='" + tag_text + "' /></div>";
	}
	$('#tag').append( strHtml );
}

// タグ削除
function delTag( buttonName ){
	$('#' + buttonName).remove();
}

function htmlspecialchars( ch ) {
    ch = ch.replace(/&/g,"&amp;") ;
    ch = ch.replace(/"/g,"&quot;") ;
    ch = ch.replace(/'/g,"&#039;") ;
    ch = ch.replace(/</g,"&lt;") ;
    ch = ch.replace(/>/g,"&gt;") ;
    return ch ;
}