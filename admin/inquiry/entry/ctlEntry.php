<?php
/**
 *	問い合わせ新規書き込み画面の制御
 *
 *	問い合わせ新規書き込み画面の制御について記述
 *
 *	@author			Nishi 2013/05/17
 *	@version		1.0
 */
class ctlEntry{
	
	/**
	 * コンストラクタ
	 *
	**/
	function __construct() {
	}
	
	
	/**
	 * 画面処理分岐
	 *
	 **/
	function process(){
		$objEntryChecker = new clsEntryChecker($_REQUEST);	//エラーチェッククラス
		//DB接続
		$objDatabase = clsMillviDatabase::getInstance();
		//データクラス作成
		$objClsEntry = new clsEntry();

		// 問い合わせ一覧で契約会社が選択されていた場合、その会社名を引き継ぐ。
		if("" !== $_POST["search_contact_id"]){
			//契約会社リスト取得
			$strContactSelectBox = $objClsEntry->getContactList($_POST["search_contact_id"]);
		}else{
			//契約会社リスト取得
			$strContactSelectBox = $objClsEntry->getContactList($_POST["contact_id"]);
		}

		$strAction	 = @$_POST["action"];

		$aryData = $_POST;

		$intTagChk	= '0';
		
		switch( $strAction ){
			case 'regist':
				//POSTデータのエラーチェック
				$blnCheckResult = $objEntryChecker->execute();

				if($blnCheckResult == false){
					//エラーがあった場合はエラー情報のJSON文字列を取得する
					if( $objEntryChecker->isError() ){
						$strErrorJson = $objEntryChecker->getErrorJson();
					}
					require_once 'dspEntry.php';
					break;
				}else{
					//登録処理
					$blnResult = $objClsEntry->regist($_POST);
					$blnModeFlg = 0;
					require_once 'dspEntryComplete.php';
				}
				
				break;

			case 'edit':

					$aryData = $objClsEntry->getInquiryData($aryData);
					$aryData["pagerNumber"] = $_POST["pagerNumber"]; // ページャーナンバー設定。

					$blnModeFlg = 1;
					
					// タグがある場合
					if( "" != $aryData["tag_name"] ){
						$intTagChk = '1';
					}
					require_once 'dspEntry.php';
				
				break;

			case 'edit_info':

				//POSTデータのエラーチェック
				$blnCheckResult = $objEntryChecker->edit();
				$blnModeFlg = 1;

				// タグがある場合
				if( "" != $aryData["tag_name"] ){
					$intTagChk = '1';
				}

				if($blnCheckResult == false){
					//エラーがあった場合はエラー情報のJSON文字列を取得する
					if( $objEntryChecker->isError() ){
						$strErrorJson = $objEntryChecker->getErrorJson();
					}
					require_once 'dspEntry.php';
					break;
				}else{

					// 編集処理
					$blnResult = $objClsEntry->edit($_POST);

					require_once 'dspEntryComplete.php';
				}
				
				break;

			case 'delete':
				
					//削除処理
					$blnResult = $objClsEntry->delete($_POST);
					$blnModeFlg = 2;
					require_once 'dspEntryComplete.php';
				
				break;
			default:
				$blnModeFlg = 0;

				require_once 'dspEntry.php';
				break;
		}
		
	}
}


?>