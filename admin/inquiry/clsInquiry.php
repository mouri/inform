<?php
/**
 *	お問い合わせ管理画面モデル
 *
 *	お問い合わせ管理画面のロジック
 *
 *	@author			Mouri 2013/05/13
 *	@version		1.0
 */
class clsInquiry{
	/**
	 * コンストラクタ
	 *
	 **/
	public function __construct(){
	
	}

	/**
	 * 対応履歴の取得
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function getFollowHistory($rows,$contact_id,$inquiry_id){
		
		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		$aryResult = array();
		
		$aryParameter = array(
			":contact_id" => $contact_id,
			":inquiry_id" =>$inquiry_id,
			":delete_flg" => 0
		);
		
		$strSql =
<<< SQL
			select 
				inquiry_id,
				contact_id,
				history_id,
				follow_memo,
				upd_time
			from follow_history_trn
			where contact_id = :contact_id
			and inquiry_id = :inquiry_id
			and delete_flg = :delete_flg
			/*and upd_time <> (select max(upd_time) from follow_history_trn where delete_flg = :delete_flg )*/
			and history_id <> (select max(history_id) from follow_history_trn where delete_flg = :delete_flg and contact_id = :contact_id and inquiry_id = :inquiry_id )
			order by history_id desc
SQL;

		$aryResult = $objDatabase->pullDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return false;
		}else{
			$tag = "";
			foreach($aryResult as $key => $val){
				$tag .=
<<<tag
<tr class="inc_{$rows}">
<td  colspan="4">更新日：{$val["upd_time"]}<br/>
tag;

				$tag .= nl2br(htmlspecialchars($val["follow_memo"]));

				$tag .=
<<<tag
</td>
<td><input type="button" name="btn_edit" id="btn_edit{$val["history_id"]}" value="編集" onclick="follow_edit({$val["contact_id"]},{$val["inquiry_id"]},{$val["history_id"]})" />
	<input type="button" name="btn_follow_del" id="btn_follow_del{$val["history_id"]}" value="削除" onclick="follow_del({$val["contact_id"]},{$val["inquiry_id"]},{$val["history_id"]})" /></td>
</tr>
tag;
			}
			
			$aryResult = $tag;
		}
		
		
		return $aryResult;
	}
	 
/**
	 * 問合せ一覧の件数取得
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function getInquiryListCnt($aryParam){
		
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//現在の時間
		$strCurrentDate = clsCommonFunction::getCurrentDate();

		$aryParameter = array(
			":delete_flg"       => 0
		);
		
		$whereSql = "where it.delete_flg = :delete_flg";
		
		if(strlen($aryParam["search_contact_id"]) > 0){
			$aryParameter[":contact_id"] = $aryParam["search_contact_id"];
			$whereSql .= " and it.contact_id = :contact_id";
		}
		
		// タグ
		if( strlen($aryParam["search_tag"]) > 0 ){
			$aryParameter[":search_tag"] = '%' . $aryParam["search_tag"] . '%';
			$whereSql .= " AND tag_name LIKE :search_tag ";
		}
		
		// 対応
		if( strlen($aryParam["search_inquiry_status"]) > 0 ){
			$aryParameter[":inquiry_status"] = $aryParam["search_inquiry_status"];
			$whereSql .= " and it.inquiry_status = :inquiry_status";
		}
		
		$strSql =
<<< SQL
	select
		it.inquiry_id,
		it.contact_id,
		cm.contact_name,
		it.inquiry_status,
		it.inquiry_title,
		it.inquiry_memo,
		it.reg_name,
		it.reg_time,
		it.upd_name,
		it.upd_time,
		tag_name,
		ifnull(fht.cnt, 0) as cnt,
		fol.inquiry_id as fol_inquiry_id,
		fol.contact_id as fol_contact_id,
		fol.history_id,
		fol.follow_memo,
		fol.upd_time as fol_upd_time
	from
		inquiry_trn AS it LEFT JOIN (
			SELECT
				contact_id,
				inquiry_id,
				GROUP_CONCAT( tag_name SEPARATOR ',' ) AS tag_name
			FROM
				inquiry_tag_trn
			WHERE
				delete_flg = :delete_flg
			GROUP BY
				contact_id,
				inquiry_id
		) AS itt
		ON  it.contact_id = itt.contact_id
		AND it.inquiry_id = itt.inquiry_id
			
			INNER JOIN (
				SELECT
					contact_id,
					contact_name
				FROM
					contact_mst
				WHERE
					delete_flg = :delete_flg
			) AS cm 
		on it.contact_id = cm.contact_id
			
			left outer join (
				select
					inquiry_id,
					contact_id,
					count(*) as cnt
				from
					follow_history_trn
				where
					delete_flg = :delete_flg
				group by
					inquiry_id, contact_id
			) fht 
		on  it.inquiry_id = fht.inquiry_id
		and it.contact_id = fht.contact_id 
			
			left outer join (
				select
					fht.inquiry_id,
					fht.contact_id,
					fht.history_id,
					fht.follow_memo,
					fht.upd_time
				from
					follow_history_trn AS fht inner join 
						(
							select
								inquiry_id,
								contact_id,
								max(history_id) as history_id
							from
								follow_history_trn
							where
								delete_flg = :delete_flg
							group by
								inquiry_id, contact_id
						) max_dt 
					on  fht.inquiry_id = max_dt.inquiry_id
					and fht.contact_id = max_dt.contact_id
					and fht.history_id = max_dt.history_id
			) fol 
		on  it.inquiry_id = fol.inquiry_id
		and it.contact_id = fol.contact_id
	{$whereSql}
	order by
		it.upd_time desc
SQL;

		$aryResult = $objDatabase->pullDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return array();
		}
		return count($aryResult);
	}
	 
	/**
	 * 問合せ一覧の取得
	 *
	 * @param int $intLimit 取得件数
	 * @return array 動画情報
	 */
	public function getInquiryList($limit,$offset,$aryParam){
		
		$objDatabase = clsMillviDatabase::getInstance();
		
//		$objDatabase->setAdminAddress( "nishi@inform-us.com" );
		
		//現在の時間
		$strCurrentDate = clsCommonFunction::getCurrentDate();
		
		$aryParameter = array(
			":delete_flg"       => 0
		);
		
		$strlimitSql = "limit ".$limit." offset ".$offset;
		
		$whereSql = "where it.delete_flg = :delete_flg";
		
		if(strlen($aryParam["search_contact_id"]) > 0){
			$aryParameter[":contact_id"] = $aryParam["search_contact_id"];
			$whereSql .= " and it.contact_id = :contact_id";
		}
		
		// タグ
		if( strlen($aryParam["search_tag"]) > 0 ){
			$aryParameter[":search_tag"] = '%' . $aryParam["search_tag"] . '%';
			$whereSql .= " AND tag_name LIKE :search_tag ";
		}
		
		// 対応
		if( strlen($aryParam["search_inquiry_status"]) > 0 ){
			$aryParameter[":inquiry_status"] = $aryParam["search_inquiry_status"];
			$whereSql .= " and it.inquiry_status = :inquiry_status";
		}
		
		$strSql =
<<< SQL
	select
		it.inquiry_id,
		it.contact_id,
		cm.contact_name,
		it.inquiry_status,
		it.inquiry_title,
		it.inquiry_memo,
		it.reg_name,
		it.reg_time,
		it.upd_name,
		it.upd_time,
		tag_name,
		ifnull(fht.cnt, 0) as cnt,
		fol.inquiry_id as fol_inquiry_id,
		fol.contact_id as fol_contact_id,
		fol.history_id,
		fol.follow_memo,
		fol.upd_time as fol_upd_time
	from
		inquiry_trn AS it LEFT JOIN (
			SELECT
				contact_id,
				inquiry_id,
				GROUP_CONCAT( tag_name SEPARATOR ',' ) AS tag_name
			FROM
				inquiry_tag_trn
			WHERE
				delete_flg = :delete_flg
			GROUP BY
				contact_id,
				inquiry_id
		) AS itt
		ON  it.contact_id = itt.contact_id
		AND it.inquiry_id = itt.inquiry_id
			
			INNER JOIN (
				SELECT
					contact_id,
					contact_name
				FROM
					contact_mst
				WHERE
					delete_flg = :delete_flg
			) AS cm 
		on it.contact_id = cm.contact_id
			
			left outer join (
				select
					inquiry_id,
					contact_id,
					count(*) as cnt
				from
					follow_history_trn
				where
					delete_flg = :delete_flg
				group by
					inquiry_id, contact_id
			) fht 
		on  it.inquiry_id = fht.inquiry_id
		and it.contact_id = fht.contact_id 
			
			left outer join (
				select
					fht.inquiry_id,
					fht.contact_id,
					fht.history_id,
					fht.follow_memo,
					fht.upd_time
				from
					follow_history_trn AS fht inner join 
						(
							select
								inquiry_id,
								contact_id,
								max(history_id) as history_id
							from
								follow_history_trn
							where
								delete_flg = :delete_flg
							group by
								inquiry_id, contact_id
						) max_dt 
					on  fht.inquiry_id = max_dt.inquiry_id
					and fht.contact_id = max_dt.contact_id
					and fht.history_id = max_dt.history_id
			) fol
		on  it.inquiry_id = fol.inquiry_id
		and it.contact_id = fol.contact_id
	{$whereSql}
	order by
		it.upd_time desc
	{$strlimitSql}
SQL;
		$aryResult = $objDatabase->pullDbData($strSql, $aryParameter);
		if($objDatabase->isError($aryResult)){
			return array();
		}
		
		return $aryResult;
	}

	/**
	 * 契約会社リスト生成 
	 * 
	 * @param param
	 * @return return
	 */
	public function getContactList($intContact){

		$objDatabase = clsMillviDatabase::getInstance();

		$objDatabase->setAdminAddress( "nishi@inform-us.com" );

		$strSelectSql =
<<<SQL
		SELECT 
			contact_id,
			contact_name
		FROM
			contact_mst
		WHERE
			delete_flg = :delete_flg
SQL;
		$aryParameters = array(
			":delete_flg"	=>	0
		);
		
		$aryResult = $objDatabase->pullDbData($strSelectSql , $aryParameters);

		if( $objDatabase->isError($aryResult) ){
			return false;
		}

		$strContactSelectBox = "<option value='' " . clsCommonFunction::chkSelectedDate( $intContact, '' ) . " >全て</option>";
		foreach( $aryResult as $key => $val ){
			$strContactSelectBox .= "<option value=" . $val["contact_id"] . " " . clsCommonFunction::chkSelectedDate( $intContact, $val["contact_id"] ) . " >" . $val["contact_name"] . "</option>";
		}
		return $strContactSelectBox;

	}
	
}
?>