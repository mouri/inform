/*
 * エラー表示制御オブジェクト
 *
 * @author kim 2013/1/31
 */
var AlertError = function(strErrorJson, intAlertType){
		
	$(".errorLine").removeClass("errorLine");
	$(".errInfo").remove();
	
	var aryError;
	var objAlertPattern;
	
	try {
		//空文字の場合はエラー無しのJson文字列を格納する
		strErrorJson = strErrorJson || '{"errFlg" : false}';
		aryError = $.parseJSON(strErrorJson);
	} catch( e ) {
		aryError = {"errFlg" : false};
	}

	switch(intAlertType){
		case 1:
			objAlertPattern = PlaneTextAlertPattern;
			break;
			
		case 2:
			objAlertPattern = ModalDialogAlertPattern;
			break;
		
		default:
			objAlertPattern = PlaneTextAlertPattern;
			break;
	}

	this.hasError = function(){
		return  aryError.errFlg;
	};

	this.setAlertPattern = function( _objAlertPattern ){
		objAlertPattern = _objAlertPattern;
	};
	
	this.show = function(){
		objAlertPattern(aryError.errInfo);
	};
};

/**
 * テキストベースエラー表示処理
 * @update 2013/03/11 kanata エラーメッセージ追加要素セレクタ修正
 *
 * @param array aryErrorInfo {errId: "id", errMsg:"エラー理由"}
 */
var PlaneTextAlertPattern = function(aryErrorInfo){
		jQuery.each( aryErrorInfo, function( index, value ){
			// エラーCSSを追加する
			$('#'+value.errId).addClass('errorLine');
			// エラーメッセージを追加する
			if(value.errMsg){
				//$("#"+value.errId).after("<div class='errInfo'>"+value.errMsg+"</div>");
				// 2013-03-11 kanata update
				// エラー出力先IDが「INPUT」の場合
				if( "DIV" == $("#"+value.errId).parent()[0].tagName ){
					$("#"+value.errId).parents("tr").children("td.message").append("<p class='errInfo'>"+value.errMsg+"</p>");
				// エラー出力先IDが「TR」の場合
				} else if( "TBODY"  == $("#"+value.errId).parent()[0].tagName ){
					$("#"+value.errId).children("td.message").append("<p class='errInfo'>"+value.errMsg+"</p>");
				} else {
					$("#"+value.errId).parents("tr").children("td.message").append("<p class='errInfo'>"+value.errMsg+"</p>");
				}
			}
			
		});
};

/**
 * ダイアログベースエラー表示処理
 *
 * @param array aryErrorInfo {errId: "id", errMsg:"エラー理由"}
 */
var ModalDialogAlertPattern = function(aryErrorInfo){

		var strMessage = '';

		jQuery.each( aryErrorInfo, function( index, value ){
			strMessage += value.errMsg + "<br />";
		});

		ModalDialog.error("認証エラー",strMessage);
};