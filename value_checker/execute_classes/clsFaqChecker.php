<?php
	/**
	 * FAQ機能エラーチェッククラス
	 *
	 * @author Nambe
	 * @date   2013/02/25
	 **/
require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR.'/value_checker/clsValueChecker.php');

class clsFaqChecker extends clsValueChecker {
	
	/**
	 * FAQ登録時のエラーチェック
	 **/
	public function chkFaqMasterInput(){

	// 質問内容のチェック
		// 未入力不可
		if( false == $this->isNotEmpty( $_POST["question_text"] ) ){
			$this->setErrorMessage( "question_text", parent::ERROR_NOT_EMPTY );
			$this->setError();
			
		}
	// 回答とフォームの整合性チェック
		// 回答が空でフォームの使用不可
		if( false == $this->isNotEmpty( $_POST["answer_text"]) && 1 == $_POST["form_need_flg"] ){
			$this->setErrorMessage( "form_need_flg", "・回答がないフォームの場合、お問合せフォームは設定できません。" );
			$this->setError();
		}
	}

	/**
	 * ユーザーがFAQのフォームから問い合わせを送信した際にエラーチェックを行う関数
	 **/
	public function chkUserSendQuestion(){
	// 質問内容のチェック
		// 未入力不可
		if( false == $this->isNotEmpty( $_POST["user_question"] ) ){
			$this->setErrorMessage( "user_question", parent::ERROR_NOT_EMPTY );
			$this->setError();
			
		}
	}
}

?>
