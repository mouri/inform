<?php
require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR.'/value_checker/clsValueChecker.php');

class clsLoginChecker extends clsValueChecker {
	
	public function execute(){
	
		//検査対象のnameを配列で登録する。
		$aryCheckNames = array(
			'mail_address',
			'password',
		);
		
		foreach($aryCheckNames as $strName){
			
			$strValue = $this->getValue($strName);
			
			switch($strName){
				case 'mail_address':
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					if(false == $this->isMailAddress($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_MAIL_ADDRESS);
						$this->setError();
					}
					break;
				
				case 'password':
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
					
				default:
					break;		
			}
		}
		
		return !$this->isError();
	}
}

?>
