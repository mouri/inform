<?php
require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR.'/value_checker/clsValueChecker.php');

class clsAdminInformationEntryChecker extends clsValueChecker {
	
	public function execute(){
	
		//検査対象のnameを配列で登録する。
		$aryCheckNames = array(
			'contact_id',
			'information_text'
		);
		
		foreach($aryCheckNames as $strName){
			
			$strValue = $this->getValue($strName);
			
			switch($strName){
				
				//契約会社選択
				case 'contact_id':
					//未入力チェック
					if(false == $this->isSelected($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_CHECKED);
						$this->setError();
					}
					break;
				//お知らせ本文
				case 'information_text':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				default:
					break;
			}
		}
		
		return !$this->isError();
	}
}

?>
