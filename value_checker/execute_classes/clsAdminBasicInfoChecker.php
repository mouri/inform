<?php
require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR.'/value_checker/clsValueChecker.php');

class clsAdminBasicInfoChecker extends clsValueChecker {
	
	public function execute(){
	
		//検査対象のnameを配列で登録する。
		$aryCheckNames = array(
			'contact_name',
			'millvi_id',
			'millvi_user_id',
			'millvi_password',
			'movie_folder',
			//'encode_pc',
			//'encode_smp',
			'start_day',
			'system_dir',
			'account_excel_name',
			'templete',
			'login_tbl_name',
			'mail_error_address',
			'sender_address',
			'sender_name',
			'signature',
			'system_name'	// 2013-10-18 kanata add
		);
		
		foreach($aryCheckNames as $strName){
			
			$strValue = $this->getValue($strName);
			
			switch($strName){
				//会社名
				case 'contact_name':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				//millvi動画フォルダ
				case 'millvi_id':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}else{
						// 数字かチェック
						if(false == $this->isDigits($strValue) ){
							$this->setErrorMessage($strName, parent::ERROR_DIGITS);
							$this->setError();
						}
					}
					break;
				//millvi動画フォルダ
				case 'millvi_user_id':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				//millvi動画フォルダ
				case 'millvi_password':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				//millvi動画フォルダ
				case 'movie_folder':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}else{
						// 数字かチェック
						if(false == $this->isDigits($strValue) ){
							$this->setErrorMessage($strName, parent::ERROR_DIGITS);
							$this->setError();
						}
					}
					break;
				//ＰＣエンコード設定
				case 'encode_pc':
					//未入力チェック
					if(false == $this->isSelected($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_CHECKED);
						$this->setError();
					}
					break;
				//スマホエンコード設定
				case 'encode_smp':
					//未入力チェック
					if(false == $this->isSelected($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_CHECKED);
						$this->setError();
					}
					break;
				//利用開始日
				case 'start_day':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue)){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					//日付チェック
					}else if(false == $this->isDate($strValue)){
						$this->setErrorMessage( $strName, parent::ERROR_DATE );
						$this->setError();
					}/*else{
						//未来日付チェック
						$dtToday = strtotime("now");
						$dtInputday = mktime(null,null,null,(int)substr($strValue,5,2),(int)substr($strValue,8,2),(int)substr($strValue,0,4));
						
						if($this->isNotEmpty($strValue) && $dtToday >= $dtInputday){
							$this->setErrorMessage($strName, date("Y/m/d",$dtToday)."以降の日付を入力してください。");
							$this->setError();
						}
					}*/
					break;
				//システムフォルダ
				case 'system_dir':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				//アカウントExcel雛形ファイル名
				case 'account_excel_name':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}else{
						if(substr($strValue, -5) != ".xlsx" ){
							$this->setErrorMessage($strName, '・拡張子をつけてください。<br />　入力例：[　○○○.xlsx　]');
							$this->setError();
						}
					}
					break;
				//デザインテンプレート
				case 'templete':
					//未入力チェック
					if(false == $this->isSelected($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_CHECKED);
						$this->setError();
					}
					break;
				//使用するログインテーブル
				case 'login_tbl_name':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				//不達となった際のエラー送信先アドレス 20130628 Nishi
				case 'mail_error_address':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}else{
						//アドレスチェック
						if( false == $this->isMailAddress( $strValue ) ){
							$this->setErrorMessage( $strName, parent::ERROR_MAIL_ADDRESS );
							$this->setError();
						}
					}
					break;
				//差出人（メールアドレス）
				case 'sender_address':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					} else if( false == $this->isMailAddress( $strValue ) ){
						$this->setErrorMessage( $strName, parent::ERROR_MAIL_ADDRESS );
						$this->setError();
					}/* else {
						$aryParametrers = array(
													"mail_address" => $strValue,
													"delete_flg"   => '0'
												);
						// メールアドレス重複チェック
						if( true == $this->existsDatabase( "contact_mst", $aryParametrers ) ){
							$this->setErrorMessage( $strName, "・入力されたメールアドレスは既に登録されています。" );
							$this->setError();
						}
					}*/
					break;
					//差出人（名称）
				case 'sender_name':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
					//署名
				case 'signature':
					//未入力チェック
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				
				// 2013-10-18 kanata add システム名称
				case 'system_name':
					if( false == $this->isNotEmpty( $strValue ) ){
						$this->setErrorMessage( $strName, parent::ERROR_NOT_EMPTY );
						$this->setError();
					}
					break;
					
				default:
					break;
			}
		}
		
		return !$this->isError();
	}

	public function adminMenu(){
	
		//検査対象のnameを配列で登録する。
		$aryCheckNames = array(
			'authCheckBox'
		);
		
		foreach($aryCheckNames as $strName){
			
			$strValue = $this->getValue($strName);
			
			switch($strName){
				case 'authCheckBox':
					// 空白チェック
					if( false == $this->isChecked( $this->getValue( 'admin_menu_cd' ) ) ){
						$this->setErrorMessage( $strName, parent::ERROR_CHECKED );
						$this->setError();
					}
					break;
					
				default:
					break;
			}
		}
		
		return !$this->isError();
	}

	public function channel(){
	
		//検査対象のnameを配列で登録する。
		$aryCheckNames = array(
			'menu_category_name'
		);
		
		foreach($aryCheckNames as $strName){
			
			$strValue = $this->getValue($strName);
			
			switch($strName){
				case 'menu_category_name':
					// 空白チェック
					if( false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage('channel_text', '・チャンネルを登録してください');
						$this->setError();
					}
					break;
					
				default:
					break;
			}
		}
		
		return !$this->isError();
	}
}

?>
