<?php
require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR.'/value_checker/clsValueChecker.php');

class clsFollowEntryChecker extends clsValueChecker {
	
	public function execute(){
	
		//検査対象のnameを配列で登録する。
		$aryCheckNames = array(
			'follow_memo'
		);
		
		foreach($aryCheckNames as $strName){
			
			$strValue = $this->getValue($strName);
			
			switch($strName){
				case 'follow_memo':
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				default:
					break;
			}
		}
		
		return !$this->isError();
	}
}

?>
