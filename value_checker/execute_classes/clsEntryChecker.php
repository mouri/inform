<?php
require_once($_SERVER["DOCUMENT_ROOT"].clsDefinition::SYSTEM_DIR.'/value_checker/clsValueChecker.php');

class clsEntryChecker extends clsValueChecker {
	
	public function execute(){
	
		//検査対象のnameを配列で登録する。
		$aryCheckNames = array(
			'contact_id',
			'inquiry_title',
			'inquiry_memo'
		);
		
		foreach($aryCheckNames as $strName){
			
			$strValue = $this->getValue($strName);
			
			switch($strName){
				case 'contact_id':
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				case 'inquiry_title':
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				case 'inquiry_memo':
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				default:
					break;
			}
		}
		
		return !$this->isError();
	}

	public function edit(){
	
		//検査対象のnameを配列で登録する。
		$aryCheckNames = array(
			'inquiry_title',
			'inquiry_memo'
		);
		
		foreach($aryCheckNames as $strName){
			
			$strValue = $this->getValue($strName);
			
			switch($strName){
				case 'inquiry_title':
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				case 'inquiry_memo':
					if(false == $this->isNotEmpty($strValue) ){
						$this->setErrorMessage($strName, parent::ERROR_NOT_EMPTY);
						$this->setError();
					}
					break;
				default:
					break;
			}
		}
		
		return !$this->isError();
	}
}

?>
