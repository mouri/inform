<?php
	//POSTデータからクラスメイト関数名を取得
	$strClassName    = $_POST['className'];
	$strFunctionName = $_POST['functionName'];
	
	//クラス名または関数名がない場合は処理終了
	if( empty($strClassName) || empty( $strFunctionName ) ){
		var_dump($_POST);
		exit();
	}
	
	$strValueCheckerPath = $_SERVER["DOCUMENT_ROOT"]."/millvi/value_checker/execute_classes/";
	
	//指定されたクラス名を読み込む
	require_once( $strValueCheckerPath . $strClassName . '.php' );
	
	//POSTデータの検査対象項目とname属性値の配列を取得
	$aryTemp =  $_POST['checkInputs'];
	//チェックボックスのデータを配列に変換する
	$aryCheckInputs = formatArray($aryTemp);
	
	//レスポンスとなる配列の初期化
	$aryResponse = array();
	
	//全入力項目の検査結果の初期化
	$blnResult = true;
	//エラーメッセージの初期化
	$aryErrorMessages = array();
	
	//指定され+ラス名のオブジェクトを生成する。
	$objValueChecker = new $strClassName($aryCheckInputs);
	//指定された関数名で値の検査を実行する
	$blnResult = $objValueChecker->$strFunctionName();
	//検査結果がfalseの場合
	if( false == $blnResult ){
		//エラーメッセージの取得
		$aryErrorMessages = $objValueChecker->getErrorMessage();
	}
		
	
	//レスポンスの配列に検査結果と鰓メッセージを登録
	$aryResponse['result'] = $blnResult;
	$aryResponse['errorMessages'] = $aryErrorMessages;
	
	//レスポンスの配列をJSON文字列にエンコード
	$strJson = json_encode($aryResponse);
	
	//JSON文字列を出力
	echo $strJson;
	
	
	/**
	 * チェックボックスを配列に変換する関数
	 * 
	 * @param array  $aryTemp        : 変換対象の配列
	 * @return array $aryCheckInputs : 変換後の配列
	 * @author 2012/04/27 Kim
	 **/
	function formatArray($aryTemp){
		$aryCheckInputs = array();
		
		$intLength = count($aryTemp);
		for($i = 0; $i < $intLength; $i++ ){
			$strName = $aryTemp[$i]["name"];
			
			//名前の末尾が[]なら
			if( preg_match( '/\[\]$/', $strName ) ){
				//名前の末尾の[]を取り除く
				$strName = preg_replace('/\[\]$/', '', $strName );
				//連想配列の該当する名前に、値を格納する配列が未生成の場合。
				if( false == isset( $aryCheckInputs[$strName ] ) ){
					//新しく値の配列を生成し、格納する
					$aryCheckInputs[$strName ] = array();
				}
				//値の配列
				$aryCheckInputs[$strName][] = $aryTemp[$i]["value"];
			} else {
				$aryCheckInputs[$strName] = $aryTemp[$i]["value"];
			}
		}
		
		return $aryCheckInputs;
	
	}
?>